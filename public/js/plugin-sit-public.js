


jQuery(document).ready(function () {
  (function() {
    var showChar = 400;
    var ellipsestext = "...";

    jQuery(".truncate").each(function() {
      var content = jQuery(this).html();
      if (content.length > showChar) {
        var c = content.substr(0, showChar);
        var h = content;
        var html =
            '<div class="truncate-text truncated-text" style="display:block">' +
            c +
            '<span class="moreellipses">' +
            ellipsestext +
            '&nbsp;&nbsp;<a href="#truncate" class="moreless more">+</a></span></div><div class="truncate-text full-text" style="display:none">' +
            h +
            '<a href="#truncate" class="moreless less">-</a></div>';

        jQuery(this).html(html);
      }
    });

    jQuery(".moreless").click(function() {
      var thisEl = jQuery(this);
      /*      var cT = thisEl.closest(".truncate-text");
            jQuery(".truncate-text").hide(500);
            var tX = ".truncate-text";
      */
      if (thisEl.hasClass("less")) {
        jQuery(".truncate-text.full-text").hide(200);
        jQuery(".truncate-text.truncated-text").show(200);
        // cT.prev(tX).toggle();
        // cT.slideToggle();
      } else {
        jQuery(".truncate-text.full-text").show(200);
        jQuery(".truncate-text.truncated-text").hide(200);
        /*
        cT.toggle();
        cT.next(tX).fadeToggle();
         */
      }
      return true;
    });

    // gestion multimoteurs

    // gestion multimoteurs : montrer la section idoine si filtre de switch actif au chargement

    // Gestion du changement de switch
    jQuery('.switchMultiMoteur').change(function (e) {
      var targetSection = jQuery(this).find(":selected").attr('data-target-section');
      var moteurContainer = jQuery(this).closest('.bridge-moteur-sit');
      var moteurId = moteurContainer.attr('data-bridge-moteur-id');
      // Ne fonctionne que quand le moteur n'est pas coupé en 2
      // moteurContainer.find('.bridge-moteur-section').each(function (index) {
      jQuery('.bridge-moteur-sit').find('.bridge-moteur-section').each(function (index) {
        // * Pour afficher les sections par défaut
        if (index > 0 && targetSection != '*') {
          jQuery(this).hide();
        } else {
          if (index > 0 && targetSection === '*') {
            jQuery(this).show();
          }
        }
      });
      // Ne fonctionne que quand le moteur n'est pas coupé en 2
      // moteurContainer.find('.moteur-section-' + targetSection).show();
      jQuery('.bridge-moteur-sit').find('.moteur-section-' + targetSection).show();
    });

    // Au chargement de la page, si un filtre est actif, on montre la section idoine
    jQuery('.switchMultiMoteur').each(function (idx) {
      var selectedSwitch = jQuery(this).val();
      if(selectedSwitch !== '') {
        var targetSection = jQuery(this).find(":selected").attr('data-target-section');
        var moteurContainer = jQuery(this).closest('.bridge-moteur-sit');
        var moteurId = moteurContainer.attr('data-bridge-moteur-id');
        // Ne fonctionne que quand le moteur n'est pas coupé en 2
        // moteurContainer.find('.bridge-moteur-section').each(function (index) {
        jQuery('.bridge-moteur-sit').find('.bridge-moteur-section').each(function (index) {
          // * Pour afficher les sections par défaut
          if (index > 0 && targetSection != '*') {
            jQuery(this).hide();
          } else {
            if (index > 0 && targetSection === '*') {
              jQuery(this).show();
            }
          }
        });
        // Ne fonctionne que quand le moteur n'est pas coupé en 2
        // moteurContainer.find('.moteur-section-' + targetSection).show();
        jQuery('.bridge-moteur-sit').find('.moteur-section-' + targetSection).show();
      }
    });

    /* end iffe */
  })();

  // ******** Carnet de voyage ********
  // bouton d'ajout
  const add_buttons = document.getElementsByClassName("add-to-cv-button");

  for(let i = 0; i < add_buttons.length; i++) {

    const button = add_buttons.item(i);

    button.onclick = addToSessionButtonClickFunction(button);
  }

  const observer = new MutationObserver( mutations => {
    mutations.forEach( mutation => {
      mutation.addedNodes.forEach( addedNode => {
        addedNode.childNodes.forEach( entry => {
          if(!entry.className || typeof entry.className !== "string" || !entry.className.includes("ficheproduit")) return;

          const allElements = document.getElementsByClassName("add-to-cv-button");

          for (let i = 0; i < allElements.length; i++) {
            const currentElement = allElements[i];
            if (typeof currentElement["onclick"] !== 'function') {
              currentElement.onclick = addToSessionButtonClickFunction(currentElement);
            }
          }
        });
      });
    });
  });

  observer.observe(document.body, { childList: true, subtree: true });

  // bouton de suppression
  const remove_buttons = document.getElementsByClassName("cv-delete-button");

  for(let i = 0; i < remove_buttons.length; i++) {

    const button = remove_buttons.item(i);

    button.addEventListener("click", () => {

      jQuery.ajax({
        url: ajaxurl,
        type: "POST",
        dataType: "json",
        data: {
          action: "bridge_remove_fiche_from_session_cv",
          id: button.id
        },
        success: () => {
          // remove line
          const row = document.getElementById(`row-${button.id}`);
          row.remove();
        },
        error: (err) => {
          console.error(err);
        }
      });
    });
  }


  /* end ready */
});

if (window.bridgeDocumentReady === undefined || typeof window.bridgeDocumentReady !== "function") {
  window.bridgeDocumentReady = function (fn) {
    // see if DOM is already available
    if (document.readyState === "complete" || document.readyState === "interactive") {
      // call on next available tick
      setTimeout(fn, 10);
    } else {
      document.addEventListener("DOMContentLoaded", fn);
    }
  };
}


/**
 *
 * @param {HTMLElement} button
 * @returns {Function}
 */
function addToSessionButtonClickFunction(button) {
  return () => {
    let city = button.getAttribute('data-sit-city');
    let id = button.getAttribute('data-sit-id');
    let name = button.getAttribute('data-sit-name');

    jQuery.ajax({
      url: ajaxurl,
      type: "POST",
      dataType: "json",
      data: {
        action: "bridge_add_fiche_to_session_cv",
        id,
        name,
        city
      },
      success: () => {

        const message = '<i class="far fa-check-circle"></i> ' + bridgeMsgAddedtoCv + '<br><a href="' + cvPreviewURL + '">Voir le carnet de voyage</a>' ;

        UIkit.notification(message, {
          pos: 'bottom-left',
          status:'success',
          timeout: 5000
        })

        // en mode texte, on remplace le texte "ajouter" par "ajouté"
        const buutonDisplayMode = jQuery(button).attr('data-display-mode');

        if(buutonDisplayMode === 'text') {
          button.innerText = bridgeMsgAddedtoCv;
        }

        button.disabled = true;
      },
      error: (err) => {
        if(err.responseJSON.data === "Cette fiche est déjà dans le CV") {
          button.disabled = true;
        }
        else {
          console.error(err);

          if(err.responseJSON)
            console.error(err.responseJSON)
        }
      }
    });
  }
}