/**
 *
 *          Librairie de gestion des moteurs bridge
 *
 *
 */

window.bridgeMoteur = function(moteurId) {
    this.eventsHandlers = {
        beforeItemChange: [],
        afterItemChange: [],
        beforeCalcMoteur: [],
        afterCalcMoteur: [],
        beforeSubmit: [],
        afterSubmit: []
    };

    this.options = {
        submitOnChange: true
    };

    this.onSubmitFunction = null;
    this.id = moteurId;
    this.currentURL = '';
    this.activeFilters = [];
    this.activeFiltersLabels = [];
    this.activeItems = [];
    this.urlParams = [];
    this.filters = [] ;

    // élément HTML <datalist> qui contiendra les options du filtre citycombodyn
    this.datalist = null;
    // liste des options du filtre
    this.cityComboDynList = null;

    // paramètres des tris
    this.activeSorts = [];
    this.sortDirections = [];

    // pour différencier une modification des filtres et un simple tri
    this.filterHasChanged = false;


    this.addEventHandler = function(handlerName, func)
    {
        if (Object.prototype.hasOwnProperty.call(this.eventsHandlers, handlerName) ) {
            this.eventsHandlers[handlerName].push(func);
        }
    };


    this.addFilter = function(obj) {
        this.filters.push(obj);
    };


    this.findFilterProp = function(key, prop) {
        var tabIds = key.split('-');

        if (tabIds.length < 2) {
            return '';
        }

        var temp = this.getFilter(tabIds[1]);

        if (temp != false && temp[prop] != undefined) {
            return temp[prop];
        } else {
            return '';
        }
    };


    this.getFilter = function(filterId) {
        var trouve = false;

        for (var i = 0 ; i < this.filters.length && !trouve ; i++) {
            if (this.filters[i].id == filterId) {
                return this.filters[i] ;
                trouve = true;
            }
        }
        return trouve;
    }


    this.deactivateWholeFilter = function(filterId) {
        var trouve = false;

        this.filterHasChanged = true;

        for (var i = 0 ; i < this.filters.length && !trouve ; i++) {
            if (this.filters[i].id == filterId) {
                this.filters[i].active = false;
                this.filters[i].value = '';
                this.filters[i].dateFrom = '';
                this.filters[i].dateTo = '';
                this.filters[i].hourFrom = '';
                this.filters[i].hourTo = '';
                this.filters[i].lat = '';
                this.filters[i].lon = '';
                this.filters[i].dist = '';
                if(this.filters[i].city) {
                    this.filters[i].city = '';
                }

                for (var j = 0 ; j < this.filters[i].items.length && !trouve ; j++) {
                    this.filters[i].items[j].active = false;
                    this.filters[i].items[j].min = this.filters[i].items[j].baseMin;
                    this.filters[i].items[j].max = this.filters[i].items[j].baseMax;
                }
                // ND 28/06/21 : effacer également les inputs text visibles
                jQuery("input[data-bridge-filter-id='" + this.filters[i].id + "'][data-bridge-filter-field='" + this.filters[i].field + "']").val('');
                trouve = true;
            }
        }
        return trouve;
    }


    this.setFilterProp = function(filterId, prop, value, triggerChange) {
        var trouve = false;

        for (var i = 0 ; i < this.filters.length && !trouve ; i++) {
            if (this.filters[i].id == filterId) {
                this.filters[i][prop] = value;
                for (var j = 0 ; j < this.filters[i].items.length && !trouve ; j++) {
                    this.filters[i].items[j].active = false;
                }
                trouve = true;
            }
        }

        if (trouve) {
            this.calcMoteur(triggerChange);
        }
        return trouve;
    }


    this.addItem = function(filterId, obj) {
        var trouve = false;
        for (var i = 0 ; i < this.filters.length && !trouve ; i++) {
            if (this.filters[i].id == filterId) {
                this.filters[i].items.push(obj);
                trouve = true;
            }
        }
    };


    this.setItem = function(filterId, itemId, obj) {
        var trouve = false;
        var x = 0;
        var self = this;

        for (var i = 0 ; i < this.filters.length && !trouve ; i++) {
            if (this.filters[i].id == filterId) {
                for (var j = 0 ; j < this.filters[i].items.length && !trouve ; j++) {
                    if (this.filters[i].items[j].id == itemId) {
                        for (x=0; x<self.eventsHandlers.beforeFilterChange.length; x++)
                            self.eventsHandlers.beforeFilterChange[x](this.filters[i].id, this.filters[i]);
                        for (x=0; x<self.eventsHandlers.beforeItemChange.length; x++)
                            self.eventsHandlers.beforeItemChange[x](this.filters[i].id, this.filters[i].items[j].id, obj, this.filters[i].items[j]);

                        this.filters[i].items[j] = obj;
                        for (x=0; x<self.eventsHandlers.afterItemChange.length; x++)
                            self.eventsHandlers.afterItemChange[x](this.filters[i].id, this.filters[i].items[j].id, obj);

                        trouve = true;
                    }
                }
            }
        }

        if (trouve)
            this.calcMoteur(true);
        return trouve;
    }


    this.findItemProp = function(key, prop) {
        var tabIds = key.split('-');
        if (tabIds.length < 3)
            return '';
        var temp = this.getItem(tabIds[1], tabIds[2]);
        return temp[prop];
    };


    this.getItem = function(filterId, itemId) {
        var trouve = false;
        for (var i = 0 ; i < this.filters.length && !trouve ; i++) {
            if (this.filters[i].id == filterId) {
                for (var j = 0 ; j < this.filters[i].items.length && !trouve ; j++) {
                    if (this.filters[i].items[j].id == itemId) {
                        return this.filters[i].items[j] ;
                        trouve = true;
                    }
                }
            }
        }
        return trouve;
    }


    this.setItemProp = function(filterId, itemId, prop, value) {
        var trouve = false;
        var x = 0;
        var self = this;

        for (var i = 0 ; i < this.filters.length && !trouve ; i++) {
            if (this.filters[i].id == filterId) {
                for (var j = 0 ; j < this.filters[i].items.length && !trouve ; j++) {
                    if (this.filters[i].items[j].id == itemId) {
                        for (x=0; x<self.eventsHandlers.beforeItemChange.length; x++)
                            self.eventsHandlers.beforeItemChange[x](this.filters[i].id, this.filters[i].items[j].id, obj, this.filters[i].items[j]);

                        this.filters[i].items[j][prop] = value;
                        trouve = true;

                        for (x=0; x<self.eventsHandlers.afterItemChange.length; x++)
                            self.eventsHandlers.afterItemChange[x](this.filters[i].id, this.filters[i].items[j].id, obj);
                    }
                }
            }
        }
        // Patch 20/11/2019 : on ne fait pas de calc pour éviter les appels en cascade, c'est aux inputs de lancer leur calc
        /*
        if(trouve) {
            this.calcMoteur(true);
        }
        */

        return trouve;
    }

    /**
     * Appelée par js-tris.php à l'activation, la modification ou la désactivation d'un tri dans l'interface
     * Transmet les informations sur les tris pour l'appel AJAX (this.searchAjax) de mise à jour de la liste
     * @param {Array} activeSorts : tris activés
     * @param {Array} sortDirections : directions choisies pour les tris activés
     */
    this.setSortsAndSortDirections = function(activeSorts, sortDirections) {
        this.activeSorts = activeSorts;
        this.sortDirections = sortDirections;
    }

    /**
     * Appelée par js-select-dropdown.php au chargement ou au rechargement d'une webliste contenant un filtre de type cityComboDyn
     * (chargement initial de la page ou actualisation après application d'un filtre)
     * Affiche la liste des communes des fiches de la webliste dans un combo box
     * @param {String} cityComboDynList : liste des communes à afficher comme options pour le filtre
     * @param {Object} datalist : élément HTML <datalist> qui contiendra les options à afficher
     */
    this.displayCityComboDynList = function(cityComboDynList, datalist) {

        if (cityComboDynList == '') {
            cityComboDynList = 'Aucune ville à afficher';
        }

        this.datalist = datalist;

        // on efface la liste qui existait déjà
        while (datalist.children.length > 0) {
            datalist.removeChild(datalist.lastChild);
        }

        let cityList = [];

        this.cityComboDynList = cityComboDynList;

        cityList = cityComboDynList.split(',');

        for (let city of cityList) {
            let option = document.createElement("option");
            option.text = city;
            option.value = city;
            option.id = "opt_<?= $moteurEnDouble ?><?= $moteur->id ?>-<?= $filter->id ?>-" + city;

            datalist.appendChild(option);
        }
    }

    /**
     * Remplace certains caractères spéciaux par les entités HTML correspondantes pour éviter les problèmes avec les saisies utilisateur
     * @param {String} text : string à modifier
     * @returns String : string modifiée
     */
    this.escapeHtml = function(text) {
        var map = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#039;',
            "`": '&grave;',
            "+": '&plus;'
        };

        return text.replace(/[&<>"'`+]/g, function(m) {return map[m];});
    }

    this.calcMoteur = function (isChanged) {

        var self = this;
        var tmpActive;
        var activeFilters = [];
        var activeFiltersLabels = [];
        var activeItems = [];
        var urlParams = {};
        var tmpFilter;
        var i;
        var j;
        var x;
        var cmpt;
        var label;
        var tmpObj;
        var tmpItems = [];
        var tempStr = '';
        var section = '';

        var tmpActiveFilters = this.activeFilters;
        var tmpActiveItems = this.activeItems;

        for (x=0; x<self.eventsHandlers.beforeCalcMoteur.length; x++) {
            self.eventsHandlers.beforeCalcMoteur[x](this);
        }

        for (i = 0 ; i < this.filters.length ; i++) {
            tmpActive = false;

            switch (this.filters[i].displayType) {
                case 'slidedoubleminmax':
                    if(this.filters[i].items.length == 0) {
                        console.log('ERREUR DE PARAMETRAGE BRIDGE : un element slide double min/max n\'a pas d\'item renseigné : filtre n° ' + this.filters[i].id);
                    } else {
                        if (this.filters[i].items[0].min != this.filters[i].items[0].baseMin || this.filters[i].items[0].max != this.filters[i].items[0].baseMax) {
                            this.filters[i].active = true;
                            activeFilters.push(this.id + '-' + this.filters[i].id );
                            activeItems.push(this.id + '-' + this.filters[i].id + '-' + this.filters[i].items[0].id );
                            urlParams['brflt_' + this.id + '-' + this.filters[i].id + '_min'] = this.filters[i].items[0].min;
                            urlParams['brflt_' + this.id + '-' + this.filters[i].id + '_max'] = this.filters[i].items[0].max;
                        } else {
                            // Important : effacer le statut actif
                            this.filters[i].active = false;
                        }
                    }
                    break;
                case 'checkbox':
                case 'boutonbox':
                case 'boutonboxdyn':
                case 'checkboxdyn':
                case 'citycombo':
                case 'combo':
                case 'combodyn':
                case 'listemulti':
                case 'onoff':
                case 'option':
                case 'optiondyb':
                case 'slidedouble':
                case 'slidesimple':
                case 'slidesimpledyn':
                    isActive = false;
                    for (j = 0; j < this.filters[i].items.length; j++) {
                        if (this.filters[i].items[j].active) {
                            isActive = true;
                            activeItems.push(this.id + '-' + this.filters[i].id + '-' + this.filters[i].items[j].id);
                        }
                    }
                    this.filters[i].active = isActive
                    if (isActive)
                        activeFilters.push(this.id + '-' + this.filters[i].id );
                    break;
                case 'calendar':
                case 'calendarhours':
                case 'prochainesperiodes':
                case 'disponibilites':
                    isActive = false;
                    if ((this.filters[i].dateFrom && this.filters[i].dateFrom !='') || (this.filters[i].dateTo && this.filters[i].dateTo !='')) {
                        isActive = true;
                        if (this.filters[i].dateFrom)
                            urlParams['brflt_' + this.id + '-' + this.filters[i].id + '_dateFrom'] = this.filters[i].dateFrom;
                        if (this.filters[i].dateTo)
                            urlParams['brflt_' + this.id + '-' + this.filters[i].id + '_dateTo'] = this.filters[i].dateTo;
                        if (this.filters[i].hourFrom)
                            urlParams['brflt_' + this.id + '-' + this.filters[i].id + '_hourFrom'] = this.filters[i].hourFrom;
                        if (this.filters[i].hourTo)
                            urlParams['brflt_' + this.id + '-' + this.filters[i].id + '_hourTo'] = this.filters[i].hourTo;

                    }
                    
                    this.filters[i].active = isActive
                    if (isActive)
                        activeFilters.push(this.id + '-' + this.filters[i].id);

                    break;
                case 'cityprox':
                case 'cityproxdyn':
                case 'gps':
                    isActive = false;
                    if ((this.filters[i].lat && this.filters[i].lat !='') && (this.filters[i].lon && this.filters[i].lon !='') && (this.filters[i].dist && this.filters[i].dist !='')) {
                        isActive = true;
                        if(this.filters[i].lat)
                            urlParams['brflt_' + this.id + '-' + this.filters[i].id + '_lat'] = this.filters[i].lat;
                        if(this.filters[i].lon)
                            urlParams['brflt_' + this.id + '-' + this.filters[i].id + '_lon'] = this.filters[i].lon;
                        if(this.filters[i].dist)
                            urlParams['brflt_' + this.id + '-' + this.filters[i].id + '_dist'] = this.filters[i].dist;
                        if(this.filters[i].city)
                            urlParams['brflt_' + this.id + '-' + this.filters[i].id + '_city'] = encodeURI(this.filters[i].city);
                    }

                    this.filters[i].active = isActive
                    if (isActive)
                        activeFilters.push(this.id + '-' + this.filters[i].id);

                    break;
                case 'citycombodyn':
                case 'cityinput':
                case 'input':
                case 'hidden':
                    isActive = false;

                    if ((this.filters[i].value && this.filters[i].value !== '')) {
                        isActive = true;
                        if (this.filters[i].value) {
                            // Patch ND 13/01/22 : les accents et espaces ne sont pas encodés
                            urlParams['brflt_' + this.id + '-' + this.filters[i].id + '_value'] = encodeURI(this.filters[i].value);
                        }
                    }

                    this.filters[i].active = isActive
                    if (isActive)
                        activeFilters.push(this.id + '-' + this.filters[i].id);
                    break;
            }
        }

        this.activeFilters = activeFilters;
        this.activeItems = activeItems;

        var activeFiltersComp = this.activeFilters;
        var activeItemsComp = this.activeItems;

        if (JSON.stringify(tmpActiveFilters) !== JSON.stringify(activeFiltersComp) || JSON.stringify(tmpActiveItems) !== JSON.stringify(activeItemsComp)) {
            this.filterHasChanged = true;
        }

        this.urlParams = urlParams;

        for (i = 0 ; i < activeFilters.length ; i++) {
            tmpFilter = activeFilters[i];
            label = this.findFilterProp(activeFilters[i], 'label');
            section = this.findFilterProp(activeFilters[i], 'sectionId');
            tmpObj = {
                sectionId: section,
                key: tmpFilter,
                label: label,
                items:[]
            };

            // Pour les items prédéfinis, on va chercher leur label
            for (cmpt = 0; cmpt < activeItems.length; cmpt++) {
                if (activeItems[cmpt].indexOf(tmpFilter) == 0) {
                    label = this.findItemProp(activeItems[cmpt], 'label');
                    tmpObj.items.push(label);

                    // Cas particulier du slidedoubleminmax qui a des items mais sans valeurs prédéfinies : on veut afficher les valeurs et le label de l'item et non ceux du filtre lui-même
                    let slideDoubleMin = this.findItemProp(activeItems[cmpt], 'min');
                    let slideDoubleMax = this.findItemProp(activeItems[cmpt], 'max');

                    if (slideDoubleMin != null && slideDoubleMax != null) {
                        tmpObj.label = tmpObj.items[0];
                        let slideDoubleMinMax = slideDoubleMin + '-' + slideDoubleMax;
                        tmpObj.items.splice(0, 1, slideDoubleMinMax);
                    }
                }
            }

            // Pour les valeurs, on les met dans un item
            var tmpValue = this.findFilterProp(activeFilters[i], 'value');
            if (tmpValue != '') {
                tmpObj.items.push(tmpValue);
            }

            var tmpDateFrom = this.findFilterProp(activeFilters[i], 'dateFrom');
            var tmpDateTo = this.findFilterProp(activeFilters[i], 'dateTo');
            if (tmpDateFrom != '' && tmpDateTo != '') {
                tmpObj.items.push(tmpDateFrom + '-' + tmpDateTo);
            } else {
                if (tmpDateFrom != '') {
                    tmpObj.items.push(tmpDateFrom);
                }
                if (tmpDateTo != '') {
                    tmpObj.items.push(tmpDateTo);
                }
            }

            // Pour la distance, on la ville et la distance en km
            var tmpCity = this.findFilterProp(activeFilters[i], 'city');
            var tmpDist = this.findFilterProp(activeFilters[i], 'dist');
            if (tmpCity != null && tmpCity != '' && tmpDist != null && tmpDist != '') {
                tmpDist = (parseInt(tmpDist) / 1000) + ' km';
                tmpObj.items.push(tmpCity + ' : ' + tmpDist);
            }

            activeFiltersLabels.push(tmpObj);
        }

        this.activeFiltersLabels = activeFiltersLabels;

        var tabAllActiveFiltersString = [];
        var tabSectionsActiveFiltersString = {};

        for (i=0 ; i < activeFiltersLabels.length ; i++) {
            tempStr = '';
            tempStr += '<div class="bridgeActiveFilter uk-label">';
            tempStr += '<span class="bridgeActiveFilterLabel">'

            if (activeFiltersLabels[i].label != undefined && activeFiltersLabels[i].label != '') {
                tempStr += activeFiltersLabels[i].label  + ': ';
            }

            tempStr += activeFiltersLabels[i].items.join(', ');
            tempStr += '</span>';
            tempStr += '<span class="bridgeActiveFilterRemove far fa fa-times" data-filter-key="' + activeFiltersLabels[i].key + '" data-moteur-id="' + this.id + '">';
            tempStr += '</span>';
            tempStr += '</div>';

            if (i + 1 === activeFiltersLabels.length) {
               //  tempStr += '<a class="bridgeActiveFilterRemoveAll">Tout effacer</a>';
            }

            tabAllActiveFiltersString.push(tempStr);

            if(tabSectionsActiveFiltersString[activeFiltersLabels[i].sectionId] == undefined) {
                tabSectionsActiveFiltersString[activeFiltersLabels[i].sectionId] = []
            }

            tabSectionsActiveFiltersString[activeFiltersLabels[i].sectionId].push(tempStr)
        }

        if(activeFiltersLabels.length > 0) {
            jQuery('.bridgeActiveFilterRemoveAll').css('display', 'inline');
            jQuery('.bridgeActiveFilterRemoveAll').show();
        } else {
            jQuery('.bridgeActiveFilterRemoveAll').css('display', 'none');
            jQuery('.bridgeActiveFilterRemoveAll').hide();
        }
        jQuery(".bridgeActiveFiltersDiv").html(tabAllActiveFiltersString.join(' '));


        for (var prop in tabSectionsActiveFiltersString) {
            if (Object.prototype.hasOwnProperty.call(tabSectionsActiveFiltersString, prop) && tabSectionsActiveFiltersString[prop].length > 0) {
                jQuery(".bridgeActiveFiltersDivSection" + prop).html(tabSectionsActiveFiltersString[prop].join(' '));
            }
        }
        jQuery("*[class^='bridgeActiveFiltersDivSection']").each(function(e) {

           var sectionId = jQuery(this).attr('data-section-id');
            if (Object.prototype.hasOwnProperty.call(tabSectionsActiveFiltersString, sectionId) && tabSectionsActiveFiltersString[sectionId].length > 0) {
                jQuery(this).show(500);
            } else {
                jQuery(this).hide(500);
            }
        });

        jQuery(".bridgeActiveFilterRemove").click(function (e) {
            var key = jQuery(this).attr('data-filter-key');
            var tabTemp = key.split('-');

            if (tabTemp.length == 2 && window['bridgeMoteur' + tabTemp[0]] != undefined) {
                window['bridgeMoteur' + tabTemp[0]].deactivateWholeFilter(tabTemp[1]);
                window['bridgeMoteur' + tabTemp[0]].calcMoteur(true);
            }
        });


        // lien qui supprime tout les filtres
        jQuery(".bridgeActiveFilterRemoveAll").click(function (e) {
            var nummoteur = '';
            jQuery(".bridgeActiveFilterRemove").each(function() {
                // Trop minimaliste
                // jQuery(this).click();
                var key = jQuery(this).attr('data-filter-key');
                var tabTemp = key.split('-');
                nummoteur = tabTemp[0];

                if (tabTemp.length == 2 && window['bridgeMoteur' + tabTemp[0]] != undefined) {
                    window['bridgeMoteur' + tabTemp[0]].deactivateWholeFilter(tabTemp[1]);
                }
            });
            window['bridgeMoteur' + nummoteur].calcMoteur(true);
        }) ;
          /*  WTF
            e => {
            for (const key of Object.keys(window)) {
                if (!key.startsWith("bridgeMoteur")) {
                    continue;
                }

                if (typeof window[key] === "function") {
                    continue;
                }

                const filterIds = window[key].activeFilters.map(id => id.replace("6-", ""));
                
                filterIds.forEach(id => window[key].deactivateWholeFilter(id));

                window[key].calcMoteur(true);
            }
        });
        */
        for (x=0; x<self.eventsHandlers.afterCalcMoteur.length; x++)
            self.eventsHandlers.afterCalcMoteur[x](this);


        if (isChanged && this.options.submitOnChange) {
            var stateObj = JSON.parse(JSON.stringify(this.getUrlState()));
            var url = "?" + this.getUrlParams();
            history.pushState(stateObj, jQuery(document).find("title").text(), url);

            this.submit();
        }
        else if (isChanged) {

        }
    }

    this.getUrlParams = function() {
        var self = this ;
        var urlParams = '';
        if(this.activeFilters !== null) {
            urlParams += 'braf=' + this.activeFilters.join(',');
        }
        if(this.activeItems !== null) {
            urlParams += '&brai=' + this.activeItems.join(',');
        }

        if (this.activeSorts.length > 0) {
            urlParams += '&bras=' + this.activeSorts.join(',');
            urlParams += '&brsd=' + this.sortDirections.join(',');
        }

        Object.keys(this.urlParams).forEach(function(key,index) {
            urlParams += '&' + key + '=' + self.urlParams[key] ;
        });

        return urlParams;
        // location.hash = "parameter1=987&parameter2=zyx";
    }

    this.getUrlState = function() {

        if(this.activeFilters === null) {
            this.activeFilters = [];
        }
        var urlParams = { braf: this.activeFilters.join(','), brai: this.activeItems, bras: this.activeSorts, brsd: this.sortDirections };
        var self = this ;
        var toto;

        Object.keys(this.urlParams).forEach(function(key,index) {
            urlParams[key] = self.urlParams[key];
            // toto = self.urlParams[key] ;
        });

        return urlParams;
        // location.hash = "parameter1=987&parameter2=zyx";
    }

    this.submit = function() {

        var self = this;
        for (x=0; x<self.eventsHandlers.beforeSubmit.length; x++)
            self.eventsHandlers.beforeSubmit[x](this);

        var url = this.currentURL ; // + '?braf=' + this.activeFilters.join(',');
        var urlParams = this.getUrlParams();

        if (this.onSubmitFunction != null) {
            this.onSubmitFunction(url, urlParams);
        } else {
            this.searchAjax();
        }
            //window.location = url + '?' + urlParams;
            //this.searchAjax();
        // window.location = url + '?' + urlParams;
    }

    this.searchAjax = function() {
        var context = this;
        var urlParams = this.getUrlParams();
        if (urlParams != '')
            urlParams = '&' + urlParams;
        var self = this;
        var change = 1;
        // si on change un filtre plutôt qu'un tri, on a besoin de recharger le filtre cityComboDyn
        if (this.filterHasChanged) {
            change = 1;
        }
        jQuery('.bridge-loader').fadeIn(100);
        jQuery.ajax ({
            type: "GET",
            dataType: "json",
            url: this.options.bridgeAjaxUrl,
            // data: 'action=brListe&id=' + this.options.webListId + '&product_codes=' + this.options.productCodes + '&first=1&max=' + this.options.limitPerPage + urlParams + '&change=' + change,
            data: 'action=brListe&id=' + this.options.webListId + '&product_codes=' + this.options.productCodes + '&first=1' + urlParams + '&change=' + change,
            success: function (response) {
                jQuery('.bridge-loader').fadeOut({ duration: 500 });
                if (true || response.html) { // response.total &&  response.total !== 0

                    // On désactive l'infinite scroll car on le remet de toute manière plus loin
                    if(window.bridgeInfiniteScroll !== undefined) {
                        window.bridgeInfiniteScroll.unbind();
                    }


                    jQuery(".listeSITCount").html(response.total);
                    if(response.total !== 0) {
                        var $items = jQuery(response.html);
                        jQuery("#listeSIT").html($items);


                        $items.find('.zoom-on-hover').hover( function() {
                            var idfiche = jQuery(this).attr('data-dnc-produit');
                            if(idfiche !== '') {
                                window.mapService.activatemarker(idfiche, true, true); // window.mapService.defaultIconOptions
                                window.mapService.centeronmarker(idfiche, "15");
                                // tricherie obligatoire pour forcer le point à s'afficher la premiere fois
                                setTimeout(function() {
                                    window.mapService.setZoom(15);
                                },500);
                                window.mapService.reDraw();

                            }
                        });

                        // setTimeout(function () {
                        $items.find('.zoom-on-click').click(function() {
                            var idfiche = jQuery(this).attr('data-dnc-produit');
                            if(idfiche !== '') {
                                window.mapService.activatemarker(idfiche, true, true); // window.mapService.defaultIconOptions
                                window.mapService.centeronmarker(idfiche, "15");
                                // tricherie obligatoire pour forcer le point à s'afficher la premiere fois
                                setTimeout(function() {
                                    window.mapService.setZoom(15);
                                },500);
                                window.mapService.reDraw();
                            }
                        });



                    } else {
                        // aucun résultat
                        jQuery("#listeSIT").html('<div>' + response.message + '</div>');
                    }
                    window.bridgeTotalResults = response.total;

                    for (x=0; x<self.eventsHandlers.afterSubmit.length; x++)
                        self.eventsHandlers.afterSubmit[x](this);

                    var currentURL = window.location.href.split('?')[0] ;
                    var nbLinks = 1;
                    var start      =  1;
                    var end        =  parseInt(response.lastPage);

                    if ( ( 1 + nbLinks ) < parseInt(response.lastPage) ) {
                        end = 1 + nbLinks
                    }

                    // Ici on met à jour la pagination en repartant la page 1
                    var pagination = '<ul class="bridge-pagination uk-pagination uk-flex-center" uk-margin>';
                    pagination +='    <li class="uk-disabled""><a class="bridge-pagination-link" data-page="1" href="#nogo">&nbsp;<span uk-pagination-previous></span></a></li>' ;
                    var tmpdisable = '';
                    for (var i = start ; i <= end ; i++) {
                        if(i === 1) {
                            pagination += '    <li class="uk-active"><a class="bridge-pagination-link" data-page="' + i + '" href="#nogo">' + i + '</a></li>';
                        } else {
                            pagination += '    <li class=""><a class="bridge-pagination-link" data-page="' + i + '" href="' + currentURL + '?brpa=' + i + urlParams + '">' + i + '</a></li>';
                        }
                    }
                    if ( end < parseInt(response.lastPage) ) {
                        pagination += '<li class="uk-disabled nolink"><span>...</span></li>';
                        pagination += '    <li class=""><a class="bridge-pagination-link" data-page="' + response.lastPage + '" href="' + currentURL + '?brpa=' + response.lastPage + urlParams + '">' + response.lastPage + '</a></li>';
                    }

                    if ( parseInt(response.lastPage) > 1 ) {
                        pagination += '<li class="">';
                        pagination += '<a class="bridge-pagination-link bridge-next" data-page="2" href="' + currentURL + '?brpa=2' +  urlParams + '"><span uk-pagination-next></span>&nbsp;</a></li>';
                    } else {
                        pagination += '<li class="uk-disabled">';
                        pagination += '<a class="bridge-pagination-link bridge-next" data-page="2" href="#nogo"><span uk-pagination-next></span>&nbsp;</a></li>';
                    }

                    jQuery('#bridge-pagination-container').html(pagination);
                    // Patch 03/06/22 : On active l'infinite scroll uniquement s'il y a plus d'une page de résultats et qu'on a pas fini
                    if(end > 1) {
                        if (window.bridgeActivateInfiniteScroll != undefined) {
                            window.bridgeActivateInfiniteScroll(response.total);
                        }
                    } else {
                        // En mode infinite scroll, comme il est désactivé quand il n'y a pas plus d'une page on doit masquer la pagination
                        if (window.bridgeActivateInfiniteScroll != undefined) {
                            jQuery('#bridge-pagination-container').hide();
                        }
                    }
/*
                    // affichage des options du filtre citycombodyn, s'il est présent sur la liste
                    if (context.datalist && response.cityComboDynList && response.cityComboDynList !== '0') {
                        context.displayCityComboDynList(response.cityComboDynList, context.datalist);
                    } else if (context.datalist && response.cityComboDynList && response.cityComboDynList === '0') {
                        // s'il y en a un dans la liste mais qu'on ne reçoit rien du contrôleur, on réutilise le précédent (cas d'un tri sans modif du moteur)
                        context.displayCityComboDynList(context.cityComboDynList, context.datalist);
                    }
*/
                } else if ((!response.total && (response.message === 'Une erreur est survenue')) ) { // || response.total === 0

                    jQuery(".listeSITCount").html(response.total);
                    jQuery("#listeSIT").html('<div>' + response.message + '</div>');
                    
                    /*if (context.datalist) {
                        context.displayCityComboDynList('', context.datalist);
                    }*/

                } else {
                    jQuery("#listeSIT").html('<div>Une erreur est survenue.</div>');
                    
                    /*if (context.datalist) {
                        context.displayCityComboDynList('', context.datalist);
                    }*/
                    console.error('Erreur : ', response);
                }

                // on réinitialise le booléen
                context.filterHasChanged = false;
            },
            error: function(response) {
                jQuery('.bridge-loader').fadeOut({ duration: 500 });
                jQuery("#listeSIT").html('<div>Une erreur est survenue.</div>');
                console.error('data error', response);
            }
        }).done(function( msg ) {
            jQuery('.bridge-loader').fadeOut({ duration: 500 });
            // alert( "Data Saved: " + msg );
        });

    }

}


