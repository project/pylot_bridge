/*
*       Service Leaflet Générique
*       Copyright DN Consultants 2014
*       www.dnconsultants.fr
*
*      v 1.1.0 : 24/10/2020 début de la numérotaiton et repository distinct
*      v 1.2.0 : 24/10/2020 support des KML ailleurs que dans Joomla
*                ATTENTION ! nécessité de charger également la librairie togeojson.js (ci-jointe)
*      v 1.3.0 : 22/09/2021 : ajout des fonctions et paramètres pour suivre la géolocalisation de l'utilisateur et la matérialiser sur la carte
*      v 1.3.1 : 29/09/2021 : ajout des fonctions switchToWordpress et switchToJoomla pour mettre en place les valeurs par défaut de chaque système
*      v 1.3.2 : 01/2022 : pour intégration des cartes interactives : ajout de nouvelles options, support des cartes IGN et OpenTopoMap
*      v 1.3.3 : 24/04/2022 : bugfix :  ajout de defaultMapOptions pour éviter la perte des boutons de zoom et autres nouvelles options sur appels historiques
*      v. 1.3.4 : fix fonction fitbounds lorsqu'il n'y a qu'un seul marker, la carte ne se recentrait pas sur l'unique marker
*      v. 1.3.5 : fix fonction fitbounds lorsqu'il n'y a qu'un seul marker, la carte ne se recentrait pas sur l'unique marker
*       v. 1.3.6: 04/07/2022 - fix fonction dellallmarkers : n'enlevait pas bien les points sur la carte
*       v. 1.3.7: ajout des fonctions addfixedmarker et delfixedmarker pour ajouter/supprimer des marqueurs fixes non clusterisés
*       v. 1.3.8 : ajout foinction getZoom + option zoomLevelOnInteraction
*/


window.dncLeafletServices = function(containerId) {
    this.version = '1.3.7';
    this.fsControl = null; // Contrôle de fullscreen
    this.eventsHandlers = {
        mapmove:[],
        mapmovestart:[],
        mapmoveend:[],
        mapzoom:[],
        mapzoomstart:[],
        mapzoomend:[],
        mapenterfullscreen:[],
        mapexitfullscreen:[],
        mapclick:[],
        mapresize:[],
        mapdblclick:[],
        mapboundschange:[],
        icondragstart:[],
        icondrag:[],
        icondragend:[]
    };
    // ajout 24/04/2022 : comme on vient taper directement de l'extérieur dans mapoptions, il faut définir ici les options par défaut
    // sinon les nouvelles options sont squizzées (ex : perte des boutons de zoom suite à l'évolution isdisplayingZoomControl)
    this.defaultMapOptions = {
        zoom: 8, // Niveau de Zoom de départ
        centerlat: 48.361724, // Latitude du centre au départ
        centerlon: 7.437315, // Longitude du centre au départ
        maptype: 'MAPTYPE_ROAD', // Type de vue : ROAD, SATTELITE, HYBRID, 3D
        gridSize: 0,
        maxZoom: 5,     // Niveau de zoom à partir duquel la clusterisation est désactivé
        maxTilesZoom: 17,       // Niveau de zoom maximum pour le fond de carte (on ne peut pas zoomer plus que ça)
        scrollWheelZoom: true,  // Activation du zoom à la molette de souris
        keyIgn: '',      // Clef d'utilisation d'un fond de carte IGN
        isDisplayingZoomControl: true,      // Affichage des boutons de zoom
        isDisplayingFullScreenControl: true, // Affichage du bouton de passage en plein écran
        typeMinZoom: 0,         // Niveau de zoom minimal toléré par le fond de carte
        typeMaxZoom: 17,         // Niveau de zoom maximal toléré par le fond de carte
        dragging: !L.Browser.mobile,        // Activer le glisser de la carte en tactile
        tap: !L.Browser.mobile,              // Activer le tap tactile
        loadPopupAjax: false,   // Charger le contenu du popup en Ajax
        zoomLevelOnInteraction: 12, // Niveau de zoom utilisé lors des actions zoom-on-click et zoom-on-hover
        popupAjaxUrl: '/wp-admin/admin-ajax.php?action=brFiche&fiche_template=popup&product_code=' // URL a appeler en ajax pour charger le contenu du popup - le code produit sera concaténé derrière avant l'appel

    };

    // Options de départ si non écrasées plus tard
    this.mapoptions = {
        zoom: 8, // Niveau de Zoom de départ
        centerlat: 48.361724, // Latitude du centre au départ
        centerlon: 7.437315, // Longitude du centre au départ
        maptype: 'MAPTYPE_ROAD', // Type de vue : ROAD, SATTELITE, HYBRID, 3D
        gridSize: 0,
        maxZoom: 5,     // Niveau de zoom à partir duquel la clusterisation est désactivé
        maxTilesZoom: 17,       // Niveau de zoom maximum pour le fond de carte (on ne peut pas zoomer plus que ça)
        scrollWheelZoom: true,  // Activation du zoom à la molette de souris
        keyIgn: '',      // Clef d'utilisation d'un fond de carte IGN
        isDisplayingZoomControl: true,      // Affichage des boutons de zoom
        isDisplayingFullScreenControl: true, // Affichage du bouton de passage en plein écran
        typeMinZoom: 0,         // Niveau de zoom minimal toléré par le fond de carte
        typeMaxZoom: 17,         // Niveau de zoom maximal toléré par le fond de carte
        zoomLevelOnInteraction: 12 // Niveau de zoom utilisé lors des actions zoom-on-click et zoom-on-hover
    };

    // Options par défaut des icônes
    this.defaultIconOptions =  {
        // shadowUrl: 'leaf-shadow.png',
        iconSize:     [50, 50],
        // shadowSize:   [50, 64],
        iconAnchor:   [25, 25],
        // shadowAnchor: [4, 62],
        popupAnchor:  [0, -3]
    };

    // Options par défaut de l'icône de géolocalisation
    this.defaultGeolocIconOptions =  {
        shadowUrl: '',
        iconSize:     [50, 50],
        shadowSize:   [0, 0],
        iconAnchor:   [25, 50],
        shadowAnchor: [0, 0],
        popupAnchor:  [-3, -76]
    };

    // Variable en lecture seule qui informe le code appelant du nivau de zoom actuel (se modifie lorsque l'utilisateur zoome ou dézoome)
    this.currentZoom = 1;

    // Options par défaut des Clusters cf. https://github.com/Leaflet/Leaflet.markercluster
    this.defaultClusterOptions =  {
        spiderfyOnMaxZoom: true,
        showCoverageOnHover: true,  // Au survol,montrer la zone couverte par les point
        zoomToBoundsOnClick: true,  // Zommer lorsqu'on clique sur le cluser
        disableClusteringAtZoom:17, // Zoom maxi - interagit avec spiderfyOnMaxZoom
        maxClusterRadius:80         // Taille en pixels du rond du cluster
    };

    this.baseUrl = '/' ;            //  Url de base utilisée pour les images par défaut et les appel AJAX

    /* Code gardé sous le coude : permet de faire la transformation KML > GeoJson côté serveur
        this.kmlToGeoJsonUrlJoomla = '/index.php?option=com_joomlei&controller=utilslei&task=getgeojson&urlkml=' ;  //  Url de conversion KML vers GeoJSon - Version Joomla
        this.kmlToGeoJsonUrlWordpress = '/index.php?action=bridgekmltogeojson&urlkml=' ;    //  Url de conversion KML vers GeoJSon - Version WordPress
        this.kmlToGeoJsonUrl = this.kmlToGeoJsonUrlJoomla ;
    */

    // Icone par défaut
    this.defaultIconUrlJoomla = this.baseUrl + 'media/com_joomlei/img/standard-icon.png';
    this.defaultIconUrlWordpress = this.baseUrl + 'wp-content/plugins/plugin-sit/assets/img/standard-icon.png';
    this.defaultIconUrl = this.defaultIconUrlJoomla;

    // Options pour le rendu des KML (options pour ls objets google KmlLayer)
    this.kmloptions = {
        //        preserveViewport: true,     // Ne pas bouger la carte lorsqu'on affiche le KML
        //        suppressInfoWindows: true   // NE pas afficher de popup au click sur le KML
    };

    // Options pour le rendu des KML (options pour ls objets tracé)
    this.kmlStyleOptions = {
        stroke: true,               // Trait
        color: '#ffa41c',           // Couleur
        weight: 5,                  // epaisseur du trait
        opacity: 1.0,               // Opacité du trait
        fillopacity: 0.9               // opacité du remplissage
    };

    // Options pour la popup InfoBox
    this.boxStyle = {
        background: '#f0e6dc',
        border: 'solid 1px #ff7e00',
        padding: '10px',
        borderRadius: '4px',
        boxShadow: '0 1px 5px #603913'
    };

    this.containerid = containerId;         // Id du conteneur HTML
    this.mapbusy = false;                   // Occupé ou non
    this.mapobject = {};                    // Objet Leaflet Maps
    this.initdone = false;                  // True après que l'initialisation Google est finie
    this.markers = {};                      // Tableau de markers
    this.fixedMarkers = {};                 // Tableau de markers fixes
    this.currentinfowindow = null;          // Popup couramment ouvert
    this.showkmlon = 'mouseover';           // Nom de l'évènement qui déclenche l'apparition des kml
                                            //  mouseover: passage au-dessus d'un maker
                                            // click : click sur un marker
                                            // load : dés le chargement
    this.clickmarkerfunction = {};
    this.markercluster = {};

    // Mise à jour des options
    this.setoptions = function (options) {
        this.mapoptions = options;
        // Paramètre ajouté après le déploiement de la librairie : on doit s'assurer que ces propriétés sont bien toujours renseignées
        if(! this.mapoptions.maxTilesZoom)
            this.mapoptions.maxTilesZoom = 17;
    };

    // Fonction d'initialisation appelée juste au premier chargement
    this.init = function (mapobjectoptions) {
        var self = this;
        if (!self.initdone) {

            if(self.mapobject.off != undefined) {
                self.mapobject.off();
                self.mapobject.remove();
            }

            if(! this.mapoptions.maxTilesZoom)
                this.mapoptions.maxTilesZoom = 17;

            // Patch 24/04/2022 : on complète l'objet mapoptions avec les options par défaut qui y manqueraient
            for (const opt in self.defaultMapOptions) {
                if(!self.mapoptions.hasOwnProperty(opt)) {
                    self.mapoptions[opt] = self.defaultMapOptions[opt];
                }
            }
            // Fin patch 24/04/2022

            if(!mapobjectoptions)
                mapobjectoptions = {};

            var maptypes = self.get_maptypes();                 // Liste des types (key=>value)
            // On vérifie que le type de fond de carte n'est pas farfelu
            if (!(self.mapoptions.maptype in maptypes)) {
                console.error('Le type de carte est inconnu'); // TODO : mieux gérer les erreurs
                return false;
            }

            // On s'assure que les niveaux de zoom possibles resteront dans les limites imposées par le fond de carte si elles sont plus restreintes que les valeurs de base
            // (le zoom de départ s'ajuste en fonction si besoin)
            switch (self.mapoptions.maptype) {
                case 'MAPTYPE_IGN':
                    self.mapoptions.typeMinZoom = 6;
                    self.mapoptions.typeMaxZoom = 16;
                    break;
                case 'MAPTYPE_OPENTOPOMAPS':
                    self.mapoptions.typeMinZoom = 1;
                    break;
                default:
                    self.mapoptions.typeMinZoom = 0;
                    self.mapoptions.typeMaxZoom = 17;
                    break;
            }

            var tileUrl = maptypes[self.mapoptions.maptype];        // Numéro du type d'affichage

            var tileLayer = L.tileLayer(tileUrl[0], {
                maxZoom: self.mapoptions.maxTilesZoom,
                attribution: tileUrl[1]
            });

            // Objet Leaflet Maps
            // Pour les WebMaps, on ajoutera ensuite les boutons de contrôle pour les placer correctement selon le template
            self.mapobject = L.map(self.containerid, {
                fullscreenControl: self.mapoptions.isDisplayingFullScreenControl,
                zoomControl: self.mapoptions.isDisplayingZoomControl,
                scrollWheelZoom: self.mapoptions.scrollWheelZoom,
                minZoom: self.mapoptions.typeMinZoom,
                maxZoom: self.mapoptions.typeMaxZoom,
                dragging: !L.Browser.mobile,
                tap: !L.Browser.mobile,
                preferCanvas: false
//                renderer: L.canvas() // Empeche les Geojson de s'afficher correctement
            }).setView([self.mapoptions.centerlat, self.mapoptions.centerlon], self.mapoptions.zoom);
            tileLayer.addTo(self.mapobject);

            // On ajoute un gros bouton de fermeture sur la carte en plein écran
            if(self.mapoptions.isDisplayingFullScreenControl) {
                self.mapobject.on('enterFullscreen', function(e) {
                    self.fullScreenCloseButton = new L.Control.Button('<i class="fa far fa-times fa-2x"></i>');
                    self.fullScreenCloseButton.addTo(self.mapobject);
                    self.fullScreenCloseButton.on('click', function () {
                        self.toggleFullScreen();
                    });
                });
                self.mapobject.on('exitFullscreen', function(e) {
                    self.fullScreenCloseButton.remove(); //self.fullScreenCloseButton._container.style.display = 'none';
                });
            }

            if (self.mapoptions.maxZoom && self.mapoptions.maxZoom > 0)
                self.defaultClusterOptions.disableClusteringAtZoom = self.mapoptions.maxZoom;

            self.markercluster = L.markerClusterGroup(self.defaultClusterOptions);
            self.mapobject.addLayer(self.markercluster);


            /*
             Gestion des évènements
             */
            self.mapobject.on('move', function(e) {
                for(var i=0; i<self.eventsHandlers.mapmove.length; i++)
                    self.eventsHandlers.mapmove[i](e);
            });
            self.mapobject.on('movestart', function(e) {
                for(var i=0; i<self.eventsHandlers.mapmovestart.length; i++)
                    self.eventsHandlers.mapmovestart[i](e);
            });
            self.mapobject.on('moveend', function(e) {
                self.majinfoscarte();
                for(var i=0; i<self.eventsHandlers.mapmoveend.length; i++)
                    self.eventsHandlers.mapmoveend[i](e);
                for(var i=0; i<self.eventsHandlers.mapboundschange.length; i++)
                    self.eventsHandlers.mapboundschange[i](e);
            });
            self.mapobject.on('zoom', function(e) {
                for(var i=0; i<self.eventsHandlers.mapzoom.length; i++)
                    self.eventsHandlers.mapzoom[i](e);
            });
            self.mapobject.on('zoomstart', function(e) {
                for(var i=0; i<self.eventsHandlers.mapzoomstart.length; i++)
                    self.eventsHandlers.mapzoomstart[i](e);
            });
            self.mapobject.on('zoomend', function(e) {
                self.majinfoscarte();
                for(var i=0; i<self.eventsHandlers.mapzoomend.length; i++)
                    self.eventsHandlers.mapzoomend[i](e);
                for(var i=0; i<self.eventsHandlers.mapboundschange.length; i++)
                    self.eventsHandlers.mapboundschange[i](e);
            });
            self.mapobject.on('dblclick', function(e) {
                for(var i=0; i<self.eventsHandlers.mapdblclick.length; i++)
                    self.eventsHandlers.mapdblclick[i](e);
            });
            self.mapobject.on('click', function(e) {
                for(var i=0; i<self.eventsHandlers.mapclick.length; i++)
                    self.eventsHandlers.mapclick[i](e);
            });
            self.mapobject.on('resize', function(e) {
                self.majinfoscarte();

                for(var i=0; i<self.eventsHandlers.mapresize.length; i++)
                    self.eventsHandlers.mapresize[i](e);
            });
            self.mapobject.on('enterFullscreen', function(e) {
                for(var i=0; i<self.eventsHandlers.mapenterfullscreen.length; i++)
                    self.eventsHandlers.mapenterfullscreen[i](e);
            });
            self.mapobject.on('exitFullscreen', function(e) {
                for(var i=0; i<self.eventsHandlers.mapexitfullscreen.length; i++)
                    self.eventsHandlers.mapexitfullscreen[i](e);
            });
            // Spécial géoloc
            var gpsMarker = null;
            var gpsCircleMarker;


            self.mapobject.on('locationfound', function(e) {

                var radius = e.accuracy / 2;
                var LeafIconGeoloc = L.Icon.extend({
                    options: self.defaultGeolocIconOptions
                });

                var objIcon = new LeafIconGeoloc({iconUrl: self.defaultGeolocIconUrl});

                var popupContent  = ""; // Vous êtes ici !

                if (gpsMarker == null) {
                    gpsMarker = L.marker(e.latlng,{icon:objIcon}).addTo(self.mapobject);
                    // gpsMarker.bindPopup(popupContent).openPopup();
                    gpsCircleMarker = L.circle(e.latlng, radius).addTo(self.mapobject);
                } else {
                    // gpsMarker.getPopup().setContent(popupContent);
                    gpsMarker.setLatLng(e.latlng);
                    gpsCircleMarker.setLatLng(e.latlng);
                    gpsCircleMarker.setRadius(radius);
                }
            });

            // On écoute le changement de dimensions de la div porteuse pour rafraischir l'affichage
            /*
             google.maps.event.addDomListener(window, "resize", function () {
             setTimeout(function () {
             var center = self.googlemapobject.getCenter();
             google.maps.event.trigger(self.googlemapobject, "resize");
             self.googlemapobject.setCenter(center);
             });
             });
             */
            //this.mapobject = self.mapobject;
            self.reDraw();
            this.initdone = true;
        }
    };

    /**
     * Fonction a appeler avant ou après l'init pour metre en place les spécificités pour Joomla
     */
    this.switchToJoomla = function() {
        this.kmlToGeoJsonUrl = this.kmlToGeoJsonUrlJoomla ;
    }
    /**
     * Fonction a appeler avant ou après l'init pour metre en place les spécificités pour Wordpress
     */
    this.switchToWordpress = function() {
        this.kmlToGeoJsonUrl = this.kmlToGeoJsonUrlWordpress ;
        this.defaultIconUrl = this.defaultIconUrlWordpress;
    }
    /**
     * Fonction a appeler avant ou après l'init pour metre en place les spécificités pour Drupal
     */
    this.switchToDrupal = function() {
        this.kmlToGeoJsonUrl = this.kmlToGeoJsonUrlWordpress ;
        this.defaultIconUrl = this.defaultIconUrlWordpress;
    }
    /**
     * Ajout d'un bouton de passage en plein écran
     * Necessite de charge le script /media/com_joomlei/leaflet-fullscreen/Control.FullScreen.js
     */
    this.addFullScreenButton = function (options) {
        var self = this;
        if(!options)
            options = {};
        // create fullscreen control : nécessite la librairie full screen -- https://unpkg.com/leaflet.fullscreen@1.4.3/Control.FullScreen.js
        this.fsControl = new L.Control.FullScreen(options);

        // add fullscreen control to the map
        if(!options){
            this.fsControl = L.control.fullscreen({
                position: 'topleft', // change the position of the button can be topleft, topright, bottomright or bottomleft, defaut topleft
                title: 'Show me the fullscreen !', // change the title of the button, default Full Screen
                titleCancel: 'Exit fullscreen mode', // change the title of the button when fullscreen is on, default Exit Full Screen
                content: null, // change the content of the button, can be HTML, default null
                forceSeparateButton: true, // force seperate button to detach from zoom buttons, default false
                forcePseudoFullscreen: true, // force use of pseudo full screen even if full screen API is available, default false
                fullscreenElement: false // Dom element to render in full screen, false by default, fallback to map._container
            }).addTo(self.mapobject);
        } else {
            this.fsControl = L.control.fullscreen(options).addTo(self.mapobject);
        }
    };

    this.setLayersSwitch = function(layers) {
        var self = this;
        var leafletLayers = [];
        var layersmenu = {};

        var maptypes = self.get_maptypes();  // Liste des types (key=>value)
        var tileUrl = '';
        var estvide = true;

        for(i=0 ; i < layers.length ; i++ ) {
            if (layers[i] in maptypes) {
                tileUrl = maptypes[layers[i]];      // Numéro du type d'affichage
                layersmenu[tileUrl[2]] = L.tileLayer(tileUrl[0], {
                    maxZoom: self.mapoptions.maxTilesZoom,
                    attribution: tileUrl[1]
                }) ;
                estvide = false;
            }
        }

        if(! estvide )
            L.control.layers(layersmenu).addTo(self.mapobject);


    };

    // Set option
    this.setOption = function (name, value) {
        var self = this;
        var options = this.mapobject.options;
        options[name] = value;
        self.mapobject.setOptions(options);
    };

    // Get zoom
    this.getZoom = function () {
        if(self.mapobject) {
            return self.mapobject.getZoom();
        } else {
            return 10;
        }
    };

    // Mise à jour des infos de la carte (fonction interne appelée lors d'un changement)
    this.majinfoscarte = function() {
        var self = this;
        var center = self.mapobject.getCenter();
        self.mapoptions.centerlat = center.lat;
        self.mapoptions.centerlon = center.lng;
        self.mapoptions.zoom = self.mapobject.getZoom();
        self.mapoptions.radius = self.get_radius_from_bounds(self.mapobject.getBounds());
        self.mapobject.invalidateSize(); // Force le redessiner
    };

    // Renvoie un rayon en Km qui couvre les bounds de la Map courante
    // Utilisé pour le mode de navigation par carte
    this.get_radius_from_bounds = function (bounds) {
        //var bounds = new google.maps.LatLngBounds();
        var sw = bounds.getSouthWest();
        var ne = bounds.getNorthEast();
        var lat1 = sw.lat;
        var lon1 = sw.lng;
        var lat2 = ne.lat;
        var lon2 = ne.lng;

        var dist = 11 * 10000 * Math.sqrt(Math.pow(lat1 - lat2, 2) + Math.pow(lon1 - lon2, 2));
        var rayon = Math.round(dist / 2);
        return rayon;
    };



    // Retourne tous les calques possibles
    this.get_maptypes = function () {
        let self = this;
        return {
            MAPTYPE_ROAD: ['https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png','&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>', 'Plan'],
            MAPTYPE_SATTELITE: ['https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}','Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community', 'Sattelite'],
            MAPTYPE_HYBRID: ['http://tile.mtbmap.cz/mtbmap_tiles/{z}/{x}/{y}.png','&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>', 'Hybride'],
            MAPTYPE_TERRAIN: ['https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png','Map data: &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)', 'Relief'],
            MAPTYPE_MTBMAP: ['http://tile.mtbmap.cz/mtbmap_tiles/{z}/{x}/{y}.png','&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>', 'Plan'],
            MAPTYPE_HIKEBIKE: ['http://{s}.tiles.wmflabs.org/hikebike/{z}/{x}/{y}.png','&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>', 'Plan cyclable'],
            MAPTYPE_BLACK_WHITE: ['http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png', '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>', 'Plan'],
            MAPTYPE_OPENSTREETMAPS: ['https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>', 'Plan'],
            MAPTYPE_GREYSCALE: ['http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png', '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>', 'Plan'],
            MAPTYPE_OPENTOPOMAPS : ['https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png', '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors', 'Plan'],
            MAPTYPE_IGN : ['https://wxs.ign.fr/' + self.mapoptions.keyIgn + '/geoportail/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&STYLE=normal&TILEMATRIXSET=PM&FORMAT=image/jpeg&LAYER=GEOGRAPHICALGRIDSYSTEMS.MAPS.SCAN25TOUR&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}', '<a target="_blank" href="https://www.geoportail.gouv.fr/">Geoportail France</a>', 'Plan']
        };
    };

    // Toggle fullscreen
    this.toggleFullScreen = function() {
        var self = this ;
        if (self.mapobject && self.mapobject.fullscreenControl) {
            self.mapobject.fullscreenControl.toggleFullScreen();
        }
    };

    // Obtenir le nombre de markers dans al carte
    this.getMarkersCount = function() {
        return Object.keys(this.markers).length;
    };

    // Ajouter un marqueur
    this.addmarker = function (id, lat, lon, title, iconurl, activeiconurl, popupcontent, onclickfunction, kmlpath, options, iconOptions, markerOptions, kmlStyleOptions) {
        var self = this;
        if (!popupcontent)
            popupcontent = null;
        if(!kmlpath)
            kmlpath = '';
        if (!self.initdone) {           // TODO : mieux gérer l'erreur
            console.error('addmarker appele avant init!');
            return false;
        }

        if(!id || id == undefined)
            id = Object.keys(self.markers).length;

        if(!iconurl ||  iconurl==undefined || iconurl== null || iconurl == '') {
            iconurl = self.defaultIconUrl;
        }

        self.markers[id] = {};
        if(!iconOptions) {
            iconOptions = this.defaultIconOptions;
        }
        var LeafIcon = L.Icon.extend({
            options: iconOptions
        });


        var objIcon = new LeafIcon({iconUrl: iconurl});

        if(! markerOptions)
            markerOptions = {};

        markerOptions.icon = objIcon;
        self.markers[id].marker = new L.marker([lat, lon], markerOptions);

        self.markers[id].marker.id = id;

        self.markers[id].marker.on('dragstart', function(e) {
            for(var i=0; i<self.eventsHandlers.icondragstart.length; i++)
                self.eventsHandlers.icondragstart[i](e);
        });
        self.markers[id].marker.on('drag', function(e) {
            for(var i=0; i<self.eventsHandlers.icondrag.length; i++)
                self.eventsHandlers.icondrag[i](e);
        });
        self.markers[id].marker.on('dragend', function(e) {
            for(var i=0; i<self.eventsHandlers.icondragend.length; i++)
                self.eventsHandlers.icondragend[i](e);
        });

        if(popupcontent && popupcontent !== '')
            self.markers[id].marker.bindPopup(popupcontent);



        // self.mapobject.addLayer( self.markers[id].marker);
        self.markercluster.addLayer( self.markers[id].marker);


        self.markers[id].lat = lat;
        self.markers[id].lon = lon;
        self.markers[id].title = title;
        self.markers[id].iconurl = iconurl;                 // Icone inactif
        self.markers[id].activeiconurl = activeiconurl;     // Icone actif
        self.markers[id].popupcontent = popupcontent;       // Contenu du popup
        self.markers[id].infowindow = null;                 // Objet infowindow de Google
        self.markers[id].state = 0;                         // 0 : inactif - 1 : actif
        self.markers[id].marker.setZIndexOffset(100);
        self.markers[id].kml = null;
        self.markers[id].KmlLayer = null;
        self.markers[id].options = {};
        if(options)
            self.markers[id].options = options;

        if(! kmlStyleOptions)
            kmlStyleOptions = self.kmlStyleOptions;


        if(kmlpath !== '') {
            /* Code gardé sous le coude : permet de faire la transformation KML > GeoJson côté serveur
            var lurl = self.kmlToGeoJsonUrl + encodeURIComponent(kmlpath) ;
            // console.log('url kml ',lurl);
            jQuery.ajax({
                url: lurl,
                success: function (data) {
             */
            jQuery.ajax({
                url: kmlpath, // lurl,
                success: function (dataKML) {
                    if (dataKML && dataKML !== '') {
                        var data = toGeoJSON.kml(dataKML);

                        if (data && data !== '') {
                            // console.log('add kml ', data);
                            self.markers[id].KmlLayer = L.geoJSON(data, {
                                //  style: function (feature) {
                                //      return {color: feature.properties.color};
                                //  }
                                //}).bindPopup(function (layer) {
                                //    return layer.feature.properties.description;
                            }).setStyle(kmlStyleOptions);

                            // Si Apparition direct
                            if (self.showkmlon === 'load') {
                                setTimeout(function () {
                                    // self.markers[id].KmlLayer.addTo(self.mapobject);
                                    self.markercluster.addLayer(self.markers[id].KmlLayer);
                                    self.fitboundstomarkers();
                                }, 500);
                            }
                        }
                    }
                }
            });

            self.markers[id].kml = kmlpath;
            // kmloptions['url'] = kmlpath;
            // self.markers[id].KmlLayer = new google.maps.KmlLayer(kmloptions);
            if(self.showkmlon === 'mouseover') {
                self.markers[id].marker.on('mouseover', function(e) {
                    self.activatemarker (id, true, false);
                });
            } else if(self.showkmlon === 'click') {
                self.markers[id].marker.on('click', function(e) {
                    self.activatemarker (id, true, false);
                });
            }
        }

        if (onclickfunction) {
            self.markers[id].marker.on('click', function(e) {
                // self.centeronmarker(id);
                setTimeout(onclickfunction(id), 50);
                if(self.showkmlon === 'click') {
                    self.activatemarker (id, true, false);
                }

            });
        } else {
            // Apparition du KML sur click
            if(self.showkmlon === 'click') {
                self.markers[id].marker.on('click', function(e) {
                    self.activatemarker (id, true, false);
                });
            }
            // Gestion de l'appel du popup en ajax
            if(self.mapoptions.loadPopupAjax) {
                self.markers[id].marker.on('click', function(e) {
                    self.openJoomleiInfowindowAjax(id);
                });
            }
        }
    };


    // Ajouter un marqueur
    this.addfixedmarker = function (id, lat, lon, title, iconurl, options, iconOptions, markerOptions) {
        var self = this;

        if (!self.initdone) {           // TODO : mieux gérer l'erreur
            console.error('addmarker appele avant init!');
            return false;
        }

        if(!id || id == undefined)
            id = Object.keys(self.fixedMarkers).length;

        if(!iconurl ||  iconurl==undefined || iconurl== null || iconurl == '') {
            iconurl = self.defaultIconUrl;
        }

        self.fixedMarkers[id] = {};
        if(!iconOptions) {
            iconOptions = this.defaultIconOptions;
        }
        var LeafIcon = L.Icon.extend({
            options: iconOptions
        });


        var objIcon = new LeafIcon({iconUrl: iconurl});

        if(! markerOptions)
            markerOptions = {};

        markerOptions.icon = objIcon;
        markerOptions.clickable = false;
        markerOptions.draggable = false;

        self.fixedMarkers[id].marker = new L.marker([lat, lon], markerOptions);
        self.fixedMarkers[id].marker.id = id;

        // self.mapobject.addLayer( self.markers[id].marker);
        // self.mapobject.addLayer(self.fixedMarkers[id].marker);
        self.fixedMarkers[id].mapMarker = self.fixedMarkers[id].marker.addTo( self.mapobject);

        self.fixedMarkers[id].lat = lat;
        self.fixedMarkers[id].lon = lon;
        self.fixedMarkers[id].title = title;
        self.fixedMarkers[id].iconurl = iconurl;                 // Icone inactif
        self.fixedMarkers[id].marker.setZIndexOffset(100);
        self.fixedMarkers[id].options = {};
        if(options)
            self.fixedMarkers[id].options = options;
    };

    // Ajouter un marqueur
    this.delfixedmarker = function (id) {
        var self = this;

        if(self.fixedMarkers.hasOwnProperty(id) ) {
            self.mapobject.removeLayer(self.fixedMarkers[id].mapMarker);
            delete self.fixedMarkers[id];
        }

    };


    // Afficher un kml sur la carte
    // UrlKML : url du kml
    // id : chaine ou null : id auquel rattacher le kml (permet de masquer / afficher le kml )
    // Styleoptions : objet JS avec les options de style de GeoJSON
    // forcerStyle : booléen : vrai si on veut forcer le style passé, sinon le style est extrait du KML (par défaut)
    this.addkml = function(urlkml, id, styleOptions, forcerStyle, onclickfunction) {
        var self = this;
        if(urlkml !== '') {

            var lesoptions = self.kmlStyleOptions;
            if(styleOptions) {
                lesoptions = styleOptions;
            }
            /* Code gardé sous le coude : permet de faire la transformation KML > GeoJson côté serveur
                    var lurl = self.kmlToGeoJsonUrl + encodeURI(urlkml) ;
                    // console.log('url kml ',lurl);
                    jQuery.ajax({
                        url: lurl,
                        success: function (data) {
                            if (data && data != '') {
                                // console.log('add kml ', data);
                                // console.log('id ', id);
                                // console.log('styleoptions',lesoptions);
         */
            jQuery.ajax({
                url: urlkml, // lurl,
                success: function (dataKML) {
                    if (dataKML && dataKML !== '') {
                        var data = toGeoJSON.kml(dataKML);
                        /*
                        if(data.hasOwnProperty('features') && data.features.length === 1) {
                            data = data.features[0];
                        }
                        */
                        // Si id est fourni on ratache le KML à un marker existant d'id connu
                        if(id) {

                            // console.log('On met en place le KML sur marjers de id : ' + id);
                            if(! self.markers[id])
                                self.markers[id] = {};
                            self.markers[id].kml = urlkml;
                            if(forcerStyle && forcerStyle === true) {
                                // On force le style du tracé
                                self.markers[id].KmlLayer = L.geoJSON(data, { style: lesoptions}).setStyle(lesoptions);
                            } else {
                                // On va extraire le style des propriétés GeoJSON stroke ou color
                                self.markers[id].KmlLayer = L.geoJSON(data, {
                                    style: function(feature) {
                                        // console.log('debug prop 2', feature.properties);
                                        if(feature.properties.hasOwnProperty('color') && feature.properties.color != '') {
                                            lesoptions.color = feature.properties.color ;
                                        }
                                        if(feature.properties.hasOwnProperty('stroke') && feature.properties.stroke != '') {
                                            lesoptions.color = feature.properties.stroke ;
                                        }
                                        return lesoptions;
                                    }
                                });
                            }

                            //self.markers[id].KmlLayer.addTo(self.mapobject);
                            self.markercluster.addLayer( self.markers[id].KmlLayer);
                        } else {
                            // Si id n'est fourni on met un KML en place qu'on ne pourra plus enlever
                            if(forcerStyle == true) {
                                // On force le style du tracé

                                var geojson = new L.geoJSON(data, {}).setStyle(lesoptions).addTo(self.mapobject);
                                // self.mapobject.addLayer(geojson);
                            } else {
                                new L.geoJSON(data, {
                                    style: function (feature) {
                                        // console.log('debug prop', feature.properties);
                                        if (feature.properties.hasOwnProperty('color') && feature.properties.color != '') {
                                            lesoptions.color = '#000000'; // feature.properties.color ;
                                        }
                                        if (feature.properties.hasOwnProperty('stroke') && feature.properties.stroke != '') {
                                            lesoptions.color = '#FFFFFF'; // feature.properties.stroke ;
                                        }
                                        return lesoptions;
                                    }
                                }).setStyle(lesoptions).addTo(self.mapobject);
                            }
                        }


                        if (onclickfunction) {
                            self.markers[id].KmlLayer.on('click', function(e) {
                                setTimeout(onclickfunction(id, e), 50);
                            });
                        }

                        self.fitboundstomarkers();
                    }
                }
            });

        }
    };


    // Récupère le contanu du popup en ajax et affiche le popup sur un marker
    // id : id de marker sur la carte : numéro fiche SIT ou moda-fichelei (pour Joomla)
    // id_param_module : n° id param module (Joomla uniquement)
    this.openJoomleiInfowindowAjax = function(id, id_param_module){
        var self = this;
        //http://devalsaceaveloj3.dnconsultants.fr/index.php?option=com_dnccarto&view=cartolei&format=actions&action=getAjaxHtml&id_params_mod=2&produit=222004544
        if(!id_param_module)
            id_param_module = 1;
        if(id != '') {
            var numprod = id ;
            if(id.indexOf('-') >=0) {
                var letemp = id.indexOf('-');
                numprod = id.substr(letemp + 1);
            }

            jQuery.ajax({
                type: "GET",
                url: self.mapoptions.popupAjaxUrl + id,
                dataType: 'json'
            }).done(function(json) {
                if(self.markers[id] != undefined) {
                    if(json.success && json.data !== '') {
                        self.markers[id].marker.setPopupContent(json.data);
                        self.markers[id].marker.openPopup();
                    }
                } else {
                    console.log('erreur : map.markers[' + id + '] est undefined');
                }
            });
        }
    };


    // Vérifier l'existence d'un marker
    this.markerexists = function (id) {
        return (id in this.markers);
    };

    // Supprime un marqueur
    this.delmarker = function (id) {
        var self = this;

        // self.mapobject.removeLayer(self.markers[id].marker);
        if(this.markers[id].marker)
            this.markercluster.removeLayer(this.markers[id].marker);

        if(this.markers[id].kml != '' && this.markers[id].KmlLayer)
            this.markercluster.removeLayer(this.markers[id].KmlLayer);

        delete this.markers[id];
    };

    // Supprime tous les marqueurs
    this.delallmarkers = function () {

        this.markercluster.clearLayers() ;
        this.markers = {};
        /*
        for (var markerid in this.markers) {
            this.delmarker(markerid);
        }
         */
        // this.markercluster.clearMarkers();
    };

    // Activer l'icone d'un marqueur
    this.activatemarker = function (id, toggl, animate, iconOptions) {
        var self = this;
        // Un seul icone actif à la fois
        if (toggl) {
            // On désactive tout
            this.deactivatemarkers(iconOptions);
        }
        if(!iconOptions) {
            iconOptions = this.defaultIconOptions;
        }
        var LeafIcon = L.Icon.extend({
            options: iconOptions
        });


        // On active un marqueur
        if (this.markers[id] != undefined) {

            if (this.markers[id].marker != undefined && this.markers[id].marker != null ) {

                var objIcon = new LeafIcon({iconUrl: this.markers[id].activeiconurl});
                this.markers[id].marker.setOpacity(0);
                this.markers[id].marker.setOpacity(1);
                this.markers[id].marker.setIcon(objIcon);
                this.markers[id].marker.setZIndexOffset(1000);
                this.markers[id].state = 1;
            }
            // Si kml on l'affiche
            if(this.markers[id].KmlLayer) {
                // this.markers[id].KmlLayer.addTo(this.mapobject);
                self.markercluster.addLayer(self.markers[id].KmlLayer);
                // console.log('ACTIV KML LAYER');
                // console.log(this.markers[id].KmlLayer );
            }


            /*
             // Si animation est true, on anime le marqueur
             if (animate) {
             this.markers[id].marker.setAnimation(google.maps.Animation.BOUNCE);
             }
             */
        }
    };


    // Activer l'icone d'un marqueur
    this.centeronmarker = function (id, zoom) {
        var self = this;
        if (this.markers[id] != undefined) {
            var center = self.markers[id].marker.getLatLng();
            if (!zoom || zoom === null) {
                zoom = 13;
            }
            // self.mapobject.panTo(center);
            self.mapobject.setView(center, zoom);
        }
    };

    // Fixer le niveau de Zoom
    this.setZoom = function (zoom) {
        this.mapobject.setZoom(zoom);
    };

    // Redessinne la carte
    this.reDraw = function () {
        var self = this;
        self.mapobject.invalidateSize();
    };

    // Recentrer la carte sur un point
    this.setcenter = function (lat, lon) {
        var self = this;
        self.mapoptions.centerlat = lat;
        self.mapoptions.centerlon = lon;
        self.mapobject.panTo(new L.latLng(lat, lon));
        // On déclenche un évènement broadcast pour avertir tout le monde que la carte a bougé
        /*
         $rootScope.$broadcast( 'dncMapsServices.mapChanged', {
         mapoptions: self.mapoptions
         });
         */

    };

    // Ouvrir l'info window d'un marqueur
    // this.openinfomarker = function (id) {
    //     if (this.currentinfowindow != null) {
    //         this.currentinfowindow.close();
    //     }
    //     this.activatemarker(id, true, false);
    //     this.markers[id].infowindow.open(this.googlemapobject, this.markers[id].marker);
    //     this.currentinfowindow = this.markers[id].infowindow;
    // };

    // Désactiver un marqueur
    this.deactivatemarker = function (markerid,iconOptions) {
        if(!iconOptions) {
            iconOptions = this.defaultIconOptions;
        }
        var LeafIcon = L.Icon.extend({
            options: iconOptions
        });

        if (this.markers[markerid].state != 0) {
            var objIcon = new LeafIcon({iconUrl: this.markers[markerid].iconurl});
            this.markers[markerid].marker.setIcon(objIcon);
            this.markers[markerid].marker.setZIndexOffset(100);
            this.markers[markerid].state = 0;
            // this.markers[markerid].marker.setAnimation(null);                        // Enlever l'animation s'il
            // On desactive aussi lees kml sauf si affichage en load
            if(this.showkmlon != 'load' && this.markers[markerid].KmlLayer) {
                // this.markers[markerid].KmlLayer.setMap(null);
                // this.mapobject.removeLayer(this.markers[markerid].KmlLayer);
                this.markercluster.removeLayer(this.markers[markerid].KmlLayer);
            }
        }
    };

    // Masquer un KML
    this.hidekml = function (markerid) {
        if (this.showkmlon != 'load' && this.markers[markerid].KmlLayer) {
            // this.markers[markerid].KmlLayer.setMap(null);
            // this.mapobject.removeLayer(this.markers[markerid].KmlLayer);
            this.markercluster.removeLayer(this.markers[markerid].KmlLayer);
        }
    };
    // Masquer tous les KMLS
    this.hideallkmls = function () {
        // On désactive tout
        for (var markerid in this.markers) {
            this.hidekml(markerid);
        }
    };

    // Désactiver tous les marqueurs
    this.deactivatemarkers = function (iconOptions) {
        // On désactive tout
        for (var markerid in this.markers) {
            this.deactivatemarker(markerid,iconOptions);
        }
    };


    // MAsquer un marker
    this.hidemarker = function(id) {
        if(id in this.markers ) {
            if(this.markers[id].marker) {
                // this.mapobject.removeLayer(this.markers[id].marker);
                this.markercluster.removeLayer(this.markers[id].marker);
            }
        }
        //self.mapobject.addLayer( self.markers[id].marker);
    };
    // Montrer un marker masqué
    this.showmarker = function(id) {
        if(id in this.markers) {
            // this.mapobject.addLayer(this.markers[id].marker);
            this.markercluster.addLayer(this.markers[id].marker);
        }
        //self.mapobject.addLayer( self.markers[id].marker);
    };

    // Masquer tous les markers
    this.hideallmarkers = function () {
        for (var markerid in this.markers) {
            // this.markers[markerid].marker.setOpacity(0);
            // this.mapobject.removeLayer(this.markers[markerid].marker);
            this.markercluster.removeLayer(this.markers[markerid].marker);
        }
    };
    // Masquer tous les markers
    this.showallmarkers = function () {
        for (var markerid in this.markers) {
            // this.mapobject.addLayer( this.markers[markerid].marker);
            this.markercluster.addLayer( this.markers[markerid].marker);
        }
    };

    this.getActiveIconUrl = function (id) {
        return this.markers[id].activeiconurl;
    };

    this.getBounds = function() {
        return this.mapobject.getBounds();
    };

    // Recentrer-zommer la carte sur tous ses marqueurs
    this.fitboundstomarkers = function () {
        var self = this;
        // LA solution en Leaflet
        // var lmarkrs = [];
        var latlngs  = [];
        var bounds = null;
        // var nbmarkers = self.getMarkersCount();
        var keysmarkers = Object.keys(this.markers);

        if(keysmarkers.length > 1) {
            bounds = new L.latLngBounds(self.markers[keysmarkers[0]].marker.getLatLng(), self.markers[keysmarkers[1]].marker.getLatLng());

            for (const i in keysmarkers) {
                if(self.markers[keysmarkers[i]] && self.markers[keysmarkers[i]].marker){
                    bounds.extend(self.markers[keysmarkers[i]].marker.getLatLng());
                }
                if(self.markers[keysmarkers[i]] && self.markers[keysmarkers[i]].KmlLayer){
                    bounds.extend(self.markers[keysmarkers[i]].KmlLayer.getBounds());
                }
            }
            self.mapobject.fitBounds(bounds); // [2]
            self.mapobject.panInsideBounds(bounds); // [2]
        }
        // Cas particulier d'une seule fiche
        if(keysmarkers.length == 1) {
            this.centeronmarker(keysmarkers[0], 13);
        }
        self.reDraw();
        // var featureGroup = L.featureGroup(lmarkrs);
        // map.fitBounds(featureGroup.getBounds());
        // var bounds = L.latLngBounds(latlngs);
        /*
         var bounds = new google.maps.LatLngBounds();
         for (var markerid in this.markers)
         bounds.extend(this.markers[markerid].marker.position);
         this.googlemapobject.fitBounds(bounds);
         */
    };

    // Désactivation du MouseWheel
    this.disabledMouseWheel = function(){
        var self = this;
        self.mapobject.scrollWheelZoom.disable();
    };

    // Activation du scrollwheelzoom
    this.enableMouseWheel = function() {
        let self = this;
        self.mapobject.scrollWheelZoom.enable();
    }

    // Rafraichir la carte après un redimensionnement
    this.afterresize = function () {
        /*
         var self = this;
         setTimeout(function () {
         var center = self.googlemapobject.getCenter();
         google.maps.event.trigger(self.googlemapobject, "resize");
         self.googlemapobject.setCenter(center);
         // google.maps.event.trigger(self.googlemapobject, 'resize');
         // console.log('resize');
         }, 50);
         */
    };

    this.on = function(handlername, func) {
        this.eventsHandlers[handlername].push(func);
    };

    this.locate = function(options){
        var self = this;
        if(! options || options === {} )
            options = {setView: true, maxZoom: self.mapoptions.zoom}
        self.mapobject.locate(options);
    };

    // Spécial Tripplanner
    this.ajoutFichesTrip = function(fichesMeres,vider){
        //On charge les fichesMeres dans la carte (vider permet de supprimer les markers avant l'ajout)
        var self = this;
        var listPoints = [];
        jQuery.each(fichesMeres.children(),function(key,ficheMere){//Création de la liste des points (utilisée pour la suppression par liste)
            jQuery.each(jQuery(ficheMere.miniFiches),function(key,miniFiche){
                var lat = parseFloat(jQuery(ficheMere.donnees)[0].CHAMP10);
                var lon = parseFloat(jQuery(ficheMere.donnees)[0].CHAMP9);
                var jour = jQuery(miniFiche[0].elementPosition).attr('data-dnc-plannerj');//donne le jour de la miniFiche

                listPoints.push(lat+'_'+lon);
            });
        });

        if(vider)//On supprime les markers qui ne sont pas dans la liste
            this.delallmarkers();
        jQuery.each(fichesMeres.children(),function(key,ficheMere){
            jQuery.each(jQuery(ficheMere.miniFiches),function(key,miniFiche){//Ici on regarde si les fichesMeres ont des miniFiches, si oui on les affichent
                var id = parseFloat(ficheMere.donnees.CHAMP2);
                var title = parseFloat(ficheMere.donnees.CHAMP3);
                var lat = parseFloat(ficheMere.donnees.CHAMP10);
                var lon = parseFloat(ficheMere.donnees.CHAMP9);
                var jour = jQuery(miniFiche[0].elementPosition).attr('data-dnc-plannerj');//donne le jour de la miniFiche
                var modalite = ficheMere.donnees.CHAMP2;
                var data = ficheMere.donnees;
                // "picto_fond_jour.png
                // mages/dnc_tripplanner/
                // components/com_joomlei/assets/icontxt.php?size=9&colr=255&colv=255&colb=255&posx=2&centrer=1&ombre=1&fontname=arialbd&file=


                var lurlpicto = '/components/com_joomlei/assets/icontxt.php??size=9&colr=255&colv=255&colb=255&posx=2&centrer=1&ombre=1&fontname=arialbd&file=' + this.options.urlBase+this.options.dossierBase+this.options.iconJour+'&letxt='+jour;
                if(typeof(self.markers[id]) == 'undefined' && !isNaN(lat) ){
                    self.addmarker(id,lat,lon,title,lurlpicto, lurlpicto)//Ajoute une miniFiche dans la carte
                    //function (id, lat, lon, title, iconurl, activeiconurl, popupcontent, onclickfunction, kmlpath, options, iconOptions, markerOptions)
                }
            });
        });
    }





};


/**************************************************************
 Utilitaire pour la géolocalisation
 ***************************************************************/

window.dncGeolocalisationServices = function() {
    this.getGeoloc = function (successfunc, errorfunc) {
        if (navigator.geolocation) {
            var options = {
                enableHighAccuracy: false,
                timeout: 10000,
                maximumAge: 50
            };
            // console.log('Avant geo Geolocalise ');

            navigator.geolocation.getCurrentPosition(successfunc, errorfunc, options);

        }
        else {
            var errorobj = {code: 1, message: "Dommage... Votre navigateur ne prend pas en compte la géolocalisation"};
            errorfunc(errorobj);
        }
    };

};


