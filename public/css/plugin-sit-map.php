<style>

/* ====== */
/* Map.php */

/* la carte Leaflet doit toujours avoir une hauteur définie */
.bridge-carte {
    width: 100%;
    height: 800px;
}
/* Fin Map */
/* ====== */

/* ====== */
/* Templates de carte */
.bridge-map-container {
    width: 100%;
    height: 100%;
}

.bridge-map {
    width: 100%;
    height: 100%;
}

.entry-content .bridge-map a, .bridge-map-popup-content a {
    text-decoration: none;
}

/* Spinner */
.bridge-map-spinner.uk-icon.uk-spinner {
    position: absolute;
    z-index: 0;
    width: 100%;
    height: 100%;
    display: flex;
    align-content: center;
    justify-content: center;
    background-color: rgba(255, 255, 255, 0.5);
}

/* Bouton d'affichage de la légende (dont overrides UIKit) */
.bridge-map-legend-show.uk-overlay {
    z-index: 600;
    display: none;
    padding: 20px;
    height: fit-content;
    background-color: #FFF;
    border-radius: 30px;
    font-size: x-large;
}
/* Fin Templates de carte */
/* ====== */

/* ====== */
/* Legend */

.bridge-map-legend {
    height: fit-content;
    overflow: auto;
}

.bridge-map-legend:hover {
    cursor: default;
}

/* override de UIKit */
.bridge-map-legend.uk-overlay {
    z-index: 600;
    padding: 10px;
    width: 30%;
    max-width: 350px;
}

.bridge-map-legend-carte1, .bridge-map-legend-carte2 {
    max-height: 90%;
}

.bridge-map-legend-carte3, .bridge-map-legend-carte4 {
    max-height: 100%;
}

/* ====== */
/* Header de la légende */

.bridge-map-legend-title {
    display: flex;
    align-items: center;
    margin-bottom: 20px;
}

/* Intitulé de la légende */
.bridge-map-legend-title h3 {
    width: auto;
    margin: auto;
    margin-bottom: 0;
}

/* Bouton pour masquer la légende */
.bridge-map-legend-hide {
    font-size: x-large;
    align-self: baseline;
}

/* Fin Header de la légende */
/* ====== */

/* ======= */
/* Items de la légende */

.bridge-map-item, .bridge-map-subitem {
    display: flex;
    justify-content: space-between;
    margin-bottom: 5px;
}

.bridge-map-item {
    max-width: 100%;
}

.bridge-map-subitem {
    font-size: 1.2em;
    font-family:
        -apple-system,BlinkMacSystemFont,
        "Segoe UI",
        Roboto,
        "Helvetica Neue",
        Arial,
        "Noto Sans",
        sans-serif,
        "Apple Color Emoji",
        "Segoe UI Emoji",
        "Segoe UI Symbol",
        "Noto Color Emoji";
}

.bridge-map-item:hover, .bridge-map-subitem:hover, .bridge-map-legend-hide:hover, .bridge-map-legend-show:hover {
    cursor: pointer;
}

.bridge-map-item.active, .bridge-map-subitem.active {
    background-color: lightgrey;
    border-radius: 30px;
}

/* overrides de UIKit */
.bridge-map-item.uk-accordion-title {
    display: flex;
    align-items: center;
    margin-top: 10px;
}
.uk-accordion-title::before {
    display: none;
}
.uk-accordion div {
    margin-top: 0px;
}

.bridge-map-item-text {
    display: flex;
    align-items: center;
}

/* Icônes des items dans la légende */
.bridge-map-legend-icon {
    width: auto;
    margin-right: 10px;
    max-height: 30px;
    max-width: 30px;
}

/* Intitulé des items */
.bridge-map-item-label {
    margin-bottom: 0;
}

/* Nombre de fiches associées à l'item */
.bridge-map-subitem-count, .bridge-map-item-count {
    margin-left: 30px;
    border: 1px solid black;
    border-radius: 50%;
    text-align: center;
    align-self: center;
    height: 25px;
    width: 30px;
}
.bridge-map-item-count {
    /* height: 30px;
    width: 35px; */
    font-size: initial;
}

/* Items catégories */
.bridge-map-item.category .bridge-map-item-line {
    height: 1px;
    width: 100%;
    margin-left: 15%;
}

/* Items non-catégories */
.bridge-map-item.not-category .bridge-map-item-label {
    margin-top: unset;
}

<?php foreach ($mapData->items as $item) : ?>

    
    #bridge-map-item-label-<?= $item->id ?>, #bridge-map-subitem-label-<?= $item->id ?> {
        <?php if (isset($item->parameters->color)) : ?>
            color: <?= $item->parameters->color ?>;
        <?php endif; ?>
        white-space: nowrap;
    }

    #bridge-map-item-<?= $item->id ?>.category .bridge-map-item-line {
        <?php if (isset($item->parameters->color)) : ?>
            background: <?= $item->parameters->color ?>;
        <?php else : ?>
            background: #000;
        <?php endif; ?>
    }

    <?php foreach ($item->subItems as $subItem) : ?>
        #bridge-map-subitem-label-<?= $subItem->id ?> {
            <?php if (isset($subItem->parameters->color)) : ?>
                color: <?= $subItem->parameters->color ?>;
            <?php else : ?>
                color: <?= $item->parameters->color ?>;
            <?php endif; ?>
        }
    <?php endforeach; ?>
<?php endforeach; ?>

/* Fin Items de la légende */
/* ======= */

/* Fin Legend */
/* ====== */


/* ======= */
/* Pop-ups */

/* Titre de la fiche */
.bridge-map-popup-content h3 {
    font-weight: bold;
}

/* Lignes d'informations supplémentaires */
.bridge-map-popup-info {
    margin-top: 5px;
}
a.bridge-map-popup-info-item {
    color: #333!important;
}
a.bridge-map-popup-info-item:hover {
    background-color: lightgrey;
    border-radius: 30px;
    padding: 0 5px 0 5px;
    margin: 0 -5px 0 -5px;
}

/* Bouton En savoir plus */
.bridge-map-popup-permalink.uk-button {
    text-transform: inherit;
    color: #fff;
}
.bridge-map-popup-permalink.uk-button:hover {
    background-color: #f8f8f8;
    color: #222;
    border: 1px solid #222;
}

/* Icônes des boutons liens */
.bridge-map-popup-icon {
    color: black;
}

/* Pour afficher correctement les icônes Font Awesome de type Solid (.fas) */
.bridge-map-popup-icon.fas {
    font-family: 'Font Awesome 5 Pro';
    font-weight: 900;
}

/* ====== */
/* Styles spécifiques à chaque template de pop-up */

/* Popup1 */
.bridge-map-popup1 .bridge-map-popup-btn-group {
    display: flex;
    justify-content: space-around;
    align-items: center;
}
.bridge-map-popup1 .bridge-map-popup-btn-group a {
    border: 1px solid black;
    border-radius: 50%;
    height: 30px;
    width: 30px;
    padding-top: 5px;
}

/* popup3 */
.bridge-map-popup3 .bridge-map-popup-titles, .bridge-map-popup3 .bridge-map-popup-grid.uk-grid {
    padding: 0px 15px;
}
.bridge-map-popup3 .bridge-map-popup-btn-group, .bridge-map-popup4 .bridge-map-popup-btn-group {
    padding: 15px 0px 0px 0px;
    margin-left: -15px;
    margin-right: -15px;
}

/* popup4 */
.bridge-map-popup4 .bridge-map-popup-permalink {
    margin-bottom: 10px;
}

/* popup5 */
.bridge-map-popup5 h3 {
    margin-bottom: 0px;
}
.bridge-map-popup5 .bridge-map-popup-surtitre {
    margin-bottom: 20px;
}

</style>
