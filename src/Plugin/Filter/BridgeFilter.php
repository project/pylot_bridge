<?php

/**
 * @file
 * Contains Drupal\pylot_bridge\Plugin\Filter.
 */

namespace Drupal\pylot_bridge\Plugin\Filter;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\pylot_bridge\Services\BridgeShortCodeParser;
use Drupal\pylot_bridge\Services\BridgeUtils;

/**
 * Provides a filter to replace  with code wrappers.
 *
 * @Filter(
 *  id = "bridge_filter",
 *  module = "pylot_bridge",
 *  title = @Translation("Pylot Bridge : shortcodes"),
 *  type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE,
 *  cache = FALSE,
 *  weight = 0
 * )
 */
class BridgeFilter extends FilterBase {

    private $attachListLibraries = false;
    private $attachFicheLibraries = false;

    /**
     * {@inheritdoc}
     */
    public function process($text, $langcode) {
        $new_text = $text;

        $data = BridgeShortCodeParser::process($text, $langcode);

        // Shortcode [brliste id="XX"]
        $new_text = $data->text;
        $result = new FilterProcessResult($new_text);
        if($data->shortcodeListeMatched) {
            $libraries = BridgeUtils::getListLibraries();
            $result->setAttachments(array(
                'library' => $libraries,
            ));
        }
        if($data->shortcodeFicheMatched) {
            $libraries = BridgeUtils::getFicheLibraries();
            $result->setAttachments(array(
                'library' => $libraries,
            ));
        }
        return $result ;
    }




}