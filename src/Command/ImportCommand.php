<?php

namespace Drupal\pylot_bridge\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Drupal\Console\Core\Command\ContainerAwareCommand;
use Drupal\pylot_bridge\Services\BridgeUtils;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ImportCommand.
 *
 * Drupal\Console\Annotations\DrupalCommand (
 *     extension="pylot_bridge",
 *     extensionType="module"
 * )
 */
class ImportCommand extends ContainerAwareCommand
{

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('pylot_bridge:import')
            ->setDescription($this->trans('commands.pylot_bridge.import.description'));
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getIo()->info('Début de l\'import Bridge complet');

        $credentials = BridgeUtils::getBridgeCredentials();
        $importService = \Drupal::service('pylot_bridge.import');
        $res = $importService->doImport(true);
        if($res === false) {
            $this->getIo()->info("\nImport arrêté en erreur\n");
            $this->getIo()->info($importService->getErrorMsg());
            die('');
        } else {
            $warnings = $importService->getWarnings();
            if(count($warnings) > 0) {
                $this->getIo()->info ("\nImport terminé avec avertissements\n");
                echo implode("\n", $warnings);
                die('');
            } else {
                $this->getIo()->info("\nImport terminé avec succès\n");
            }
        }
        // $data = \Drupal::service('pylot_bridge.front_utils')->hello2();
        // $data = BridgeUtils::foo();

        $this->getIo()->info('Import terminé');


    }

}
