<?php

namespace Drupal\pylot_bridge\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Drupal\Console\Core\Command\ContainerAwareCommand;
use Drupal\pylot_bridge\Services\BridgeUtils;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ImportTaxonomiesCommand.
 *
 * Drupal\Console\Annotations\DrupalCommand (
 *     extension="pylot_bridge",
 *     extensionType="module"
 * )
 */
class ImportTaxonomiesCommand extends ContainerAwareCommand
{

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('pylot_bridge:import_taxonomies')
            ->setDescription($this->trans('commands.pylot_bridge.import_taxonomies.description'));
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getIo()->info('Début de l\'import des termes');

        $importService = \Drupal::service('pylot_bridge.import');
        $res = $importService->update_sit_terms_from_bridge();
        if($res === false) {
            $this->getIo()->info("\nImport arrêté en erreur - erreur à la mise à jour initiale des termes\n");
            $this->getIo()->info($importService->getErrorMsg());
            die('');
         } else {
            $this->getIo()->info("\nImport terminé avec succès\n");
        }

    }

}
