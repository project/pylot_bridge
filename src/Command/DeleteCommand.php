<?php

namespace Drupal\pylot_bridge\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Drupal\Console\Core\Command\ContainerAwareCommand;
use Drupal\pylot_bridge\Services\BridgeUtils;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DeleteAllDataCommand.
 *
 * Drupal\Console\Annotations\DrupalCommand (
 *     extension="pylot_bridge",
 *     extensionType="module"
 * )
 */
class DeleteCommand extends ContainerAwareCommand
{

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('pylot_bridge:delete')
            ->setDescription('Supprimme toutes les fiches et taxonomies SIT');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getIo()->info('Début de la suppression');

        $credentials = BridgeUtils::getBridgeCredentials();
        $importService = \Drupal::service('pylot_bridge.import');
        $res = $importService->deleteAllData(true);
        if($res === false) {
            $this->getIo()->info("\nSuppression arrêtée en erreur\n");
            $this->getIo()->info($importService->getErrorMsg());
            die('');
        } else {
           $this->getIo()->info("\nSuppression terminée avec succès\n");
        }
        // $data = \Drupal::service('pylot_bridge.front_utils')->hello2();
        // $data = BridgeUtils::foo();

        // $this->getIo()->info('Import terminé');


    }

}
