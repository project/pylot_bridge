<?php

namespace Drupal\pylot_bridge\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class ImportController.
 */
class ImportController extends ControllerBase {

  /**
   * Display_import.
   *
   * @return string
   *   Return Hello string.
   */
  public function display_import() {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: display_import')
    ];
  }
  /**
   * Import_ajax.
   *
   * @return string
   *   Return Hello string.
   */
  public function import_ajax() {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: import_ajax')
    ];
  }
  /**
   * List_langues_ajax.
   *
   * @return string
   *   Return Hello string.
   */
  public function list_langues_ajax() {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: list_langues_ajax')
    ];
  }
  /**
   * List_weblists_ajax.
   *
   * @return string
   *   Return Hello string.
   */
  public function list_weblists_ajax() {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: list_weblists_ajax')
    ];
  }
  /**
   * List_products_ajax.
   *
   * @return string
   *   Return Hello string.
   */
  public function list_products_ajax() {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: list_products_ajax')
    ];
  }
  /**
   * Update_terms_ajax.
   *
   * @return string
   *   Return Hello string.
   */
  public function update_terms_ajax() {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: update_terms_ajax')
    ];
  }
  /**
   * Update_post_categories_ajax.
   *
   * @return string
   *   Return Hello string.
   */
  public function update_post_categories_ajax() {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: update_post_categories_ajax')
    ];
  }
  /**
   * Delete_obsolete_posts_ajax.
   *
   * @return string
   *   Return Hello string.
   */
  public function delete_obsolete_posts_ajax() {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: delete_obsolete_posts_ajax')
    ];
  }
  /**
   * Update_rewrite_rules_ajax.
   *
   * @return string
   *   Return Hello string.
   */
  public function update_rewrite_rules_ajax() {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: update_rewrite_rules_ajax')
    ];
  }
  /**
   * Delete_all_data_ajax.
   *
   * @return string
   *   Return Hello string.
   */
  public function delete_all_data_ajax() {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: delete_all_data_ajax')
    ];
  }

}
