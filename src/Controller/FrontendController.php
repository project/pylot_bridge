<?php

namespace Drupal\pylot_bridge\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Drupal\pylot_bridge\Services\BridgeUtils ;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Cache\CacheableMetadata;


/**
 * Class ImportController.
 */
class FrontendController extends ControllerBase
{
    use StringTranslationTrait;

    public function __construct() {
        if (!$this->stringTranslation) {
            $this->stringTranslation = \Drupal::service('string_translation');
        }
    }

    /**
     * Redimensionneur dynamique d'images
     * @return string|void
     */
    public function resize_image()
    {
        $timeToCache = 1800 ; // Durée de cache navigateur

        /* retourne les données brutes d'une image redimensionnée
          $file : URL de la photo originale
          $mode : mode de redimensonnement :
          - ajust pour ajuster aux dimensions passées (ne déborde pas)
          - remplir : pour remplir le cadre de dimensions passées
          - deform : pour déformer l'image et forcer les dimensions exactes
          $selwidth : largeur souhaitée
          $selheight : hauteur souhaitée
          $timeToCache : durée du cache navigateur renvoyé dans les entêtes http
         */

        $file = "";
        $mode = "";
        $selwidth = "";
        $selheight = "";
        $def = 60;
        $debug = false;

        if (array_key_exists('timeToCache', $_REQUEST))
            $timeToCache = (int) $_REQUEST['timeToCache'];
        if (array_key_exists('file', $_REQUEST))
            $file = $_REQUEST['file'];
        if (array_key_exists('mode', $_REQUEST))
            $mode = $_REQUEST['mode'];
        if (array_key_exists('selwidth', $_REQUEST))
            $selwidth = $_REQUEST['selwidth'];
        if (array_key_exists('selheight', $_REQUEST))
            $selheight = $_REQUEST['selheight'];
        if (array_key_exists('quality', $_REQUEST))
            $quality = $_REQUEST['quality'];
        if (array_key_exists('debug', $_REQUEST))
            $debug = ($_REQUEST['debug'] ==1);


        if ($mode == "")
            $mode = "ajust";
        if ($selwidth == "" && $selheight == "")
            $selwidth = "100";



        // Mise en cache des images redimensionnées
        $cid = 'pylot_bridge:image_' . $file . '-' . $selwidth . '-' . $selheight . '-' . $mode . '-' . $quality;
        $testCache = \Drupal::cache()->get($cid);
        if ($testCache === false) {
            $res = BridgeUtils::resize_image($file, $mode, $selwidth, $selheight, $quality, false);
            \Drupal::cache()->set($cid, $res);
        } else {
            $res = $testCache->data;
        }

        $valid = false;
        if($res['success']) {
            switch ($res['ext']) {
                case 'gif':
                    $headers = array(
                        'Content-Type'     => 'image/gif',
                        'Expires' => gmdate('D, d M Y H:i:s', time() + $timeToCache) . ' GMT',
                        'Cache-Control' => 'public',
                        'Cache-Control' => 'max-age=' . $timeToCache,
                        'Last-Modified' => gmdate("D, d M Y H:i:s") . " GMT",
                        'Pragma' => 'cache',
                        'Cache-Control' => 'public',
                    );
                    $valid = true;
                    return new Response ($res['data'], 200, $headers);
                    break;

                case 'jpg':
                    $headers = array(
                        'Content-Type'     => 'image/jpeg',
                        'Expires' => gmdate('D, d M Y H:i:s', time() + $timeToCache) . ' GMT',
                        'Cache-Control' => 'public',
                        'Cache-Control' => 'max-age=' . $timeToCache,
                        'Last-Modified' => gmdate("D, d M Y H:i:s") . " GMT",
                        'Pragma' => 'cache',
                        'Cache-Control' => 'public',
                    );
                    $valid = true;
                    return new Response ($res['data'], 200, $headers);
                    break;

                case 'png':
                    $headers = array(
                        'Content-Type'     => 'image/png',
                        'Expires' => gmdate('D, d M Y H:i:s', time() + $timeToCache) . ' GMT',
                        'Cache-Control' => 'public',
                        'Cache-Control' => 'max-age=' . $timeToCache,
                        'Last-Modified' => gmdate("D, d M Y H:i:s") . " GMT",
                        'Pragma' => 'cache',
                        'Cache-Control' => 'public',
                    );
                    $valid = true;
                    return new Response ($res['data'], 200, $headers);
                    break;

                default:
                    $valid = false;
                    return new Response ('-');
                    break;
            }
        } else {
            return new Response ('error');
        }
    }


    /**
     * Récupération des POI à mettre sur la carte à côté de la liste
     */
    public function listeAjaxCities()
    {
        // On crée un tableau d'attributs pour l'affichage Ajax
        $attributes = array();
        $attributes['max'] = 900;
        if(isset($_REQUEST['max'])) {
            $attributes['max'] = $_REQUEST['max'];
        }
        $attributes['id'] = '';
        if(isset($_REQUEST['id'])) {
            $attributes['id'] = $_REQUEST['id'];
        }
        $attributes['product_codes'] = '';
        if(isset($_REQUEST['product_codes'])) {
            $attributes['product_codes'] = $_REQUEST['product_codes'];
        }
        $get_as_label_array = '';
        if(isset($_REQUEST['get_as_label_array'])) {
            $get_as_label_array = $_REQUEST['get_as_label_array'];
        }

        $res = array(
            'success' => false,
            'max' => $attributes['max'],
            'total' => null,
            'message' => '',
            'data' => null
        );

        try {
            $dataReturned =  BridgeUtils::getCitiesData($attributes, true);
        } catch (\Exception $e) {
            $res['message'] = 'Une erreur est survenue : ' . $e->getMessage();
            return new JsonResponse($res);
        }

        // $lang = BridgeUtils::getLanguage();
        if (empty($dataReturned) || !isset($dataReturned['success']) || !isset($dataReturned['data'])) {
            if (!empty($get_as_label_array)) {
                $res = array();
            } else {
                $res['success'] = true;
                $res['data'] = array();
            }
            return new JsonResponse($res);
        }

        // si erreur
        if ($dataReturned['success'] === false) {
            $res['message'] = $dataReturned['message'];
            return new JsonResponse($res);
        }

        if (!empty($get_as_label_array)) {
            $res = array();
            for($i = 0 ; $i < count($dataReturned['data']) ; $i++) {
                $res[] = (object) array('label' => $dataReturned['data'][$i]->city, 'lat' => $dataReturned['data'][$i]->latitude, 'lon' => $dataReturned['data'][$i]->longitude);
            }
        } else {
            $res['success'] = true;
            $res['data'] = $dataReturned['data'];
        }
        return new JsonResponse($res);
    }

    /**
     * Récupération dynamique de données Bridge pour affichage en liste
     * @return JsonResponse
     */
    public function list_products_json() {

        if (isset($_REQUEST['action'])) {
            $action = $_REQUEST['action'];
            if ($action == 'brListCities') {
                return $this->listeAjaxCities();
            }
        }
        // On crée un tableau d'attributs pour l'affichage Ajax
        $attributes = array();
        if (isset($_REQUEST['id']))
            $attributes['id'] = $_REQUEST['id'];
        if (isset($_REQUEST['max']))
            $attributes['max'] = $_REQUEST['max'];
        else
            $attributes['max'] = '';
        if (isset($_REQUEST['first']))
            $attributes['first'] = $_REQUEST['first'];
        if (isset($_REQUEST['change']))
            $attributes['change'] = $_REQUEST['change'];

        $res = array(
            'max' => $attributes['max'],
            'total' => null,
            'message' => '',
            'html' => ''
        );

        try {
            $dataReturned = BridgeUtils::getListData($attributes, true);
        } catch(\Exception $e) {
            $res['message'] = 'Une erreur est survenue : ' . $e->getMessage();
            return new JsonResponse($res);
        }

        // $lang = BridgeUtils::getLanguage();
        if (empty($dataReturned) || !isset($dataReturned['success']) || !isset($dataReturned['message']) || !isset($dataReturned['total'])) {
            if(isset($dataReturned['message']))
                $res['message'] = $dataReturned['message'];
            else
                $res['message'] = 'Une erreur est survenue';
            return new JsonResponse($res);
        }

        // si erreur
        if ($dataReturned['success'] === false) {
            $res['message'] = $dataReturned['message'];
            return new JsonResponse($res);
        }
        // si success mais aucun résultat
        if ($dataReturned['total'] === 0) {
            $res['total'] = $dataReturned['total'];
            $res['message'] = $dataReturned['message'];
            return new JsonResponse($res);
        }

        $html = '';
        // On charge l'HTML des vignettes uniquement
        if (isset($_REQUEST['itemTemplate']) && !empty($_REQUEST['itemTemplate'])) {
            $attributes['itemTemplate'] = $_REQUEST['itemTemplate'];
        }

        try {
            // On récupère les données prêtes à être passées à la vue
            $data = BridgeUtils::prepareListDataForRender($dataReturned);

            $build = array(
                '#theme' => 'liste_sit_ajax',
                '#attached' => array('library' => array()),
                '#children' => array(),
                '#listInfos' => $data['listInfos'],
                '#listParameters' => $data['listParameters'],
                '#data' => $data['data'],
                '#moteur' => $data['moteur'],
                '#bridgeParameters' => $data['bridgeParameters'],
                '#pageTitle' => $data['pageTitle'],
                '#pageDescription' => $data['pageDescription'],
            );
            $html = \Drupal::service('renderer')->renderPlain($build);

            $res['total'] = $dataReturned['total'];
            $res['html'] = $html;
            // $res['lang'] = $lang;

            if (isset($data['listInfos']->cityComboDynList)) {
                $res['cityComboDynList'] = $data['listInfos']->cityComboDynList;
            } else {
                $res['cityComboDynList'] = "0";
            }
            if (isset($data['listInfos']->lastPage)) {
                $res['lastPage'] = $data['listInfos']->lastPage;
            }
        } catch (\Exception $e) {
            $res['message'] = 'Erreur 09 : ' . $e->getMessage();
            return new JsonResponse($res);
        }

        return new JsonResponse($res);
    }

    /**
     * Récupération dynamique de données Bridge pour affichage des points sur la carte
     * @return JsonResponse
     */
    public function list_products_json_for_map_poi() {

        // On crée un tableau d'attributs pour l'affichage Ajax
        $attributes = array();
        if (isset($_REQUEST['id']))
            $attributes['id'] = $_REQUEST['id'];
        if (isset($_REQUEST['max']))
            $attributes['max'] = $_REQUEST['max'];
        else
            $attributes['max'] = '';
        if (isset($_REQUEST['first']))
            $attributes['first'] = $_REQUEST['first'];

        if (isset($_REQUEST['change']))
            $attributes['change'] = $_REQUEST['change'];

        // Pour réduire le nombre de champs retournés par bridge
        $attributes['minimal_select'] = '1';
        /*
        if (isset($_REQUEST['lang']))
            $attributes['lang'] = $_REQUEST['lang'];
        if (isset($_REQUEST['braf']))
            $attributes['braf'] = $_REQUEST['braf'];
        if (isset($_REQUEST['brai']))
            $attributes['brai'] = $_REQUEST['brai'];
        if (isset($_REQUEST['bras']))
            $attributes['bras'] = $_REQUEST['bras'];
        if (isset($_REQUEST['brsd']))
            $attributes['brsd'] = $_REQUEST['brsd'];
        */
        $res = array(
            'success' => false,
            'max' => $attributes['max'],
            'total' => null,
            'message' => '',
            'data' => null
        );

        try {
            $dataReturned = BridgeUtils::getListData($attributes, true);
        } catch(\Exception $e) {
            $res['message'] = 'Une erreur est survenue : ' . $e->getMessage();
            return new JsonResponse($res);
        }

        // $lang = BridgeUtils::getLanguage();
        if (empty($dataReturned) || !isset($dataReturned['success']) || !isset($dataReturned['message']) || !isset($dataReturned['total'])) {
            if(isset($dataReturned['message']))
                $res['message'] = $dataReturned['message'];
            else
                $res['message'] = 'Une erreur est survenue';
            return new JsonResponse($res);
        }

        // si erreur
        if ($dataReturned['success'] === false) {
            $res['message'] = $dataReturned['message'];
            return new JsonResponse($res);
        }

        $res['success'] = true;
        $res['data'] = $dataReturned['data'];
        return new JsonResponse($res);
    }



    /**
     * bridge_send_email_recaptcha
     * Appelée en ajax sur envoi d'un formulaire de contact dans la fiche : vérifie le token Recaptcha V3 et envoie le mail au prestataire
     */
    public function bridge_send_email_recaptcha() {
        $productcode = $_REQUEST['productCode'];
        $webPageId = $_REQUEST['webPageId'];
        $token = $_REQUEST['token'];
        $name = $_REQUEST['name'];
        $email = $_REQUEST['email'];
        $message = $_REQUEST['message'];
        $subject = $_REQUEST['subject'];
        $dest = $_REQUEST['dest'];

        $res = array('success' => false, 'data' => '');

        // On vérifie que l'appel est correct
        if(empty($dest) || empty($productcode) || empty($webPageId) || empty($name) || empty($email) || empty($message) || empty($token) || empty($subject) ) {
            $res['data'] = 'Erreur : appel incorrect';
            return new JsonResponse($res);
        }

        try {
            // On vérifie le token Recaptcha V3
            $dataFiche = BridgeUtils::getDataFiche($productcode, $webPageId);

            $body = '';
            if(!empty($dataFiche->parameters->emailPrestBody ))
                $body = $dataFiche->parameters->emailPrestBody;

            if(empty($body)) {
                $body = "Bonjour, \n\nUne demande de renseignements a été envoyée depuis le site " . get_site_url() ;
            }
            $emailBody = $body . "\n";
            $emailBody .= "\nNom : ". stripcslashes($name) ;
            $emailBody .= "\nEmail : $email";
            $emailBody .= "\nMessage : " . stripcslashes($message);
            if(!empty($dataFiche->parameters->emailPrestBodyEnd ))
                $emailBody .= "\n\n" . $dataFiche->parameters->emailPrestBodyEnd;

            // Vérification recaptcha
            if ($dataFiche->parameters->emailFormAntispam == 'RECAPTCHAV2') {
                $recaptchaSecretKey = $dataFiche->parameters->recaptchav2_secret_key;
                $recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
                $recaptcha = file_get_contents($recaptcha_url . '?secret=' . $recaptchaSecretKey . '&response=' . $token);
                $recaptcha = json_decode($recaptcha);
                if($recaptcha->success === true) {
                    $mailManager = \Drupal::service('plugin.manager.mail');
                    $module = 'pylot_bridge';
                    $key = 'email_form';
                    $params = array('subject' => $subject, 'message' => $emailBody, 'reply-to' => stripcslashes($name) . '<' . $email . '>');
                    $langcode = 'fr';
                    $send = true;
                    $result = $mailManager->mail($module, $key, $dest, $langcode, $params, NULL, $send);
                    if ($result['result'] !== true) {
                        $message = $this->t('Erreur lors de l\'envoi de l\'email');
                        $res = array('success' => false, 'data' => $message);
                        return new JsonResponse($res);
                    } else {
                        $res = array('success' => true, 'data' => $this->t('Message envoyé avec succès'));
                        return new JsonResponse($res);
                    }
                } else {
                    $prop = 'error-codes';
                    $message = $this->t('La vérification antispam a échoué');
                    if(isset($recaptcha->$prop) && is_array($recaptcha->$prop)) {
                        $message .= ' ' . implode (' - ', $recaptcha->$prop);
                    }
                    $res = array('success' => false, 'data' => $message);
                    return new JsonResponse($res);
                }
            } elseif ($dataFiche->parameters->emailFormAntispam == 'RECAPTCHAV3') {
                $recaptchaSecretKey = $dataFiche->parameters->recaptchav3_secret_key;
                $recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
                $recaptcha = file_get_contents($recaptcha_url . '?secret=' . $recaptchaSecretKey . '&response=' . $token);
                $recaptcha = json_decode($recaptcha);
                // Selon le score recaptcha on envoie le mail ou pas
                if ($recaptcha->score >= 0.5) {
                    $mailManager = \Drupal::service('plugin.manager.mail');
                    $module = 'pylot_bridge';
                    $key = 'email_form';
                    $params = array('subject' => $subject, 'message' => $emailBody, 'reply-to' => stripcslashes($name) . '<' . $email . '>');
                    $langcode = 'fr';
                    $send = true;
                    $result = $mailManager->mail($module, $key, $dest, $langcode, $params, NULL, $send);
                    if ($result['result'] !== true) {
                        $message = $this->t('Erreur lors de l\'envoi de l\'email');
                        $res = array('success' => false, 'data' => $message);
                        return new JsonResponse($res);
                    } else {
                        $res = array('success' => true, 'data' => $this->t('Message envoyé avec succès'));
                        return new JsonResponse($res);
                    }
                } else {
                    $prop = 'error-codes';
                    $message = $this->t('La vérification antispam a échoué');
                    if(isset($recaptcha->$prop) && is_array($recaptcha->$prop)) {
                        $message .= ' ' . implode (' - ', $recaptcha->$prop);
                    }
                    $res = array('success' => false, 'data' => $message);
                    return new JsonResponse($res);
                }
            } else {
                $message = $this->t('Mode de vérification captcha non supporté');
                $res = array('success' => false, 'data' => $message);
                return new JsonResponse($res);
            }

        } catch (\Exception $e) {
            $message = $this->t('Erreur lors de l\'envoi de l\'email');
            $res = array('success' => false, 'data' => $message);
            return new JsonResponse($res);
        }
    }

    /**
     * Fonction proxy pour afficher le widget dispos Avizi depuis le site courant pour éviter les erreurs CORS lors du redimensionement de l'IFRAME
     */
    public function dispos_avizi() {
        if(!isset($_REQUEST['dispos-avizi']))
            return;

        if(!isset($_REQUEST['avizi_entity']) || !isset($_REQUEST['avizi_id']) || !isset($_REQUEST['avizi_lang']) || empty($_REQUEST['avizi_entity']) || empty($_REQUEST['avizi_id']) || $_REQUEST['avizi_lang'] === '') {
            die("appel incorrect");
        }

        die(file_get_contents('https://app.avizi.fr/disponibilites/public/' . $_REQUEST['avizi_entity'] . '/' . $_REQUEST['avizi_id'] .'&consult=a5a5a5&libre=abc638&complet=cc4a4a&fermer=000000&langue=' . $_REQUEST['avizi_lang']));

    }
}
