<?php

namespace Drupal\pylot_bridge\Controller;

use Drupal\pylot_bridge\Services\BridgeUtils;
use Drupal\views\Routing\ViewPageController;
use Drupal\Core\Routing\RouteMatchInterface;

class TaxonomyTermViewPageController extends ViewPageController {

    /**
     * {@inheritdoc}
     */
    public function handle($view_id, $display_id, RouteMatchInterface $route_match) {

            $term = $route_match->getParameter('taxonomy_term');
            $vid = $term->get('vid')->first()->getValue();
            $vid = $vid['target_id'];

            //on teste le vocabulaire
            if ($vid === 'rubrique_sit') {
                try {
                    $listId = $term->get('field_sit_list_id')->value;
                    if(empty($listId)) {
                        return array(
                            '#type' => 'markup',
                            '#markup' => 'Erreur 01 : pas de liste Bridge attachée au terme ' . $term->get('name')->value . ' (n°' . $term->id() . ')'
                        );
                    }

                    $attributes = array (
                        'id' => $listId,
                        'first' => '1',
                        'change' => '1',
                    );

                    // Appel à Bridge
                    $data = BridgeUtils::getListData($attributes);

                    if(! is_array($data) || !isset($data['success'])) {
                        return array(
                            '#type' => 'markup',
                            '#markup' => 'Erreur 02 : erreur système interne sur getListData'
                        );
                    }

                    if($data['success'] !== true) {
                        return array(
                            '#type' => 'markup',
                            '#markup' => 'Erreur 03 : erreur lors de l\'appel à Bridge : ' . $data['message']
                        );
                    }

                    // On prépare l'organisation des données en vue de l'affichage - avec conversion des tableaux associatifs en objets au passage pour simplifier
                    $data = BridgeUtils::prepareListDataForRender($data);

                    // On attache les librairies scripts, css ainsi que le tableau de données à passer aux templates d'affichage
                    $build = BridgeUtils::prepareListViewRenderer($data);
                    $build['#cache']['contexts'][] = 'url.path';
                    $build['#cache']['contexts'][] = 'url.query_args';

                    // Gestion de la balise link rel=next et rel=prev prev pour la pagination
                    $displayRelNextPrev = $data['listParameters']->displayRelNextPrev;
                    if($displayRelNextPrev == true) {
                        $prevPageURL = $data['listInfos']->prevPageURL;
                        $nextPageURL = $data['listInfos']->nextPageURL;

                        if (!empty($prevPageURL)) {
                            $relnext = [
                                '#tag' => 'link',
                                '#attributes' => [
                                    'rel' => 'prev',
                                    'href' => $prevPageURL,
                                ],
                            ];
                            $build['#attached']['html_head'][] = [$relnext, 'rel-next'];
                        }
                        if (!empty($nextPageURL)) {
                            $relnext = [
                                '#tag' => 'link',
                                '#attributes' => [
                                    'rel' => 'next',
                                    'href' => $nextPageURL,
                                ],
                            ];
                            $build['#attached']['html_head'][] = [$relnext, 'rel-next'];
                        }
                    }

                    return $build;
                } catch (\Exception $e) {
                    if($data['success'] !== true) {
                        return array(
                            '#type' => 'markup',
                            '#markup' => 'Erreur 05 : ' . $e->getMessage()
                        );
                    }
                }

        }

        return parent::handle($view_id, $display_id, $route_match);
    }

}