<?php
namespace Drupal\pylot_bridge\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;
use Drupal\pylot_bridge\Services\BridgeBlockService;
use Drupal\pylot_bridge\Services\BridgeUtils;
use Drupal\pylot_bridge\Services\DateUtils;

/**
 * Custom twig functions.
 */
class BridgeTwig extends \Twig_Extension {

    public function getName() {
        return 'pylot_bridge.twig_extension';
    }
    public function getFilters() {
        return [
            new TwigFilter('bridge_render_block', [$this, 'renderBlock']),
            new TwigFilter('bridge_render_named_block', [$this, 'renderNamedBlock']),
            new TwigFilter('bridge_render_reseaux_sociaux', [$this, 'renderReseauxSociaux']),
            new TwigFilter('bridge_extract_photos', [$this, 'extractPhotos']),
            new TwigFilter('bridge_extract_photos_from_block', [$this, 'extractPhotosFromBlock']),
            new TwigFilter('bridge_get_elevation_data', [$this, 'getElevationData']),
            new TwigFilter('bridge_get_product_dispos', [$this, 'getProductDispos']),
            new TwigFilter('bridge_get_child_colums', [$this, 'getChildColumns']),
            new TwigFilter('bridge_get_block_size', [$this, 'getBlockSize']),
            new TwigFilter('bridge_get_item_width', [$this, 'getItemWidth']),
            new TwigFilter('prepare_custom_blocks_display', [$this, 'prepareCustomBlocksDisplay']),
            new TwigFilter('extract_first_text_value', [$this, 'extractBlockFirstTextValue']),
            new TwigFilter('extract_block_data_first_text_value', [$this, 'extractBlockDataFirstTextValue']),
            new TwigFilter('extract_first_item_label', [$this, 'extractBlockFirstItemLabel']),
            new TwigFilter('bridge_resize_image', [$this, 'bridgeResizeImage']),
            new TwigFilter('bridge_render_icon', [$this, 'bridgeRenderIcon']),
            new TwigFilter('bridge_get_coupled_products', [$this, 'getCoupledProducts']),
            new TwigFilter('bridge_product_permalink', [$this, 'getPermalinkFromProduct']),
            new TwigFilter('bridge_product_gmap_link', [$this, 'getgmapLinkFromProduct']),
            new TwigFilter('get_min_value_for_filter_item', [$this, 'getMinValueForfilterItem']),
            new TwigFilter('get_max_value_for_filter_item', [$this, 'getMaxValueForfilterItem']),
            new TwigFilter('get_block_component', [$this, 'getBlockComponent']),
            new TwigFilter('json_decode', [$this, 'jsonDecode']),
            new TwigFilter('cast_to_array', [$this, 'castToArray']),
            new TwigFilter('filter_modalities', [$this, 'filterModalities']),
            new TwigFilter('scroll_to_id', [$this, 'scrollTo'])
        ];
    }
    public function getFunctions() {
        return [
            new TwigFunction('items_block_is_not_empty', [$this, 'itemsBlockIsNotEmpty']),
            new TwigFunction('block_is_not_empty', [$this, 'blockIsNotEmpty']),
            new TwigFunction('text_block_is_not_empty', [$this, 'textBlockIsNotEmpty']),
            new TwigFunction('text_block_data_is_not_empty', [$this, 'textBlockDataIsNotEmpty']),
            new TwigFunction('bridge_is_object', [$this, 'bridgeIsObject']),
            new TwigFunction('get_fiche_hours_as_array_for_display_as_calendar', [$this, 'getFicheHoursAsArrayForDisplayAsCalendar']),
            new TwigFunction('get_fiche_hours_as_array_for_display_as_array', [$this, 'getFicheHoursAsArrayForDisplayAsArray']),
        ];
    }

    /*          FILTRES          */


    /**
     * Utilitaire pour trouver un critère depuis la priopriété modalities d'un objet de données product issu de Bridge
     *
     * @param       $mods : passer product->modalities tel quel
     * @param array $filters : tableau associatif (ex: {criterionCode: 9000000, modalityCode:123456 }
     *
     * @return array Tableau d'objets product->modalities filtré sur les critères de recherche
     */
    public function filterModalities($mods, $filters = array()) {
        $filters = (array) $filters ;
        return BridgeUtils::filterModalities($mods, $filters);
    }

    /**
     * @param string $url : url 
     * @param string $section : section jusqu'a laquelle scroller
     * 
     * @return string lien amenant a la section du site
     */
    public function scrollTo($url, $section = "") {
        // On vire les query string qui empeche le scroll   
        if(str_contains($url, "?")) {
            $url = substr($url, 0, strpos($url, "?"));
        }

        return $url . "#$section";
    }

    /**
     * jsonDecode
     * Decode une chaine json
     */
    public function jsonDecode($str) {
        return json_decode($str);
    }

    /**
     * castToArray
     * Cast Object to Array
     */
    public function castToArray($obj) {
        $response = (array)$obj;
        return $response;
    }

    /**
     * getgmapLinkFromProduct
     * Retoune un lien d'itinéraire google maps
     * @param $fiche : objet product tel que retourné par l'API Bridge
     * @return string : URL d'itineraire google Maps
     */
    public function getgmapLinkFromProduct($fiche) {
        return BridgeBlockService::getGmapLink($fiche);
    }

    /**
     * getBlockSize
     * Retoune une chaine à insérer dans la propriété class d'une balise HTML pour lui donner la largeur définie dans Bridge
     * contient les noms de classes css du framework uikit à insérer pour obtenir la laerrgeur responsive souhaitée
     * @param $blockData : objet block tel que retourné par l'API Bridge
     * @return string : chaine de classe CSS
     */
    public function getBlockSize($blockData) {
        return BridgeBlockService::renderSizeBlock($blockData);
    }

    /**
     * getItemWidth
     * Retoune une chaine à insérer dans la propriété class d'une balise HTML pour lui donner la largeur définie dans Bridge
     * contient les noms de classes css du framework uikit à insérer pour obtenir la laerrgeur responsive souhaitée
     * @param $widthData : propriété width d'un objet  retourné par l'API Bridge (objet avec propriété xl, l, m, s et default)
     * @return string : chaine de classe CSS
     */
    public function getItemWidth($widthData) {
        return BridgeBlockService::renderWidth($widthData);
    }

    /**
     * getChildColumns
     * Retoune une chaine à insérer dans la propriété class d'une balise HTML contenant des sous balises
     * contient les noms de classes css du framework uikit à insérer pour obtenir le colonnage responsive paramétré dans Bridge
     * @param $columsObject : objet columns tel que retourné par l'API Bridge
     * @return string : chaine de classe CSS
     */
    public function getChildColumns($columsObject) {
        return BridgeBlockService::getChildColumns($columsObject);
    }

    /**
     * renderReseauxSociaux
     * Retoune une chaine html de rendu du block réseaux sociaux de fiche
     * @param $fiche : objet product tel que retourné par l'API Bridge
     * @return string : chaine de classe CSS
     */
    public function renderReseauxSociaux($fiche, $blockData) {
        return BridgeBlockService::renderReseauxSociaux($fiche, $blockData);
    }

    /**
     * getProductDispos
     * Interroge l'API Bridge temps réel pour obtenir les infos de disponibilités d'une fiche
     * @param $fiche : objet product tel que retourné par l'API Bridge
     * @return mixed|null : données de dispos sous la forme d'un tableau d'objets
     */
    public function getProductDispos($fiche) {
        return BridgeUtils::getProductDispos($fiche);
    }

    /**
     * getElevationData
     * Interroge l'API Bridge pour obtenir la courbe d'élévation d'une fiche
     * @param $fiche : objet product tel que retourné par l'API Bridge
     * @return mixed|void|null  : données d'altitude sous la forme d'un tableau d'objets
     */
    public function getElevationData($fiche) {
        return BridgeUtils::getElevationData($fiche);
    }

    /**
     * Retoune l'URL de lien vers une fiche à partir de son productCode - tient copte de la langue courante
     * @param $product : objet product  issu de l'API Brodge
     * @return string URL de restination vers la fiche détaillée
     */
    public function getPermalinkFromProduct($product) {
        return BridgeUtils::getPostPermalinkFromProductCode($product->productCode);
    }

    /**
     * Retourne la valeur mini à prendre en compte pour un item de filtre Bridge selon le contexte
     * @param $item objet filter item bridge
     */
    public function getMinValueForfilterItem($item, $moteur, $filter) {

        if (isset($_GET['brflt_'.$moteur->id.'-'.$filter->id.'_min']) && $_GET['brflt_'.$moteur->id.'-'.$filter->id.'_min'] != '')
            $min = $_GET['brflt_'.$moteur->id.'-'.$filter->id.'_min'];
        else
            $min = $item->min;
        return $min;
    }

    /**
     * Retourne la valeur mini à prendre en compte pour un item de filtre Bridge selon le contexte
     * @param $item objet filter item bridge
     */
    public function getMaxValueForfilterItem($item, $moteur, $filter) {
        if (isset($_GET['brflt_'.$moteur->id.'-'.$filter->id.'_max']) && $_GET['brflt_'.$moteur->id.'-'.$filter->id.'_max'] != '')
            $min = $_GET['brflt_'.$moteur->id.'-'.$filter->id.'_max'];
        else
            $min = $item->max;
        return $min;
    }

    /**
     * Retourne une balise <i> d'afichage d'icône font awesome à partir d'un nom d'icône
     * @param $icon nom de l'icône
     * @return string balise <i> pour font-awesome
     */
    public function bridgeRenderIcon($icon) {
        return BridgeBlockService::renderIcon($icon);
    }

    /**
     * getCoupledProducts
     * Retourne un tableau d'objets products des fiches associées à l'objet product passé, enrichis des prioriété photo et shortComment
     * @param $fiche Objet prodct issu de l'API Bridge
     * @return array|false|mixed
     */
    public function getCoupledProducts($fiche) {
        $res = array();
        $coupled = BridgeUtils::getCoupledProducts($fiche);
        if (isset($coupled) && isset($coupled->products) && is_array($coupled->products) && count($coupled->products) > 0) {
            // On récupère les critères photos via un bloc dédié à l'import des photos qu'on définit dans les paramètres du plugin
            for( $i = 0 ; $i < count($coupled->products) ; $i++) {
                $coupled->products[$i]->photo = '';
            }
            $blockPhoto = BridgeUtils::getIdBlockPhotos();
            if (!empty($blockPhoto)) {
                $blockContent = BridgeUtils::callBridgeApi('GET', '/api/blocks/' . $blockPhoto);
                $criteresPhotos = BridgeUtils::extractBlockCriterions(json_decode(json_encode($blockContent)));

                for( $i = 0 ; $i < count($coupled->products) ; $i++) {
                    if (isset($criteresPhotos) && is_array($criteresPhotos) && count($criteresPhotos) > 0) {
                        $testImage = BridgeUtils::filterModalities($coupled->products[$i]->modalities, $criteresPhotos[0]);
                        if (is_array($testImage) && count($testImage) > 0 && isset($testImage[0]->value) && trim($testImage[0]->value) != '') {
                            $photo = "http://" . $testImage[0]->value;
                            $photo = str_replace("http://http://", "http://", $photo);
                            $photo = str_replace("http://https://", "https://", $photo);
                            $coupled->products[$i]->photo = $photo;
                        }
                    }
                    $coupled->products[$i]->shortComment = '';
                    if(!empty($coupled->products[$i]->comment)) {
                        if(mb_strlen($coupled->products[$i]->comment) > 150) {
                            $coupled->products[$i]->shortComment = mb_substr($coupled->products[$i]->comment,0,150) . '...';
                        } else {
                            $coupled->products[$i]->shortComment = $coupled->products[$i]->comment ;
                        }
                    } else {
                        $coupled->products[$i]->shortComment = '';
                    }
                }
            }
            $res = $coupled->products;
        }


        return $res;
    }

    /**
     * extractBlockFirstTextValue
     * Retourne la valeur texte du premier élément trouvé. Sert particulièrement pour extraire les composants de coordonnées
     * @param $blockData Données du block issues de l'API Bridge
     * @return string valeur texte extraite
     */
    public function extractBlockFirstTextValue($blocks_object, $block_name) {
        if(is_object($blocks_object) && property_exists($blocks_object, $block_name)) {
            return BridgeUtils::extractBlockFirstTextValue($blocks_object->$block_name);
        }
        return '';
    }
    /**
     * extractBlockDataFirstTextValue
     * Retourne la valeur texte du premier élément trouvé. Sert particulièrement pour extraire les composants de coordonnées
     * @param $blockData Données du block issues de l'API Bridge
     * @return string valeur texte extraite
     */
    public function extractBlockDataFirstTextValue($blockData) {
        return BridgeBlockService::extractBlockFirstTextValue($blockData);
    }

    /**
     * extractBlockFirstTextValue
     * Retourne la valeur texte du premier élément trouvé. Sert particulièrement pour extraire les composants de coordonnées
     * @param $blockData Données du block issues de l'API Bridge
     * @return string valeur texte extraite
     */
    public function extractBlockFirstItemLabel($blocks_object, $block_name) {
        if(is_object($blocks_object) && property_exists($blocks_object, $block_name)) {
            return BridgeUtils::extractBlockFirstItemLabel($blocks_object->$block_name);
        }
        return '';
    }
    /**
     * Retourne le composant attaché au bloc s'il y en a et chaine vide sinon
     * @param $fiche : objet product retourné par l'API Bridge
     * @param $blockData : donnée block ou tableau blocks (si prop est fournie)
     * @return string nom du composant attaché au bloc ou chaine vide
     */
    public function getBlockComponent($fiche, $blockData, $prop='') {
        return BridgeBlockService::getBlockComponent($fiche, $blockData, $prop);
    }
    /**
     * @param $blocks_object : tableau de blocks retourné dans l'objet product retourné par l'API Bridge
     * @param $block_name : nom de la sous-propriété de l'objet blocks_object
     * @param string $sectionTitleTag Nom de Balise HTML pour les titres de sections
     * @param string $sectionsSeparator Chaine à insérer entre les sections
     * @param string $sectionCSSClass Nom de classe CSS pour les balises de sections
     * @param string $itemsSeparator Chaine à insérer entre les items
     * @param string $itemCSSclass Nom de classe CSS pour les balises d'items
     * @param false $insertItemProps Faut-il insérer les props dans les attributs HTML des champs (itemprop : micro données)
     * @return string Code HTML d'affichage
     */
    public function renderNamedBlock($blocks_object, $block_name, $sectionTitleTag = 'h4', $sectionsSeparator='', $sectionCSSClass = '', $itemsSeparator = '', $itemCSSclass = '', $insertItemProps = false) {
        if(is_object($blocks_object) && property_exists($blocks_object, $block_name)) {
            return BridgeBlockService::renderBlock($blocks_object->$block_name, $sectionTitleTag, $sectionsSeparator, $sectionCSSClass, $itemsSeparator, $itemCSSclass, $insertItemProps);
        }
        return '';
    }

    /**
     * renderBlock
     * Retourne le code HTML d'affichage d'un block Bridge
     * @param $renderBlockData Données du block issues de l'API Bridge
     * @param string $sectionTitleTag Nom de Balise HTML pour les titres de sections
     * @param string $sectionsSeparator Chaine à insérer entre les sections
     * @param string $sectionCSSClass Nom de classe CSS pour les balises de sections
     * @param string $itemsSeparator Chaine à insérer entre les items
     * @param string $itemCSSclass Nom de classe CSS pour les balises d'items
     * @param false $insertItemProps Faut-il insérer les props dans les attributs HTML des champs (itemprop : micro données)
     * @return string Code HTML d'affichage
     */
    public function renderBlock($renderBlockData, $sectionTitleTag = 'h4', $sectionsSeparator='', $sectionCSSClass = '', $itemsSeparator = '', $itemCSSclass = '', $insertItemProps = false) {
        return BridgeBlockService::renderBlock($renderBlockData, $sectionTitleTag, $sectionsSeparator, $sectionCSSClass, $itemsSeparator, $itemCSSclass, $insertItemProps);
    }

    /**
     * hierarchicalBlocks
     * Renvoit un objet structuré permettant l'affichage des blocks personnalisés
     * @param $fiche
     * @return array
     */
    public function prepareCustomBlocksDisplay($fiche) {
        if(isset($fiche) && is_object($fiche) && !empty($fiche->ficheBlocks) && isset($fiche->ficheBlocks->custom) && count($fiche->ficheBlocks->custom) > 0) {

            return BridgeBlockService::prepareCustomBlocksDisplay($fiche, $fiche->ficheBlocks->custom);
        } else {
            return array();
        }
    }

    /**
     * extractPhotos
     * Extrait les infos de photos d'un bloc dédié aux photos
     * @param stdClass $blocks : proprété blocks issue de l'objet product renvoyé par l'API Bridge (parent)
     * @param string $blockName : nom du block
     * @param int $maxPhotos : nom maximum de photos à extraire
     * @return array|string|string[]|null tableau de chaines d'URL de photos
     */
    public function extractPhotos($blocks, $blockName, $maxPhotos = 999) {
        $images = array();
        if(isset($blocks) && is_object($blocks) && isset($blocks->$blockName) && isset($blocks->$blockName->content) && is_object($blocks->$blockName->content) && is_array($blocks->$blockName->content->fields) && !empty($blocks->$blockName->content->fields) && isset($blocks->$blockName->content->fields[0]->items) && is_array($blocks->$blockName->content->fields[0]->items) && !empty($blocks->$blockName->content->fields[0]->items)) {
            foreach($blocks->$blockName->content->fields as $field) {
                foreach ($field->items as $item) {
                    $image = new \stdClass();
                    if(isset($item->subItems) && count($item->subItems) > 0) {
                        foreach($item->subItems as $subItem) {
                            $image->url = $subItem;
                            if (substr(strtolower($image->url), 0, 4) !== 'http') {
                                $image->url = 'http://' . $image->url;
                            }
                            $image->caption = '';
                            if ( isset($item) && is_object($item) && isset($item->extraData) && ! empty($item->extraData) ) {
                                foreach ($item->extraData as $extra)
                                    $image->caption .= $extra->value . ' ';
                            }
                            $images[] = $image;
                        }
                    } else {
                        $image->url = $item->textValue;
                        if (substr(strtolower($image->url), 0, 4) !== 'http') {
                            $image->url = 'http://' . $image->url;
                        }
                        $image->caption = '';
                        if ( isset($item) && is_object($item) && isset($item->extraData) && ! empty($item->extraData) ) {
                            foreach ($item->extraData as $extra)
                                $image->caption .= $extra->value . ' ';
                        }
                        $images[] = $image;
                    }
                }
            }
        }
        return $images;
    }
    /** Extrait les infos de photos d'un bloc dédié aux photos
     * @param stdClass $blockData : objet block issu de l'objet product renvoyé par l'API Bridge (parent)
     * @return array|string|string[]|null tableau de chaines d'URL de photos
     */
    public function extractPhotosFromBlock($blockData) {
        return BridgeBlockService::extractPhotosFromBlock($blockData);
    }

    /**
     * bridgeResizeImage
     * Renvoit l'URL du service qui redimensionne une image dont l'URL originale est passée en paramètre
     * @param $file URL de la photo originale
     * @param string $mode mode de redimensonnement :
     * - ajust pour ajuster aux dimensions passées (ne déborde pas)
     * - remplir : pour remplir le cadre de dimensions passées
     * - deform : pour déformer l'image et forcer les dimensions exactes
     * @param string $selwidth largeur souhaitée
     * @param string $selheight hauteur souhaitée
     * @param string $quality qualité de compresion (sur 100 pour le jpg et de 0 à 9 pour le png)
     * @param int $timeToCache durée de cache navigateur à indiquer dans l'en-tête http
     * @return string l'URL du service qui affiche les données d'image redimentsionnée
     */
    public function bridgeResizeImage($url, $mode = "ajust", $width = "150", $height = "150", $quality = "60", $timeToCache = "1800") {
        if(!empty($url) && strlen($url) >= 4 && strtolower(substr($url, 0, 4)) != 'http')
            $url = 'http://' . $url;
        $baseUrl = \Drupal::request()->getSchemeAndHttpHost();
        return $baseUrl . "/pylot_bridge/resize_image?mode=$mode&selwidth=$width&selheight=$height&quality=$quality&timeToCache=$timeToCache&file=" . urlencode($url);
    }

    /*          FONCTIONS */

    public function bridgeIsObject($var) {
        return is_object($var);
    }
    /**
     * Retourne true si le block nommé ne contient aucun item renseigné
     * @param $fiche : objet product retourné par l'API Bridge
     * @param $blocks : proprété blocks de l'objet product (parent du block qui nous intéresse)
     * @param string $blockName : nom du block
     * @return bool true s'il y a des items dans le block false sinon
     */
    public function itemsBlockIsNotEmpty($fiche, $blocks, $blockName) {
        if(isset($blocks) && isset($blocks->$blockName)) {
            return $this->blockIsNotEmpty($fiche, $blocks->$blockName);
        } else {
            return false;
        }
    }

    /**
     * Retourne true si le block ne contient aucune donnée (item ou gabarit  renseigné)
     * @param $fiche : objet product retourné par l'API Bridge
     * @param $blockData : block à tester (parent du block qui nous intéresse)
     * @return bool true s'il y a des items dans le block false sinon
     */
    public function blockIsNotEmpty($fiche, $blockData) {
        return BridgeBlockService::blockIsNotEmpty($fiche, $blockData);
    }

    /**
     * Retourne true si le block nommé ne contient aucun item texte renseigné
     * @param $fiche : objet product retourné par l'API Bridge
     * @param $blocks : proprété blocks de l'objet product (parent du block qui nous intéresse)
     * @param $blockName : nom du block
     * @return bool true s'il y a des items texte dans le block false sinon
     */
    public function textBlockIsNotEmpty($fiche, $blocks, $blockName) {
        if(isset($blocks->$blockName) && isset($blocks->$blockName->content) && is_object($blocks->$blockName->content) && is_array($blocks->$blockName->content->fields) && !empty($blocks->$blockName->content->fields))
            return true;
        else
            return false;
    }

    /**
     * Retourne true si le block nommé ne contient aucun item texte renseigné
     * @param $fiche : objet product retourné par l'API Bridge
     * @param $blocks : donnée bloc de l'objet product
     * @return bool true s'il y a des items texte dans le block false sinon
     */
    public function textBlockDataIsNotEmpty($fiche, $blockData) {
        return BridgeBlockService::textBlockDataIsNotEmpty($fiche, $blockData);
    }

    /**
     * Retourne un tableau contenant toutes les dates au jour le jour avec un statut et les infos d'horaires correspondant - utilisé pour l'affichage en calendrier
     * @param $fiche : objet product retourné par l'API Bridge
     * @param string $lang : langue d'affichage
     */
    public function getFicheHoursAsArrayForDisplayAsCalendar($fiche, $lang) {
        return DateUtils::getFicheHoursAsArrayForDisplayAsCalendar($fiche, $lang);
    }

    /**
     * Retourne un tableau contenant toutes les périodes et pour chacune tous les jours de la semaine avec les horaires de la fiche
     * @param $fiche : objet product retourné par l'API Bridge
     * @param string $lang : langue d'affichage
     */
    public function getFicheHoursAsArrayForDisplayAsArray($fiche, $lang) {
        return DateUtils::getFicheHoursAsArrayForDisplayAsArray($fiche, $lang);
    }

}