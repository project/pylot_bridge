<?php
namespace Drupal\pylot_bridge\Services ;

/**
 * Utilitaires d'affichage des blocks
 */
class BridgeBlockService
{

    /**
     * Constructs a new BridgeAdminUtils object.
     */
    public function __construct()
    {

    }

    /**
     * Retourne le code HTML de mise en forme d'un bloc nommé
     * @param Object $blocks_object : propriété blocks d'une fiche telle que renvoyées par l'Api Bridge
     * @param String $block_name : Nom de la zone de block à charger
     * @param Array $options : tableau associatif d'options forcées (sectionTitleTag, sectionCSSClass, itemCSSclass)
     * @return string : resultat HTML
     */
    public static function renderNamedBlockHtml($blocks_object, $block_name, $options = array())
    {
        $res = '';
        if (is_object($blocks_object) && property_exists($blocks_object, $block_name)) {
            $renderBlockData = $blocks_object->$block_name;
            $res = self::renderBlockHtml($renderBlockData, $options);
        }
        return trim($res);
    }


    /**
     * @param $blocks_object : tableau de blocks retourné dans l'objet product retourné par l'API Bridge
     * @param $block_name : nom de la sous-propriété de l'objet blocks_object
     * @param string $sectionTitleTag Nom de Balise HTML pour les titres de sections
     * @param string $sectionsSeparator Chaine à insérer entre les sections
     * @param string $sectionCSSClass Nom de classe CSS pour les balises de sections
     * @param string $itemsSeparator Chaine à insérer entre les items
     * @param string $itemCSSclass Nom de classe CSS pour les balises d'items
     * @param false $insertItemProps Faut-il insérer les props dans les attributs HTML des champs (itemprop : micro données)
     * @return string Code HTML d'affichage
     */
    public static function renderNamedBlock($blocks_object, $block_name, $sectionTitleTag = 'h4', $sectionsSeparator = '', $sectionCSSClass = '', $itemsSeparator = '', $itemCSSclass = '', $insertItemProps = false)
    {
        if (is_object($blocks_object) && property_exists($blocks_object, $block_name)) {
            return self::renderBlock($blocks_object->$block_name, $sectionTitleTag, $sectionsSeparator, $sectionCSSClass, $itemsSeparator, $itemCSSclass, $insertItemProps);
        }
        return '';
    }


    /**
     * Retourne le code HTML de mise en forme d'un bloc
     * @param Object $block_data : données de block telles que renvoyées par l'Api Bridge
     * @param Array $options : tableau associatif d'options forcées (sectionTitleTag, sectionCSSClass, itemCSSclass)
     * @return string : resultat HTML
     */
    public static function renderBlockHtml($renderBlockData, $options = array())
    {
        $res = '';
        $sectionTitleTag = 'h4';
        if (isset($options['sectionTitleTag']))
            $sectionTitleTag = $options['sectionTitleTag'];

        $sectionCSSClass = '';
        if (isset($option['sectionCSSClass']))
            $sectionCSSClass = $option['sectionCSSClass'];

        $sectionsSeparator = '';
        if (isset($option['sectionsSeparator']))
            $sectionsSeparator = $option['sectionsSeparator'];

        $itemsSeparator = '';
        if (isset($option['itemsSeparator']))
            $itemsSeparator = $option['itemsSeparator'];

        $insertItemProps = false;
        if (isset($option['insertItemProps']))
            $insertItemProps = $option['insertItemProps'];

        $itemCSSclass = '';
        if (isset($options['itemCSSclass']))
            $itemCSSclass = $options['itemCSSclass'];
        try {
            if (is_object($renderBlockData)) {
                $res = self::renderBlock($renderBlockData, $sectionTitleTag, $sectionsSeparator, $sectionCSSClass, $itemsSeparator, $itemCSSclass, $insertItemProps);
            }
        } catch(\Exception $e) {
            $res = 'Erreur de rendu de block : ' . $e->getMessage() ;
        }
        return trim($res);
    }

    /**
     * Permet d'afficher une icône Bridge issue de la libriarie font-awesome
     * Charge au besoin la libririe font-awesome nécessaire
     * @param $icon Nom de l'icône
     * @param string $lemode Style de l'icône font-awesome (fas, fab, fal, far, fad, fat)
     * @return string code HTML de l'icônedu type <i class="fa far fa-check"></i>
     */
    public static function renderIcon($icon, $lemode = '')
    {

        $modIcon = 'far';
        if (is_object($icon)) {
            if (isset($icon->name) && !empty($icon->name) && isset($icon->type) && isset($icon->prefix) && $icon->type == 'fontawesome') {
                $prefixToMode = array('fas' => 'solid', 'fab' => 'bold', 'fal' => 'light', 'fad' => 'duotone', 'far' => 'regular', 'fat' => 'thin');
                if ($lemode == '' && isset($prefixToMode[$icon->prefix]))
                    $lemode = $prefixToMode[$icon->prefix];
                $icon = $icon->name;
            } elseif (isset($icon->name) && !empty($icon->name)) {
                $icon = $icon->name;
            } else {
                return '';
            }
        }

        if (empty($icon)) return '';

        // BridgeUtils::BridgeEnqueueStyle('fontawesome-v6-fa');
        switch ($lemode):
            case 'solid':
                BridgeUtils::BridgeEnqueueStyle('fontawesome-v6-solid');
                $modIcon = 'fas';
                break;
            case 'bold':
                BridgeUtils::BridgeEnqueueStyle('fontawesome-v6-bold');
                $modIcon = 'fab';
                break;
            case 'light':
                BridgeUtils::BridgeEnqueueStyle('fontawesome-v6-light');
                $modIcon = 'fal';
                break;
            case 'duotone':
                BridgeUtils::BridgeEnqueueStyle('fontawesome-v6-duotone');
                $modIcon = 'fad';
                break;
            case 'thin':
                BridgeUtils::BridgeEnqueueStyle('fontawesome-v6-thin');
                $modIcon = 'fat';
                break;
            default:
                BridgeUtils::BridgeEnqueueStyle('fontawesome-v6-regular');
                $modIcon = 'far';
                break;
        endswitch;
        BridgeUtils::BridgeEnqueueStyle('fontawesome-v5-fa');
        return '<i class="itemIcon '.$modIcon.' fa-'.$icon.'"></i>  ';
    }

    /**
     * renderSizeBlock
     * Retourne un nom de classe CSS à qui s'appliquera au conteneur d'une section de bloc à partir de la prioriété itemsColumns de la section
     * @param $item objet section de block Bridge
     * @return string|void nom de classe à ajouter (frameword uikit)
     */
    public static function renderSizeBlock($item){
        //if(!isset($item)) return;
        $result = '';

        if(isset($item->width) && is_object($item->width)) {
            $result = self::renderWidth($item->width);
        }

        if($result=='')
            $result = ' uk-width-1-1';

        return $result;
    }

    /**
     * renderSizeBlock
     * Retourne un nom de classe CSS à qui s'appliquera au conteneur d'une section de bloc à partir de la prioriété itemsColumns de la section
     * @param renderWidth objet width portant les propriété xl, l, etc
     * @return string|void nom de classe à ajouter (frameword uikit)
     */
    public static function renderWidth($widthObject){
        //if(!isset($item)) return;
        $result = '';

        if(isset($widthObject) && is_object($widthObject)) {
            if (!empty($widthObject->xl) || !empty($widthObject->l) || !empty($widthObject->m) || !empty($widthObject->s)) {
                if (!empty($widthObject->xl))
                    $result .= ' uk-width-' . $widthObject->xl . '@xl ';
                if (!empty($widthObject->l))
                    $result .= ' uk-width-' . $widthObject->l . '@l ';
                if (!empty($widthObject->m))
                    $result .= ' uk-width-' . $widthObject->m . '@m ';
                if (!empty($widthObject->s))
                    $result .= ' uk-width-' . $widthObject->s . '@s';
            } elseif (!empty($widthObject->default)) {
                $result .= ' uk-width-' . $widthObject->default . '';
            }
        }

        if($result=='')
            $result = ' uk-width-1-1';

        return $result;
    }

    /**
     * renderBlockSeparatorVisibility
     * Retourne un nom de classe CSS de visibilité d'un conteneur en fonction de la taille du block qui suit : si 1-1, on affiche le séparateur
     * @param $item objet section de block Bridge
     * @return string|void nom de classe à ajouter (frameword uikit)
     */
    public static function renderBlockSeparatorVisibility($item)
    {
        //if(!isset($item)) return;
        $result = '';

        if (isset($item->width) && is_object($item->width)) {
            if (!empty($item->width->xl) || !empty($item->width->l) || !empty($item->width->m) || !empty($item->width->s)) {
                if (!empty($item->width->xl) && $item->width->xl != '1-1')
                    $result .= ' uk-hidden@xl ';
                if (!empty($item->width->l) && $item->width->l != '1-1')
                    $result .= ' uk-hidden@l ';
                if (!empty($item->width->m) && $item->width->m != '1-1')
                    $result .= ' uk-hidden@m ';
                if (!empty($item->width->s) && $item->width->s != '1-1')
                    $result .= ' uk-hidden@s';
            } elseif (!empty($item->width->default) && $item->width->default != '1-1') {
                $result .= ' uk-hidden';
            }
        }

        if ($result == '')
            $result = ' uk-visible';

        return $result;
    }

    /**
     * renderColumnsChild
     * Retourne un nom de classe CSS à qui s'appliquera aux éléments enfants d'une section de bloc à partir de la prioriété itemsColumns de la section
     * @param $item objet section de block Bridge
     * @return string|void nom de classe à ajouter (frameword uikit)
     */
    public static function renderColumnsChild($item)
    {
        if (!isset($item)) return;
        $result = '';

        if (isset($item->itemsColumns) && is_object($item->itemsColumns)) :
            if (!empty($item->itemsColumns->xl))
                $result .= ' uk-child-width-1-' . $item->itemsColumns->xl . '@xl ';
            if (!empty($item->itemsColumns->l))
                $result .= ' uk-child-width-1-' . $item->itemsColumns->l . '@l ';
            if (!empty($item->itemsColumns->m))
                $result .= ' uk-child-width-1-' . $item->itemsColumns->m . '@m ';
            if (!empty($item->itemsColumns->s))
                $result .= ' uk-child-width-1-' . $item->itemsColumns->s . '@s';
            if (!empty($item->itemsColumns->default))
                $result .= ' uk-child-width-1-' . $item->itemsColumns->default . '';
        endif;

        if ($result == '')
            $result = ' uk-child-width-1-1';

        return $result;
    }

    /**
     * getChildColumns
     * Retourne le nom d'une classe CSS uikit permettant d'afficher les noeuds d'un objet columns de moteur avec le nombre de colonnes  paramétré
     * @param object $object objet de propriété columns
     * @return string propriété de classe CSS générée
     */
    public static function getChildColumns($object)
    {
        $res = "";
        if ($object->xl):
            $res .= " uk-child-width-1-" . $object->xl . "@xl";
        endif;
        if ($object->l):
            $res .= " uk-child-width-1-" . $object->l . "@l";
        endif;
        if ($object->m):
            $res .= " uk-child-width-1-" . $object->m . "@m";
        endif;
        if ($object->s):
            $res .= " uk-child-width-1-" . $object->s . "@s";
        endif;
        if ($object->default):
            $res .= " uk-child-width-1-" . $object->default;
        endif;

        return $res;
    }

    /**
     * getMicroData
     * Personnalisation des micro data pour l'accessibilité et le référencement
     * Retourne l'atribut HTML itemprop adéquat à partir d'un item de block
     * @param object $type propriété type de l'item : passer $item->type
     * @return string|void code à insérer dans la balise HTML où est insérée l'item
     */
    public static function getMicroData($type)
    {
        $res = '';
        switch ($type):
            case 'pro.streetName':
                $res = 'itemprop="streetAddress"';
                break;
            case 'pro.zipCode':
                $res = 'itemprop="postalCode"';
                break;
            case 'pro.city':
                $res = 'itemprop="addressLocality"';
                break;
            case 'pro.phone':
                $res = 'itemprop="telephone"';
                break;
            case 'pro.email':
            case 'pro.url':
                $res = 'itemprop="' . explode('.', $type)[1] . '"';
                break;
            default:
                $res = '';
                break;
        endswitch;
        return $res;
    }


    /**
     * renderBlock
     * Retourne le code HTML d'affichage d'un block Bridge (hors composants)
     * @param $renderBlockData Données du block issues de l'API Bridge
     * @param string $sectionTitleTag Nom de Balise HTML pour les titres de sections
     * @param string $sectionsSeparator Chaine à insérer entre les sections
     * @param string $sectionCSSClass Nom de classe CSS pour les balises de sections
     * @param string $itemsSeparator Chaine à insérer entre les items
     * @param string $itemCSSclass Nom de classe CSS pour les balises d'items
     * @param false $insertItemProps Faut-il insérer les props dans les attributs HTML des champs (itemprop : micro données)
     * @return string Code HTML d'affichage
     */
    public static function renderBlock($renderBlockData, $sectionTitleTag = 'h4', $sectionsSeparator = '', $sectionCSSClass = '', $itemsSeparator = '', $itemCSSclass = '', $insertItemProps = false)
    {
        $render = '';
        // Gestion de l'affichage du block
        if (isset($renderBlockData) && is_object($renderBlockData->content) && !empty($renderBlockData->content->fields)) { // Si pas de composant, on affiche le bloc s'il n'est pas vide
            $mode = 'inline';

            // Boucle des sections
            foreach ($renderBlockData->content->fields as $index => $section) {
                // On initialiser le conteneur
                $containerStart = '<div class="containerblockSection ' . BridgeBlockService::renderSizeBlock($section) . ' ' . $section->CSSClass . ' ' . $sectionCSSClass . '">';
                $render .= $containerStart;

                if ($index > 0) $render .= $sectionsSeparator;
                // Définition du système de la boucle
                switch ($section->itemsDisplayType) {
                    case 'ARRAY':
                        $mode = 'tableau';
                        break;
                    case 'TAGS':
                        $mode = 'tags';
                        break;
                    case 'COLUMNS':
                        $mode = 'column';
                        break;
                    default:
                        $mode = 'inline';
                        if (isset($section->itemsSeparator) && $index > 1) $render .= $section->itemsSeparator;
                        break;
                }

                $render .= ($section->label != '') ? '<' . $sectionTitleTag . '>' . BridgeBlockService::renderIcon($section->icon) . $section->label . '</' . $sectionTitleTag . '>' : '';

                // Initilisation des variables de rendus
                $titleItems = '';
                $resSubItem = '';
                $resSubItemLabel = '';
                $cmpSubItem = '';
                foreach ($section->items as $indexItem => $item) {
                    $resSubItemLabel = ''; // Patch 08/06/22 : double apparition de label
                    $itemIsEmpty = true;
                    $rendericon = '';
                    $itemIcon = '';
                    $itemProp = '';
                    if ($insertItemProps)
                        $itemProp = self::getMicroData($item->type);

                    if ($mode == 'tableau' || $mode == 'tags'):
                        $resSubItem .= '';
                    elseif ($mode == 'column'):
                        $resSubItem .= '';
                    else :
                        $resSubItem .= '<span ' . $itemProp . ' class="blockItem ' . $itemCSSclass . '">';
                    endif;

                    // On récupère l'icon du titre
                    if (isset($item->iconClass) && is_object($item->iconClass) && !empty($item->iconClass->name)):
                        $itemIcon = BridgeBlockService::renderIcon($item->iconClass);
                    endif;
                    // On récupère l'icon des éléments enfants qu'on affichera en mode column ou tags
                    if (isset($section->itemsIcons->name) && $section->itemsIcons->name != ''):
                        $rendericon = BridgeBlockService::renderIcon($section->itemsIcons);
                    endif;

                    // Si l'élément inclu une image on l'ajoute
                    if (isset($item->image) && $item->image != ''):
                        $imageTitle = '';
                        if (isset($item->imageTitle) && !empty($item->imageTitle))
                            $imageTitle = $item->imageTitle;
                        if (empty($urlBridge))
                            $urlBridge = BridgeUtils::getUrlBridge();
                        $urlImg = $urlBridge . '/media/' . $item->image;
                        if (BridgeBlockService::bridgeIsSvg($urlImg))
                            $rendericon = BridgeBlockService::bridgeInlineSvg($urlImg, $imageTitle);
                        else
                            $rendericon = '<img src="' . $urlBridge . '/media/' . $item->image . '" height="30" alt="' . htmlspecialchars($imageTitle) . '" title="' . htmlspecialchars($imageTitle) . '" uk-tooltip="' . htmlspecialchars($imageTitle) . '" />';
                    else :
                        if (!empty($itemIcon)):
                            $rendericon = $itemIcon;
                        endif;
                        if (!empty($item->label)):
                            $resSubItemLabel = '<span class="itemLabel">' . $item->label . '</span>';
                        endif;
                    endif;

                    // On va adapter l'affichage suivant le mode de bloc sélectionné
                    $textValue = '';
                    if (property_exists($item, 'textValue') && !empty($item->textValue)) {
                        $textValue = $item->textValue;
                    }

                    switch ($mode) {
                        case 'tableau':
                            foreach ($item->subItems as $subitem):
                                $subitemTab = explode(' : ', $subitem);
                                if (is_array($subitemTab) && count($subitemTab) > 0) {
                                    $itemIsEmpty = false;
                                    $resSubItem .= '<tr><td class="' . $itemCSSclass . '">' . $subitemTab[0] . '</td>';
                                    if (count($subitemTab) > 1) {
                                        $itemIsEmpty = false;
                                        $resSubItem .= '<td class="' . $itemCSSclass . '">' . $subitemTab[1] . '</td>';
                                    } /*else {
                                    $resSubItem .= '<td></td>';
                                } */
                                    $resSubItem .= '</tr>';
                                } else {
                                    $itemIsEmpty = false;
                                    $resSubItem .= '<tr><td class="' . $itemCSSclass . '">' . $subitem . '</td></tr>';
                                }

                            endforeach;
                            break;
                        case 'tags':
                            if (isset($item->subItems) && count($item->subItems) > 1) {
                                foreach ($item->subItems as $subitem) {
                                    $itemIsEmpty = false;
                                    $resSubItem .= '<div class="uk-tile uk-tile-muted uk-padding-small uk-margin-small-right uk-margin-small-bottom ' . $itemCSSclass . '">' . $rendericon . ' ' . $resSubItemLabel . ' ' . $subitem . '</div>';
                                }
                            } elseif ($textValue != '' || $rendericon != '') {
                                $itemIsEmpty = false;
                                $resSubItem .= '<div class="uk-tile uk-tile-muted uk-padding-small uk-margin-small-right uk-margin-small-bottom ' . $itemCSSclass . '">' . $rendericon . ' ' . $resSubItemLabel . ' ' . $item->textValue . '</div>';
                            }
                            break;
                        case 'column':
                            if (isset($item->subItems) && count($item->subItems) > 1 && $mode == 'column') {
                                foreach ($item->subItems as $subitem) {
                                    $itemIsEmpty = false;
                                    $resSubItem .= '<span class="itemTextValue ' . $itemCSSclass . '">' . $rendericon . ' ' . $resSubItemLabel . ' ' . $subitem . '</span>';
                                }
                            } elseif ($textValue != '' || $rendericon != '') {
                                $itemIsEmpty = false;
                                $resSubItem .= '<span ' . $itemProp . ' class="itemTextValue ' . $itemCSSclass . '">' . $rendericon . ' ' . $resSubItemLabel . ' ' . $textValue . '</span>';
                            } elseif ($resSubItemLabel != '') {
                                $itemIsEmpty = false;
                                $resSubItem .= '<span ' . $itemProp . ' class="itemTextValue ' . $itemCSSclass . '">' . $rendericon . ' ' . $resSubItemLabel . ' ' . $textValue . '</span>';
                            }
                            break;
                        default:
                            $itemIsEmpty = false;
                            $resSubItem .= '<span ' . $itemProp . ' class="itemTextValue">' ;
                            if(!empty($rendericon))
                                $resSubItem .= $rendericon . ' ';
                            if(!empty($resSubItemLabel))
                                $resSubItem .= $resSubItemLabel;
                            if(!empty($resSubItemLabel) && !empty($textValue))
                                $resSubItem .= ' ';

                            // patch 28/09/22 pour éviter un espace après le suffixe "couverts" mais je ne suis pas sûr que ce soit bien
                            $resSubItem .=  trim($textValue) . '</span>';
                            // $resSubItem .=  $textValue . '</span>';
                            // $resSubItem .= '<span ' . $itemProp . ' class="itemTextValue">' . $rendericon . ' ' . $resSubItemLabel . ' ' . $textValue . '</span>';

                    }

                    if ($mode == 'tableau' || $mode == 'tags'):
                        $resSubItem .= '';
                    elseif ($mode == 'column'):
                        $resSubItem .= ''; // '</div></div>';
                    else :
                        $resSubItem .= '</span>';
                    endif;

                    // Patch 23/04/2022 : on ne met le séparateur que si les items sont renseignés et qu'on est en mode inline
                    if ($mode == 'inline' && $itemIsEmpty === false) {
                        $resSubItem .= (isset($section->itemsSeparator) && $indexItem < count($section->items) - 1 && $section->itemsSeparator != '') ? '<span class="itemSeparator">' . $section->itemsSeparator . '</span>' : '';
                        $resSubItem .= ($indexItem < count($section->items) - 1) ? $itemsSeparator : '';
                    }

                } // Fin des items

                switch ($mode):
                    case 'tableau':
                        $render .= '<table class="uk-table blockSection">' . $resSubItem . '</table>';
                        break;
                    case 'column':
                        $render .= '<div uk-grid class="uk-grid-small blockSection ' . BridgeBlockService::renderColumnsChild($section) . '">' . $resSubItem . '</div>';
                        break;
                    case 'tags':
                        $render .= '<div class="uk-flex uk-flex-wrap uk-flex-left blockSection">' . $resSubItem . '</div>';
                        break;
                    case 'inline':
                        $render .= '<div class="uk-display-inline-block blockSection">' . $resSubItem . '</div>';
                        break;
                    default:
                        $render .= $resSubItem;
                        break;
                endswitch;

                // On termine le conteneur d'une section
                $containerEnd = '</div>';
                $render .= $containerEnd;

            } // Fin boucle des sections
        } // Fin if test sur contenu du block

        return $render;
    }

    /**
     * extractBlockFirstItemLabel
     * Retourne le premier label d'item d'un block
     * @param $blockData objet block
     * @return false|mixed chaine de label trouvé ou false si non trouvé
     */
    public static function extractBlockFirstItemLabel($blockData)
    {
        $res = false;

        if (!isset($blockData->content->fields) || count($blockData->content->fields) == 0) {
            return $res;
        }
        foreach ($blockData->content->fields as $field) {
            if (count($field->items) == 0)
                continue;
            elseif (isset($field->items[0]->label) && $field->items[0]->label != '')
                return $field->items[0]->label;
        }
        return $res;
    }

    /**
     * extractBlockFirstTextValue
     * Extrait la première valeur texte d'un block
     * @param $blockData objet block
     * @return false|mixed false si non trouvé ou chaine trouvée
     */
    public static function extractBlockFirstTextValue($blockData)
    {
        $res = false;

        if (!isset($blockData->content->fields) || count($blockData->content->fields) == 0) {
            return $res;
        }
        foreach ($blockData->content->fields as $field) {
            if (count($field->items) == 0)
                continue;
            elseif (isset($field->items[0]->textValue) && $field->items[0]->textValue != '')
                return $field->items[0]->textValue;
        }
        return $res;
    }

    /**
     * extractBlockFirstTextValue
     * Retourne la valeur texte du premier élément trouvé. Sert particulièrement pour extraire les composants de coordonnées
     * @param $blockData Données du block issues de l'API Bridge
     * @return string valeur texte extraite
     */
    public static function extractNamedBlockFirstTextValue($blocks_object, $block_name)
    {
        if (is_object($blocks_object) && property_exists($blocks_object, $block_name)) {
            return self::extractBlockFirstTextValue($blocks_object->$block_name);
        }
        return '';

    }

    /**
     * extractBlockFirstTextValue
     * Retourne la valeur texte du premier élément trouvé. Sert particulièrement pour extraire les coordonnées
     * @param $blockData Données du block issues de l'API Bridge
     * @return string valeur texte extraite
     */
    public static function extractNamedBlockFirstItemLabel($blocks_object, $block_name)
    {
        if (is_object($blocks_object) && property_exists($blocks_object, $block_name)) {
            return self::extractBlockFirstItemLabel($blocks_object->$block_name);
        }
        return '';
    }

    /**
     * extractPhotos
     * Extrait les infos de photos d'un bloc dédié aux photos
     * @param stdClass $blocks : proprété blocks issue de l'objet product renvoyé par l'API Bridge (parent)
     * @param string $blockName : nom du block
     * @param int $maxPhotos : nom maximum de photos à extraire
     * @return array|string|string[]|null tableau de chaines d'URL de photos
     */
    public static function extractPhotos($blocks, $blockName, $maxPhotos = 999)
    {
        $images = array();
        if (isset($blocks) && is_object($blocks) && isset($blocks->$blockName) && isset($blocks->$blockName->content) && is_object($blocks->$blockName->content) && is_array($blocks->$blockName->content->fields) && !empty($blocks->$blockName->content->fields) && isset($blocks->$blockName->content->fields[0]->items) && is_array($blocks->$blockName->content->fields[0]->items) && !empty($blocks->$blockName->content->fields[0]->items)) {
            $images = self::extractPhotosFromBlock($blocks->$blockName, $maxPhotos);
        }
        return $images;
    }

    /**
     * extractPhotos
     * Extrait les infos de photos d'un bloc dédié aux photos
     * @param stdClass $blockData : objet block issu de l'objet product renvoyé par l'API Bridge (parent)
     * @param int $maxPhotos : nom maximum de photos à extraire
     * @return array|string|string[]|null tableau de chaines d'URL de photos
     */
    public static function extractPhotosFromBlock($blockData, $maxPhotos = 999)
    {
        $nbPhotos = 0;
        $images = array();
        if (isset($blockData) && is_object($blockData) && isset($blockData->content) && is_object($blockData->content) && is_array($blockData->content->fields) && !empty($blockData->content->fields) && isset($blockData->content->fields[0]->items) && is_array($blockData->content->fields[0]->items) && !empty($blockData->content->fields[0]->items)) {
            foreach ($blockData->content->fields as $field) {
                foreach ($field->items as $item) {
                    $urls = array();
                    if (isset($item->subItems) && is_array($item->subItems) && count($item->subItems) > 0) {
                        foreach ($item->subItems as $subItem)
                            $urls[] = $subItem;
                    } else {
                        $urls[] = $item->textValue;
                    }
                    foreach ($urls as $url) {
                        $image = new \stdClass();
                        $image->url = $url;
                        if (substr(strtolower($image->url), 0, 4) !== 'http') {
                            $image->url = 'http://' . $image->url;
                        }
                        $image->caption = '';
                        if (isset($item) && is_object($item) && isset($item->extraData) && !empty($item->extraData)) {
                            foreach ($item->extraData as $extra)
                                $image->caption .= $extra->value . ' ';
                        }
                        $images[] = $image;
                        $nbPhotos++;
                        if ($nbPhotos >= $maxPhotos) {
                            return $images;
                        }
                    }
                }
            }
        }
        return $images;
    }

    /**
     * Utilitaire qui renvoit un tableau hierarchisé en vue de l'affichage des blocks personnalisés dans les fiches
     * Met les bloks et groupes de premier niveau ensemble pour pouvoir les afficher de la même manière dans les titres et la sous-navigation
     * @param $fiche Objet product renvoyé par l'API Bridge
     * @param $blocks objets tableau d'objets block ($fiche->blocks->cutsom)
     * @return array tableau avec menus de premier niveau et contenu : [{ blockId:ID, title:TITRE, icon: ICONE, items:[{title:TITRE, html: HTML, component:NOM, componentParams:{}}}]}]
     */
    public static function prepareCustomBlocksDisplay($fiche, $blocks)
    {
        $hierarchicalBlocks = array();
        $previousGroupId = -1;
        $previousBlockId = -1;
        $previousObgletId = -1;
        $tmp = new \stdClass();
        $tmp->group = "";
        $tmp->items = array();
        $isFlushed = true;
        if (isset($blocks) && count($blocks) > 0) {
            foreach ($blocks as $blockData) {
                if (self::blockIsNotEmpty($fiche, $blockData)) {
                    $icon = '';
                    // nouveau groupe
                    if (isset($blockData->wpGroup) && is_object($blockData->wpGroup) && (!empty($blockData->wpGroup->label) || !empty($blockData->wpGroup->wpGroupIcon)) && $previousGroupId != $blockData->wpGroup->id) {
                        if (!$isFlushed) { // Si on passe d'un groupe à un autre c'est là qu'on insère
                            $hierarchicalBlocks[] = $tmp;
                            $tmp = new \stdClass();
                            $tmp->items = array();
                        }
                        if (!empty($blockData->wpGroupIcon)) {
                            $icon = BridgeBlockService::renderIcon($blockData->wpGroupIcon);
                        }
                        $tmp->group = 1;
                        $tmp->title = $icon . $blockData->wpGroup->label;
                        $tmp->class = $blockData->wpGroup->cssClass . ' ' . BridgeBlockService::renderSizeBlock($blockData->wpGroup);
                        $tmp->separatorClass = BridgeBlockService::renderBlockSeparatorVisibility($blockData->wpGroup);
                        $tmp->blockId = $blockData->blockId;
                    }

                    // On prépare le contenu de l'onglet
                    $item = new \stdClass();
                    $item->title = '';
                    $item->class = '';
                    // S'il y a un composant, il a priorité
                    // TODO : gérer les paramètres
                    if (isset($blockData) && isset($blockData->isGabarit) && $blockData->isGabarit && !empty($blockData->bridgeComponent)) {
                        $item->component = $blockData->bridgeComponent;
                        $item->componentParams = null;
                        $item->html = '';
                        $item->class = $blockData->cssClass . ' ' . BridgeBlockService::renderSizeBlock($blockData);
                        $item->separatorClass = BridgeBlockService::renderBlockSeparatorVisibility($blockData);
                        $item->blockData = $blockData;
                        $item->blockId = $blockData->blockId;
                    } else {
                        $item->component = '';
                        $item->componentParams = null;
                        $item->class = $blockData->cssClass . ' ' . BridgeBlockService::renderSizeBlock($blockData);
                        $item->separatorClass = BridgeBlockService::renderBlockSeparatorVisibility($blockData);
                        $item->html = self::renderBlockHtml($blockData);
                        $item->blockData = $blockData;
                        $item->blockId = $blockData->blockId;
                    }

                    if (!empty($blockData->blockLabel) || !empty($blockData->blockIcon)) {
                        if (!empty($blockData->blockIcon))
                            $icon = BridgeBlockService::renderIcon($blockData->blockIcon);
                        $item->title = $icon . $blockData->blockLabel;
                    }

                    // Si le block n'est pas dans un groupe, on remonte son label en niveau 1
                    if (!isset($blockData->wpGroup) || !is_object($blockData->wpGroup) || (empty($blockData->wpGroup->label) && empty($blockData->wpGroup->wpGroupIcon))) {
                        if (!$isFlushed) { // Si on passe d'un groupe à un autre c'est là qu'on insère
                            $hierarchicalBlocks[] = $tmp;
                            $tmp = new \stdClass();
                            $tmp->items = array();
                        }
                        $tmp->title = $icon . $blockData->blockLabel;
                        $tmp->blockId = $blockData->blockId;

                    }

                    $tmp->items[] = $item;
                    $isFlushed = false; // A-t-on ajouté $tmp à $hierarchicalBlocks ?

                    // intitulé de groupe
                    if (isset($blockData->wpGroup) && is_object($blockData->wpGroup) && (!empty($blockData->wpGroup->label) || !empty($blockData->wpGroup->wpGroupIcon))) {
                        $previousGroupId = $blockData->wpGroup->id;
                    } else {
                        $hierarchicalBlocks[] = $tmp;
                        $tmp = new \stdClass();
                        $tmp->group = '';
                        $tmp->items = array();
                        $isFlushed = true;
                        $previousGroupId = 0;
                    }

                }
            }
            if (!$isFlushed && count($tmp->items) > 0) { // Si on passe d'un groupe à un autre c'est là qu'on insère
                $hierarchicalBlocks[] = $tmp;
            }
        }

        return $hierarchicalBlocks;
    }

    /**
     * Retourne le composant attaché au bloc s'il y en a et chaine vide sinon
     * @param $fiche : objet product retourné par l'API Bridge
     * @param $blockData : donnée block ou tableau blocks (si prop est fournie)
     * @return string nom du composant attaché au bloc ou chaine vide
     */
    public static function getBlockComponent($fiche, $blockData, $prop = '')
    {
        if ($prop === '') {
            // On passe directement une donnée blockData
            if (isset($blockData) && isset($blockData->isGabarit) && $blockData->isGabarit === true && !empty($blockData->bridgeComponent))
                return $blockData->bridgeComponent;
            else
                return '';
        } else {
            if (isset($blockData) && isset($blockData->$prop) && isset($blockData->$prop->isGabarit) && $blockData->$prop->isGabarit === true && !empty($blockData->$prop->bridgeComponent))
                return $blockData->$prop->bridgeComponent;
            else
                return '';
        }
        return '';
    }

    /**
     * Retourne true si le block ne contient aucun item renseigné
     * @param $fiche : objet product retourné par l'API Bridge
     * @param $blockData : block à tester (parent du block qui nous intéresse)
     * @return bool true s'il y a des items dans le block false sinon
     */
    public static function blockIsNotEmpty($fiche, $blockData)
    {
        $testerLeContenu = true;
        // S'il y a un composant, il a priorité
        if (isset($blockData) && isset($blockData->isGabarit) && $blockData->isGabarit && !empty($blockData->bridgeComponent)) {
            $testerLeContenu = false;
            switch ($blockData->bridgeComponent) {
                case 'carte_interactive':
                case 'carte_position':
                    return (!empty($fiche->latitude) && !empty($fiche->longitude));
                    break;
                case 'dates_calendrier':
                case 'dates_tableau':
                    return (!empty($fiche->hours));
                    break;
                case 'dispos_calendrier':
                case 'dispos_tableau':
                case 'fiches_a_proximite':
                    return true;
                case 'fiches_associees':
                    return (!empty($fiche->coupled));
                    break;
                default:
                    // Par défaut, on teste le contenu du block attaché s'il y en a un
                    if (isset($blockData) && isset($blockData->content))
                        $testerLeContenu = true;
                    break;
            }
        }
        // bypass du test du contenu pour éviter de perdre des gabarits spéciaux inconnus
        if (!$testerLeContenu)
            return true;
        if (isset($blockData) && isset($blockData->content) && is_object($blockData->content) && is_array($blockData->content->fields) && !empty($blockData->content->fields)) {
            $notEmpty = false;
            foreach ($blockData->content->fields as $field) {
                if (isset($field->items) && is_array($field->items) && !empty($field->items)) {
                    $notEmpty = true;
                    break;
                }
            }
            return $notEmpty;
        } else {
            return false;
        }
    }

    /**
     * Retourne true si le block nommé ne contient aucun item renseigné
     * @param $fiche : objet product retourné par l'API Bridge
     * @param $blocks : proprété blocks de l'objet product (parent du block qui nous intéresse)
     * @param string $blockName : nom du block
     * @return bool true s'il y a des items dans le block false sinon
     */
    public static function itemsBlockIsNotEmpty($fiche, $blocks, $blockName)
    {
        if (isset($blocks) && isset($blocks->$blockName)) {
            return self::blockIsNotEmpty($fiche, $blocks->$blockName);
        } else {
            return false;
        }
    }


    /**
     * Retourne true si le block nommé ne contient aucun item texte renseigné
     * @param $fiche : objet product retourné par l'API Bridge
     * @param $blocks : proprété blocks de l'objet product (parent du block qui nous intéresse)
     * @param $blockName : nom du block
     * @return bool true s'il y a des items texte dans le block false sinon
     */
    public static function textBlockIsNotEmpty($fiche, $blocks, $blockName)
    {
        if (isset($blocks->$blockName) && isset($blocks->$blockName->content) && is_object($blocks->$blockName->content) && is_array($blocks->$blockName->content->fields) && !empty($blocks->$blockName->content->fields))
            return true;
        else
            return false;
    }

    /**
     * Retourne true si le block ne contient aucun item texte renseigné
     * @param $fiche : objet product retourné par l'API Bridge
     * @param $blockData : objet block de l'objet product (parent du block qui nous intéresse)
     * @return bool true s'il y a des items texte dans le block false sinon
     */
    public static function textBlockDataIsNotEmpty($fiche, $blockData)
    {
        if (isset($blockData) && isset($blockData->content) && is_object($blockData->content) && is_array($blockData->content->fields) && !empty($blockData->content->fields))
            return true;
        else
            return false;
    }

    /**
     * getGmapLink
     * Retourne une URL de lien d'itinéraire Google Maps
     * @param $fiche : objet product tel que renvoyé par l'API Bridge
     * @return string URL d'itinéraire Google Maps vers la fiche
     */
    public static function getGmapLink($fiche)
    {
        $address = '';
        $address2 = '';
        $gmapLink = '';
        // Si pas d'adresse mais des coordonnées GPS, on va préférer celles-ci
        // Si pas d'adresse mais des coordonnées GPS, on va préférer celles-ci
        if ((!isset($fiche->streetName) || empty($fiche->streetName) && !empty($fiche->latitude) && !empty($fiche->longitude))) {
            $address = $fiche->latitude . ',' . $fiche->longitude;
        } else {
            if (isset($fiche->streetNumber) && !empty($fiche->streetNumber)) {
                $address .= urlencode($fiche->streetNumber);
            }
            if (isset($fiche->streetName) && !empty($fiche->streetName)) {
                $address .= ' ' . urlencode($fiche->streetName);
            }
            if (isset($fiche->additionnalAddress) && !empty($fiche->additionnalAddress)) {
                $address .= ' ' . urlencode($fiche->additionnalAddress);
            }
            if (isset($fiche->zipCode) && !empty($fiche->zipCode)) {
                $address .= ' ' . urlencode($fiche->zipCode);
            }
            if (isset($fiche->city) && !empty($fiche->city)) {
                $address .= ' ' . urlencode($fiche->city);
            }
            if (isset($fiche->country) && !empty($fiche->country)) {
                $address .= ' ' . urlencode($fiche->country);
            }
        }

        // Patch CH - Changement du lien qui renvoyait vers Place par dir pour l'itinéraire
        // 19.10.22 patch ND : le double / ets fait exprès pour que la géoloclisation se fasse automatiquement
        if (!empty($address))
            $gmapLink = 'https://www.google.com/maps/dir//' . str_replace(' ', '+', $address);

        return $gmapLink;
    }

    /**
     * Retourne le contneu svg de l'image
     * @param String $prop propriété de l'objet qu'on souhaite récupérer
     * @param Object $fiche objet product
     * @param int $criterionCode code de critère
     * @param int|null $modalityCode code de modalité
     * @return string le résultat sous forme de chaine de caractères (réduit le volume de code de l'appel)
     */
    public static function bridgeInlineSvg(string $urlImage, $imageTitle = '')
    {
        try {
            $urlImage = str_replace(" ", "%20", $urlImage);
            $rawData = file_get_contents($urlImage);
            // On doit supprimer les balises cliPath car elles ne sont pas supportées par wkhtmltopdf
            // Et le picto disparait
            $doc = new \DOMDocument;
            $doc->loadXML($rawData);
            $thedocument = $doc->documentElement;
            $list = $thedocument->getElementsByTagName('clipPath');
            foreach ($list as $domElement) {
                try {
                    $domElement->parentNode->removeChild($domElement);
                } catch (\Exception $e) {
                    return "ERR_SVG : " . $e->getMessage();
                }
                /* code gardé sous le coude si on doit tester un attribut
                $attrValue = $domElement->getAttribute('time');
                if ($attrValue == 'VALUEYOUCAREABOUT') {
                    $nodeToRemove = $domElement; //will only remember last one- but this is just an example :)
                }
                */
            }
            // On injecte un attribut title
            if (is_object($thedocument) && !empty($imageTitle)) {
                $thedocument->setAttribute('role', "img");

                $node = $doc->createElement("title");

                if(isset($thedocument->firstChild)) {
                    $firstChild = $thedocument->firstChild;
                    $node = $thedocument->insertBefore($node, $firstChild);
                    $node->appendChild(new \DOMText($imageTitle));
                    // $thedocument->setAttribute("title",$imageTitle);
                    $thedocument->setAttribute("uk-tooltip",$imageTitle);
                }
            }

            return $doc->saveXML($thedocument);
            // return $xmlData->asXML();
        } catch (\Exception $e) {
            return "ERR_SVG";
        }
    }

    /**
     * Retourne true si l'url d'image mène vers une image svg
     * @param String $urlImage url de l'image dnt on souhaite savoir si c'est du SVG
     * @return bool true si l'image est svg
     */
    public static function bridgeIsSvg(string $urlImage)
    {
        if (substr(strtolower($urlImage), -3) == 'svg')
            return true;
        else
            return false;
    }

    public static function renderReseauxSociaux($fiche, $blockData)
    {
        $res = '';
        if (BridgeBlockService::itemsBlockIsNotEmpty($fiche, $fiche->ficheBlocks, "reseaux-sociaux")) {
            $res .= '<div class="reseaux uk-text-center ' . (string)$blockData->cssClass . '">';
            $res .= '<div class="" uk-grid>';

            $blocks_object = $fiche->ficheBlocks;
            $block_name = "reseaux-sociaux";
            if (is_object($blocks_object) && property_exists($blocks_object, $block_name)) {
                $blockData = $blocks_object->$block_name;
                if (isset($blockData) && is_object($blockData->content) && !empty($blockData->content->fields)) {
                    foreach ($blockData->content->fields as $index => $section) {
                        foreach ($section->items as $indexItem => $item) {
                            // On récupère l'icon du titre
                            if (isset($item->iconClass) && is_object($item->iconClass) && !empty($item->iconClass->name))
                                $itemIcon = self::renderIcon($item->iconClass);
                            // On récupère l'icon des éléments enfants qu'on affichera en mode column ou tags
                            if (isset($section->itemsIcons->name) && $section->itemsIcons->name != '')
                                $rendericon = self::renderIcon($section->itemsIcons);

                            // Si l'élément inclu une image on l'ajoute
                            if (isset($item->image) && $item->image != '') {
                                $urlBridge = BridgeUtils::getUrlBridge();
                                $urlImg = $urlBridge . '/media/' . $item->image;
                                if (self::bridgeIsSvg($urlImg))
                                    $rendericon = self::bridgeInlineSvg($urlImg);
                                else
                                    $rendericon = '<img src="' . $urlBridge . '/media/' . $item->image . '" height="30" alt="' . $item->label . '" title="' . $item->label . '" uk-tooltip />';
                            } else {
                                if (!empty($itemIcon))
                                    $rendericon = $itemIcon;
                                if (!empty($item->label))
                                    $resSubItemLabel = '<span class="itemLabel"> ' . $item->label . '</span>';
                            }

                            if (isset($item->textValue)) {
                                $res .= '<div>';
                                $link = str_replace('http://https://', 'https://', 'http://' . $item->textValue);
                                $link = str_replace('http://http://', 'http://', 'http://' . $link);

                                $res .= '<a href="' . $link . '" target="_blank">';
                                $res .= $rendericon;
                                $res .= '</a>';
                                $res .= '</div>';
                            }
                        }
                    }

                }
            }

            $res .= '</div>';
            $res .= '</div>';

        }
        return $res;
    }

    /**
     * Retourne la distance en Km entre deux points d'après leurs coordonnées GPS
     * @param $lat1
     * @param $lng1
     * @param $lat2
     * @param $lng2
     * @param false $miles
     * @return float|int
     */
    public static function distance($lat1, $lng1, $lat2, $lng2, $miles = false)
    {
        $pi80 = M_PI / 180;
        $lat1 *= $pi80;
        $lng1 *= $pi80;
        $lat2 *= $pi80;
        $lng2 *= $pi80;

        $r = 6372.797; // rayon moyen de la Terre en km
        $dlat = $lat2 - $lat1;
        $dlng = $lng2 - $lng1;
        $a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin(
                $dlng / 2) * sin($dlng / 2);
        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
        $km = $r * $c;

        return ($miles ? ($km * 0.621371192) : $km);
    }
}