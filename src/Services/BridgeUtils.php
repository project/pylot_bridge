<?php
namespace Drupal\pylot_bridge\Services ;

use Drupal\pylot_bridge\Services\BridgeCmsAbstractLayer ;
use Drupal\pylot_bridge\Services\BridgeBlockService ;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;
use Drupal\pylot_bridge\Services\BridgeShortCodeParser;


/**
 * BridgeUtils
 * Utilistaires pour le plugin Bridge
 */
class BridgeUtils
{


    /**
     * Retourne les données d'une liste d'après les paramètres passés
     * @param $attributes : paramètres d'affichage
     * @param $isAjax : si true, la fonction se comporte en mode ajax et ne retourne que les templateItems
     *
     * @return array
     */
    public static function getListData($attributes) {

        $res = array(
            'success' => false,
            'message' => '',
            'total' => null,
            'data' => null,
            'infos' => array(),
            'parameters' => array()
        );

        $bridgeCredentials = BridgeUtils::getBridgeCredentials();
        $urlBridge = $bridgeCredentials->urlBridge;

        if(empty($urlBridge)) {
            $res['message'] = "L'URL de Bridge n'est pas définie - contrôlez les paramètres du plugin";
            return $res;
        }

        if(!is_array($attributes) || empty($attributes['id'])){
            $res['message'] = 'Le paramètre id est obligatoire dans le shortcode';
            return $res;
        }
        // La liste est filtrée par défaut en lisant les paramètres GET POST du contexte
        // l'attribut useRequestFilters permet de désactiver ce comportement
        if(!isset($attributes['useRequestFilters']))
            $attributes['useRequestFilters'] = true;

        // Variable utilisée à différents endroits du code
        $webListId = $attributes['id'];
        $res['infos']['webListId'] = $webListId;
        $res['infos']['useRequestFilters'] = $attributes['useRequestFilters'];
        $res['infos']['debug'] = array() ;

        // Pagination des résultats
        // Première fiche
        $first = (isset($attributes['first']) && $attributes['first'] != '') ? $attributes['first'] : '1';
        $res['infos']['first'] = $first;

        // Si pagination : page à afficher
        $currentPage = (isset($attributes['brpa']) && $attributes['brpa'] != '') ? $attributes['brpa'] : '';
        $res['infos']['currentPage'] = $currentPage;
        $res['infos']['lastPage'] = $currentPage;

        // Nb de fiches à afficher
        $max = (isset($attributes['max']) && $attributes['max'] != '') ? $attributes['max'] : '';
        $limitPerPage = (isset($attributes['max']) && $attributes['max'] != '') ? $attributes['max'] : '12';
        $res['infos']['max'] = $max;
        $res['infos']['limitPerPage'] = $limitPerPage;

        $lang = BridgeUtils::getLanguage();
        $res['infos']['lang'] = $lang;


        // On ajoute les paramètres de filtre du moteur et les paramètres de tri
        $brParamsUrlFiltre = '';
        $brParamsUrlSort = '';

        // On démarre un session pour pouvoir enregistrer des paramètres de filtres de localisation
        if (session_status() === PHP_SESSION_NONE) {
            session_start();
        }
        $geoLat = '';
        $geoLon = '';
        $geoCity = '';

        if (count($_REQUEST) > 0 && $attributes['useRequestFilters']) {
            foreach ($_REQUEST as $key=>$value) {
                // braf: Bridge Active Filter
                // brai: Bridge Active Item
                if ($key == 'braf' || $key == 'brai' || (strlen($key) > 6 && substr($key,0,6) == 'brflt_')) {
                    // On doit encoder en URI les paramètres de type valeur saisie par l'internaute
                    if (strpos($key, '_value') !== false || strpos($key, '_city') !== false) {
                        $brParamsUrlFiltre .= "&$key=" . urlencode($value);
                    } else {
                        $brParamsUrlFiltre .= "&$key=$value";
                    }
                    // On mémorise les paramètres de recherche géographique en session
                    if (strpos($key, '_city') !== false) {
                        $geoCity = $value;
                    }
                    if (strpos($key, '_lat') !== false) {
                        $geoLat = $value;
                    }
                    if (strpos($key, '_lon') !== false) {
                        $geoLon = $value;
                    }
                }
                // bras: Bridge Active Sort
                // brsd: Bridge Sort Direction
                if ($key == 'bras' || $key == 'brsd') {
                    $brParamsUrlSort .= "&$key=$value";
                }

                if ($key == 'max' && !empty($value)) {
                    $limitPerPage = (int) $value;
                    $res['infos']['limitPerPage'] = $limitPerPage;
                }

                // Pagination : paramètre brpa = numéro de page
                if ($key == 'brpa' && !empty($value)) {
                    $res['infos']['debug'][] = "ON A UN BRPA DANS LURL";
                    $currentPage = (int) $value;
                    $res['infos']['currentPage'] = $currentPage;
                    // La paramètre de pagination a priorité sur le first
                    $first= '';
                }
            }
        }

        // On enregistre les paramètres de recherche géolocalisée en session pour pouvoir réafficher la distance de chaque fiche
        if(!empty($geoLat) && !empty($geoLon) && !empty($geoCity) ) {
            $_SESSION['bridge_geolat'] = $geoLat;
            $_SESSION['bridge_geolon'] = $geoLon;
            $_SESSION['bridge_geocity'] = $geoCity;
        } else {
            //  ND 27.10.22 - Bug : lorsqu'on efface le filtre par loacalisation, l'info reste toutefois rémanente en session
            if(isset($_SESSION['bridge_geolat']))
                unset($_SESSION['bridge_geolat']);
            if(isset($_SESSION['bridge_geolon']))
                unset($_SESSION['bridge_geolon']);
            if(isset($_SESSION['bridge_geocity']))
                unset($_SESSION['bridge_geocity']);
        }

        // on regarde si on change le contenu de la liste ou si on met juste à jour l'affichage (scroll, tri)
        $change = (isset($attributes['change']) && $attributes['change'] != '') ? $attributes['change'] : '0';

        // JM 22/03/2022 : on change le nom du paramètre (update -> v2), moins risqué
        $url = $urlBridge . "/weblist/getProductsForList/" . $webListId . "?v2=1&brpa=" . $currentPage . "&first=" . $first . "&max=" . $max . "&lang=" . $lang . "&change=" . $change;

        // Filtres et tris forcés
        if(isset($attributes['braf']) && !empty($attributes['braf'])) {
            $brParamsUrlFiltre .= "&braf=" . $attributes['braf'];
        }
        if(isset($attributes['brai']) && !empty($attributes['brai'])) {
            $brParamsUrlFiltre .= "&brai=" . $attributes['brai'];
        }
        if(isset($attributes['bras']) && !empty($attributes['bras'])) {
            $brParamsUrlSort .= "&bras=" . $attributes['bras'];
        }
        if(isset($attributes['brsd']) && !empty($attributes['brsd'])) {
            $brParamsUrlSort .= "&brsd=" . $attributes['brsd'];
        }

        if ($brParamsUrlFiltre != '') {
            $url .= $brParamsUrlFiltre;
        }

        if ($brParamsUrlSort != '') {
            $url .= $brParamsUrlSort;
        }

        // Feat : 23/10/2022 : possibilité de passer un nom de commune pour cumulr un filtre par commune à la séelction de la liste
        if(isset($attributes['filter_city']) && !empty($attributes['filter_city'])) {
            $url .= "&filter_city=" . $attributes['filter_city'];
        }

        // Feat : 25/05/2022 : possibilité de passer une liste de productCodes pour forcer les fiches à afficher dans le shortcode
        $productCodes = '';
        if(isset($attributes['product_codes']) && !empty($attributes['product_codes'])) {
            $productCodes = $attributes['product_codes'];
            $url .= "&productCodes=" . $productCodes;
        }
        // Support des requetes minimalistes pour les points sur la carte
        if(isset($attributes['minimal_select']) && !empty($attributes['minimal_select'])) {
            $url .= "&minimalSelect=1" ;
        }

        if(isset($_REQUEST['dncdebug']) && $_REQUEST['dncdebug'] == 'Y')
            echo "<a href='".$url."' target='_blank' class='uk-button uk-button-primary'>Détails de liste</a>";

        $res['infos']['debug_url'] = $url ;
        /*if(isset($_REQUEST['dncdebug']) && $_REQUEST['dncdebug'] == 'Y') {
            echo "<a href='".$url."' target='_blank' class='uk-button uk-button-primary'>Détails de liste</a>";
        }*/
        $tmpData = file_get_contents($url);
        $data = null;

        if (!empty($tmpData)) {
            $tmpDataDecoded = json_decode($tmpData);
            if ($tmpDataDecoded->success && $tmpDataDecoded->data) {
                $data = $tmpDataDecoded->data;
            } else {
                $res['message'] = $tmpDataDecoded->message;
                return $res;
            }
        } else {
            $res['message'] = 'Échec de la récupération de la liste';
            return $res;
        }

        if (!is_object($data)) {
            $res['message'] = "ERREUR d'appel à Bridge : aucune donnée valide renvoyée";
            return $res;
        }

        if (!isset($data->selection) || !isset($data->selection->results) || !isset($data->selection->results->products ) || !is_array($data->selection->results->products)) {
            $res['message'] = "ERREUR d'appel à Bridge : aucune donnée valide renvoyée - CODE 2";
            return $res;
        }

        $res['data'] = $data;

        // Préparation du permalink
        $permalink = BridgeUtils::getPermalink($data);
        if($permalink !== '' && substr($permalink, 0 , 1) === '/') $permalink = substr($permalink, 1);
        $res['infos']['permalink'] = $permalink;


        // Inutile => devoiement Wordpress
        // Mis en commentaire le 07/04/2022 par ND après avoir vérifié que ce n'était plus utilisé
        // $tabProductsPosts = BridgeUtils::getPostIdsFromProductList($data->selection->results->products, $lang );

        // Nombre total de fiches
        $totalProducts = $data->selection->results->total;
        $totalproducts = $totalProducts; // Compatibilite
        $res['infos']['totalProducts'] = $totalProducts;
        $res['total'] = $totalProducts;
        $res['infos']['productCodes'] = $productCodes;

        // On corrige la variable limitPerPage par rapport à ce qu'il y avait dans Bridge
        if(isset($data->parameters) && isset($data->parameters->WEBLIST_MAX_ITEMS) && isset($data->parameters->WEBLIST_MAX_ITEMS->value))
            $limitPerPage = (int) $data->parameters->WEBLIST_MAX_ITEMS->value;

        $res['infos']['limitPerPage'] = $limitPerPage;

        // Si on a pas passé de paramètre de page dans l'URL, on va quand même recalculer sur quelle page on est à partir du paramètre first
        if(empty($res['infos']['currentPage'] )) {
            $currentPage = (round(((int) $first - 1) / (int) $limitPerPage)) + 1 ;
            $res['infos']['currentPage'] = $currentPage;
            $res['infos']['debug'][] = "ON RECALCULE currentPage à partir de first $first et limitperpage : $limitPerPage ";
        }

        // Maintenant on va préparer une variable stockant le dernier numéro de page pour la pagination
        $res['infos']['lastPage'] = ceil( $totalProducts / $limitPerPage );

        // On injecte les liens vers les fiches de details et on met à jour la propriété isInSession pour dire si la fiche est dans le carnet de voyages courant
        // On procède de la même manière pour calculer la distance par rapport au point de recherche geographique s'il y en a
        if(count($data->selection->results->products) > 0 ) {
            for($i = 0 ; $i < count($data->selection->results->products) ; $i++) {
                $data->selection->results->products[$i]->link = BridgeUtils::getPostPermalinkFromProductCode($data->selection->results->products[$i]->productCode, $data->webPageId);
                // On insère l'information que la fiche se trouve dans le carnet de voyages ou non
                $isInSession = false;
                if(!empty($_SESSION) && !empty($_SESSION["cv"])) {
                    foreach ($_SESSION["cv"] as $f) {
                        if ($f["id"] == $data->selection->results->products[$i]->productCode) {
                            $isInSession = true;
                            break;
                        }
                    }
                }
                $geoSearch = false;
                $geoDistance = 0;
                $geoCity = '';
                // calcul de la distance au point de recherche
                if(!empty($_SESSION) && !empty($_SESSION["bridge_geolat"]) && !empty($data->selection->results->products[$i]->latitude) && !empty($data->selection->results->products[$i]->longitude)) {
                    $geoLat = (float) $_SESSION['bridge_geolat'];
                    $geoLon = (float) $_SESSION['bridge_geolon'];
                    $geoSearch = true;
                    $geoCity = $_SESSION['bridge_geocity'] ;
                    $geoDistance = BridgeBlockService::distance($data->selection->results->products[$i]->latitude, $data->selection->results->products[$i]->longitude, $geoLat, $geoLon );
                }

                $data->selection->results->products[$i]->isInSession = $isInSession;
                $data->selection->results->products[$i]->geoSearch = $geoSearch;
                $data->selection->results->products[$i]->geoDistance = $geoDistance;
                $data->selection->results->products[$i]->geoCity = $geoCity;
            }
        }

        // proprété inutile laissée pour des raisons historiques
        $res['infos']['cityComboDynList'] = null;

        // On récupère les paramètres spécifiques
        $params = BridgeUtils::getParametersListe($data->parameters);

        // Paramètres de modèles
        $bridgeModeleListe = $data->listTemplate;
        $bridgeModeleListeitem = $data->listItemTemplate;
        if ($bridgeModeleListe == '') $bridgeModeleListe = 'liste1';
        if ($bridgeModeleListeitem == '') $bridgeModeleListeitem = 'carte1';

        // recalage erreur d'orthographe
        $bridgeModeleListe = str_replace('caroussel', 'carousel', $bridgeModeleListe);
        $res['infos']['bridgeModeleListe'] = $bridgeModeleListe;
        $res['infos']['bridgeModeleListeitem'] = $bridgeModeleListeitem;

        // Feat 25/05/2022 : gestion des attributs bridgeModeleListe et bridgeModeleListeitem dans le shortcode pour overrider ce qui est défini dans la liste
        if(isset($attributes['template']) && !empty($attributes['template']))
            $res['infos']['bridgeModeleListe'] = $attributes['template'];
        else
            $res['infos']['bridgeModeleListe'] = $bridgeModeleListe;

        // ND : NE PAS CORRIGER LE CAMELCASE SVP
        if(isset($attributes['item_template']) && !empty($attributes['item_template']))
            $res['infos']['bridgeModeleListeitem'] = $attributes['item_template'];
        else
            $res['infos']['bridgeModeleListeitem'] = $bridgeModeleListeitem;

        // Paramètres
        if ($bridgeModeleListe == "carousel" || $bridgeModeleListe == "carrousel") {
            $res['parameters']['numberItems'] = (isset($params->WEBLIST_CAROUSEL_MAX_ITEMS) && $params->WEBLIST_CAROUSEL_MAX_ITEMS != '') ? $params->WEBLIST_CAROUSEL_MAX_ITEMS : '';
            $res['parameters']['showImage'] = (isset($params->WEBLIST_CAROUSEL_SHOW_IMAGE) && $params->WEBLIST_CAROUSEL_SHOW_IMAGE == 1) ? true : false;
            $res['parameters']['defaultImage'] = (isset($params->WEBLIST_CAROUSEL_DEFAULT_IMAGE) && $params->WEBLIST_CAROUSEL_DEFAULT_IMAGE != '') ? $params->WEBLIST_CAROUSEL_DEFAULT_IMAGE : '';
            $res['parameters']['paginationType'] = (isset($params->WEBLIST_PAGINATION_TYPE) && $params->WEBLIST_PAGINATION_TYPE != '') ? $params->WEBLIST_PAGINATION_TYPE : '';
            $res['parameters']['numberColumn'] = (is_object($params->WEBLIST_CAROUSEL_NUMBER_DISPLAYED_PHOTOS) && is_object($params->WEBLIST_CAROUSEL_NUMBER_DISPLAYED_PHOTOS)) ? $params->WEBLIST_CAROUSEL_NUMBER_DISPLAYED_PHOTOS : '';
            $res['parameters']['spacingColumn'] = (isset($params->WEBLIST_CAROUSEL_PHOTO_SPACING) && $params->WEBLIST_CAROUSEL_PHOTO_SPACING != '') ? $params->WEBLIST_CAROUSEL_PHOTO_SPACING : '';
            $res['parameters']['displayCvButton'] = (isset($params->WEBLIST_CAROUSEL_DISPLAY_BUTTON) && $params->WEBLIST_CAROUSEL_DISPLAY_BUTTON == 1);
            $res['parameters']['displayListTitle'] = (isset($params->WEBLIST_CAROUSEL_AFFICHER_TITRE) && $params->WEBLIST_CAROUSEL_AFFICHER_TITRE == 1);
            $res['parameters']['titleTag'] =  (isset($params->WEBLIST_CAROUSEL_TITLE_TAG) && $params->WEBLIST_CAROUSEL_TITLE_TAG != '') ? $params->WEBLIST_CAROUSEL_TITLE_TAG : 'h1';
            $res['parameters']['displayDescription'] = (isset($params->WEBLIST_CAROUSEL_AFFICHER_DESCRIPTION) && $params->WEBLIST_CAROUSEL_AFFICHER_DESCRIPTION == 1);
            $res['parameters']['displayImage'] = (isset($params->WEBLIST_CAROUSEL_AFFICHER_IMAGE) && $params->WEBLIST_CAROUSEL_AFFICHER_IMAGE == 1);
            $res['parameters']['displayInContainer'] = (isset($params->WEBLIST_CAROUSEL_AFFICHER_CONTAINER) && $params->WEBLIST_CAROUSEL_AFFICHER_CONTAINER == 1);

            // Navigation
            if (isset($params->WEBLIST_CAROUSEL_NAVIGATION) && $params->WEBLIST_CAROUSEL_NAVIGATION != '') :
                switch ($params->WEBLIST_CAROUSEL_NAVIGATION):
                    case 'AUCUN':
                        $res['parameters']['arrow'] = false;
                        $res['parameters']['dots'] = false;
                        break;
                    case 'FLECHE':
                        $res['parameters']['arrow'] = true;
                        $res['parameters']['dots'] = false;
                        break;
                    case 'PUCE':
                        $res['parameters']['arrow'] = false;
                        $res['parameters']['dots'] = true;
                        break;
                    case 'FLECHE_ET_PUCE':
                        $res['parameters']['arrow'] = true;
                        $res['parameters']['dots'] = true;
                        break;
                    default:
                        $res['parameters']['arrow'] = true;
                        $res['parameters']['dots'] = true;
                        break;
                endswitch;
            endif;

            // Paramètres du carousel
            $paramsCarousel = '';
            $paramsCarousel .= (isset($params->WEBLIST_CAROUSEL_CENTERED) && $params->WEBLIST_CAROUSEL_CENTERED == 1) ? 'center:true;' : '';
            $paramsCarousel .= (isset($params->WEBLIST_CAROUSEL_AUTOPLAY) && $params->WEBLIST_CAROUSEL_AUTOPLAY == 1) ? 'autoplay:true;' : '';
            $paramsCarousel .= (isset($params->WEBLIST_CAROUSEL_SETS) && $params->WEBLIST_CAROUSEL_SETS == 1) ? 'sets:true;' : '';
            $paramsCarousel .= (isset($params->WEBLIST_CAROUSEL_AUTOPLAY_INTERVAL) && $params->WEBLIST_CAROUSEL_AUTOPLAY_INTERVAL != '') ? 'autoplay-interval:' . ($params->WEBLIST_CAROUSEL_AUTOPLAY_INTERVAL * 1000) . ';' : '';
            $res['parameters']['paramsCarousel'] = $paramsCarousel;

        } else {
            // Paramètres des modèles de liste

            $res['parameters']['numberItems'] = (isset($params->WEBLIST_MAX_ITEMS) && $params->WEBLIST_MAX_ITEMS != '') ? $params->WEBLIST_MAX_ITEMS : '';
            $res['parameters']['showImage'] = (isset($params->WEBLIST_SHOW_IMAGE) && $params->WEBLIST_SHOW_IMAGE != 1) ? false : true;
            $res['parameters']['defaultImage'] = (isset($params->WEBLIST_DEFAULT_IMAGE) && $params->WEBLIST_DEFAULT_IMAGE != '') ? $params->WEBLIST_DEFAULT_IMAGE : '';
            $res['parameters']['paginationType'] = (isset($params->WEBLIST_PAGINATION_TYPE) && $params->WEBLIST_PAGINATION_TYPE != '') ? $params->WEBLIST_PAGINATION_TYPE : '';
            $res['parameters']['numberColumn'] = (isset($params->WEBLIST_NUMBER_COLUMNS) && $params->WEBLIST_NUMBER_COLUMNS != '') ? $params->WEBLIST_NUMBER_COLUMNS : '';
            $res['parameters']['spacingColumn'] = (isset($params->WEBLIST_COLUMN_SPACING) && $params->WEBLIST_COLUMN_SPACING != '') ? $params->WEBLIST_COLUMN_SPACING : '';
            $res['parameters']['displayCvButton'] = (isset($params->WEBLIST_DISPLAY_BUTTON) && $params->WEBLIST_DISPLAY_BUTTON == 1);
            $res['parameters']['displayRelNextPrev'] = (isset($params->WEBLIST_REL_NEXT_PREV) && $params->WEBLIST_REL_NEXT_PREV == 1);
			$res['parameters']['displayListTitle'] = (isset($params->WEBLIST_AFFICHER_TITRE) && $params->WEBLIST_AFFICHER_TITRE == 1);
			$res['parameters']['titleTag'] =  (isset($params->WEBLIST_TITLE_TAG) && $params->WEBLIST_TITLE_TAG != '') ? $params->WEBLIST_TITLE_TAG : 'h1';
			$res['parameters']['displayDescription'] = (isset($params->WEBLIST_AFFICHER_DESCRIPTION) && $params->WEBLIST_AFFICHER_DESCRIPTION == 1);
			$res['parameters']['displayImage'] = (isset($params->WEBLIST_AFFICHER_IMAGE) && $params->WEBLIST_AFFICHER_IMAGE == 1);
			$res['parameters']['displayInContainer'] = (isset($params->WEBLIST_AFFICHER_CONTAINER) && $params->WEBLIST_AFFICHER_CONTAINER == 1);
            $res['parameters']['modeMultiMoteurs'] = (isset($params->WEBLIST_MOTEUR_MODE_MULTI_MOTEURS) && $params->WEBLIST_MOTEUR_MODE_MULTI_MOTEURS == 'OUI');
            $res['parameters']['multiMoteursIndexfiltreSwitch'] = (isset($params->WEBLIST_MULTIMOTEURS_INDEX_FILTRE_SWITCH)) ? $params->WEBLIST_MULTIMOTEURS_INDEX_FILTRE_SWITCH: '1';
            $res['parameters']['multiMoteursAfficherSections'] = (isset($params->WEBLIST_MULTIMOTEURS_AFFICHER_SECTIONS) && $params->WEBLIST_MULTIMOTEURS_AFFICHER_SECTIONS == 'OUI');

            // Normalisation du parametre  numberColumn
            if(!isset( $res['parameters']['numberColumn']) || !is_object( $res['parameters']['numberColumn']))
                $res['parameters']['numberColumn'] = new \stdClass();

            if(!isset( $res['parameters']['numberColumn']->default ))
                $res['parameters']['numberColumn']->default = '2';
            if(!isset( $res['parameters']['numberColumn']->xl ))
                $res['parameters']['numberColumn']->xl = $res['parameters']['numberColumn']->default;
            if(!isset( $res['parameters']['numberColumn']->l ))
                $res['parameters']['numberColumn']->l = $res['parameters']['numberColumn']->default;
            if(!isset( $res['parameters']['numberColumn']->m ))
                $res['parameters']['numberColumn']->m = $res['parameters']['numberColumn']->default;
            if(!isset( $res['parameters']['numberColumn']->s ))
                $res['parameters']['numberColumn']->s = $res['parameters']['numberColumn']->default;
            if(!isset( $res['parameters']['numberColumn']->xs ))
                $res['parameters']['numberColumn']->xs = $res['parameters']['numberColumn']->default;

        }

        // S'il n'y a aucun produit avec les critères sélectionnés, on affiche la liste avec un message (AJAX)
        if (empty($data->selection->results->products)) {
            $res['total'] = 0;
            $res['message'] = 'Aucun résultat';
        }

        // Paramètres des modèles et on fixe des paramètre au cas ou
        if($res['parameters']['numberItems'] != '') $res['parameters']['limitPerPage'] = $res['parameters']['numberItems'];
        if($res['parameters']['paginationType'] == '') $res['parameters']['paginationType'] = 'INFINITE_SCROLL'; // 'Pagination' 'Aucune pagination'

        $res['success'] = true;

        // On transforme les propriété infos et parameters en objets
        $res['parameters'] = json_decode(json_encode($res['parameters']));
        $res['infos'] = json_decode(json_encode($res['infos']));
        return $res;

    }


    /**
     * Appelle le contrôleur Bridge et récupère les données d'une carte dans la langue souhaitée, avec tous ses paramètres
     * et l'arborescence de ses items et subitems, avec les paramètres de ceux-ci et les produits qui leur sont associés
     *
     * @param $attributes : tablea associatif de paramètres d'affichage
     *
     * @return bool
     */
    public static function getMapData($attributes) {
        // prepareMapDataForRender

        $res = array(
            'success' => false,
            'message' => '',
            'total' => null,
            'data' => null,
            'infos' => array(),
            'parameters' => array()
        );

        $bridgeCredentials = BridgeUtils::getBridgeCredentials();
        $urlBridge = $bridgeCredentials->urlBridge;

        if(empty($urlBridge)) {
            $res['message'] = "L'URL de Bridge n'est pas définie - contrôlez les paramètres du plugin";
            return $res;
        }

        if(!is_array($attributes) || empty($attributes['id'])){
            $res['message'] = 'Le paramètre id est obligatoire dans le shortcode';
            return $res;
        }

        // Variable utilisée à différents endroits du code
        $webMapId = $attributes['id'];
        $res['infos']['mapId'] = $webMapId;

        $lang = BridgeUtils::getLanguage();
        $res['infos']['lang'] = $lang;
        $langUpper = strtoupper($lang);

        $url = $urlBridge . "/webmap/getWebMapData/" . $webMapId . "?lang=" . $lang;

        if (isset($_REQUEST['dncdebug']) && $_REQUEST['dncdebug'] == 'Y') {
            echo "<a href='".$url."' target='_blank' class='uk-button uk-button-primary'>Détails de liste</a>";
        }

        $tmpData = file_get_contents($url);
        $mapTemplate = '';
        $popupTemplate = '';

        if (!empty($tmpData)) {
            $tmpDataDecoded = json_decode($tmpData);
            if ($tmpDataDecoded->success && !empty($tmpDataDecoded->data)) {
                $mapData = $tmpDataDecoded->data;
                $res['data'] = $mapData;
                if (is_object($mapData)) {
                    $mapTemplate = $mapData->mapTemplate;
                    $res['infos']['mapTemplate'] = $mapTemplate;
                    $popupTemplate = $mapData->popupTemplate;
                    $res['infos']['popupTemplate'] = $popupTemplate;
                    $mapParameters = $mapData->parameters;

                    $res['parameters'] = $mapParameters;


                    $mapTest = $langUpper;
                    if ($langUpper === 'FR' && !empty($mapParameters->legendTitle)) {
                        $legendFrontTitle = $mapParameters->legendTitle;
                    } elseif ($langUpper !== 'FR' && isset($mapParameters->legendTitleTranslations) && !empty($mapParameters->legendTitleTranslations->$langUpper)) {
                        $legendFrontTitle = $mapParameters->legendTitleTranslations->$langUpper;
                    }
                    $res['infos']['legendFrontTitle'] = $legendFrontTitle;
                } else {
                    $res['message'] = 'Erreur : données invalides 1 mapData n\'est pas un objet';
                    return $res;
                }
            } else {
                if (isset($tmpDataDecoded->message)) {
                    $res['message'] = 'Erreur 3 : ' . $tmpDataDecoded->message;
                    return $res;
                } else {
                    $res['message'] = 'Erreur : données invalides 2';
                    return $res;
                }
            }
        } else {
            $res['message'] = 'Erreur à la récupération des données';
            return $res;
        }

        if (empty($mapTemplate)) {
            $res['message'] = 'Vous devez choisir un template de carte';
            return $res;
        }

        if (isset($mapParameters->isDisplayingPopup) && $mapParameters->isDisplayingPopup == true && empty($popupTemplate)) {
            $res['message'] = 'Vous devez choisir un template de popup';
            return $res;
        }

        if (!isset($mapParameters->mapType)) {
            $res['message'] = 'Vous devez choisir un fond de carte';
            return $res;
        }

        if ($mapParameters->mapType === 'MAPTYPE_IGN' && empty($mapParameters->keyIgn)) {
            $res['message'] = 'Pour utiliser un fond de carte IGN, vous devez enregistrer une clef';
            return $res;
        }

        $res['success'] = true;
        return $res;
    }

    /**
     * Appelle le contrôleur Bridge et récupère un moteur dans la langue souhaitée,
     * avec le permalien de la liste indiquée en paramètre et l'id du moteur de cette liste
     *
     * @param array $attributes : paramètres d'affichage (idmoteur et idliste)
     *
     * @return bool
     */
    public static function getMoteur($attributes) {
        $onlyMoteur = true;

        $bridgeCredentials = BridgeUtils::getBridgeCredentials();
        $urlBridge = $bridgeCredentials->urlBridge;

        $customCss = BridgeUtils::getCustomCssURl();

        if (empty($urlBridge)) {
            die("L'URL de Bridge n'est pas définie - contrôlez les paramètres du plugin");
        }

        if (!is_array($attributes) || !isset($attributes['idmoteur'])){
            die('Le paramètre idmoteur est obligatoire dans le shortcode');
        } else {
            $moteurId = $attributes['idmoteur'];
        }
        if (!is_array($attributes) || !isset($attributes['idliste'])){
            die('Le paramètre idliste est obligatoire dans le shortcode');
        } else {
            $webListId = $attributes['idliste'];
        }

        $lang = BridgeUtils::getLanguage();

        // Dans cette configuration, on sera toujours en train de récupérer la liste pour la première fois, on veut toutes les données
        $change = true;

        $url = $urlBridge . '/weblist/getProductsForList/' . $webListId . '?v2=1&engine=' . $moteurId . '&lang=' . $lang . '&change=' . $change;

        if (isset($_REQUEST['dncdebug']) && $_REQUEST['dncdebug'] == 'Y') {
            echo "<a href='".$url."' target='_blank' class='uk-button uk-button-primary'>Détails de liste</a>";
        }

        $tmpData = file_get_contents($url);
        $data = null;
        $moteur = null;
        $webListEngineId = null;

        if (!empty($tmpData)) {
            $tmpDataDecoded = json_decode($tmpData);
            if ($tmpDataDecoded->success && $tmpDataDecoded->data) {
                $data = $tmpDataDecoded->data;
            } else {
                $res['message'] = $tmpDataDecoded->message;
                die($res['message']);
            }
        } else {
            $res['message'] = 'Échec de la récupération du moteur';
            die($res['message']);
        }

        if (!empty($data)) {
            // On a reçu la liste entière, mais on veut juste l'id de la liste, son permalien et le moteur souhaité :
            // On prend le moteur passé en paramètre
            $moteur = $data->moteur;
            // et on récupère la liste des communes pour le filtre citycombodyn
            $cityComboDynList = null;

            if (!empty($moteur) && !empty($moteur->sections) && is_array($moteur->sections)) {
                foreach ($moteur->sections as $section) {
                    if (!empty($section->subSections)) {
                        foreach ($section->subSections as $subSection) {
                            if (!empty($subSection->filters) && is_array($subSection->filters)) {
                                foreach ($subSection->filters as $filter) {
                                    if ($filter->displayType === 'citycombodyn' && !empty($filter->cityComboDynList) && is_array($filter->cityComboDynList)) {
                                        foreach ($filter->cityComboDynList as $city) {
                                            $cityComboDynList .= (array_search($city, $filter->cityComboDynList) == 0 ? '' : ',') . $city;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $webListEngineId = null;

            if (isset($data->moteurOriginal)) {
                // quand le moteur passé en paramètre est le même que celui associé à la liste, on n'a noté que son id pour éviter de doublonner les données
                if (gettype($data->moteurOriginal) === 'integer') {
                    $webListEngineId = $data->moteurOriginal;
                } else {
                    $webListEngineId = $data->moteurOriginal->id;
                }
            }
            // On récupère le permalien de la liste
            $webListPermalink = BridgeUtils::getPermalink($data);

            // Quelques variables nécessaires à l'affichage des templates et au fonctionnement des scripts
            // nombre total de fiches
            $totalProducts = $data->selection->results->total;
            // nombre de fiches max par affichage
            $limitPerPage = 12;
            $bridgeAjaxUrl = BridgeUtils::getAjaxURL();

            BridgeUtils::BridgeEnqueueStyle('fontawesome-v6-fa');
            BridgeUtils::BridgeEnqueueStyle('fontawesome-v6-regular');
            BridgeUtils::BridgeEnqueueStyle('uikit-css');
            BridgeUtils::BridgeEnqueueStyle('plugin-sit-style');
            BridgeUtils::BridgeEnqueueStyle('sit-custom-css');

            BridgeUtils::BridgeEnqueueScript('uikit-js');
            BridgeUtils::BridgeEnqueueScript('plugin-sit-script');
            BridgeUtils::BridgeEnqueueScript( 'bridge-moteur', BridgeUtils::bridgeDirUrl() . 'public/js/bridge-moteur.js', array( 'jquery' ), null, true );

            $template = BridgeUtils::bridge_locate_template('templates-bridge/engine/moteur1.php');
            include($template);
            return '';
        }

        // return $moteurId;
    }

    /**
     * Appelée par le contrôleur AJAX par les vues de cartes interactives
     * Construit la liste des produits associés aux WebMapItems sélectionnés
     * @param array $webMapItems : tableau des id (string) des items sélectionnés
     * @param string $lang : langue dans laquelle la carte est affichée
     *
     * @return array $res avec bool success, string message, array data (objets produits avec les infos de base)
     */
    public static function getWebMapProductsToShow($webMapItems, $lang) {
        $bridgeCredentials = BridgeUtils::getBridgeCredentials();
        $urlBridge = $bridgeCredentials->urlBridge;

        $res = array(
            'success' => false,
            'message' => null,
            'data' => null
        );

        if (empty($urlBridge)) {
            $res['message'] = "L'URL de Bridge n'est pas définie - contrôlez les paramètres du plugin";
            return $res;
        }

        if (isset($_REQUEST['dncdebug']) && $_REQUEST['dncdebug'] == 'Y') {
           echo "<a href='".$urlBridge."' target='_blank' class='uk-button uk-button-primary'>Détails de liste</a>";
        }

        // On a un tableau de string
        $webMapItemsInt = array();

        foreach ($webMapItems as $webMapItem) {
            $webMapItemsInt[] = (int) $webMapItem;
        }

        $items = implode(',', $webMapItemsInt);

        $productsToShow = array();

        $url = $urlBridge . '/webmap/getProductListByItems?items=' . $items . '&lang=' . $lang;

        $tmpData = file_get_contents($url);

        if (!empty($tmpData)) {
            $tmpDataDecoded = json_decode($tmpData);

            if (isset($tmpDataDecoded->success) && $tmpDataDecoded->success && isset($tmpDataDecoded->data)) {
                $productsToShow = $tmpDataDecoded->data;

                if (isset($tmpDataDecoded->message)) {
                    $res['message'] = $tmpDataDecoded->message;
                }

            } elseif (isset($tmpDataDecoded->success) && !$tmpDataDecoded->success && isset($tmpDataDecoded->message)) {
                $res['message'] = $tmpDataDecoded->message;
                return $res;

            } else {
                $res['message'] = 'Erreur : données invalides';
                return $res;
            }

        } else {
            $res['message'] = "Erreur à la récupération des données";
            return $res;
        }

        $res['success'] = true;
        $res['data'] = $productsToShow;

        return $res;
    }

    /**
     * Appelée par le contrôleur AJAX par les vues de cartes interactives au clic sur un marqueur
     * Récupère les données du produit à afficher dans le pop-up
     * Et les renvoie directement via BridgeUtils::sendJson
     *
     * @param int $webMapId : l'id de la carte affichée
     * @param string $lang : la langue dans laquelle la carte est affichée (fr, de, en, etc.)
     * @param int $productCode : le code du produit à afficher
     *
     * @return json
     */
    public static function getWebMapProductData($webMapId = '', $lang = 'fr', $productCode = '', $popupTemplate = '') {

        $res = array(
            'success' => false,
            'message' => '',
            'total' => null,
            'data' => null,
            'infos' => array(),
            'parameters' => array()
        );

        $bridgeCredentials = BridgeUtils::getBridgeCredentials();
        $urlBridge = $bridgeCredentials->urlBridge;

        if(empty($urlBridge)) {
            $res['message'] = "L'URL de Bridge n'est pas définie - contrôlez les paramètres du plugin";
            return $res;
        }
        if(empty($webMapId)) {
            $res['message'] = "Pas d\'id de map fourni";
            return $res;
        }
        if(empty($productCode)) {
            $res['message'] = "Pas de product code fourni";
            return $res;
        }
        if(empty($popupTemplate)) {
            $res['message'] = "Veuillez choisir un modèle de popup";
            return $res;
        }

        if (isset($_REQUEST['dncdebug']) && $_REQUEST['dncdebug'] == 'Y') {
            echo "<a href='".$urlBridge."' target='_blank' class='uk-button uk-button-primary'>Détails de liste</a>";
        }
        $res['infos']['webMapId'] = $webMapId;
        $res['infos']['popupTemplate'] = $popupTemplate;
        $res['infos']['lang'] = $lang;

        $url = $urlBridge . '/webmap/getProductData/' . $webMapId . '/' . $productCode . '?lang=' . $lang;

        $tmpData = file_get_contents($url);
        if (!empty($tmpData)) {
            $tmpDataDecoded = json_decode($tmpData);
            if (isset($tmpDataDecoded->success) && $tmpDataDecoded->success == true && isset($tmpDataDecoded->data)) {
                // On insère la présence en CV et le lien
                $tmpDataDecoded->data->product->link = BridgeUtils::getPostPermalinkFromProductCode($tmpDataDecoded->data->product->productCode);
                // On insère l'information que la fiche se trouve dans le carnet de voyages ou non
                $isInSession = false;
                if(!empty($_SESSION) && !empty($_SESSION["cv"])) {
                    foreach ($_SESSION["cv"] as $f) {
                        if ($f["id"] == $tmpDataDecoded->data->product->productCode) {
                            $isInSession = true;
                            break;
                        }
                    }
                }
                $tmpDataDecoded->data->product->isInSession = $isInSession;
                $popupData = $tmpDataDecoded->data->product;
                $res['data'] = $popupData;
            } elseif (isset($tmpDataDecoded->success) && !$tmpDataDecoded->success && isset($tmpDataDecoded->message)) {
                $res['message'] = "Erreur 06 : " . $tmpDataDecoded->message;
                return $res;
            } else {
                $res['message'] = "Erreur : données invalides";
                return $res;
            }
        } else {
            $res['message'] = "Erreur à la récupération des données";
            return $res;
        }
        $res['success'] = true;
        return $res;
    }

    /**
     * Génère une suite de 10 lettres et chiffres servant d'identifiant probablement unique à chaque instance de carte créée
     * @return string
     */
    public static function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Retourne l'URL relative pour les appels AJAX des templates front-end qui lancent la récupération des données des listes
     */
    public static function getAjaxURL() {
        return '/wp-admin/admin-ajax.php';
    }

    /**
     * Met à jour un réglage de Bridge en base de données
     *
     * @param $optionName
     * @param $optionValue
     */
    public static function bridgeUpdateOption($optionName, $optionValue) {
        global $wpdb;
        $sql = "INSERT INTO " . $wpdb->prefix . "options (option_name, option_value, autoload) VALUES ";
        $sql .= "('" . $optionName . "','" . serialize($optionValue) . "', 'yes') ";
        $sql .= "ON DUPLICATE KEY UPDATE option_value = '" . serialize($optionValue) . "' ;";
        return $wpdb->query($sql);
    }

    /**
     * @return Retourne le code de la langue courante sous la forme fr-FR ou en-GB (langue - pays)
     */
    public static function getLanguage()
    {
        return BridgeCmsAbstractLayer::getCurrentLanguage();
    }

    /**
     * Retourne le code de langue d'un post donné (wordpress)
     * @param $postId numéro de post
     * @return string
     */
    public static function getPostLanguage($postId)
    {
        return BridgeCmsAbstractLayer::getPostLanguage($postId);
    }

    /**
     * Définit le code de langue d'un post donné
     * @param $postId
     * @return string
     */
    public static function setPostLanguage($postId, $lang)
    {
        return BridgeCmsAbstractLayer::setPostLanguage($postId, $lang);
    }

    /**
     * Sauvegarde les traductions d'un produit (WordPress)
     * @param $translationsArray tableau associatif $lang => $postId ('fr' => 4)
     */
    public static function savePostTranslations($translationsArray)
    {
        return BridgeCmsAbstractLayer::savePostTranslations($translationsArray);
    }

    /**
     * Définit le code de langue d'un post donné (WordPress)
     * @param $termId : id du terme
     * @return string
     */
    public static function setTermLanguage($termId, $lang)
    {
        return BridgeCmsAbstractLayer::setTermLanguage($termId, $lang);
    }

    /**
     * Retourne la langue d'un terme (WordPress)
     * @param $termId : id du terme
     * @return string code de langue
     */
    public static function getTermLanguage($termId)
    {
        return BridgeCmsAbstractLayer::getTermLanguage($termId);
    }

    /**
     * Retourne la langue d'un terme de taxonomie (WordPress)
     * @param $termId : id du terme
     * @return array taleau associatif lang => id de terme
     */
    public static function getTermTranslations($termId)
    {
        return BridgeCmsAbstractLayer::getTermTranslations($termId);
    }

    /**
     * Sauvegarde les traductions d'un terme (Wordpress)
     * @param $translationsArray tableau associatif $lang => $termId ('fr' => 4)
     */
    public static function saveTermTranslations($translationsArray)
    {
        return BridgeCmsAbstractLayer::saveTermTranslations($translationsArray);
    }

    /**
     * Retourne la liste des langues actives
     * @return string[] tableau de codes de langues
     */
    public static function getLanguagesList()
    {
        return BridgeCmsAbstractLayer::getLanguagesList();
    }

    /**
     * @return mixed|string URL du serveur Pylot Bridge
     */
    public static function getBridgeUrl()
    {
        $config = \Drupal::config('pylot_bridge.bridgeconfig');
        return $config->get('url_bridge');
    }

    /**
     * Charge une feuille de style css dans la page
     * @param $handle : id texte du css
     * @param string $src : url du css
     * @param array $deps : tableau de dépendances (liste d'id texte)
     * @param false $ver : version
     * @param string $media
     * @return mixed
     */
    public static function BridgeEnqueueStyle($handle, $src = "", $deps = array(), $ver = false, $media = 'all') {
        return BridgeCmsAbstractLayer::enqueueStyle($handle, $src, $deps, $ver, $media);
    }

    /**
     * Charge un script js dans la page
     * @param $name identifiant texte du script
     * @param $link URL du script
     * @return bool
     */
    public static function BridgeEnqueueScript($handle, $src = "", $deps = array(), $ver = false, $in_footer = false ) {
        return BridgeCmsAbstractLayer::enqueueScript($handle, $src, $deps, $ver, $in_footer);
    }

    /**
     * Charge les styles CSS de base des affichages frontend
     * @param bool $style
     * @return bool
     */
    public static function loadBridgeStyles($style=true)
    {
        BridgeUtils::BridgeEnqueueStyle('uikit-css');
        BridgeUtils::BridgeEnqueueStyle('plugin-sit-style');
        $customCss = BridgeUtils::getCustomCssURl();
        if(!empty($customCss))
            BridgeUtils::BridgeEnqueueStyle('sit-custom-css');

        return true;
    }

    /**
     * charge les scripts  Js de base des affichages front-end
     * @param bool $script
     * @return bool
     */
    public static function loadBridgeScripts($script=true)
    {
        BridgeUtils::BridgeEnqueueScript('uikit-js');
        BridgeUtils::BridgeEnqueueScript('plugin-sit-script');
        $customJs = BridgeUtils::getCustomJsURl();
        if(!empty($customJs))
            BridgeUtils::BridgeEnqueueScript('sit-custom-js');

        return true;
    }

    /**
     * Charge les styles et scripts Js de base des affichages front-end
     * @param bool $styles
     * @param bool $scripts
     */
    public static function loadBridgeAssets($styles = true,$scripts = true){
        if($scripts)
            BridgeUtils::loadBridgeScripts();

        if($scripts)
            BridgeUtils::loadBridgeStyles();

        return true;
    }

    /**
     * Retourne le header du theme front-end du site pour affichage avant le code de la fichede détail (WordPress)
     * @return mixed
     */
    public static function getHeaderBridge(){
        return BridgeCmsAbstractLayer::getHeaderBridge();
    }

    /**
     * Retourne le footer du theme front-end du site pour affichage après le code de la fichede détail (WordPress)
     * @return mixed
     */
    public static function getFooterBridge(){
        return BridgeCmsAbstractLayer::getFooterBridge();
    }

    /**
     * Filtre un tableau associatif de paramètres et le transforme en obet après en avoir retiré les paramètres aux valeurs nulles
     * @param $params
     * @return stdClass|void
     */
    public static function getParametersListe($params){
        if(!isset($params)) return;
        $res = new \stdClass();

        foreach($params as $key=>$param){
            if (isset($param) && is_object($param)) {
                $res->$key = $param->value;
            }
        }
        return $res;
    }

    // ************************************************************************
    //
    //              FONCTIONS DE RECUPERATION DES PARAMETRES
    //
    // *************************************************************************

    /**
     * @deprecated utiliser plutot getBridgeParameters
     * @return mixed|string URL de la feuille de styles personnalisée définie dans le paramètres de l'extension
     */
    public static function getCustomCssURl()
    {
        $options = get_option('reglages_sit_design_option');
        if (isset($options['custom_css_sit']))
            return $options['custom_css_sit'];
        else
            return '';
    }

    /**
     * @deprecated utiliser plutot getBridgeParameters
     * @return mixed|string URL du script personnalisé défini dans le paramètres de l'extension
     */
    public static function getCustomJsURl()
    {
        $options = get_option('reglages_sit_design_option');
        if (isset($options['custom_js_sit']))
            return $options['custom_js_sit'];
        else
            return '';
    }

    /**
     * @deprecated : utiliser plutot getBridgeParameters
     * @return stdClass Paramètres de l'extension relatifs à l'affichage des cartes
     */
    public static function getMapsDefaultSettings()
    {
        $obj = new \stdClass();
        $obj->latitude = '48.8534';
        if (isset($options['latitude']))
            $obj->latitude = $options['latitude'];
        $obj->longitude = '2.3488';
        if (isset($options['longitude']))
            $obj->longitude = $options['longitude'];
        $obj->marker = get_site_url() . '/wp-content/plugins/plugin-sit/assets/img/map-marker-icon.png';
        if (!empty($options['marker_map']))
            $obj->marker = $options['marker_map'];

        $obj->activeMarker = get_site_url() . '/wp-content/plugins/plugin-sit/assets/img/map-marker-icon.png';
        if (!empty($options['marker_map_actif']))
            $obj->activeMarker = $options['marker_map_actif'];

        return $obj;
    }

    /**
     * @return false|mixed|string Chemin de base relatif à la réécriture des URL de taxonomies (hors permalien personnalisé)
     */
    public static function getTaxonomyRootUrl()
    {
        $config = \Drupal::config('pylot_bridge.bridgeconfig');
        $url =  $config->get('fiche_root_url');
        if (!empty($url)) {
            $temp = $url;
            if (substr($temp, 0, 1) == '/')
                $temp = substr($temp, 1);
            if (substr($temp, 0, 1) == '/')
                $temp = substr($temp, 1);
            if (substr($temp, 0, 1) == '/')
                $temp = substr($temp, 1);
            return '/' . $temp;
        } else {
            return '/sit';
        }
    }

    /**
     * @return false|mixed|string Chemin de base relatif à la réécriture des URL des fiches (hors permalien personnalisé)
     */
    public static function getFicheRootUrl()
    {
        $config = \Drupal::config('pylot_bridge.bridgeconfig');
        $url =  $config->get('fiche_root_url');
        if (!empty($url)) {
            $temp = $url;
            if (substr($temp, 0, 1) == '/')
                $temp = substr($temp, 1);
            if (substr($temp, 0, 1) == '/')
                $temp = substr($temp, 1);
            if (substr($temp, 0, 1) == '/')
                $temp = substr($temp, 1);
            return '/' . $temp;
        } else {
            return '/fiche-sit';
        }
    }

    /**
     * @return mixed URL du dossier du plugin Pylot Bridge
     */
    public static function bridgeDirUrl()
    {
        $rootUrl = \Drupal::request()->getSchemeAndHttpHost();
        $module_handler = \Drupal::service('module_handler');
        $bridgePath = $module_handler->getModule('pylot_bridge')->getPath();
        return $rootUrl . '/' . $bridgePath;
    }

    /**
     * @return mixed Chemin du dossier du plugin Pylot Bridge
     */
    public static function bridgeDirPath()
    {
        return realpath(dirname(__FILE__) . '');
    }

    /**
     * @return string URL du dossier des telmplates d'affichage du plugin Pylot Bridge
     */
    public static function bridgeTemplatesPath()
    {
        return BridgeUtils::bridgeDirPath().'public/templates/';
    }

    /**
     * TODO : voir si besoin de cette fonction sous drupal
     * Retourne le chemin URL d'une liste Bridge (pour créer des liens vers les fiches et vers les listes)
     * @param $webList objet webList tel que retourné par l'API Bridge
     * @return array|string|string[] Portion d'URL de la liste
     */
    public static function getPermalink($webList) {
        // TODO : gérer les langues
        $deb = '';
        $lang = BridgeUtils::getLanguage();
        if(!is_object($webList) || !isset($webList->id))
            return '###ERR-WEBLIST###';

        $termId = self::getTermIdByWeblistId($webList->id);
        if(empty($termId))
            return '###ERR-WEBLIST-TERM###';

        $path = '/taxonomy/term/' . $termId ;
        $alias_manager = \Drupal::entityTypeManager()->getStorage('path_alias');
        // On recherche s'il existe un alias pour ce node
        $aliasNode = $alias_manager->loadByProperties([
            'path'     => $path,
            'langcode' => $lang
        ]);
        if(empty($aliasNode)) {
            return self::getLanguagePrefix() . $path; // Route par défaut de Drupal ; '/taxonomy/term/' . $term->get('tid')->value;
        } else {
            foreach($aliasNode as $alias_object) {
                $type = gettype($alias_object->alias);
                if($type == 'string')
                    return self::getLanguagePrefix() . $alias_object->alias ;
                else
                    return '#ErrPermalink#TermTypeIsNotString#' . $type ;
            }
        }
    }

    /**
     * Fonction pour WordPress : indique si on se trouve actuellement sur une page de taxonomie SIT
     * @return bool True si on est sur une page de taxonomie SIT
     *
     */
    public static function IsFicheSitTaxonomy(){
        // Modification Nicolas 27/04/21 : normalmeent c'est ça
        //return is_tax( get_object_taxonomies( 'fiche-sit' ) );
        return is_tax( get_object_taxonomies( 'fiche_sit' ) );
        // Pareil normalement
        // return is_tax( 'rubrique_sit_primaire' );
    }

    /**
     * @return bool true si on est sur une page de fiche SIT - WordPress
     */
    public static function IsFicheSit(){
        // Patch nicolas 27/04/21 : c'est un underscore !
        return is_singular( array( 'fiche_sit' ) );
    }


    // ***************************************************************************
    //
    //      FIN PARAMETRES
    //
    // ****************************************************************************


    /**
     * Retourne des données complètes de fiches associées (tableau d'objets products) à partir d'un objet product
     *
     * @param $fiche
     * @return array|false|mixed objet de réponse de l'API Bridge
     */
    public static function getCoupledProducts($fiche) {
        $res = array();
        if(!is_object($fiche) || !isset($fiche->coupled) || empty($fiche->coupled))
            return $res;

        $codes = array();
        foreach($fiche->coupled as $tmpObj)
            $codes[] = $tmpObj->coupledProductCode;

        $lang = BridgeUtils::getLanguage();
        // Appel à Bridge
        $params = 'language=' . $lang .  '&completeResponse=1&first=1&max=5';
        $items = array('tag' => 'pro.productCode', 'value' => array('value' => implode('|',$codes)));
        $params .= '&items[]=' . urlencode(json_encode($items));

        $products = BridgeUtils::callBridgeApi('GET', '/api/product/getByRequest', $params);
        return $products;
    }

    /**
     * getCoupledProducts
     * Retourne un tableau d'objets products des fiches associées à l'objet product passé, enrichis des prioriété photo et shortComment
     * @param $fiche Objet prodct issu de l'API Bridge
     * @return array|false|mixed
     */
    public static function getCoupledProductsData($fiche) {
        $res = array();
        $coupled = self::getCoupledProducts($fiche);
        if (isset($coupled) && isset($coupled->products) && is_array($coupled->products) && count($coupled->products) > 0) {
            // On récupère les critères photos via un bloc dédié à l'import des photos qu'on définit dans les paramètres du plugin
            for( $i = 0 ; $i < count($coupled->products) ; $i++) {
                $coupled->products[$i]->photo = '';
            }
            $blockPhoto = self::getIdBlockPhotos();
            if (!empty($blockPhoto)) {
                $blockContent = self::callBridgeApi('GET', '/api/blocks/' . $blockPhoto);
                $criteresPhotos = self::extractBlockCriterions(json_decode(json_encode($blockContent)));

                for( $i = 0 ; $i < count($coupled->products) ; $i++) {
                    if (isset($criteresPhotos) && is_array($criteresPhotos) && count($criteresPhotos) > 0) {
                        $testImage = self::filterModalities($coupled->products[$i]->modalities, $criteresPhotos[0]);
                        if (is_array($testImage) && count($testImage) > 0 && isset($testImage[0]->value) && trim($testImage[0]->value) != '') {
                            $photo = "http://" . $testImage[0]->value;
                            $photo = str_replace("http://http://", "http://", $photo);
                            $photo = str_replace("http://https://", "https://", $photo);
                            $coupled->products[$i]->photo = $photo;
                        }
                    }
                    if(!empty($coupled->products[$i]->comment)) {
                        if(mb_strlen($coupled->products[$i]->comment) > 150) {
                            $coupled->products[$i]->shortComment = mb_substr($coupled->products[$i]->comment,0,150) . '...';
                        } else {
                            $coupled->products[$i]->shortComment = $coupled->products[$i]->comment ;
                        }
                    } else {
                        $coupled->products[$i]->shortComment = '';
                    }

                    // Lien
                    $link = self::getPostPermalinkFromProductCode($coupled->products[$i]->productCode);
                    $coupled->products[$i]->link = $link;
                }
            }
            $res = $coupled->products;
        }


        return $res;
    }

    /**
     * Retourne l'URL du resizer d'image Bridge
     * @return string URL du resizer d'image Bridge
     */
    public static function getResizeImage(){
        return BridgeUtils::bridgeDirUrl().'assets/resize_image_dnc.php';
    }

    /**
     * Retourne le premier label d'item d'un block
     * @param $blockData objet block
     * @return false|mixed chaine de label trouvé ou false si non trouvé
     * @deprecated utiliser BridgeBlockService::extractBlockFirstItemLabel
     */
    public static function extractBlockFirstItemLabel($blockData) {
        return BridgeBlockService::extractBlockFirstItemLabel($blockData);
    }

    /**
     * Extrait la première valeur texte d'un block
     * @param $blockData objet block
     * @return false|mixed false si non trouvé ou chaine trouvée
     * @deprecated utiliser BridgeBlockService::extractBlockFirstTextValue
     */
    public static function extractBlockFirstTextValue($blockData) {
        return BridgeBlockService::extractBlockFirstTextValue($blockData);
    }

    /**
     * Transforme une chaine de caractèes accentuée en slug sans espaces ni accents ni carctères spéciaux
     * @param $string chaine à slugifier
     * @param string $delimiter caractère délimiteur pour remplacer les accents et caractères spéciaux
     * @return string
     */
    public static function slugify($string, $delimiter = '-') {
        $oldLocale = setlocale(LC_ALL, '0');
        setlocale(LC_ALL, 'en_US.UTF-8');
        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
        $clean = strtolower($clean);
        $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
        $clean = trim($clean, $delimiter);
        setlocale(LC_ALL, $oldLocale);
        return $clean;
    }

    /**
     * retourne une chaine traduite à partir d'une chaine fr de l'interface
     * @param $texte
     * @return mixed
     */
    public static function traduction($texte) {
        return __($texte, 'plugin-sit');
    }

    /*=============================================
                    BREADCRUMBS
    =============================================*/

    /**
     * fonction appelée depuis un modèle de fiche
     * Retourne le code HTML du fil d'Ariane affiché dans les fiches
     * @param string $delimiter délimiteur d'entrées
     * @param $home texte pour l'entrée 'Accueil'
     * @param int $showCurrent mettre 1 pour afficher l'entre courante
     * @param string $before texte à afficher avant l'élément courant
     * @param string $after texte à afficher après l'élément courant
     *
     * @return string code HTML du fil d'Ariane affiché dans les fiches
     */
    public static function BridgeBreadcrumbs($delimiter = '&raquo;',$home = '', $showCurrent = 1,$before = '<span class="current">',$after = '</span>')
    {
        if (!isset($home) || $home == '') $home = __('Accueil', 'plugin-sit');

        /*$showOnHome = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
        $delimiter = '&raquo;'; // delimiter between crumbs
        $home = 'Home'; // text for the 'Home' link
        $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
        $before = '<span class="current">'; // tag before the current crumb
        $after = '</span>'; // tag after the current crumb */

        global $post;

        $retour = '';
        if (get_post_type() != 'fiche_sit') {
            return "Votre article n'est pas une fiche SIT";
        }
        $homeLink = get_bloginfo('url');
        $retour .= '<div id="crumbs"><a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . ' ';

        if (get_post_type() != 'fiche_sit') {
            $post_type = get_post_type_object(get_post_type());
            $slug = $post_type->rewrite;
            $retour .= '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a>';
        } else {
            $cat = wp_get_post_terms($post->ID, 'rubrique_sit_primaire');
            $cat = $cat[0];
            $cats = get_term_parents_list($cat, 'rubrique_sit_primaire', array('seperator' => ' ' . $delimiter . ' '));
            if ($showCurrent != 1) {
                $cats = preg_replace("#^(.+)\s$delimiter\s$#", "$1", $cats);
            }
            if(mb_substr($cats, -1) == '/')
                $cats = mb_substr($cats, 0, -1);
            $retour .= $cats ;
        }
        if ($showCurrent == 1) {
            $retour .= ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
        }
        $retour .= '</div>';
        return $retour;
    }



    /**
     * Locate template.
     *
     * Localise un fichier template demandé (prend en compte les dossiers d'override du theme)
     * Pour wordpress, le recherche s'effectue dans cet ordre :
     * 1. /themes/theme/templates/$template_name
     * 2. /themes/theme/$template_name
     * 3. /plugins/plugin/templates/$template_name.
     *
     * @param   string  $template_name          Template to load.
     * @param   string  $string $template_path  Path to templates.
     * @param   string  $default_path           Default path to template files.
     * @return  string                          Path to the template file.
     */
    public static function bridge_locate_template( $template_name, $template_path = '', $default_path = '' ) {
        // Set variable to search in the templates folder of theme.
        $tmpl = get_stylesheet_directory() . '/plugin-sit/' . $template_name;

        if ( ! file_exists( $tmpl ) ) {
            // If the override template does NOT exist, fallback to the default template.
            $tmpl = realpath(dirname(__FILE__) . '/../public/templates') . '/' . $template_name;
        }
        return apply_filters( 'bridge_locate_template', $tmpl, $template_name, $template_path, $default_path );
    }


    /**
     * Retourne le code HTML de mise en forme d'un bloc nommé
     * @param Object $blocks_object : propriété blocks d'une fiche telle que renvoyées par l'Api Bridge
     * @param String $block_name : Nom de la zone de block à charger
     * @param Array $options : tableau associatif d'options forcées (sectionTitleTag, sectionCSSClass, itemCSSclass)
     * @return string : resultat HTML
     * @deprecated utiliser BridgeBlockService::renderNamedBlockHtml
     */
    public static function renderNamedBlockHtml($blocks_object, $block_name, $options = array()) {
        return BridgeBlockService::renderNamedBlockHtml($blocks_object, $block_name, $options);
    }

    /**
     * Retourne le code HTML de mise en forme d'un bloc
     * @param Object $block_data : données de block telles que renvoyées par l'Api Bridge
     * @param Array $options : tableau associatif d'options forcées (sectionTitleTag, sectionCSSClass, itemCSSclass)
     * @return string : resultat HTML
     * @deprecated utiliser BridgeBlockService::renderBlockHtml
     */
    public static function renderBlockHtml($renderBlockData, $options = array()) {
        return BridgeBlockService::renderBlockHtml($renderBlockData, $options);
    }

    /**
     * Permet de localiser une feuille de style dynamique dans /public/css
     *
     * @param   string  $styleFileName          Feuille de style recherchée
     * @param   string  $styleFilePath          Chemin des styles
     * @param   string  $default_path           Chemin des styles par défaut
     * @return  string                          Chemin de la feuille de style recherchée
     */

    public static function bridge_locate_style($styleFileName, $styleFilePath = '', $default_path = ''){
        $tmpl = realpath(dirname(__FILE__) . '/../public/css') . '/' . $styleFileName;
        return apply_filters('bridge_locate_style', $tmpl, $styleFileName, $styleFilePath, $default_path);
    }

    /**
     * Permet d'afficher une icône Bridge issue de la libriarie font-awesome
     * Charge au besoin la libririe font-awesome nécessaire
     * @param $icon Nom de l'icône
     * @param string $lemode Style de l'icône font-awesome (fas, fab, fal, far, fad, fat)
     * @return string code HTML de l'icônedu type <i class="fa far fa-check"></i>
     */
    public static function renderIcon($icon,$lemode=''){
        return BridgeBlockService::renderIcon($icon, $lemode);
    }

    /**
     * Retourne un nom de classe CSS à qui s'appliquera au conteneur d'une section de bloc à partir de la prioriété itemsColumns de la section
     * @param $item objet section de block Bridge
     * @return string|void nom de classe à ajouter (frameword uikit)
     * @deprecated utiliser BridgeBlockService::renderColumnsChild
     */
    public static function renderColumnsChild($item){
        return BridgeBlockService::renderColumnsChild($item);
    }

    /**
     * Personnalisation des micro data pour l'accessibilité et le référencement
     * Retourne l'atribut HTML itemprop adéquat à partir d'un item de block
     * @param $type propriété type de l'item : passer $item->type
     * @return string|void code à insérer dans la balise HTML où est insérée l'item
     * @deprecated utiliser BridgeBlockService::getMicroData
     */
    public static function getMicroData($type){
        return BridgeBlockService::getMicroData($type);
    }

    /**
     * Retourne le nom d'une classe CSS uikit permettant d'afficher les noeuds d'un item de block en colonnes
     * @param $object objet $item de block Bridge
     * @return string propriété de classe CSS générée
     */
    public static function getChildColumns($object)
    {
        return BridgeBlockService::getChildColumns($object);
    }


    /**
     * Utilitaire pour trouver un critère depuis la priopriété modalities d'un objet de données product issu de Bridge
     *
     * @param       $mods : passer product->modalities tel quel
     * @param array $filters : tableau associatif (ex: {criterionCode: 9000000, modalityCode:123456 }
     *
     * @return array Tableau d'objets product->modalities filtré sur les critères de recherche
     */
    public static function filterModalities($mods, $filters = array())
    {
        $res = array();
        if (isset($mods) && count($mods)) {
            foreach ($mods as $mod) {
                $trouve = true;
                if (isset($filters) && count($filters)) {
                    foreach ($filters as $filter => $value) {
                        if (!isset($mod->$filter) || $mod->$filter != $value) {
                            $trouve = false;
                        }
                    }
                }
                if ($trouve) {
                    $res[] = $mod;
                }
            }
        }

        return $res;
    }


    /**
     * Retourne les données d'une fiche SIT à partir du contexte (appelée depuis la page fiche)
     * @return mixed objet product issu de l'API Bridge
     */
    public static function getDataFiche($productCode, $idFicheBridge = '') {
        global $post;
        // On démarre un session pour pouvoir enregistrer des paramètres de filtres de localisation
        if (session_status() === PHP_SESSION_NONE) {
            session_start();
        }
        $dataFiche = new \stdClass();



        if(isset($productCode)) {
            $urlBridge = BridgeUtils::getBridgeUrl();

            if(empty($idFicheBridge)) {
                $dataFiche = new \stdClass();
                $dataFiche-> error = "Erreur de paramétrage - pas de modèle de fiche défini dans Bridge";
            } else {

                $lang = BridgeUtils::getLanguage();
                $lurl = $urlBridge . '/webpage/getProductForFiche/' . $idFicheBridge . '?productCode=' . $productCode . "&lang=" . $lang ;

                if(isset($_REQUEST['dncdebug']) && $_REQUEST['dncdebug']=="Y") {
                    die('<a href="'.$lurl.'" class="uk-width-1-1 uk-button uk-button-primary uk-margin">Détails fiche</a>');

                }

                // TODO : implémenter un système de mise en cache volatile des requetes Bridge
                $dataFiche = file_get_contents( $lurl);
                $dataFiche = json_decode( $dataFiche);
                $dataFiche = json_decode( json_encode($dataFiche));

                // On insère l'information que la fiche se trouve dans le carnet de voyages ou non
                $isInSession = false;
                if(is_object($dataFiche) && property_exists($dataFiche, 'product') && is_object($dataFiche->product)) {
                    if(!empty($_SESSION) && !empty($_SESSION["cv"])) {
                        foreach ($_SESSION["cv"] as $f) {
                            if ($f["id"] == $dataFiche->product->productCode) {
                                $isInSession = true;
                                break;
                            }
                        }
                    }
                    $geoSearch = false;
                    $geoDistance = 0;
                    $geoCity = '';
                    // calcul de la distance au point de recherche
                    if(!empty($_SESSION) && !empty($_SESSION["bridge_geolat"]) && !empty($dataFiche->product->latitude) && !empty($dataFiche->product->longitude)) {
                        $geoLat = (float) $_SESSION['bridge_geolat'];
                        $geoLon = (float) $_SESSION['bridge_geolon'];
                        $geoSearch = true;
                        $geoCity = $_SESSION['bridge_geocity'] ;
                        $geoDistance = BridgeBlockService::distance($dataFiche->product->latitude, $dataFiche->product->longitude, $geoLat, $geoLon );
                    }

                    $dataFiche->product->isInSession = $isInSession;
                    $dataFiche->product->geoSearch = $geoSearch;
                    $dataFiche->product->geoDistance = $geoDistance;
                    $dataFiche->product->geoCity = $geoCity;

                    $dataFiche->product->isInSession = $isInSession;
                }

                if(isset($_REQUEST['dncdebug']) && $_REQUEST['dncdebug']=="Y") {
                    $dataFiche->dncdebug = $lurl;
                }
                // On traite les paramètres
                if(!isset($dataFiche))
                    return false;
                $dataFiche->ficheTemplate = ($dataFiche->ficheTemplate) ? $dataFiche->ficheTemplate : 'fiche1';
                if(empty($dataFiche->parameters))
                    $dataFiche->parameters = new \stdClass();
                $dataFiche->parameters->displayCvButton = (isset($dataFiche->parameters->FICHE_DISPLAY_BUTTON)  && isset($dataFiche->parameters->FICHE_DISPLAY_BUTTON->value) && $dataFiche->parameters->FICHE_DISPLAY_BUTTON->value == 1);
                $dataFiche->parameters->displayToc = (isset($dataFiche->parameters->AFFICHER_SOMMAIRE)  && isset($dataFiche->parameters->AFFICHER_SOMMAIRE->value) && $dataFiche->parameters->AFFICHER_SOMMAIRE->value == 1);
                $dataFiche->parameters->limitDescriptionFieldHeight = (isset($dataFiche->parameters->LIMITER_HAUTEUR_DESCRIPTIF)  && isset($dataFiche->parameters->LIMITER_HAUTEUR_DESCRIPTIF->value) && $dataFiche->parameters->LIMITER_HAUTEUR_DESCRIPTIF->value == 1);
                $dataFiche->parameters->tocPosition = (isset($dataFiche->parameters->POSITION_SOMMAIRE)  && isset($dataFiche->parameters->POSITION_SOMMAIRE->value))?$dataFiche->parameters->POSITION_SOMMAIRE->value:'GAUCHE' ;
                $dataFiche->parameters->sliderAutoplay = (isset($dataFiche->parameters->SLIDER_AUTOPLAY)  && isset($dataFiche->parameters->SLIDER_AUTOPLAY->value) && $dataFiche->parameters->SLIDER_AUTOPLAY->value == 1);
                $dataFiche->parameters->hidePhoneWithXX = (isset($dataFiche->parameters->HIDE_PHONE)  && isset($dataFiche->parameters->HIDE_PHONE->value) && $dataFiche->parameters->HIDE_PHONE->value == 1);
                $dataFiche->parameters->emailLinkType = (isset($dataFiche->parameters->EMAIL_LINK_TYPE)  && isset($dataFiche->parameters->EMAIL_LINK_TYPE->value))?$dataFiche->parameters->EMAIL_LINK_TYPE->value:'MAILTO' ;
                $dataFiche->parameters->emailPrestSubject = (isset($dataFiche->parameters->EMAIL_PREST_SUBJECT)  && isset($dataFiche->parameters->EMAIL_PREST_SUBJECT->value))?$dataFiche->parameters->EMAIL_PREST_SUBJECT->value:'Demande de renseignements' ;
                $dataFiche->parameters->emailPrestBody = (isset($dataFiche->parameters->EMAIL_PREST_BODY)  && isset($dataFiche->parameters->EMAIL_PREST_BODY->value))?$dataFiche->parameters->EMAIL_PREST_BODY->value:'' ;
                $dataFiche->parameters->emailPrestBodyEnd = (isset($dataFiche->parameters->EMAIL_PREST_BODY_END)  && isset($dataFiche->parameters->EMAIL_PREST_BODY_END->value))?$dataFiche->parameters->EMAIL_PREST_BODY_END->value:'' ;
                $dataFiche->parameters->emailFormAntispam = (isset($dataFiche->parameters->EMAIL_FORM_ANTISPAM)  && isset($dataFiche->parameters->EMAIL_FORM_ANTISPAM->value))?$dataFiche->parameters->EMAIL_FORM_ANTISPAM->value:'' ;
                $dataFiche->parameters->recaptchav3_site_key = (isset($dataFiche->parameters->RECAPTCHAV3_SITE_KEY)  && isset($dataFiche->parameters->RECAPTCHAV3_SITE_KEY->value))?$dataFiche->parameters->RECAPTCHAV3_SITE_KEY->value:'' ;
                $dataFiche->parameters->recaptchav3_secret_key = (isset($dataFiche->parameters->RECAPTCHAV3_SECRET_KEY)  && isset($dataFiche->parameters->RECAPTCHAV3_SECRET_KEY->value))?$dataFiche->parameters->RECAPTCHAV3_SECRET_KEY->value:'' ;
                $dataFiche->parameters->recaptchav2_site_key = (isset($dataFiche->parameters->RECAPTCHAV2_SITE_KEY)  && isset($dataFiche->parameters->RECAPTCHAV2_SITE_KEY->value))?$dataFiche->parameters->RECAPTCHAV2_SITE_KEY->value:'' ;
                $dataFiche->parameters->recaptchav2_secret_key = (isset($dataFiche->parameters->RECAPTCHAV2_SECRET_KEY)  && isset($dataFiche->parameters->RECAPTCHAV2_SECRET_KEY->value))?$dataFiche->parameters->RECAPTCHAV2_SECRET_KEY->value:'' ;
                $dataFiche->parameters->emailTestEmailAddress = (isset($dataFiche->parameters->EMAIL_TEST_EMAIL_ADDRESS)  && isset($dataFiche->parameters->EMAIL_TEST_EMAIL_ADDRESS->value))?$dataFiche->parameters->EMAIL_TEST_EMAIL_ADDRESS->value:'' ;
                $dataFiche->parameters->emailModeTest = (isset($dataFiche->parameters->EMAIL_MODE_TEST)  && isset($dataFiche->parameters->EMAIL_MODE_TEST->value) && $dataFiche->parameters->EMAIL_MODE_TEST->value == 1);

            }
        }

        return $dataFiche;
    }

    /**
     * Permet de récupérer l'url d'une fiche d'après son productCode
     * @param $productCode champ productCode de Bridge
     * @return string URL de la fiche
     */
    public static function getPostPermalinkFromProductCode($productCode) {

        $lang = BridgeUtils::getLanguage();
        $permalink = '';
        $post = self::findPostByProductCode($productCode, 'fr');
        if(!empty($post)) {
            $path = '/node/'.$post->id() ;
            $alias_manager = $permalink = \Drupal::service('path_alias.manager') ;
            $permalink = \Drupal::service('path_alias.manager')->getAliasByPath('/node/'.$post->id());

            // On ajoute le prefixe de langue si nécessaire
            if(empty($permalink))
                $permalink = self::getLanguagePrefix() . $path;
            else
                $permalink = self::getLanguagePrefix() . $permalink;

            // Correction double /
            if(mb_substr($permalink, 0, 2) == '//') {
                $permalink = substr($permalink, 1);
            }
        } else {
            $permalink = "#ErrorPermalink#PostNotFoundForProduct#" . $productCode;
        }
        if(!empty($permalink))
            return $permalink;

        return "#ErrorPermalink#";
    }



    /**
     * Effectue un appel à l'API Bridge
     *
     * @param string $method méthode HTTP
     * @param string $entryPoint nom du point d'entrée d'API
     * @param mixed $data données à passer à l'API (tableau associatif clé => valeur)
     * @return false|mixed objet de la réponse de Bridge ou false si échec
     */
    public static function callBridgeApi($method = 'GET', $entryPoint = '', $data = null)
    {

        $tmpOptions = BridgeUtils::getBridgeCredentials();
        $urlBridge = $tmpOptions->urlBridge;

        $token = '';

        try {
            $token = BridgeUtils::getBridgeToken();
        }
        catch(Exception $ex) {
            if(isset($_REQUEST['dncdebug']) && $_REQUEST['dncdebug']=='Y') {
                echo "Exception call api Bridge : " . $ex->getMessage();
            }
            return false;
        }
        $urlparams = '';
        if (strtoupper($method) == 'GET') {
            if (is_array($data) && count($data) > 0) {
                $urlparams = '?';
                foreach ($data as $key => $val)
                    $urlparams .= $key . "=" . $val . "&";
            } elseif(!is_array($data) && isset($data) && !empty($data)) {
                $urlparams = '?';
                $urlparams .= $data ;
            }
        }
        if ($token == '') {
          if(isset($_REQUEST['dncdebug']) && $_REQUEST['dncdebug']=='Y') {
            echo "Call api Bridge token vide";
          }
          return false;
        }

        $options = array(
            'http' => array(
                'method' => $method,
                'header' => array("Content-Type: application/json", "Authorization: Bearer " . $token),
                'ignore_errors' => true,
                'timeout' => 10,
                'content' => json_encode($data),
            ),
        );
        try {
            $context = stream_context_create($options);
            $rawData = file_get_contents($urlBridge . $entryPoint . $urlparams, false, $context);
            $res = json_decode($rawData);
            if (isset($res)) {
                return $res;
            } else {
                if(isset($_REQUEST['dncdebug']) && $_REQUEST['dncdebug']=='Y') {
                    echo "Call api Bridge donne vide ou mal formatee JSON -" . $rawData;
                }
                return false;
            }
        } catch (Exception $e) {
            if(isset($_REQUEST['dncdebug']) && $_REQUEST['dncdebug']=='Y') {
                echo "Call api Bridge - erreur réponse Bridge : " . $e->getMessage();
            }
            return false;
        }
        if(isset($_REQUEST['dncdebug']) && $_REQUEST['dncdebug']=='Y') {
            echo "Call api Bridge - on ne devrait pas etre ici";
        }

        return false;
    }

    /**
     * Retourne un objet contenant les paraméètres de connexion au serveur Bridge (serveur et identifiants Bridge)
     *
     * @return stdClass
     */
    public static function getBridgeCredentials()
    {
        $config = \Drupal::config('pylot_bridge.bridgeconfig');

        $obj = new \stdClass();
        $obj->urlBridge = $config->get('url_bridge');
        $obj->loginBridge = $config->get('login_bridge');
        $obj->passwordBridge = $config->get('password_bridge');

        return $obj;
    }

    /**
     * Permet de récupérer un token de connexion à l'API Bridge à partir d'identifiants
     *
     * @param $url Url du serveur Bridge
     * @param $login login Bridge
     * @param $pw mot de passe Bridge
     * @throws Exception
     * @return string token de connexion
     */
    public static function getBridgeTokenFromCredentials($url, $login, $pw){

        if ($url != '' && $login != '' && $pw != '') {
            $data = array(
                'username' => $login,
                'password' => $pw
            );
            $options = array(
                'http' => array(
                    'method' => 'POST',
                    'header' => "Content-Type: application/json",
                    'ignore_errors' => true,
                    'timeout' => 10,
                    'content' => json_encode($data),
                )
            );

            $context = stream_context_create($options);
            $res = json_decode(file_get_contents($url . '/login_check', false, $context));

            if (isset($res) && isset($res->token)) {
                return $res->token;
            } else {
                throw new Exception("Invalid credentials");
            }
        }
    }

    /**
     * Permet de récupérer le token de connexion à l'API Bridge
     * @return string token de connexion à l'API Bridge
     */
    public static function getBridgeToken()
    {
        $tmpOptions = BridgeUtils::getBridgeCredentials();
        $urlBridge = $tmpOptions->urlBridge;
        $loginBridge = $tmpOptions->loginBridge;
        $passwordBridge = $tmpOptions->passwordBridge;

        return BridgeUtils::getBridgeTokenFromCredentials($urlBridge, $loginBridge, $passwordBridge);
    }

    /**
     * Extrait les critères / modalités d'un block pour faire une recherche par critère en rétro-ingéniérie
     * @param $blockData object block
     * @param int $maxSubItemsToExtract nombre maximum d'items à extraire
     * @return array tableau de tableaux associatifs [{ criterionCode: xxx, modalitycode: yyy }]
     */
    public static function extractBlockCriterions($blockData, $maxSubItemsToExtract = 1000)
    {
        $res = array();
        if (!isset($blockData->fields) || count($blockData->fields) == 0)
            return $res;
        foreach ($blockData->fields as $field) {
            $tmp = array();
            if (count($field->items) == 0)
                continue;
            foreach ($field->items as $item) {
                if (!isset($item->criterion) || !is_object($item->criterion))
                    continue;
                $tmp['criterionCode'] = $item->criterion->criterionCode;
                if (isset($item->modality) && is_array($item->modality) && count($item->modality) > 0) {
                    $modas = array();
                    foreach ($item->modality as $moda) {
                        if (isset($moda->modalityCode))
                            $modas[] = $moda->modalityCode;
                    }
                    $tmp['modalityCode'] = implode('|', $modas);
                }
                $res[] = $tmp;
            }
        }
        return $res;
    }

    /**
     * Permet de récupérer le dossier des templates Bridge stocké sur le serveur Bridge sous forme de zip et de l'intégrer au plugin pour affichage
     *
     * @return array(bool success, string message)
     */
    public static function updateBridgeTemplates()
    {
        $res = array(
            "success" => false,
            "message" => 'Échec de la mise à jour des gabarits'
        );

        $urlBridge = '';
        $options = get_option('reglages_sit_bridge_option');

        if (isset($options['url_bridge'])) {
            $urlBridge = $options['url_bridge'];
        } else {
            $res['message'] = 'URL Bridge indéfinie - contrôlez les paramètres';
            return $res;
        }

        $urlUpdate = $urlBridge . "/webtemplates/downloadUpdate";

        // On cherche le dossier des templates
        $targetDir = self::bridgeTemplatesPath();

        $data = array();
        $tmpData = file_get_contents($urlUpdate);

        if ($tmpData && is_dir($targetDir)) {

            // si le dossier templates-bridge existe déjà, on le supprime
            $templatesBridgeDir = $targetDir . 'templates-bridge';
            if (is_dir($templatesBridgeDir)) {
                self::deleteDir($templatesBridgeDir);
            }

            // On place le fichier d'archive récupéré dans le dossier des templates
            $targetZipFile = $targetDir . '/templates-bridge.zip';
            $testPutcontent = file_put_contents($targetZipFile, $tmpData);

            if ($testPutcontent !== false) {
                // On extrait les templates du fichier zip

                $zip = new ZipArchive();

                if ($zip->open($targetZipFile) === TRUE) {

                    $extraction = $zip->extractTo($targetDir);
                    $zip->close();

                    if ($extraction) {
                        // On supprime le zip devenu inutile pour éviter les problèmes à la prochaine update
                        unlink($targetZipFile);

                        $res['success'] = true;
                        $res['message'] = 'Mise à jour des gabarits Bridge effectuée';

                        return $res;
                    } else {
                        $res['message'] = 'Échec de l\'extraction des gabarits : données invalides';

                        return $res;
                    }
                } else {
                    $res['message'] = 'Échec de l\'intégration des gabarits';

                    return $res;
                }
            } else {
                $res['message'] = 'Échec de la récupération des gabarits';

                return $res;
            }

        } else {
            $res['message'] = 'Échec lors de l\'accès aux données';

            return $res;
        }
    }

    /**
     * Supprime un dossier et tout son contenu
     * @param string $dirPath : chemin du dossier à supprimer
     */
    public static function deleteDir($dirPath) {
        $files = glob($dirPath . "/*");
        if (!empty($files)) {
            foreach ($files as $file){
                if (is_dir($file)) {
                    self::deleteDir($file);
                }
                if (is_file($file)) {
                    unlink($file);
                }
            }
            rmdir($dirPath);
        }
    }

    /**
     * Récupère un id d'attachment à partir d'une URL (WordPress)
     * @param $url
     * @return int|mixed
     */
    public static function get_attachment_id($url)
    {

        $attachment_id = 0;
        $dir = wp_upload_dir();
        if (false !== strpos($url, $dir['baseurl'] . '/')) { // Is URL in uploads directory?
            $file = basename($url);
            $query_args = array(
                'post_type' => 'attachment',
                'post_status' => 'inherit',
                'fields' => 'ids',
                'meta_query' => array(
                    array(
                        'value' => $file,
                        'compare' => 'LIKE',
                        'key' => '_wp_attachment_metadata',
                    ),
                )
            );

            $query = new WP_Query($query_args);
            if ($query->have_posts()) {
                foreach ($query->posts as $post_id) {
                    $meta = wp_get_attachment_metadata($post_id);
                    $original_file = basename($meta['file']);
                    $cropped_image_files = wp_list_pluck($meta['sizes'], 'file');
                    if ($original_file === $file || in_array($file, $cropped_image_files)) {
                        $attachment_id = $post_id;
                        break;
                    }
                }
            }
        }
        return $attachment_id;
    }

    /**
     * Renvoie une reponse http Json d'erreur
     * @param mixed|null $data données à renvoyer en Json
     * @param int|null $status_code code HTTP à renvoyer
     * @param int $options options d'après les specifications de la fonction BridgeUtils::sendJsonError
     */
    public static function sendJsonError( $data = null, int $status_code = null, int $options = 0) {
        BridgeCmsAbstractLayer::sendJsonError( $data, $status_code, $options);
    }

    /**
     * Renvoie une reponse http Json de succes
     * @param mixed|null $data données à renvoyer en Json
     * @param int|null $status_code code HTTP à renvoyer
     * @param int $options options d'après les specifications de la fonction BridgeUtils::sendJsonSuccess
     */
    public static function sendJsonSuccess( $data = null, int $status_code = null, int $options = 0) {
        BridgeCmsAbstractLayer::sendJsonSuccess( $data, $status_code, $options);
    }

    /**
     * Renvoie une reponse http Json de succes
     * @param mixed|null $data données à renvoyer en Json
     * @param int|null $status_code code HTTP à renvoyer
     * @param int $options options d'après les specifications de la fonction BridgeUtils::sendJsonSuccess
     */
    public static function sendJson( $data = null, int $status_code = null, int $options = 0) {
        BridgeCmsAbstractLayer::sendJson( $data, $status_code, $options);
    }

    /**
     * Remplace les caractères accentués par des verions non accentués
     * @param $texte texte dont on souhaite retirer les accents
     * @return string chaine traitée
     */
    public static function stripAccents($texte){
        return strtr($texte,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
    }

    /**
     * Remplace les espaces d'une chaine par des _
     * @param $texte texte dont on veut retirer les espace
     * @return array|string|string[] chaine triatée
     */
    public static function stripSpaces($texte){
        return str_replace(' ','_',$texte);
    }

    // ---------------------------------------------------------
    //          NOUVELLES FONCTIONS
    // ---------------------------------------------------------

    /**
     * Retourne le terme de taxonomie associé à une liste bridge - Drupal
     * @param $id id de liste Bridge
     * @param string $lang langue dans laquelle on veut récupérer le terme
     * @return \Drupal\Core\Entity\EntityBase[]|\Drupal\Core\Entity\EntityInterface[]|Term[] tableau associatif $tid => $term
     */
    public static function getTermIdByWeblistId($id, $lang = 'fr') {
        $query = \Drupal::entityQuery('taxonomy_term');
        $query->condition('vid', 'rubrique_sit');
        $query->condition('field_sit_list_id', $id );
        $query->condition('langcode', $lang) ; // On récupère en fr
        $results = $query->execute();
        //$terms = \Drupal\taxonomy\Entity\Term::loadMultiple($query->execute());
        if(count($results) > 0 ) {
            // $term =  reset($terms);
            $temp = array_keys($results);
            return $temp[0];
        } else {
            return null;
        }
    }
    /**
     * Retourne le terme de taxonomie associé à une liste bridge - Drupal
     * @param $id id de liste Bridge
     * @param string $lang langue dans laquelle on veut récupérer le terme
     * @return \Drupal\Core\Entity\EntityBase[]|\Drupal\Core\Entity\EntityInterface[]|Term[] tableau associatif $tid => $term
     */
    public static function getWebPageIdFromTermId($id, $lang = 'fr') {
        $pageId = null;
        $term = \Drupal\taxonomy\Entity\Term::load($id);
        if(!empty($term)) {
            $pageId = $term->get('field_sit_fiche_id')->value;
        }
        return $pageId;
    }
    /**
     * Retounre l'URL de Pylot Bridge telle qu'enregistrée dans les paramètres
     * @return array|mixed|null URL de Pylot Bridge
     */
    public static function getUrlBridge() {
        $config = \Drupal::config('pylot_bridge.bridgeconfig');
        return $config->get('url_bridge');
    }
    /**
     * Retounre l'id de site Pylot Bridge telle qu'enregistré dans les paramètres
     * @return array|mixed|null id de site Pylot Bridge
     */
    public static function getSiteBridge() {
        $config = \Drupal::config('pylot_bridge.bridgeconfig');
        return $config->get('bridge_site');
        // fiche_root_url
    }

    /**
     * Créé un terme de la taxonomie rubrique_sit à partir d'une liste web Bridge
     * @param $webList
     * @param string $lang
     * @return \Drupal\Core\Entity\ContentEntityBase|\Drupal\Core\Entity\EntityBase|\Drupal\Core\Entity\EntityInterface|Term|\stdClass|string|null
     * @throws \Drupal\Core\Entity\EntityStorageException
     */
    public static function createTermFromWebList($webList, $language='') {

        $res = new \stdClass();
        $res->success = false;
        $res->term = null;
        $term = null;
        $slug = $webList->slug;
        $permalink = $webList->permalinkUrl;
        $title = $webList->title;
        $description = $webList->description;
        $langs = array();
        if(empty($language)) {
            $langs = BridgeUtils::getLanguagesList();
        } elseif(is_string($language) ) {
            $langs = array($language);
        }

        foreach($langs as $lang) {
            $langUpper = strtoupper($lang);

            if ($lang !== 'fr') {
                $slug = $webList->slugTranslation->$langUpper;
                $permalink = isset($webList->permalinkUrlTranslation->$langUpper) ? $webList->permalinkUrlTranslation->$langUpper : null;
                $title = $webList->titleTranslations->$langUpper;
                $description = $webList->descriptionTranslations->$langUpper;
            }
            // Patch 18/05/2022 : si pas de titre en langue, on passe
            if(empty($title))
                continue;

            // Ici on gère le permalien de destination : en fait on ne créé par de term pour la liste originale
            /*
            if(!empty($webList->destinationWebListId)) {
                $permalink = $webList->destinationWebListPermalinkUrl;
                if ($lang !== 'fr') {
                    $permalink = isset($webList->destinationWebListPermalinkUrlTranslation->$langUpper) ? $webList->destinationWebListPermalinkUrlTranslation->$langUpper : null;
                }
            }
            */
            /* Drupal :on exploite pas le slug
            if (!empty($title) && empty($slug)) {
                $res->errorMsg = "ERREUR 1 : Slug vide dans la liste n°" . $webList[0]->id . " (" . $lang . ")";
                return $res;
            }
            */

            if (empty($title) && empty($slug)) {
                continue;
            }
            $term = null;

            // On cherche si le term (catégorie) existe
            $tid = BridgeUtils::getTermIdByWeblistId($webList->id, 'fr');
            // S'il n'existe pas || $term == '' on le crée
            if (!isset($tid) || empty($tid)) {
                // Create the taxonomy term.
                $options = array(
                    'name' => $title,
                    "description" => [
                        'value' => $description,
                        'format' => 'full_html',
                    ],
                    'langcode' => 'fr',
                    'vid' => 'rubrique_sit',
                    'field_sit_list_id' => $webList->id,
//                    'field_dont_create_alias' => $webList->doNotCreateRewriteRuleForList,
                    'field_sit_fiche_id' => $webList->ficheId,
                );

                $term = Term::create($options);
                // Save the taxonomy term.
                $tid = $term->save();
            } else {
                $term = \Drupal\taxonomy\Entity\Term::load($tid);
            }
            if($lang == 'fr') {

            } else {
                $termFr = \Drupal\taxonomy\Entity\Term::load($tid);
                if(! $termFr->hasTranslation($lang)) {
                    $term = $termFr->addTranslation($lang);
                } else {
                    $term = $termFr->getTranslation($lang);
                }
            }


            // On met à jour l'alias et les champs additionnels
            if (!empty($permalink)) {
                if (substr($permalink, -1, 1) == '/')
                    $permalink = substr($permalink, 0, -1);
                if (substr($permalink, 0, 1) == '/')
                    $permalink = substr($permalink, 1);
                if (substr($permalink, 0, 1) == '/')
                    $permalink = substr($permalink, 1);
                $permalink = '/' . $permalink;
            } else {
                $permalink = '';
            }

            // On mémorise l'id de weblist dans le meta du terme
            $term->name->setValue($title);
            $term->description->setValue([
                'value' => $description,
                'format' => 'full_html',
            ]);

            $term->field_sit_list_id->setValue($webList->id);
            // $term->field_dont_create_alias->setValue($webList->doNotCreateRewriteRuleForList) ;
            // $term->field_id_liste_destination->setValue($webList->destinationWebListId) ;
//        $term->field_sit_fiche_id->setValue($webList->ficheId) ;
            $tid = $term->save();
            // $term = \Drupal\taxonomy\Entity\Term::load($tid);
            // $name = $term->get('name')->value;
            // On met à jour le lien du terme
            self::updateTermPermalink($term, $permalink, $lang);
        }


        return $term;
    }

    /**
     * Met à jour le lien d'un terme de taxonomie
     * @param $term Objet terme à mettre à jour
     * @param $permalink : lien
     * @param $lang : code de langue
     * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
     * @throws \Drupal\Core\Entity\EntityStorageException
     */
    public static function updateTermPermalink($term, $permalink = '', $lang = 'fr') {

        $path = '/taxonomy/term/' . $term->get('tid')->value;

        $alias_manager = \Drupal::entityTypeManager()->getStorage('path_alias');
        // On recherche s'il existe un alias pour ce node
        $aliasNode = $alias_manager->loadByProperties([
            'path'     => $path,
            'langcode' => $lang
        ]);

        // Pour Drupal, on doit s'assurer que lalias commence bien par un /
        if(!empty($permalink) && mb_substr($permalink,0,1) !== '/')
            $permalink = '/' . $permalink;

        // Pour Drupal, on ne doit pas mettre de préfixe de langue dans l'alias
        if(!empty($permalink) && mb_substr($permalink,0,4) === '/' . $lang . '/')
            $permalink = mb_substr($permalink,3);


        // On force l'alias du node - on le créé s'il n'y en a pas
        if(empty($aliasNode) && !empty($permalink)) {
            $alias_manager->create([
                'path'     => $path,
                'alias'    => $permalink,
                'langcode' => $lang
            ])->save();
        } else {
            if(!empty($permalink)) {
                foreach ($aliasNode as $alias_object) {
                    $alias_object->alias = $permalink;
                    $alias_object->save();
                }
            } else {
                // On a enlevé le permalien perso de Bridge, on enlève pas l'alias pour ne pas casser un travail de réécriture par pattern fait en externe
                // Mais il faut que l'utilisteur entre dans le terme te le réenregistre
            }
        }
    }

    /**
     * Renvoit un objet Fiche SIT du CMS à partir d'un code produit
     * @param $productCode Code produit
     * @param string $lang Langue
     * @return false|mixed Objet Fiche SIT CMS
     * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
     */
    public static function findPostByProductCode($productCode, $lang = 'fr') {
        $node_manager = \Drupal::entityTypeManager()->getStorage('node');
        $nodes = $node_manager->loadByProperties([
            'field_code_sit'     => $productCode,
            'langcode' => $lang
        ]);
        if(is_array($nodes) && count($nodes) > 0) {
            return reset($nodes);
        } else {
            return false;
        }
    }

    /**
     * Retourne la liste de tous les termes de taxonomie liés aux listes Bridge
     * @param string $lang langue
     * @return array|\Drupal\Core\Entity\EntityInterface[] tableau de d'objets termes
     * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
     */
    public static function getAllBridgeTerms($lang = 'fr') {
        $tids = \Drupal::entityQuery('taxonomy_term')
            ->condition('vid', 'rubrique_sit')
            ->condition('langcode', $lang)
            ->execute();

        if(!empty($tids)) {
            $controller = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
            $entities = $controller->loadMultiple($tids);
            return $entities;
        } else {
            return array();
        }

    }

    /**
     * Retourne l'id du bock photos par défaut de Pylot Bridge tel qu'enregistré dans les paramètres
     * @return array|mixed|null id de bloc photos
     */
    public static function getIdBlockPhotos() {
        $config = \Drupal::config('pylot_bridge.bridgeconfig');
        return $config->get('product_media_block');
        // fiche_root_url
    }

    /**
     * getElevationData
     * Renvoit les données d'altitude du tracé d'une fiche
     * @param $fiche objet product tel que retourné par l'API Bridge
     * @return mixed|void|null données altimétriques du tracé
     */
    public static function getElevationData($fiche)
    {
        if (isset($fiche) && is_object($fiche) && property_exists($fiche, 'kml') && !empty($fiche->kml)) {
            $user = 'bridge';
            $urlkml = $fiche->kml;
            $key = md5('RT$4861' . $user . '_d?cnj' . $urlkml . 'WP$$?0');

            $urlRecup = 'https://pubalsace.dnconsultants.fr/index.php?option=com_leipub&task=itineraire.getElevationData&user=' . $user . '&key=' . $key . '&urlkml=' . urlencode($urlkml);
            $data = file_get_contents($urlRecup);
            if (!empty($data))
                return json_decode($data);
            else
                return null;
        }
    }

    /**
     * getProductDispos
     * Renvoit les disponibilités en temps réel d'une fiche (donnée chaudes)
     * @param $fiche
     * @return mixed|null
     */
    public static function getProductDispos($fiche) {
        $urlBridge = self::getUrlBridge();
        $json = file_get_contents( $urlBridge . '/product/getDispos?productCode=' . $fiche->productCode );
        $dispos = json_decode( $json );
        if(!empty($dispos))
            return $dispos;
        else
            return null;
    }

    /**
     * getBridgeParameters
     * Retourne un tableau contenant les paramètres généraux du plugin Pylot Bridge ainsi que des données d'environnement
     * @return array
     */
    public static function getBridgeParameters() {
        // On récupère les paramètres de Bridge pour les passer au Twig
        $bridgeParameters = array();
        $config = \Drupal::config('pylot_bridge.bridgeconfig');
        $bridgeParameters['url_bridge'] = $config->get('url_bridge');
        $bridgeParameters['custom_css_sit'] = $config->get('custom_css_sit');
        $bridgeParameters['custom_js_sit'] = $config->get('custom_js_sit');
        $bridgeParameters['bridge_site'] = $config->get('bridge_site');
        $bridgeParameters['marker_map'] = $config->get('marker_map');
        $bridgeParameters['marker_map_actif'] = $config->get('marker_map_actif');
        $bridgeParameters['product_media_block'] = $config->get('product_media_block');

        $rootUrl = \Drupal::request()->getSchemeAndHttpHost();
        $module_handler = \Drupal::service('module_handler');
        $bridgePath = $module_handler->getModule('pylot_bridge')->getPath();
        $bridgeParameters['url_root'] = $rootUrl;
        $bridgeParameters['url_module_bridge'] = $bridgePath;
        $uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
        $bridgeParameters['currentUrl'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$uri_parts[0]" ;

        // Paramètres d'URL pour la pagination des listes
        $paginationUrlParams = $_SERVER['QUERY_STRING'] ;
        if(!empty($paginationUrlParams)) {
            $tabUrlParams = array();
            parse_str($paginationUrlParams, $tabUrlParams);
            // On retire le paraetre brpa de l'url courante pour construire les liens de pagination
            if(isset($tabUrlParams['brpa'])) {
                unset($tabUrlParams['brpa']);
                $paginationUrlParams = http_build_query($tabUrlParams);
            }
            if(!empty($paginationUrlParams))
                $paginationUrlParams = '?' . $paginationUrlParams;
        }
        $bridgeParameters['paginationUrlParams'] = $paginationUrlParams;

        if(empty($bridgeParameters['marker_map']))
            $bridgeParameters['marker_map'] = $bridgeParameters['url_root']  . '/'  . $bridgeParameters['url_module_bridge'] . '/assets/img/map-marker-icon.png';

        if(empty($bridgeParameters['marker_map_actif']))
            $bridgeParameters['marker_map_actif'] = $bridgeParameters['marker_map'] ;

        $bridgeParameters['marker_home'] = $bridgeParameters['url_root']  . '/'  . $bridgeParameters['url_module_bridge'] . '/assets/img/map-marker-home.png';

        // On donne des dimensions proportionnelles au marker avec en visée 50 px de haut
        $image_info = getImageSize($bridgeParameters['marker_map']);

        // Protection contre les erreurs de paramétrage
        if(is_array($image_info) && count($image_info) > 1 && $image_info[1] != 0 && $image_info[0] != 0) {
            $ratio = 50 / $image_info[1];
            $bridgeParameters['marker_map_width'] = $image_info[0] * $ratio;
            $bridgeParameters['marker_map_height'] = $image_info[1] * $ratio;

            $image_info = getImageSize($bridgeParameters['marker_map_actif']);
            $ratio = 50 / $image_info[1];
            $bridgeParameters['marker_map_actif_width'] = $image_info[0] * $ratio;
            $bridgeParameters['marker_map_actif_height'] = $image_info[1] * $ratio;
        } else {
            $bridgeParameters['marker_map_width'] = 30;
            $bridgeParameters['marker_map_height'] = 40;
            $bridgeParameters['marker_map_actif_width'] = 30;
            $bridgeParameters['marker_map_actif_height'] = 40;
        }


        $bridgeParameters['maps_center_lat'] = $config->get('maps_center_lat');
        if(empty($bridgeParameters['maps_center_lat']))
            $bridgeParameters['maps_center_lat'] = 48.856614;
        $bridgeParameters['maps_center_lon'] = $config->get('maps_center_lon');
        if(empty($bridgeParameters['maps_center_lon']))
            $bridgeParameters['maps_center_lon'] = 2.3522219;

        // On ajoute aux paramètres d'application les constantes système de manière à tout avoir sous la main et ne pas avoir à faire trop d'appels
        $bridgeParameters['url_ajax_liste'] = $rootUrl . '/pylot_bridge/list_products_json' ;
        $bridgeParameters['url_ajax_liste_json_poi'] = $rootUrl . '/pylot_bridge/list_products_json_for_map_poi' ;
        $bridgeParameters['url_ajax_send_email_recaptcha'] = $rootUrl . '/pylot_bridge/send_email_recaptcha' ;

        // Et la langue courante tant qu'on y est
        $bridgeParameters['lang'] = BridgeCmsAbstractLayer::getCurrentLanguage();

        // Facebook App Id pour la social box
        $bridgeParameters['facebookAppId'] = '495036869019149';
        $bridgeParameters['facebookAppNonce'] = 'awkTZqCe';


        return $bridgeParameters;
    }

    /**
     * prepareListDataForRender
     * Prépare un tableau contenant les données à passer au renderer de liste
     * @param $data objet data tel que renvoyé par BridgeUtils::getListData
     * @return array tableau associatif contenant les donnée sutilisées par les vues listes de rendu
     */
    public static function prepareListDataForRender($data) {
        $lang = BridgeUtils::getLanguage();
        $langUpper = strtoupper($lang);

        // Pour que tout soit objet : plus simple
        $data = json_decode(json_encode($data));

        // On stocke les infos relatives à l'URL et aux filtres et tris passés en paramètre
        $uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
        if(is_object($data) && isset($data->infos) && is_object($data->infos)) {
            $data->infos->currentUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$uri_parts[0]" ;

            $data->infos->activeFilters = (isset($_REQUEST["braf"])) ? $_REQUEST["braf"] : "" ;
            $data->infos->activeItems = (isset($_REQUEST["brai"])) ? $_REQUEST["brai"] : "" ;
            $data->infos->activeSorts = (isset($_REQUEST["bras"])) ? $_REQUEST["bras"] : "" ;
            $data->infos->sortDirections = (isset($_REQUEST["brsd"])) ? $_REQUEST["brsd"] : "" ;
            $data->infos->httpRequest = $_REQUEST;
        }

        $bridgeParameters = json_decode(json_encode(BridgeUtils::getBridgeParameters()));

        // On calcule les url des pages suivantes et précédentes pour le rel=next/prev
        $separator = '&';
        if(empty($bridgeParameters->paginationUrlParams)) {
            $separator = '?';
        }

        if($data->infos->currentPage == 2 ) {
            // Pas de paramètre brpa pour la première page
            $data->infos->prevPageURL = $bridgeParameters->currentUrl . $bridgeParameters->paginationUrlParams ;
        } elseif($data->infos->currentPage > 2 ) {
            $data->infos->prevPageURL = $bridgeParameters->currentUrl . $bridgeParameters->paginationUrlParams . $separator . 'brpa=' . (((int)$data->infos->currentPage) - 1);
        } else {
            $data->infos->prevPageURL = '';
        }
        if($data->infos->currentPage < $data->infos->lastPage ) {
            $data->infos->nextPageURL = $bridgeParameters->currentUrl . $bridgeParameters->paginationUrlParams . $separator . 'brpa=' . (((int)$data->infos->currentPage) + 1);
        } else {
            $data->infos->nextPageURL = '';
        }


        // Pour réutilisation plus simple dans les templates de moteurs / filtres
        $moteur = null;
        if(isset($data->data->moteur))
            $moteur = $data->data->moteur;

        self::applyTranslations($data->data, $lang);

        // On enrichit la réponse avec les infos de la Weblist
        $pageTitle = '';
        if (!empty($data->data->title)) {
            $pageTitle = $data->data->title;
        }

        $pageDescription = '';
        if (!empty($data->data->description)) {
            $pageDescription = $data->data->description;
        }

        return array (
            'listInfos' => $data->infos,
            'listParameters' => $data->parameters,
            'data' => $data->data,
            'moteur' => $moteur,
            'bridgeParameters' => $bridgeParameters,
            'pageTitle' => $pageTitle,
            'pageDescription' => $pageDescription,
        );

    }

    /**
     * @param $object
     * @param $parameter
     * @return mixed|null
     */
    private static function getParameterInObject($object, $parameter)
    {
        if (
            isset($object->$parameter) &&
            !empty($object->$parameter)
        ) {
            return $object->$parameter;
        }

        return null;
    }

    /**
     * @param $data
     * @param string $lang
     */
    public static function applyTranslations(&$data, string $lang = NULL)
    {

        $langParam              = strtoupper(($lang ? $lang : BridgeUtils::getLanguage()));

        $titleTranslation       = self::getParameterInObject($data->titleTranslations, $langParam);
        $slugTranslation        = self::getParameterInObject($data->slugTranslation, $langParam);
        $overTitleTranslation   = self::getParameterInObject($data->overTitleTranslations, $langParam);
        $subTitleTranslation    = self::getParameterInObject($data->subTitleTranslations, $langParam);
        $descriptionTranslation = self::getParameterInObject($data->descriptionTranslation, $langParam);

        if (!empty($titleTranslation)) {
            $data->title = $titleTranslation;
        }

        if (!empty($slugTranslation)) {
            $data->slug = $slugTranslation;
        }

        if (!empty($overTitleTranslation)) {
            $data->overTitle = $overTitleTranslation;
        }

        if (!empty($subTitleTranslation)) {
            $data->subTitle = $subTitleTranslation;
        }

        if (!empty($descriptionTranslation)) {
            $data->description = $descriptionTranslation;
        }

    }

    /**
     * prepareMapDataForRender
     * Prépare un tableau contenant les données à passer au renderer de carte interactive
     * @param $data objet data tel que renvoyé par BridgeUtils::getMapData
     * @return array tableau associatif contenant les données utilisées par les vues map
     */
    public static function prepareMapDataForRender($data) {
        $lang = BridgeUtils::getLanguage();
        $langUpper = strtoupper($lang);

        // Pour que tout soit objet : plus simple
        $data = json_decode(json_encode($data));

        // On stocke les infos relatives à l'URL et aux filtres et tris passés en paramètre
        $uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
        if(is_object($data) && isset($data->infos) && is_object($data->infos)) {
            $data->infos->currentUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$uri_parts[0]" ;
            $data->infos->activeFilters = (isset($_REQUEST["braf"])) ? $_REQUEST["braf"] : "" ;
            $data->infos->activeItems = (isset($_REQUEST["brai"])) ? $_REQUEST["brai"] : "" ;
            $data->infos->httpRequest = $_REQUEST;
        }

        $bridgeParameters = json_decode(json_encode(BridgeUtils::getBridgeParameters()));

        return array (
            'mapInfos' => $data->infos,
            'mapParameters' => $data->parameters,
            'data' => $data->data,
            'bridgeParameters' => $bridgeParameters,
        );
    }
    /**
     * prepareMapPopupDataForRender
     * Prépare un tableau contenant les données à passer au renderer de popup de carte interactive
     * @param $data objet data tel que renvoyé par BridgeUtils::getMapData
     * @return array tableau associatif contenant les données utilisées par les vues map
     */
    public static function prepareMapPopupDataForRender($data) {
        $lang = BridgeUtils::getLanguage();
        $langUpper = strtoupper($lang);

        // Pour que tout soit objet : plus simple
        $data = json_decode(json_encode($data));

        // On stocke les infos relatives à l'URL et aux filtres et tris passés en paramètre
        $uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
        if(is_object($data) && isset($data->infos) && is_object($data->infos)) {
            $data->infos->currentUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$uri_parts[0]" ;
            $data->infos->activeFilters = (isset($_REQUEST["braf"])) ? $_REQUEST["braf"] : "" ;
            $data->infos->activeItems = (isset($_REQUEST["brai"])) ? $_REQUEST["brai"] : "" ;
            $data->infos->httpRequest = $_REQUEST;
        }

        $bridgeParameters = json_decode(json_encode(BridgeUtils::getBridgeParameters()));

        return array (
            'mapPopupInfos' => $data->infos,
            'data' => $data->data,
            'bridgeParameters' => $bridgeParameters,
        );
    }
    /**
     * prepareFicheDataForRender
     * Prépare un tableau contenant les données à passer au renderer de Fiche
     * @param $data objet data tel que renvoyé par BridgeUtils::getDataFiche
     * @return array tableau associatif contenant les données utilisées par les vues fiches de rendu
     */
    public static function prepareFicheDataForRender($data) {
        $lang = BridgeUtils::getLanguage();
        $langUpper = strtoupper($lang);

        // Pour que tout soit objet : plus simple
        $data = json_decode(json_encode($data));

        // Si pas d'infos, on retourne false
        if(!is_object($data) || !isset($data->product)) {
            return array();
        }
        if(is_object($data) && (!isset($data->infos) || !is_object($data->infos))) {
            $data->infos = new \stdClass();
        }

        // On stocke les infos relatives à l'URL et aux filtres et tris passés en paramètre
        $uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
        if(is_object($data) && isset($data->infos) && is_object($data->infos)) {
            $data->infos->currentUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$uri_parts[0]" ;
            $data->infos->activeFilters = (isset($_REQUEST["braf"])) ? $_REQUEST["braf"] : "" ;
            $data->infos->activeItems = (isset($_REQUEST["brai"])) ? $_REQUEST["brai"] : "" ;
            $data->infos->httpRequest = $_REQUEST;
        }

        $bridgeParameters = json_decode(json_encode(BridgeUtils::getBridgeParameters()));

        // Initilisation des fiches associées
        $coupledLegacy = BridgeUtils::getCoupledProducts($data->product);
        $coupledProducts = BridgeUtils::getCoupledProductsData($data->product);

        return array (
            'ficheInfos' => $data->infos,
            'ficheParameters' => $data->parameters,
            'dataFicheBridge' => $data,
            'ficheTemplate' => $data->ficheTemplate,
            'fiche' => $data->product,
            'bridgeParameters' => $bridgeParameters,
            'coupledLegacy' => $coupledLegacy,
            'coupledProducts' => $coupledProducts,
        );

    }

    /**
     * getLanguagePrefix
     * Fonction indiquant le préfixe de langue à ajouter aux liens URL - sans objet dans Wordpress
     */
    public static function getLanguagePrefix() {
        if($prefixes = \Drupal::config('language.negotiation')->get('url.prefixes')) {
            $language = \Drupal::languageManager()->getCurrentLanguage()->getId();

            return "/" . $prefixes[$language];
        }
        return '';
    }

    /**
     * Retourne un tableau des librairies CSS / Js utilisée pour afficher les listes / moteurs
     * @return array
     */
    public static function getListLibraries() {
        $libraries = array();
        $libraries[] = 'core/jquery';
        $libraries[] = 'pylot_bridge/uikit';
        $libraries[] = 'pylot_bridge/bridge-autocomplete';
        $libraries[] = 'pylot_bridge/fontawesome-v6';
        $libraries[] = 'pylot_bridge/leaflet';
        $libraries[] = 'pylot_bridge/leaflet-addons-bridge';
        $libraries[] = 'pylot_bridge/dncleafletservices';
        $libraries[] = 'pylot_bridge/bridge-base';
        $libraries[] = 'pylot_bridge/mc-datepicker';
        $libraries[] = 'pylot_bridge/bridge-datepicker';
        $libraries[] = 'pylot_bridge/bridge-datepicker-locale';
        $libraries[] = 'pylot_bridge/calendar';
        $libraries[] = 'pylot_bridge/bridge-pagination';
        $libraries[] = 'pylot_bridge/bridge-moteur';
        $libraries[] = 'pylot_bridge/bridge-custom';
        return $libraries;
    }

    /**
     * Retourne un tableau des librairies CSS / Js utilisée pour afficher une fiche de détail
     * @return array
     */
    public static function getFicheLibraries() {
        $libraries = array();
        // On injecte les scripts et feuilles de style nécessaires
        $libraries[] = 'core/jquery';
        $libraries[] = 'pylot_bridge/uikit';
        $libraries[] = 'pylot_bridge/fontawesome-v6';
        $libraries[] = 'pylot_bridge/leaflet';
        $libraries[] = 'pylot_bridge/leaflet-addons-bridge';
        $libraries[] = 'pylot_bridge/dncleafletservices';
        $libraries[] = 'pylot_bridge/calendar';
        $libraries[] = 'pylot_bridge/calendar';
        $libraries[] = 'pylot_bridge/mc-datepicker';
        $libraries[] = 'pylot_bridge/bridge-datepicker';
        $libraries[] = 'pylot_bridge/bridge-datepicker-locale';
        $libraries[] = 'pylot_bridge/bridge-base';
        $libraries[] = 'pylot_bridge/bridge-custom';
        $libraries[] = 'pylot_bridge/chart-js';
        return $libraries;
    }

    /**
     * prepareListViewRenderer
     * Prépare l'affichage d'une liste dans une page (non ajax)
     * Attache les scripts et CSS et retourne un tableau associatif contenant toutes les données qui seront utilisées dans les vues
     * @param $data Objet data tel que renvoyé par BridgeUtils::prepareListdataForRender
     * @param $theme nom du fichier twig initial à charger
     * @return array tableau associatif contenant toutes les données qui seront utilisées dans les vues
     */
    public static function prepareListViewRenderer($data, $theme = 'liste_sit') {
        // On prépare le tableau qui contiendra toutes les données utilisées dans les vues de listes
        $libraries = self::getListLibraries();
        $build = array (
            '#theme' => $theme,
            '#attached' => array('library' => array()),
            '#children' => array(),
            '#listInfos' => $data['listInfos'],
            '#listParameters' => $data['listParameters'],
            '#data' => $data['data'],
            '#moteur' => $data['moteur'],
            '#bridgeParameters' => $data['bridgeParameters'],
            '#pageTitle' => $data['pageTitle'],
            '#libraries' => $libraries,
            '#pageDescription' => $data['pageDescription'],
        );

        // On injecte les scripts et feuilles de style nécessaires

        foreach( $libraries as $lib)
            $build['#attached']['library'][] = $lib;

        return $build;
    }

    /**
     * prepareFicheViewRenderer
     * Prépare l'affichage d'une fiche dans une page (non ajax)
     * Attache les scripts et CSS et retourne un tableau associatif contenant toutes les données qui seront utilisées dans les vues
     * @param $dataFiche Objet data tel que renvoyé par BridgeUtils::prepareFichedataForRender
     * @param $theme nom du fichier twig initial à charger
     * @return array tableau associatif contenant toutes les données qui seront utilisées dans les vues
     */
    public static function prepareFicheViewRenderer($datafiche, $theme = 'fiche_sit_ajax') {
        // On prépare le tableau qui contiendra toutes les données utilisées dans les vues de listes
        $libraries = self::getFicheLibraries();
        $build = array (
            '#theme' => $theme,
            '#attached' => array('library' => array()),
            '#children' => array(),
            '#libraries' => $libraries,
            '#content' => array(),
        );
        // Si erreur, on injecte le message dans la variable dédiée
        if(isset($datafiche->error) && !empty($datafiche->error)) {
            $build['#content']['body'] = array(
                'error' => $datafiche->error
            );
        } else {
            // On récupère les paramètres de Bridge pour les passer au Twig
            $bridgeParameters = BridgeUtils::getBridgeParameters();
            // $test = BridgeUtils::getElevationData($datafiche);

            $build['#content']['body'] = array(
                'fiche' => $datafiche,
                'bridgeParameters' => $bridgeParameters,
            );
        }

        // On injecte les scripts et feuilles de style nécessaires

        foreach( $libraries as $lib)
            $build['#attached']['library'][] = $lib;

        return $build;
    }


    /**
     * Retourne une liste de communes répondant aux paramètres de recherche passés
     * @param $attributes : paramètres d'affichage
     * @param $isAjax : si true, la fonction se comporte en mode ajax et ne retourne que les templateItems
     *
     * @return array
     */
    public static function getCitiesData($attributes) {

        $res = array(
            'success' => false,
            'message' => '',
            'data' => null,
            'infos' => array(),
            'parameters' => array()
        );

        $bridgeCredentials = self::getBridgeCredentials();
        $urlBridge = $bridgeCredentials->urlBridge;

        if(empty($urlBridge)) {
            $res['message'] = "L'URL de Bridge n'est pas définie - contrôlez les paramètres du plugin";
            return $res;
        }

        if(!is_array($attributes) || empty($attributes['id'])){
            $res['message'] = 'Le paramètre id est obligatoire dans le shortcode';
            return $res;
        }

        // La liste est filtrée par défaut en lisant les paramètres GET POST du contexte
        // l'attribut useRequestFilters permet de désactiver ce comportement
        if(!isset($attributes['useRequestFilters']))
            $attributes['useRequestFilters'] = true;

        // Variable utilisée à différents endroits du code
        $webListId = $attributes['id'];
        $res['infos']['debug'] = array() ;

        // Pagination des résultats
        // Première fiche
        $first = '1' ;

        $lang = self::getLanguage();
        $res['infos']['lang'] = $lang;


        // On ajoute les paramètres de filtre du moteur et les paramètres de tri
        $brParamsUrlFiltre = '';
        $brParamsUrlSort = '';

        if (count($_REQUEST) > 0 && $attributes['useRequestFilters']) {
            foreach ($_REQUEST as $key=>$value) {
                // braf: Bridge Active Filter
                // brai: Bridge Active Item
                if ($key == 'braf' || $key == 'brai' || (strlen($key) > 6 && substr($key,0,6) == 'brflt_')) {
                    // On doit encoder en URI les paramètres de type valeur saisie par l'internaute
                    if (strpos($key, '_value') !== false)
                        $brParamsUrlFiltre .= "&$key=" . urlencode($value);
                    elseif (strpos($key, '_city') !== false)
                        $brParamsUrlFiltre .= "&$key=" . urlencode($value);
                    else
                        $brParamsUrlFiltre .= "&$key=$value";
                }
            }
        }

        // JM 22/03/2022 : on change le nom du paramètre (update -> v2), moins risqué
        $url = $urlBridge . "/weblist/getCities/" . $webListId . "?v2=1&lang=" . $lang ;

        // Filtres et tris forcés
        if(isset($attributes['braf']) && !empty($attributes['braf'])) {
            $brParamsUrlFiltre .= "&braf=" . $attributes['braf'];
        }
        if(isset($attributes['brai']) && !empty($attributes['brai'])) {
            $brParamsUrlFiltre .= "&brai=" . $attributes['brai'];
        }
        if(isset($attributes['bras']) && !empty($attributes['bras'])) {
            $brParamsUrlSort .= "&bras=" . $attributes['bras'];
        }
        if(isset($attributes['brsd']) && !empty($attributes['brsd'])) {
            $brParamsUrlSort .= "&brsd=" . $attributes['brsd'];
        }

        if ($brParamsUrlFiltre != '') {
            $url .= $brParamsUrlFiltre;
        }

        if ($brParamsUrlSort != '') {
            $url .= $brParamsUrlSort;
        }

        // Feat : 25/05/2022 : possibilité de passer une liste de productCodes pour forcer les fiches à afficher dans le shortcode
        $productCodes = '';
        if(isset($attributes['product_codes']) && !empty($attributes['product_codes'])) {
            $productCodes = $attributes['product_codes'];
            $url .= "&productCodes=" . $productCodes;
        }
        // Support des requetes minimalistes pour les points sur la carte
        if(isset($attributes['minimal_select']) && !empty($attributes['minimal_select'])) {
            $url .= "&minimalSelect=1" ;
        }

        if(isset($_REQUEST['dncdebug']) && $_REQUEST['dncdebug'] == 'Y')
            echo "<a href='".$url."' target='_blank' class='uk-button uk-button-primary'>Détails de liste</a>";

        $tmpData = file_get_contents($url);
        $data = null;

        if (!empty($tmpData)) {
            $tmpDataDecoded = json_decode($tmpData);
            if ($tmpDataDecoded->success && $tmpDataDecoded->data) {
                $data = $tmpDataDecoded->data;
            } else {
                $res['message'] = $tmpDataDecoded->message;
                return $res;
            }
        } else {
            $res['message'] = 'Échec de la récupération de la liste';
            return $res;
        }

        if (!is_array($data)) {
            $res['message'] = "ERREUR d'appel à Bridge : aucune donnée valide renvoyée";
            return $res;
        }


        $res['data'] = $data;
        $res['success'] = true;
        return $res;

    }


    /**
     * Retourne le contneu svg de l'image
     * @param String $prop propriété de l'objet qu'on souhaite récupérer
     * @param Object $fiche objet product
     * @param int $criterionCode code de critère
     * @param int|null $modalityCode code de modalité
     * @return string le résultat sous forme de chaine de caractères (réduit le volume de code de l'appel)
     */
    public static function bridgeInlineSvg(String $urlImage) {
        return BridgeBlockService::bridgeInlineSvg($urlImage);
    }

    /**
     * Retourne true si l'image dont l'URL est passée est un fichier SVG
     * @param String $urlImage url de l'image dnt on souhaite savoir si c'est du SVG
     * @return bool true si l'image est svg
    */
    public static function bridgeIsSvg(String $urlImage) {
        if(substr(strtolower($urlImage), -3) == 'svg')
            return true;
        else
            return false;
    }

    public static function parseCss($css) {
        preg_match_all( '/(?ims)([a-z0-9\s\.\:#_\-@,]+)\{([^\}]*)\}/', $css, $arr);
        $result = array();
        foreach ($arr[0] as $i => $x){
            $selector = trim($arr[1][$i]);
            $rules = explode(';', trim($arr[2][$i]));
            $rules_arr = array();
            foreach ($rules as $strRule){
                if (!empty($strRule)){
                    $rule = explode(":", $strRule);
                    $rules_arr[trim($rule[0])] = trim($rule[1]);
                }
            }

            $selectors = explode(',', trim($selector));
            foreach ($selectors as $strSel){
                $result[$strSel] = $rules_arr;
            }
        }
        return $result;
    }

    /**
     * resize_image
     * Redimensionneur dynamique d'images
     * @param string $file url de l'image source
     * @param string $mode mode de redimensionnement
     * - ajust pour ajuster aux dimensions passées (ne déborde pas)
     * - remplir : redimensionne l'image pour remplir le cadre de dimensions passées sans la recadrer
     * - crop : redimensionne l'image pour qu'elle remplisse le cadre puis recadre selon les dimensions
     * - deform : pour déformer l'image et forcer les dimensions exactes
     * @param string|int $selwidth : largeur souhaitée en pixels
     * @param string|int $selheight : hauteur souhaitée en pixels
     * @param string|int $quality : qaulité de compression (qualité / 100)
     * @param bool $debug : mode debug
     * @return array|false[]|string|void
     */
    public static function resize_image($file = '', $mode = "ajust", $selwidth = "", $selheight = "", $quality = 60, $debug = false)
    {

        $file = str_replace("http://http://", "http://", $file);
        $file = str_replace("http://https://", "https://", $file);
        $res = array('success' => false);

        if($debug) {
            ini_set('display_errors','on');
            error_reporting(E_ALL);
        }

        if ($file == "")
            return $res;
        if ($mode == "")
            $mode = "ajust";
        if ($selwidth == "" && $selheight == "")
            $selwidth = "100";


        $extension = 'jpg';
        $image_info = getImageSize($file);
        $width = $image_info[0];
        $height = $image_info[1];
        $image_type = $image_info[2];
        switch ($image_info['mime']) {
            case 'image/gif':
                $extension = 'gif';
                break;
            case 'image/jpeg':
                $extension = 'jpg';
                break;
            case 'image/png':
                $extension = 'png';
                break;
            default:
                // handle errors
                break;
        }

        //Test si l'image a déjà été redimensionnée avec exactement les mêmes paramètres
        $file = str_replace(' ', '%20', $file);
        if ($selwidth == "" && $selheight == "") {
            $selwidth = $width;
            $selheight = $height;
        }

        if ($selwidth != "" || $selheight != "") {
            $max_width = $selwidth * 1;
            $max_height = $selheight * 1;
            // On charge l'image dans $src
            switch ($image_type) {
                case 1:
                    $src = imagecreatefromgif($file);
                    $extension = 'gif';
                    break;
                case 2:
                    $src = imagecreatefromjpeg($file);
                    $extension = 'jpg';
                    break;
                case 3:
                    $src = imagecreatefrompng($file);
                    $extension = 'png';
                    break;
                default:
                    return '';
                    break;
            }
        }

        $x_ratio = $max_width / $width;
        $y_ratio = $max_height / $height;

        if ($mode == "ajust") {
            if ($selwidth == "")
                $ratio = $y_ratio;
            elseif ($selheight == "")
                $ratio = $x_ratio;
            else
                $ratio = min($x_ratio, $y_ratio);
            $tn_width = ceil($ratio * $width);
            $tn_height = ceil($ratio * $height);
        } elseif ($mode == "remplir" || $mode == "tronq" || $mode == "crop") {
            if ($selwidth == "")
                $ratio = $y_ratio;
            elseif ($selheight == "")
                $ratio = $x_ratio;
            else
                $ratio = max($x_ratio, $y_ratio);
            $tn_width = ceil($ratio * $width);
            $tn_height = ceil($ratio * $height);
        } else {
            $tn_width = ceil($x_ratio * $width);
            $tn_height = ceil($y_ratio * $height);
        }
        $tmp = imagecreatetruecolor($tn_width, $tn_height);
        /* Check if this image is PNG or GIF, then set if Transparent */
        // TODO : Ne permet pas de faire un tronq sur des png et gif!!!
        if ($mode == "tronq" || $mode == "crop") {
            if ($tn_height > $max_height || $tn_width > $max_width) {
                $tmp = imagecreatetruecolor($max_width, $max_height);
            }
            imagecopyresampled($tmp, $src, 0, 0, round(($tn_width / $ratio / 2) - ($max_width / $ratio / 2)), round(($tn_height / $ratio / 2) - ($max_height / $ratio / 2)), $tn_width, $tn_height, $width, $height);
        } else {
            if (($image_type == 1) or ($image_type == 3)) {
                if ($image_type == 1) {
                    imagecolortransparent($tmp, imagecolorallocatealpha($tmp, 255, 255, 255, 127));
                }
                imagealphablending($tmp, false);
                imagesavealpha($tmp, true);
                $transparent = imagecolorallocatealpha($tmp, 255, 255, 255, 127);
                imagefilledrectangle($tmp, 0, 0, $tn_width, $tn_height, $transparent);
            }
            // ND 22/01/2016 : probème avec double utilisation sur gif transprent : tranpasrent devient blanc -> resized au lieu de resampled
            //imagecopyresampled($tmp, $src, 0, 0, 0, 0, $tn_width, $tn_height, $width, $height);
            imagecopyresized($tmp, $src, 0, 0, 0, 0, $tn_width, $tn_height, $width, $height);
        }


        switch ($image_type) {
            case 1:
                ob_start();
                imagegif($tmp, null); // et on l'affiche
                $binaryData = ob_get_clean();
                imagedestroy($tmp);
                $res = array('success' => true, 'ext' => 'gif', 'data' => $binaryData);
                break;

            case 2:
                ob_start();
                imagejpeg($tmp, null, $quality);
                $binaryData = ob_get_clean();
                imagedestroy($tmp);
                $res = array('success' => true, 'ext' => 'jpg', 'data' => $binaryData);
                break;

            case 3:
                ob_start();
                // Attention ! la quelité PNG va de 0 à 9
                if ($quality > 9)
                    $quality = 8;
                // imagepng($tmp, $nomfic, $quality);
                imagepng($tmp, null, $quality);
                $binaryData = ob_get_clean();
                imagedestroy($tmp);
                $res = array('success' => true, 'ext' => 'png', 'data' => $binaryData);
                break;

            default:
                break;
        }
        return $res;
    }


    public static function getBridgeCategories($id = null, $lang = 'fr') {
        $ret = new \stdClass();
        $ret->success = false;
        $ret->errorMsg = '';
        $ret->data = null;

        $urlBridge = BridgeUtils::getUrlBridge();

        if (empty($urlBridge)) {
            $ret->errorMsg = 'URL Bridge indéfinie - contrôlez les paramètres';
            return $ret;
        }

        // on envoie vers la bonne route
        // JM 22/03/2022 : on change le nom du paramètre (update -> v2), moins risqué
        $urlImport = $urlBridge . "/weblist/getForImport?v2=1";

        // si un site est renseigné, on ne récupère que les listes de ce site
        $siteId = BridgeUtils::getSiteBridge();

        if (empty($siteId)) {
            $ret->errorMsg = 'Site indéfini dans les paramètres - contrôlez les paramètres';
            return $ret;
        }

        $urlImport = $urlImport . '&site=' . $siteId ;

        if(empty($lang)) $lang = 'fr';
        $urlImport = $urlImport . '&language=' . $lang ;

        if (! empty($id)) {
            $urlImport = $urlImport . '&id=' . $id;
        }
        $tmpData = file_get_contents($urlImport);

        if (!empty($tmpData)) {
            $decodedData = json_decode($tmpData);
            if ($decodedData->success) {
                $ret->data = $decodedData->data;
                $ret->success = true;
            } else {
                $ret->errorMsg = $decodedData->message;
            }
        } else {
            $ret->errorMsg = 'Erreur de récupération des données Bridge';
        }

        return $ret;
    }
}