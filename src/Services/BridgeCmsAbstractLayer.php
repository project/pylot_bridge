<?php
namespace Drupal\pylot_bridge\Services ;

class BridgeCmsAbstractLayer {

    /**
     * Retourne vrai si le script a été enregistré / chargé
     * @param $handle : clé texte du script
     * @param $list : statut à vérifier : enqueued, registered
     * @return mixed
     */
    public static function scriptIs($handle, $list ) {
        return wp_script_is($handle, $list  = 'enqueued' );
    }
    /**
     * Ajouter un script JS à la page courante
     * Si src est absent, le handle du script doit faire référence à un script préalablement déclaré avec registerScript
     * @param $handle : id texte du script
     * @param string $src : url du script
     * @param array $deps : tableau de dépendances (liste d'id texte)
     * @param false $ver : version
     * @param false $in_footer : charger dans le footer
     * @return mixed
     */
    public static function registerScript($handle, $src = "", $deps = array(), $ver = false, $in_footer = false ) {
        return wp_register_script($handle, $src, $deps, $ver, $in_footer);
    }

    /**
     * Ajouter un script JS à la page courante
     * Si src est absent, le handle du script doit faire référence à un script préalablement déclaré avec registerScript
     * @param $handle : id texte du script
     * @param string $src : url du script
     * @param array $deps : tableau de dépendances (liste d'id texte)
     * @param false $ver : version
     * @param false $in_footer : charger dans le footer
     * @return mixed
     */
    public static function enqueueScript($handle, $src = "", $deps = array(), $ver = false, $in_footer = false ) {
        return true;
        // return wp_enqueue_script($handle, $src, $deps, $ver, $in_footer);
    }

    /**
     * Référencer une feuille de style CSS : associer un id texte à une URL
     * Si src est absent, le handle du css doit faire référence à un fichier préalablement déclaré avec registerStyle
     * @param $handle : id texte du css
     * @param string $src : url du css
     * @param array $deps : tableau de dépendances (liste d'id texte)
     * @param false $ver : version
     * @param string $media
     * @return mixed
     */
    public static function registerStyle($handle, $src = "", $deps = array(), $ver = false, $media = 'all' ) {
        return true;
        //return wp_register_style($handle, $src, $deps, $ver, $media);
    }

    /**
     * Ajouter une feuille de style CSS à la page courante
     * Si src est absent, le handle du css doit faire référence à un fichier préalablement déclaré avec registerStyle
     * @param $handle : id texte du css
     * @param string $src : url du css
     * @param array $deps : tableau de dépendances (liste d'id texte)
     * @param false $ver : version
     * @param string $media
     * @return mixed
     */
    public static function enqueueStyle($handle, $src = "", $deps = array(), $ver = false, $media = 'all' ) {
        return true;
        // return wp_enqueue_style($handle, $src, $deps, $ver, $media);
    }

    /**
     * @return Retourne le code de la langue courante (2 lettres en minuscules / fr par défaut)
     */
    public static function getCurrentLanguage()
    {
        // Get current language code
        return \Drupal::languageManager()->getCurrentLanguage()->getId();
    }

    /**
     * Retourne le code de langue d'un post donné (wordpress)
     * @param $postId numéro de post
     * @return string
     */
    public static function getPostLanguage($postId)
    {
        // Aucun sens sous Drupal
        return self::getCurrentLanguage();
    }

    /**
     * Définit le code de langue d'un post donné (surtout pour WordPress qui ne gère pas nativement le multilingue
     * @param $postId : id du post
     * @return string code de langue sur 2 lettres
     */
    public static function setPostLanguage($postId, $lang)
    {
        // Aucun sens sous Drupal
        return true;
    }

    /**
     * Sauvegarde les traductions d'un produit (WordPress)
     * @param $translationsArray tableau associatif $lang => $postId ('fr' => 4)
     */
    public static function savePostTranslations($translationsArray)
    {
        // Aucun sens en Drual
        return true;
    }

    /**
     * Définit le code de langue d'un post donné (WordPress)
     * @param $termId : id du terme
     * @return string
     */
    public static function setTermLanguage($termId, $lang)
    {
        // Aucun sens sous Drupal
        return true;
    }

    /**
     * Retourne la langue d'un terme (WordPress)
     * @param $termId : id du terme
     * @return string code de langue
     */
    public static function getTermLanguage($termId)
    {
        // Aucun sens sous Drupal
        return self::getCurrentLanguage();
    }

    /**
     * Retourne la langue d'un terme de taxonomie (WordPress)
     * @param $termId : id du terme
     * @return array taleau associatif lang => id de terme
     */
    public static function getTermTranslations($termId)
    {
        // Aucun sens en Drupal
        $trans = array();
        return $trans;
    }

    /**
     * Sauvegarde les traductions d'un terme (Wordpress)
     * @param $translationsArray tableau associatif $lang => $termId ('fr' => 4)
     */
    public static function saveTermTranslations($translationsArray)
    {
        // Aucun sens pour Drupal car il s'agi de termes différents par langue
        return true;
    }

    /**
     * Retourne la liste des langues actives
     * @return string[] tableau de codes de langues
     */
    public static function getLanguagesList()
    {
        $langs = array('fr');

        $langcodes = \Drupal::languageManager()->getLanguages();
        $langcodesList = array_keys($langcodes);

        // Patch 16/05/2022 : on doit s'assurer que le français est en premier
        if(!empty($langcodesList)) {
            if($langcodesList[0] !== 'fr') {
                $res = array('fr');
                foreach($langcodesList as $lang) {
                    if($lang !== 'fr') $res[] = $lang;
                }
                $langcodesList = $res;
            }
        }


        if(empty($langcodesList))
            return $langs;
        else
            return $langcodesList;
    }

    /**
     * Retourne le header du theme front-end du site pour affichage avant le code de la fichede détail (WordPress)
     * @return mixed
     */
    public static function getHeaderBridge() {
        // Pour Drupal on verra
        return '';
        // return get_header();
    }

    /**
     * Retourne le footer du theme front-end du site pour affichage après le code de la fichede détail (WordPress)
     * @return mixed
     */
    public static function getFooterBridge() {
        // Pour Drupal on verra
        return '';
        // return get_footer();
    }

    /**
     * Renvoie une reponse http Json d'erreur
     * @param mixed|null $data données à renvoyer en Json
     * @param int|null $status_code code HTTP à renvoyer
     * @param int $options options d'après les specifications de la fonction wp_send_json_error
     */
    public static function sendJsonError( mixed $data = null, int $status_code = null, int $options = 0) {
        header('Content-Type: application/json; charset=utf-8');
        die(json_encode(array( 'success' => false, 'data' => $data)));
    }

    /**
     * Renvoie une reponse http Json de succes
     * @param mixed|null $data données à renvoyer en Json
     * @param int|null $status_code code HTTP à renvoyer
     * @param int $options options d'après les specifications de la fonction wp_send_json_success
     */
    public static function sendJsonSuccess( mixed $data = null, int $status_code = null, int $options = 0) {
        header('Content-Type: application/json; charset=utf-8');
        die(json_encode(array( 'success' => true, 'data' => $data)));
    }

    /**
     * Renvoie une reponse http Json de succes
     * @param mixed|null $data données à renvoyer en Json
     * @param int|null $status_code code HTTP à renvoyer
     * @param int $options options d'après les specifications de la fonction wp_send_json_success
     */
    public static function sendJson( mixed $data = null, int $status_code = null, int $options = 0) {
        header('Content-Type: application/json; charset=utf-8');
        die(json_encode($data));
    }
}
