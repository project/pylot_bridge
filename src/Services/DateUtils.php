<?php
namespace Drupal\pylot_bridge\Services ;

/**
 * Class DateUtils
 * Utilistaires pour manipuler les dates avec Bridge
 * Il ne s'agit que d'utilitaires - tout y est déclaré en static et s'appelle ainsi : DateUtils::method()
 * @package App\Service
 */
class DateUtils
{

//--- Mois ---
    public static $nomMois = array(
        'fr' => array("Mois", "janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"),
        'en' => array("Mois", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"),
        'de' => array("Mois", "Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"),
        'nl' => array("Mois", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"),
        'it' => array("Mois", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"),
        'es' => array("Mois", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"),
        'pt' => array("Mois", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"),
    );

    public static $nomMoisAbr = array(
        'fr' => array("Mois", "JAN.", "FEV.", "MAR.", "AVR.", "MAI", "JUIN", "JUIL.", "AOUT", "SEPT.", "OCT.", "NOV.", "DEC."),
        'en' => array("Mois", "JAN.", "FEB.", "MAR.", "APR.", "MAY", "JUNE", "JULY", "AUG.", "SEPT.", "OCT.", "NOV.", "DEC."),
        'de' => array("Mois", "JAN.", "FEB.", "MAR.", "APR.", "MAI", "JUNI", "JULI", "AUG.", "SEPT.", "OKT.", "NOV.", "DEZ."),
        'nl' => array("Mois", "JAN.", "FEB.", "MAR.", "APR.", "MAY", "JUNE", "JULY", "AUG.", "SEPT.", "OCT.", "NOV.", "DEC."),
        'it' => array("Mois", "JAN.", "FEB.", "MAR.", "APR.", "MAY", "JUNE", "JULY", "AUG.", "SEPT.", "OCT.", "NOV.", "DEC."),
        'es' => array("Mois", "JAN.", "FEB.", "MAR.", "APR.", "MAY", "JUNE", "JULY", "AUG.", "SEPT.", "OCT.", "NOV.", "DEC."),
        'pt' => array("Mois", "JAN.", "FEB.", "MAR.", "APR.", "MAY", "JUNE", "JULY", "AUG.", "SEPT.", "OCT.", "NOV.", "DEC."),
    );

//---- Jours  ---- date("w");
    public static $nomJour = array(
        'fr' => array('Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'),
        'en' => array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'),
        'de' => array('Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'),
        'nl' => array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'),
        'it' => array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'),
        'es' => array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'),
        'pt' => array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'),
    );

    public static $nomJourLundi = array(
        'fr' => array('Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'),
        'en' => array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'),
        'de' => array('Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag', 'Sonntag'),
        'nl' => array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'),
        'it' => array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'),
        'es' => array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'),
        'pt' => array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'),
    );

    public static $nomJourAbregeLundi = array(
        'fr' => array('L.', 'M.', 'Me.', 'J.', 'V.', 'S.', 'D.'),
        'en' => array('Mo.', 'Tu.', 'We.', 'Th.', 'Fr.', 'Sa.', 'Su.'),
        'de' => array('Mo.', 'Di.', 'Mi.', 'Do.', 'Fr.', 'Sa.', 'So.'),
        'nl' => array('Mo.', 'Tu.', 'We.', 'Th.', 'Fr.', 'Sa.', 'Su.'),
        'it' => array('Mo.', 'Tu.', 'We.', 'Th.', 'Fr.', 'Sa.', 'Su.'),
        'es' => array('Mo.', 'Tu.', 'We.', 'Th.', 'Fr.', 'Sa.', 'Su.'),
        'pt' => array('Mo.', 'Tu.', 'We.', 'Th.', 'Fr.', 'Sa.', 'Su.'),
    );

    public static $nomJourAbr = array(
        'fr' => array("Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim"),
        'en' => array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"),
        'de' => array("Son", "Mon", "Die", "Mit", "Don", "Fre", "Sam", "Son"),
        'nl' => array("Zon", "Maa", "Din", "Woe", "Don", "Vri", "Zat", "Zon"),
        'it' => array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"),
        'es' => array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"),
        'pt' => array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"),
    );

    public static $textesDates = array(
        'fr' => array("midi" => "midi", "soir" => "soir", "de" => "de", "à" => "à"),
        'en' => array("midi" => "midi", "soir" => "soir", "de" => "from", "à" => "to"),
        'de' => array("midi" => "midi", "soir" => "soir", "de" => "von", "à" => "bis"),
        'nl' => array("midi" => "midi", "soir" => "soir", "de" => "from", "à" => "to"),
        'it' => array("midi" => "midi", "soir" => "soir", "de" => "from", "à" => "to"),
        'es' => array("midi" => "midi", "soir" => "soir", "de" => "from", "à" => "to"),
        'pt' => array("midi" => "midi", "soir" => "soir", "de" => "from", "à" => "to"),
    );


    /**
     * retourne un numéro de jour du mois en clair
     * @param $numjour : numéro du jour du mois
     * @param string $lang : langue d'affichage
     * @return int|mixed|string : texte à afficher (1er, 21st, etc...)
     */
    public static function numjour_clair($numjour, $lang = 'fr')
    {
        switch ($lang) {
            case "en":
                $numjour = (int)$numjour;
                if ($numjour == 1 || $numjour == 21 || $numjour == 31) {
                    $numjour .= "st";
                } elseif ($numjour == 2 || $numjour == 22) {
                    $numjour .= "nd";
                } elseif ($numjour == 3 || $numjour == 23) {
                    $numjour .= "rd";
                } elseif ($numjour > 3) {
                    $numjour .= "th";
                }
                break;

            case "de":
                break;

            default:
                if ($numjour == 1) {
                    $numjour .= "er";
                } // "<sup>er</sup>"; // "<sup style=\"vertical-align: top; text-transform: lowercase; line-height: 12px\">er</sup>";
                break;
        }

        return $numjour;
    }

    /**
     * affiche_mois
     * Retourne le nom d'un mois à partir d'une date en texte sous la forme jj/mm/aaaa
     * @param $datetxt date texte au format jj/mm/aaaa
     * @param string $lang : langue
     * @param false $abrege : true si on veut les noms de mois abrégés en 3 lettres
     * @return string : nom du mois de la date passée
     */
    public static function affiche_mois($datetxt, $lang = 'fr', $abrege = false)
    {
        $mois = substr($datetxt, 3, 2);
        if ($abrege) {
            $tabmois = self::$nomMoisAbr;
        } else {
            $tabmois = self::$nomMois;
        }

        $res = '';
        switch ($lang) {
            case 'en':
                $res = $tabmois[$lang][(int)$mois];
                break;
            case 'de':
                $res = $tabmois[$lang][(int)$mois] . ' ';
                break;
            default:
                if (isset($tabmois[$lang][(int)$mois]))
                    $res = $tabmois[$lang][(int)$mois];
                else
                    $res = $tabmois['en'][(int)$mois];
                break;
        }

        return $res;
    }


    /**
     *
     * Retourne une date texte à partir d'une expression smartdate type @+7J ou date normale au format texte Y-m-d ou d/m/Y (qui restera inchangée, sauf éventuellement le format)
     * $date string : expression type @+7J [MARQUEUR_TEMPOREL] [OPERATEUR] [NOMBRE] [UNITE]
     * MARQUEUR TEMPOREL
     *    - @ pour aujourd'hui
     *  - DS : Début de cette semaine (Lundi)
     *  - FS : Fin de cette semaine (Dimanche)
     *  - DM : Début de mois
     *  - FM : Fin de mois
     *  - DA : Début année
     *  - FA : Fin d'année
     *
     * OPERATEUR : + ou -
     *
     * UNITES :
     *  - J : Jour
     *  - S : SEMAINES
     *  - M : MOIS
     *  - A : ANNEES
     *
     * Exemple : Ce Dimanche : FS / Ce Samedi : FS-1J / Dimanche prochain : FS+1S / Dans 7 jours : @+7J
     *
     * $output_format string : format de chain de sortie (paramètre de la fonction date de PHP)
     *
     * @param        $inputDate
     * @param string $output_format
     * @param string $input_format
     *
     * @return string
     * @throws \Exception
     */
    public static function smartdate($inputDate, $output_format = "Y-m-d", $input_format = "Y-m-d")
    {
        $dat = (string)$inputDate;
        $lad = null;

        $res = null;
        $ladate = null;

        // Ici on détecte si l'expression est une vraie date et pas une smartdate auquel cas on renvoie la date reformatée
        if ($input_format == "d/m/Y" || $input_format == "d.m.Y") {
            if (strpos($dat, "/") !== false || strpos($dat, ".") !== false) {
                // Conversion de format des dates
                $ladate = \DateTime::createFromFormat("d/m/Y", substr($dat, 0, 2) . "/" . substr($dat, 3, 2) . "/" . substr($dat, 6, 4));
//                $ladate = mktime(0, 0, 0, intval(substr($dat, 3, 2)), intval(substr($dat, 0, 2)), intval(substr($dat, 6, 4)));
                // return date($output_format, $ladate);
                if ($ladate !== false)
                    return $ladate->format($output_format);
                else
                    return '';
            }
        }
        if ($input_format == "Y-m-d") {
            if (strpos($dat, "-") !== false && strlen($dat) == 10) {
                $tabstr = explode("-", $dat);
                if (count($tabstr) == 3) {
                    // Conversion de format des dates
                    $ladate = \DateTime::createFromFormat($input_format, $inputDate);
                    // $ladate = mktime(0, 0, 0, intval($tabstr[1]), intval($tabstr[2]), intval($tabstr[0]));
                    // return date($output_format, $ladate);
                    if ($ladate !== false)
                        return $ladate->format($output_format);
                    else
                        return '';
                }
            }
        }

        $dat = strtoupper($dat);

        // Si est un @
        if ($dat == "@") {
            $lad = new \DateTime();

            return $lad->format($output_format);
            // return date($output_format, $lad);
        }


        // if (strpos($dat, "+") === false)
        //    $dat = "+" . $dat;
        // D'abord trouver la date de départ
        $ladate = new \DateTime();
        $letemp = strtoupper($dat);
        $lapos = 0;

        if (substr($dat, 0, 1) == "@") {
            $ladate = new \DateTime();
            $lapos = 1;
        }
        if (strpos($dat, "DS") !== false) {
            $nbj = 1 - (int)$ladate->format("N"); // Lundi=1 ... Dimanche = 7
            $ladate->modify("+" . $nbj . " days");
            $lapos = strpos($dat, "DS") + 2;
        }

        if (strpos($dat, "FS") !== false) {
            $nbj = 7 - (int)$ladate->format("N"); // Lundi=1 ... Dimanche = 7
            $ladate->modify("+" . $nbj . " days");
            $lapos = strpos($dat, "FS") + 2;
        }

        if (strpos($dat, "FM") !== false) {
            $ladate->modify("last day of month");
            $lapos = strpos($dat, "FM") + 2;
        }

        if (strpos($dat, "DM") !== false) {
            $ladate->modify("first day of month");
            $lapos = strpos($dat, "DM") + 2;
        }

        if (strpos($dat, "FA") !== false) {
            $ladate->modify("last day of year");
            $lapos = strpos($dat, "FA") + 2;
        }

        if (strpos($dat, "DA") !== false) {
            $ladate->modify("first day of year");
            $lapos = strpos($dat, "DA") + 2;
        }

        // On supprime les caractères de position de date de départ déjà traités de letemp
        $letemp = substr($letemp, $lapos);
        $ik = "";
        $nbj = 0;
        $nbm = 0;
        $nba = 0;
        if (strpos($letemp, "+") !== false || strpos($letemp, "-") !== false) {
            if (strpos($letemp, "+") !== false) {
                $ik = strpos($letemp, "+");
                $ope = "+";
            } else {
                $ik = strpos($letemp, "-");
                $ope = "-";
            }
            $nb = intval(substr($letemp, $ik + 1));
            if (strpos($letemp, "J") !== false) {
                $ladate->modify($ope . $nb . " days");
            } // $nbj = $nb;
            elseif (strpos($letemp, "S") !== false) {
                $ladate->modify($ope . $nb . " weeks");
            } // $nbj = $nb * 7;
            elseif (strpos($letemp, "M") !== false) {
                $ladate->modify($ope . $nb . " months");
            } // $nbm = $nb;
            elseif (strpos($letemp, "A") !== false) {
                $ladate->modify($ope . $nb . " years");
            } // $nba = $nb;
            else {
                $ladate->modify($ope . $nb . " days");
            } // $nbj = $nb;

            /*
              if (strpos($letemp, "+") !== false)
              $ladate = mktime(0, 0, 0, date("n", $ladate) + $nbm, date("j", $ladate) + $nbj, date("Y", $ladate) + $nba);
              else
              $ladate = mktime(0, 0, 0, date("n", $ladate) - $nbm, date("j", $ladate) - $nbj, date("Y", $ladate) - $nba);
              // return date("d/m/Y", $ladate);
             *
             */
        }

        return $ladate->format($output_format); // , $ladate);
        // return date($output_format, $ladate);
    }

    /**
     * horairesDeLaPeriode
     * Cette Methode s'appelle pour extraire les dates / heures d'un product en situation de front end
     * Retourne un tableau d'horaires hebdomadaires correspndant à la période du / au fournie
     * Si mode_strict : on n'affiche que les heures de la periode exacte (adapté à un mode calendrier)
     * Si mode strict = false : on recherche tous les horaires ayant lieu dans l'intervalle du au (mode horaires compilés sans doublonner la fiche)
     * Cumuler : si true : on retourne tous les horaires cumulés sans se préoccuper des periodes )
     * @param \stdClass $fiche objet product tel que retourné par l'API Bridge
     * @param string $dateFrom - permet de réduire l'affichage des dates à une période donnée - Date de début au format Y-m-d
     * @param string $dateTo - permet de réduire l'affichage des dates à une période donnée - Date de fin au format Y-m-d
     * @param bool|null $mode_strict : ignore les périodes de la fiche hors de la période DateFrom dateto passée en paramètre
     * @param bool|null $cumuler - au 11/04/21 : ne sert à rien
     * @param bool|null $afficher_de_a - Affiche le teste de ... à ... entre les heures
     * @param bool|null $afficher_h_heure - Affiche un h entre les heures les les minutes à la place d'un double point
     * @param bool|null $lang - langue d'affichage
     * @return array|mixed tableau de péiodes avec horaires hebdomadaires
     * @deprecated utiliser la fonction prepareHoursForDisplay
     */
    public static function horairesDeLaPeriode(\stdClass $fiche, string $dateFrom, string $dateTo, ?bool $one_strict_period = true, ?bool $cumuler = false, ?bool $afficher_de_a = true, ?bool $afficher_h_heure = true, ?string $lang = 'fr')
    {
        $options = array(
            'dateFrom' => $dateFrom,
            'dateTo' => $dateTo,
            'one_strict_period' => $one_strict_period,
            'afficher_de_a' => $afficher_de_a,
            'afficher_h_heure' => $afficher_h_heure,
        );
        return self::prepareHoursForDisplay($fiche, $lang, $options);
    }

    /**
     * Cette Methode s'appelle pour extraire les dates / heures d'un product en situation de front end
     * Retourne un tableau d'horaires hebdomadaires correspndant à la période du / au fournie
     * Si mode_strict : on n'affiche que les heures de la periode exacte (adapté à un mode calendrier)
     * Si mode strict = false : on recherche tous les horaires ayant lieu dans l'intervalle du au (mode horaires compilés sans doublonner la fiche)
     * Cumuler : si true : on retourne tous les horaires cumulés sans se préoccuper des periodes )
     * @param \stdClass $fiche objet product tel que retourné par l'API Bridge
     * @param string $dateFrom - Date de début au format Y-m-d
     * @param string $dateTo - Date de fin au format Y-m-d
     * @param bool|null $mode_strict :
     * @param array|null $options - options d'affichage
     * @param string|null $lang - langue d'affichage
     *
     * @return array|mixed tableau de péiodes avec horaires hebdomadaires
     */
    public static function prepareHoursForDisplay(\stdClass $fiche, ?string $lang = 'fr', $options = array())
    {

        /**
         * Options
         * dateFrom - permet de réduire l'affichage des dates à une période donnée - Date de début au format Y-m-d
         * dateTo - permet de réduire l'affichage des dates à une période donnée - Date de fin au format Y-m-d
         * mode_strict : ignore les périodes de la fiche hors de la période DateFrom dateto passée en paramètre
         * cumuler - au 11/04/21 : ne sert à rien
         * afficher_de_a - Affiche le teste de ... à ... entre les heures
         * afficher_h_heure - Affiche un h entre les heures les les minutes à la place d'un double point
         */
        $joursem = array('LUNDI', 'MARDI', 'MERCREDI', 'JEUDI', 'VENDREDI', 'SAMEDI', 'DIMANCHE');

        // On fixe les valeurs par défaut
        $txtDe = 'de ';
        $txtA = ' à ';
        $txtHeure = 'h';
        if ($lang != 'fr' && isset(self::$textesDates[$lang]) && isset(self::$textesDates[$lang]['de'])) {
            $txtDe = self::$textesDates[$lang]['de'] . ' ';
            $txtA = ' ' . self::$textesDates[$lang]['à'] . ' ';
            $txtHeure = ':';
        }

        $tmpDate = new \Datetime();
        $dateFrom = $tmpDate->format('Y-m-d');
        if (isset($options['dateFrom']) && !empty($options['dateFrom'])) {
            $dateFrom = $options['dateFrom'];
        }

        $tmpDate = new \Datetime();
        $tmpDate->modify('+1 year');
        $dateTo = $tmpDate->format('Y-m-d');
        if (isset($options['dateTo']) && !empty($options['dateTo'])) {
            $dateTo = $options['dateTo'];
        }
        $one_strict_period = false;
        if (isset($options['one_strict_period']) && !empty($options['one_strict_period'])) {
            $one_strict_period = $options['one_strict_period'];
        }
        $afficher_de_a = true;
        if (isset($options['afficher_de_a']) && !empty($options['afficher_de_a'])) {
            $afficher_de_a = $options['afficher_de_a'];
        }
        $afficher_h_heure = true;
        if (isset($options['afficher_h_heure']) && !empty($options['afficher_h_heure'])) {
            $afficher_h_heure = $options['afficher_h_heure'];
        }

        if (!$afficher_de_a) {
            $txtDe = '';
            $txtA = '-';
        }
        if (!$afficher_h_heure) {
            $txtHeure = ':';
        }
        $tabres = array();
        $horaires = $fiche->hours;
        //print_r($horaires);
        if (count($horaires) == 0)
            return array();

        $i = 1;
        // print_r($horaires);

        $hdebnew = '';
        $hfinnew = '';
        foreach ($horaires as $key => $horaire) {

            //print_r($horaire);
            $ledu = substr($horaire->dateFrom->date, 0, 10);
            $leau = substr($horaire->dateTo->date, 0, 10);
            if ($one_strict_period && ($ledu != $dateFrom || $leau != $dateTo))
                continue;

            $dutext = substr($ledu, 8, 2) . "/" . substr($ledu, 5, 2) . "/" . substr($ledu, 0, 4);
            //echo $ledu;
            // On teste si les dates sont supérieur à 340 jours dans le futur, si c'est le cas on passe à la donnée suivante
            $timestampplusunan = time() + (340 * 24 * 60 * 60);
            $timestampdu = strtotime(str_replace('/', '-', $ledu));
            if ($timestampdu > $timestampplusunan)
                continue;


            $autext = substr($leau, 8, 2) . "/" . substr($leau, 5, 2) . "/" . substr($leau, 0, 4);

            $comm = ''; // (string) $horaire->COMMENTAIRE;
            $dates_horaires_flag = true;

            // cleper permet de regrouper les heures des memes périodes : JJMMAAAA-JJMMAAAA
            $cleper = substr($ledu, 0, 4) . substr($ledu, 5, 2) . substr($ledu, 8, 2) . substr($leau, 0, 4) . substr($leau, 5, 2) . substr($leau, 8, 2);
            // Init du tableau
            if (!isset($tabres[$cleper])) {
                $tabres[$cleper] = array(
                    "du" => $dutext,
                    "au" => $autext,
                    "duYmd" => $ledu,
                    "auYmd" => $leau,
                    "jours" => array(),
                    "comm" => trim($comm)
                );
            }

            // Nouvelles dates
            $hdeb = '';
            $hfin = '';
            if (isset($horaire->hourFrom) && isset($horaire->hourFrom->date)) {
                $hdeb = substr($horaire->hourFrom->date, 11, 5);
                $hdeb = str_replace(':', $txtHeure, $hdeb);
            }
            if (isset($horaire->hourTo) && isset($horaire->hourTo->date)) {
                $hfin = substr($horaire->hourTo->date, 11, 5);
                $hfin = str_replace(':', $txtHeure, $hfin);
            }
            // On remplit les vides
            if ($hdeb != '' || $hfin != '') {
                if ($hdeb == '')
                    $hdeb = "00" . $txtHeure . "00";

                if ($hfin == '')
                    $hfin = "23" . $txtHeure . "59"; // h00.000";
            }

            // Patch heures anglaises 03/2022 en format 12 heures
            if ($lang == 'en' && $hdeb != '') {
                $tmp = explode($txtHeure, $hdeb);
                if (count($tmp) > 0) {
                    $amPmSuffix = ' am';
                    $hh = (int)$tmp[0];

                    // Si on est entre 12 et 23 heures, on ajoute le suffixe PM
                    if ($hh / 12 >= 1 && $hh % 24 !== 0 ) {
                        $amPmSuffix = ' pm';
                    }

                    // Cas particulier : minuit (O heure ou 24 heures en format 24h) = 12 AM
                    $hh = $hh % 12;
                    if ($hh === 0) {
                        $hh = 12;
                    }

                    $hdeb = $hh;
                    if (count($tmp) > 1)
                        $hdeb .= $txtHeure . $tmp[1] . $amPmSuffix;
                }
            }
            if ($lang == 'en' && $hfin != '') {
                $tmp = explode($txtHeure, $hfin);
                if (count($tmp) > 0) {
                    $amPmSuffix = ' am';
                    $hh = (int)$tmp[0];

                    // Si on est entre 12 et 23 heures, on ajoute le suffixe PM
                    if ($hh / 12 >= 1 && $hh % 24 !== 0 ) {
                        $amPmSuffix = ' pm';
                    }

                    // Cas particulier : minuit (O heure ou 24 heures en format 24h) = 12 AM
                    $hh = $hh % 12;
                    if ($hh === 0) {
                        $hh = 12;
                    }

                    $hfin = $hh;
                    if (count($tmp) > 1)
                        $hfin .= $txtHeure . $tmp[1] . $amPmSuffix;
                }
            }

            if ($hdeb != '') {
                // Patch ND 11/11/21 : on n'ignore plus le 23:59 en heure de fin
                // if ( $hdeb == $hfin || $hfin == "23" . $txtHeure . "59" ) // . $txtHeure . "00.000" )
                // ND 12/06/2022 : c'est pas terrible d'avoir des 23h59 en heures de fin : da,ns ce cas, on affiche juste l'heure de début
                if ($hdeb == $hfin || $hfin == '23' . $txtHeure . '59') // . $txtHeure . "00.000" )
                {
                    $res = str_replace('h00.000', '', $hdeb);
                } else {
                    //if ( (string) $hfinnew == "23" . $txtHeure . "59"  && (string) $hdeb == "00" . $txtHeure . "00" ) // . $txtHeure . "00.000"
                    //	$res = $txtDe . str_replace( $txtHeure . '00.000', '', $hdebnew ) . $txtA . str_replace( $txtHeure . '00.000', '', $hfin );
                    //elseif ( (string) $hfin != "23" . $txtHeure . "59"  ) // . $txtHeure . "00.000"
                    $res = $txtDe . str_replace($txtHeure . '00.000', '', $hdeb) . $txtA . str_replace($txtHeure . '00.000', '', $hfin);

                }
                $hdebnew = $hdeb;
                $hfinnew = $hfin;
            } else {
                $res = '';
            }
            $commh = ''; //trim( (string) $horaire->hourFrom->COMMENTAIRE );

            foreach ($joursem as $jour) {
                if (count($horaire->days) > 0) {
                    foreach ($horaire->days as $jourdnsit) {
                        if ((string)$jour == (string)strtoupper($jourdnsit)) {

                            if (!isset($tabres[$cleper]['jours'][$jour]))
                                $tabres[$cleper]['jours'][$jour] = array();
                            if ($res != '')
                                $tabres[$cleper]['jours'][$jour][] = $res;
                            else
                                $tabres[$cleper]['jours'][$jour][] = ''; // Toute la journée
                        }
                        // commentaires
                        $tabres[$cleper]['jours']['comm'] = $commh;
                    }
                } else {
                    if (!isset($tabres[$cleper]['jours'][$jour]))
                        $tabres[$cleper]['jours'][$jour] = array();
                    if ($res != '')
                        $tabres[$cleper]['jours'][$jour][] = $res;
                    else
                        $tabres[$cleper]['jours'][$jour][] = ''; // Toute la journée
                    // commentaires
                    $tabres[$cleper]['jours']['comm'] = $commh;
                }
            }

            // ND 11/11/21 : on recolle les horaires de nuit à la veille
            $jourprec = 'DIMANCHE';
            $testboucle = 0;
            foreach ($joursem as $jour) {
                // JM 02.12.2021 : on s'assure que le jour est bien présent dans la fiche
                if (isset($tabres[$cleper]['jours'][$jour]) && is_array($tabres[$cleper]['jours'][$jour])) {
                    $testboucle++;
                    for ($idxhor = 0; $idxhor < count($tabres[$cleper]['jours'][$jour]); $idxhor++) {
                        $testboucle++;

                        // Si l'horaire commence à Minuit, on cherche un horaire qui se terminerait à 23h59 la veille
                        if (substr($tabres[$cleper]['jours'][$jour][$idxhor], strlen($txtDe), 5) == '00' . $txtHeure . '00') {
                            if (isset($tabres[$cleper]['jours'][$jourprec])) {
                                for ($idxhor2 = 0; $idxhor2 < count($tabres[$cleper]['jours'][$jourprec]); $idxhor2++) {
                                    $testboucle++;
                                    if (substr($tabres[$cleper]['jours'][$jourprec][$idxhor2], -5) == '23' . $txtHeure . '59') {
                                        // On colle l'horaire de fin du petit matin sur l'horaire du jour précédent
                                        $tabres[$cleper]['jours'][$jourprec][$idxhor2] = substr($tabres[$cleper]['jours'][$jourprec][$idxhor2], strlen($txtDe), 5) . $txtA . substr($tabres[$cleper]['jours'][$jour][$idxhor], -5);
                                        // On supprimme l'horaire du jour suivant commençant à 0h qu'on a utilisé
                                        unset($tabres[$cleper]['jours'][$jour][$idxhor]);
                                        // On réindexe le tableau suquel on a supprimé un élément
                                        $tabres[$cleper]['jours'][$jour] = array_values($tabres[$cleper]['jours'][$jour]); // 'reindex' array
                                        // On passe au jour suivant
                                        break 2;
                                    }
                                }
                            }
                        }
                    }
                }
                $jourprec = $jour;
            }
        }


        if (!empty($_REQUEST['dncdnc'])) {
            echo "UUU";
            // print_r($tabres);
            // die('BBB');
        }

        // ksort( $tabres );
        if ($one_strict_period && isset($cleper)) {
            if (!empty($_REQUEST['dncdnc'])) {
                echo('FFF : ' . $cleper);
            }
            return $tabres[$cleper];
        } else {

            if (!empty($_REQUEST['dncdnc'])) {
                if ($one_strict_period) {
                    echo "STRCIT";
                } else {
                    echo "PASSRIC";
                }
                echo('GGF ' . $cleper);
            }
            return $tabres;
        }
    }

    /**
     * Retourne une chaine représentant les horaires d'ouverture de la fiche aujourd'hui
     * @param $fiche : objet product tel que retourné par l'API Bridge
     * @param array|null $optionsPerso : options d'affichage
     * @param string|null $lang : langue d'affichage
     * @return array|string : chaine représentant les horaires prête à être affichée
     */
    public static function afficherHorairesDuJour($fiche, ?array $optionsPerso = array(), ?string $lang = 'fr')
    {

        // Options d'affichage par défaut
        $options = array();
        $options['supprimer_minutes_00'] = true;
        $options['separateur_heure_minute'] = 'h';
        $options['separateur_debut_fin'] = '-';
        $options['separateur_heures'] = ' / ';
        if (isset($optionsPerso))
            $options = array_merge($options, $optionsPerso);

        $tabres = array();

        if (!isset($fiche->hours))
            return $tabres;


        $auj = new \DateTime();
        $auj->setTime(0, 0, 0, 0);

        $tabjoursem = array('DIMANCHE', 'LUNDI', 'MARDI', 'MERCREDI', 'JEUDI', 'VENDREDI', 'SAMEDI');
        $joursem = $tabjoursem[date("w")];

        $debugStr = '';
        if (count($fiche->hours) > 0) {
            $i = 1;
            foreach ($fiche->hours as $horaire) {
                $ledu = substr($horaire->dateFrom->date, 0, 10);
                $leau = substr($horaire->dateTo->date, 0, 10);
                $hdeb = '';
                $hfin = '';
                if (is_object($horaire->hourFrom) && !empty($horaire->hourFrom->date))
                    $hdeb = substr($horaire->hourFrom->date, 11, 5);
                if (is_object($horaire->hourTo) && !empty($horaire->hourTo->date))
                    $hfin = substr($horaire->hourTo->date, 11, 5);
                // 2014-01-01 00:00:00
                if ((string)$ledu == '' || (string)$leau == '')
                    continue;
                $datedu = date_create_from_format('Y-m-d', substr($ledu, 0, 10)); //mktime(0, 0, 0, (int) substr($ledu, 3, 2), (int) substr($ledu, 0, 2), (int) substr($ledu, 6, 4));
                $dateau = date_create_from_format('Y-m-d', substr($leau, 0, 10)); //mktime(0, 0, 0, (int) substr($ledu, 3, 2), (int) substr($ledu, 0, 2), (int) substr($ledu, 6, 4));

                if ($options['supprimer_minutes_00']) {
                    $hdeb = str_replace(':00', ':', $hdeb);
                    $hfin = str_replace(':00', ':', $hfin);
                }
                $hdeb = str_replace(':', $options['separateur_heure_minute'], $hdeb);
                $hfin = str_replace(':', $options['separateur_heure_minute'], $hfin);


                // D'abord on teste si lé créneau est correct
                if ($datedu <= $auj && $dateau >= $auj) {
                    $debugStr .= 'P';
                    if (count($horaire->days)) {
                        $debugStr .= 'D-' . $joursem . '-';
                        foreach ($horaire->days as $key => $heure) {
                            if (strtoupper($heure) == strtoupper($joursem)) {
                                $debugStr .= 'Y';
                                // if((string)$heure->ACCES_PERMANENT != "1") {
                                $debugStr .= 'Y' . '|' . $hdeb . '|' . $hfin . '|';
                                if ($hdeb != '' && $hfin != '' && $hdeb != $hfin)
                                    $tabres[] = $hdeb . $options['separateur_debut_fin'] . $hfin;
                                elseif ($hdeb != '' && $hfin != '' && $hdeb == $hfin)
                                    $tabres[] = $hdeb;
                                elseif ($hdeb != '')
                                    $tabres[] = $hdeb;
                                elseif ($hfin != '')
                                    $tabres[] = $hfin;
                            }
                        }
                    }
                }
            }
            // return $debugStr;
        }
        return implode($options['separateur_heures'], $tabres);
    }

    /**
     * afficheHorairesRestauration
     * Retourne une chaine d'affichage des horaires spécifiquement adaptée à la restauration (midi, soir et jours/services de fermeture)
     * @param $fiche : objet product tel que retourné par l'API Bridge
     * @param string $annee année au format AAAA pour obtenir toutes les données cumulée de l'année spécifiée
     * @param string $dateFrom : restreint l'affichage à une période donnée de product->hours - date from
     * @param string $dateTo : restreint l'affichage à une période donnée de product->hours - date to
     * @param array|null $optionsPerso : options d'affichage
     * @param string|null $lang : langue d'affichage
     * @return array|void : tableau associatif de chaines avec clés soir, midi et fermetures
     */
    public function afficheHorairesRestauration($fiche, $annee = '', $dateFrom = '', $dateTo = '', ?array $optionsPerso = array(), ?string $lang = 'fr')
    {
        // Options d'affichage par défaut
        $options = array();
        $options['jours_abreges'] = 'NON';
        $options['separateur_heure_minute'] = ':';
        $options['separateur_debut_fin'] = '-';
        $options['separateur_heures'] = ' / ';
        $options['separateur_fermetures'] = ', ';
        if (isset($optionsPerso))
            $options = array_merge($options, $optionsPerso);

        $midi = '';
        $soir = '';
        $fermetures = array();

        $joursem = array('LUNDI', 'MARDI', 'MERCREDI', 'JEUDI', 'VENDREDI', 'SAMEDI', 'DIMANCHE');
        $jourseminv = array('DIMANCHE' => 7, 'LUNDI' => 1, 'MARDI' => 2, 'MERCREDI' => 3, 'JEUDI' => 4, 'VENDREDI' => 5, 'SAMEDI' => 6);
//		$tabjours = array(1 =>'LUNDI', 2 =>'MARDI', 3 => 'MERCREDI', 4 =>'JEUDI', 5=> 'VENDREDI', 6=>'SAMEDI', 7 =>'DIMANCHE');
//		$tabjourscond = array('L.', 'Ma', 'Me', 'J', 'V', 'S', 'D');

        if (isset(self::$nomJourLundi[$lang])) {
            $joursem = self::$nomJourLundi[$lang];
            if (strtoupper($options['jours_abreges']) == 'OUI') {
                $joursem = self::$nomJourAbregeLundi[$lang];
            }
        }


        $strict_period = false;
        if ($dateFrom != '' && $dateTo != '') {
            $strict_period = true;
        } else {

            if ($annee == '')
                $annee = date('Y');

            $dateFrom = date_create_from_format($annee . '-01-01', 'Y-m-d');
            $dateTo = date_create_from_format($annee . '-12-31', 'Y-m-d');
        }

        $cumuler = false;
        $afficher_de_a = false;
        $afficher_h_heure = false;

        if (!empty($_REQUEST['dncdnc'])) {
            echo "1A1";
        }
        $horaires = self::horairesDeLaPeriode($fiche, $dateFrom, $dateTo, $strict_period, $cumuler, $afficher_de_a, $afficher_h_heure);

        if (count($horaires) > 0) {
            $i = 1;

            // On prend le dernier horaire
            if ($strict_period) {
                $dernhor = array_merge($horaires);
            } else {
                foreach ($horaires as $cleper => $hors) {
                    $dernhor = array_merge($hors);
                }
            }

            $fermetures = array();
            $midide = 1000;
            $midia = -1;
            $soirde = 1000;
            $soira = -1;


            // Trier les horaires
            $dernhor['newjours'] = array();
            $dernhor['newjours'][] = array('horaires' => array());
            $dernhor['newjours'][] = array('horaires' => array());
            $dernhor['newjours'][] = array('horaires' => array());
            $dernhor['newjours'][] = array('horaires' => array());
            $dernhor['newjours'][] = array('horaires' => array());
            $dernhor['newjours'][] = array('horaires' => array());
            $dernhor['newjours'][] = array('horaires' => array());
            $dernhor['newjours'][] = array('horaires' => array());
            $dernhor['midis'] = array();
            $dernhor['soirs'] = array();


            if (count($dernhor['jours']) > 0) {
                foreach ($dernhor['jours'] as $key => $val) {
                    if (!empty($dernhor['jours'][$key]) && is_array($dernhor['jours'][$key])) {
                        sort($dernhor['jours'][$key]);
                        $dernhor['newjours'][$jourseminv[$key]]['horaires'] = $dernhor['jours'][$key];
                    }
                    // $dernhor['newjours'][$key] = array('horaires' => $dernhor['jours'][$key] );
                    // sort( $dernhor['jours'][$i]['horaires'] );
                }
            }


            // print_r($dernhor['newjours']);
            // die();

            // On recolle les heures après minuit
            // On met de côté les fermetures
            // On calcule un horaire unique pour midi et pour le soir
            $soirs = array();

            for ($i = 1; $i < count($dernhor['newjours']); $i++) {
                $dernhor['newjours'][$i]['midi'] = false;
                $dernhor['newjours'][$i]['soir'] = false;
                $soircondde = 1000;
                $soirconda = -1;
                if (count($dernhor['newjours'][$i]['horaires'])) {
                    for ($j = 0; $j < count($dernhor['newjours'][$i]['horaires']); $j++) {
                        if ($dernhor['newjours'][$i]['horaires'][$j] != '') {
                            $tabht = explode('-', $dernhor['newjours'][$i]['horaires'][$j]);
                            // print_r($tabht);

                            if (count($tabht) == 2) {

                                $tempde = (float)str_replace('h', '.', $tabht[0]);
                                $tempa = (float)str_replace('h', '.', $tabht[1]);

                                if ($tempde <= 20 && $tempa > 20) { // soir
                                    // echo "B";
                                    $dernhor['newjours'][$i]['soir'] = true;

                                    if ($tempde < $soirde) {
                                        $soirde = $tempde;
                                    }
                                    $soircondde = $tempde;
                                    if ($tempa > $soira) {
                                        $soira = $tempa;
                                    }
                                    $soirconda = $tempa;
                                } elseif ($tempde >= 6 && $tempde <= 12 && $tempa >= 12) { // midi
                                    $dernhor['newjours'][$i]['midi'] = true;
                                    $clehor = $dernhor['newjours'][$i]['horaires'][$j];
                                    if (!isset($dernhor['midis'][$clehor]))
                                        $dernhor['midis'][$clehor] = array('numeros' => array(), 'noms' => array());
                                    $dernhor['midis'][$clehor]['numeros'][] = $i;
                                    $dernhor['midis'][$clehor]['noms'][] = $joursem[$i - 1];

                                    if ($tempde < $midide) {
                                        $midide = $tempde;
                                    }
                                    if ($tempa > $midia) {
                                        $midia = $tempa;
                                    }

                                }
                                if ($tempde >= 6 && $tempde <= 12 && $tempa > 12)  // midi pour fermeture
                                {
                                    $dernhor['newjours'][$i]['midi'] = true;
                                }
                                // die('ici'.$tempde . ' - '.$tempa);
                            }
                            if ($tabht[count($tabht) - 1] == "23h59") { // Recoller
                                $joursuiv = $i + 1;
                                // die('AAA'.$i.'----'.$joursuiv);
                                if ($joursuiv == 7) {
                                    $joursuiv = 0;
                                }
                                if (isset($dernhor['newjours'][$joursuiv]) && isset($dernhor['newjours'][$joursuiv]['horaires']) && count($dernhor['newjours'][$joursuiv]['horaires'])) {
                                    if (substr($dernhor['newjours'][$joursuiv]['horaires'][0], 0, 2) == '00') {
                                        $tabht = explode('-', $dernhor['newjours'][$joursuiv]['horaires'][0]);
                                        if (count($tabht) == 2) {

                                            // $tempde = (float) str_replace('h','.',$tabht[0]);
                                            $soiratard = (float)str_replace('h', '.', $tabht[0]);
                                            $soira = (float)str_replace('h', '.', $tabht[0]);
                                            $soirconda = (float)str_replace('h', '.', $tabht[1]);
                                            // echo "N".$soirconda	;
                                            // unset($dernhor['newjours'][$joursuiv]['horaires'][0]);
                                        }
                                    }
                                }
                            }


                        }
                    }
                }
                // echo "U";
                if ($soircondde != 1000 && $soirconda != -1) {
                    // echo "Z";
                    $tempde = str_replace('.', 'h', str_pad(number_format($soircondde, 2), 5, '0', STR_PAD_LEFT));
                    $tempa = str_replace('.', 'h', str_pad(number_format($soirconda, 2), 5, '0', STR_PAD_LEFT));
                    $clesoir = str_replace("23h59", "00h00", $tempde . "-" . $tempa);
                    // Tableau inversé pour condenser les jours de mêmes horaires du soir
                    if (!isset($soirs[$clesoir])) {
                        $soirs[$clesoir] = array();
                    }
                    $soirs[$clesoir][] = $i;

                }
            }

            $tempde = '';
            $tempa = '';
            $midi = '';
            /* Patch 01/21 : ke midi est pas bien fait
            if ( $midide != 1000 )
            {
                $tempde = str_replace( '.', 'h', str_pad( number_format( $midide, 2 ), 5, '0', STR_PAD_LEFT ) );
            }
            if ( $midia != - 1 )
            {
                $tempa = str_replace( '.', 'h', str_pad( number_format( $midia, 2 ), 5, '0', STR_PAD_LEFT ) );
            }
            $midi = $tempde;
            if ( $tempde != '' && $tempa != '' )
            {
                $midi .= $options['separateur_debut_fin'];
            }
            $midi .= $tempa;
            */

            if (!empty($_REQUEST['dncdnc'])) {
                print_r($fiche->hours);
                print_r($horaires);
                print_r($dernhor);
                die();
            }

            $midis = array();
            if (count($dernhor['midis'])) {
                foreach ($dernhor['midis'] as $hors => $jours) {
                    if (count($jours['numeros']) > 0 && !empty($hors)) {
                        $tempjour = '';
                        if (count($jours['numeros']) > 1) {
                            $prem = $jours['numeros'][0];
                            $last = $jours['numeros'][count($jours['numeros']) - 1];
                            // Continuité
                            // $toto = $last - $prem;
                            // $titi = count($jours['numeros']) -1;
                            // die("toto : $toto - titi : $titi");

                            if ($last - $prem == count($jours['numeros']) - 1) {
                                // Si c'est tous les jours de la semaine, on ne met pas les noms de jours
                                if ($last == 7 && $prem == 1) {
                                    $midis[] = str_replace(':', 'h', $hors);
                                } else {
                                    $midis[] = $jours['noms'][0] . '-' . $jours['noms'][count($jours['noms']) - 1] . ' : ' . str_replace(':', 'h', $hors);
                                }

                            } else {
                                $midis[] = implode(', ', $jours['noms']) . '&nbsp;:&nbsp;' . str_replace(':', 'h', $hors);
                            }
                        } else {
                            $midis[] = implode(', ', $jours['noms']) . '&nbsp;:&nbsp;' . str_replace(':', 'h', $hors);
                        }
                    }
                }
            }
            $midi = implode(" / ", $midis);

            $tempde = '';
            $tempa = '';
            $soir = '';
            if ($soirde != 1000) {
                $tempde = str_replace('.', 'h', str_pad(number_format($soirde, 2), 5, '0', STR_PAD_LEFT));
            }
            if ($soira != -1) {
                $tempa = str_replace('.', 'h', str_pad(number_format($soira, 2), 5, '0', STR_PAD_LEFT));
            }
            $soir = $tempde;
            if ($tempde != '' && $tempa != '') {
                $soir .= $options['separateur_debut_fin'];
            }
            $soir .= $tempa;

            // Ici on met les soirs en détail si heures différentes
            if (count($soirs) > 1) {
                $soirsclair = array();
                $temp = '';
                foreach ($soirs as $hor => $jours) {
                    $temp = '';
                    $soirstemp = array();
                    if (count($jours)) {
                        foreach ($jours as $i) {
                            if (!empty($joursem[$i]))
                                $soirstemp[] = $joursem[$i];
                        }
                        $soirsclair[] = implode(', ', $soirstemp) . '&nbsp;:&nbsp;' . $hor;
                    }
                }
                $soir = implode(" / ", $soirsclair);
            }

            for ($i = 0; $i < count($joursem); $i++) {
                if (!$dernhor['newjours'][$i + 1]['midi'] && !$dernhor['newjours'][$i + 1]['soir']) {
                    $fermetures[] = ucfirst(strtolower($joursem[$i]));
                } elseif (!$dernhor['newjours'][$i + 1]['midi']) {
                    $fermetures[] = ucfirst(strtolower($joursem[$i])) . " " . self::$textesDates[$lang]['midi'];
                } elseif (!$dernhor['newjours'][$i + 1]['soir']) {
                    $fermetures[] = ucfirst(strtolower($joursem[$i])) . " " . self::$textesDates[$lang]['soir'];
                }
            }


        } // Fin if count($horaires)


        return array(
            "midi" => $midi,
            "soir" => $soir,
            "fermetures" => implode($options['separateur_fermetures'], $fermetures),
        );

    }

    /**
     * Retourne une chaine d'affichage des horaires d'une fiche pour une période donnée (horaires en text, avec jours de la semaine si horaires hebdomadaires
     * @param string $du : date de début de la période au format d/m/Y
     * @param string $au : date de fin de la période  au format d/m/Y
     * @param array $hoursTab : données d'une période telles que retournées par la fonction DateUtils::horairesDeLaPeriode (un seul élément du tableau retourné par la fonction)
     * @param array|null $optionsPerso : tableau d'options d'affichage
     * @param string|null $lang : langue d'affichage
     * @return string : chaine avec les horaires en texte à afficher en l'état
     */
    public function affiche_horaires(string $du, string $au, array $hoursTab, ?array $optionsPerso = array(), ?string $lang = 'fr')
    {
        // Options d'affichage par défaut
        $options = array();
        $options['separateur_de_a'] = '-';
        $options['separateur_jour_heure'] = ' : ';
        $options['separateur_heures'] = ' et ';
        $options['separateur_liste_joursem'] = ', ';
        $options['separateur_intervalle_joursem'] = '-';
        $options['separateur_joursem_horaires_differents'] = '<br/>';
        $options['jours_abbreges'] = false;

        if (isset($optionsPerso))
            $options = array_merge($options, $optionsPerso);
        $res = '';
        $joursemW = array('DIMANCHE' => 0, 'LUNDI' => 1, 'MARDI' => 2, 'MERCREDI' => 3, 'JEUDI' => 4, 'VENDREDI' => 5, 'SAMEDI' => 6);
        $joursem = array('LUNDI', 'MARDI', 'MERCREDI', 'JEUDI', 'VENDREDI', 'SAMEDI', 'DIMANCHE');
        $tabNomsJours = self::$nomJour[$lang];
        if ($options['jours_abbreges'])
            $tabNomsJours = self::$nomJourAbr[$lang];


        if ($du == $au) {
            $dateTemp = date_create_from_format('d/m/Y', $du);
            $numJourSem = $dateTemp->format('w');
            $numJourSemFr = $numJourSem - 1;
            if ($numJourSemFr < 0) $numJourSemFr = 6;
            $nomJour = $tabNomsJours[$numJourSem];
            sort($hoursTab[$joursem[$numJourSemFr]]);
            return implode($options['separateur_heures'], $hoursTab[$joursem[$numJourSemFr]]);
        }
        // On créé un tableau fusionné par horaires
        $tabColle = array();
        foreach ($joursem as $idx => $nj) {
            $clehor = '';
            if (isset($hoursTab[$nj]) && count($hoursTab[$nj]) > 0)
                $clehor = implode('-', $hoursTab[$nj]);
            if (!isset($tabColle[$clehor])) {
                $tabColle[$clehor] = new \stdClass();
                $tabColle[$clehor]->numjours = array();
                $tabColle[$clehor]->horaires = array();
            }
            $tabColle[$clehor]->numjours[] = $idx;
            if (isset($hoursTab[$nj])) {
                $tabColle[$clehor]->horaires = $hoursTab[$nj];
            }
        }

        // On trie les jours de chaque créneau horaire
        // Au passage, on repere s'il y a des jours vides à supprimer (présence d'horaires hebdo renseignés et de horaires hebdo son rensiegnes)
        $horairesHebdo = false;
        if (count($tabColle) > 0) {
            foreach ($tabColle as $key => $obj) {
                if ($key != '')
                    $horairesHebdo = true;
                sort($tabColle[$key]->numjours);
            }

        }


        // Maintenant on peut générer du texte fusionné
        if (count($tabColle) == 0)
            return '';
        if (count($tabColle) == 1 && count($tabColle[$key]->numjours) == 7) {
            return implode($options['separateur_heures'], $tabColle[$key]->horaires);
        }

        // s'il y a des horaire hebdo, on supprimme les jours vides
        if ($horairesHebdo && isset($tabColle['']))
            unset($tabColle['']);

        $ret = array();
        foreach ($tabColle as $clehor => $obj) {
            $premJour = $obj->numjours[0];
            $dernJour = $obj->numjours[0];
            $dernJourPrec = $obj->numjours[0];
            for ($i = 1; $i < count($obj->numjours); $i++) {
                $dernJour = $obj->numjours[$i];
                if ($dernJour != $dernJourPrec + 1) {
                    if ($premJour == $dernJourPrec) {
                        $cleTemp = $tabNomsJours[$joursemW[$joursem[$premJour]]];
                        $ret[$cleTemp] = implode($options['separateur_heures'], $tabColle[$clehor]->horaires);
                    } elseif ($premJour == $dernJourPrec - 1) {
                        $cleTemp = $tabNomsJours[$joursemW[$joursem[$premJour]]] . $options['separateur_liste_joursem'] . $tabNomsJours[$joursemW[$joursem[$dernJourPrec]]];
                        $ret[$cleTemp] = implode($options['separateur_heures'], $tabColle[$clehor]->horaires);
                    } else { // intervalle
                        $cleTemp = $tabNomsJours[$joursemW[$joursem[$premJour]]] . $options['separateur_intervalle_joursem'] . $tabNomsJours[$joursemW[$joursem[$dernJourPrec]]];
                        $ret[$cleTemp] = implode($options['separateur_heures'], $tabColle[$clehor]->horaires);
                    }
                    $premJour = $dernJour;
                }
                $dernJourPrec = $dernJour;
            }
            if ($premJour == $dernJourPrec) {
                $cleTemp = $tabNomsJours[$joursemW[$joursem[$premJour]]];
                $ret[$cleTemp] = implode($options['separateur_heures'], $tabColle[$clehor]->horaires);
            } elseif ($premJour == $dernJourPrec - 1) {
                $cleTemp = $tabNomsJours[$joursemW[$joursem[$premJour]]] . $options['separateur_liste_joursem'] . $tabNomsJours[$joursemW[$joursem[$dernJourPrec]]];
                $ret[$cleTemp] = implode($options['separateur_heures'], $tabColle[$clehor]->horaires);
            } else { // intervalle
                $cleTemp = $tabNomsJours[$joursemW[$joursem[$premJour]]] . $options['separateur_intervalle_joursem'] . $tabNomsJours[$joursemW[$joursem[$dernJourPrec]]];
                $ret[$cleTemp] = implode($options['separateur_heures'], $tabColle[$clehor]->horaires);
            }
        }

        $i = 0;
        $res = '';
        foreach ($ret as $titreJours => $hors) {
            if ($i > 0)
                $res .= $options['separateur_joursem_horaires_differents'];
            $res .= $titreJours . $options['separateur_jour_heure'] . $hors;
            $i++;
        }

        return $res;

    }

    /**
     * retourne une expression texte correspondant à un intervalle de dates(du .. au ... ou le ...)
     * datedu et dateau sont des chaines au format JJ/MM/AAAA
     *
     * @param string $datedu date du au format d/m/Y
     * @param string $dateau date au au format d/m/Y
     * @param bool $afficher_du_au_le
     * @param bool $en_clair
     * @param bool $afficher_joursem
     * @param bool $afficher_annee_debut
     * @param bool $afficher_annee_fin
     * @param bool $nomjour_abrege
     * @param bool $nommois_abrege
     *
     * @return string expresion calculéée
     */
    public static function affiche_intervalle_dates(string $datedu, string $dateau, ?string $lang = 'fr', ?array $optionsPerso = array())
    {

        // Options d'affichage par défaut
        $options = array();
        $options['afficher_du'] = true;
        $options['afficher_au'] = true;
        $options['afficher_le'] = true;
        $options['en_clair'] = true;
        $options['afficher_joursem'] = true;
        $options['afficher_annee_debut'] = false;
        $options['afficher_annee_fin'] = true;
        $options['nomjour_abrege'] = false;
        $options['nommois_abrege'] = false;

        $options = array_merge($options, $optionsPerso);

        $datedu = str_replace("-", "/", $datedu);
        $dateau = str_replace("-", "/", $dateau);

        $txtDu = ($options['afficher_du']) ? self::translateDateText('du',$lang) . ' ' : '';
        $txtAu = ($options['afficher_au']) ? ' ' . self::translateDateText('au',$lang) . ' ' : '';
        $txtLe = '';
        // On ne dit pas le dans les autres langues
        if ($lang == 'fr')
            $txtLe = ($options['afficher_le']) ? 'le ' : '';

        $tmpdu = explode("/", $datedu);
        if (count($tmpdu) == 3) {
            $jourdu = $tmpdu[0];
            $moisdu = $tmpdu[1];
            $andu = $tmpdu[2];
        } else {
            return "Err0";
        }
        $tmpau = explode("/", $dateau);
        if (count($tmpau) == 3) {
            $jourau = $tmpau[0];
            $moisau = $tmpau[1];
            $anau = $tmpau[2];
        } else {
            return "Err1";
        }
        $du = mktime(0, 0, 0, (int)$moisdu, (int)$jourdu, (int)$andu);
        $au = mktime(0, 0, 0, (int)$moisau, (int)$jourau, (int)$anau);

        $res = '';
        if ($options['nommois_abrege']) {
            $tabmois = self::$nomMoisAbr;
        } else {
            $tabmois = self::$nomMois;
        }
        if ($options['nomjour_abrege']) {
            $tabjour = self::$nomJourAbr;
        } else {
            $tabjour = self::$nomJour;
        }

        // Pas utilisé ici mais important pour après (horaires)
        $tabjourAbr = array_merge(self::$nomJourAbr[$lang]);

        if ($datedu == $dateau) {
            // Affochage des dates en chiffres
            if (!$options['en_clair']) {
                if ($options['afficher_le'] && !$options['afficher_joursem']) {
                    $res .= $txtLe;
                }
                switch ($lang) {
                    case 'en':
                        $res .= $moisdu . '/' . $jourdu;
                        break;
                    default:
                        $res .= $jourdu . '/' . $moisdu;
                        break;
                }
                if ($options['afficher_annee_debut']) {
                    $res .= '/' . $andu;
                }
            } else {
                // Affichage des dates en texte
                if ($options['afficher_joursem']) {
                    $res .= $tabjour[$lang][date("w", $du)] . ' ';
                } elseif ($options['afficher_le']) {
                    $res .= $txtLe;
                }

                $numjourdu = self::numjour_clair((int)$jourdu, $lang);
                switch ($lang) {
                    case 'en':
                        $res .= $tabmois[$lang][(int)$moisdu] . " " . $numjourdu . ' ';
                        break;
                    case 'de':
                        $res .= $numjourdu . ". " . $tabmois[$lang][(int)$moisdu] . ' ';
                        break;
                    case 'es':
                    case 'pt':
                        $res .= $numjourdu . " de " . $tabmois[$lang][(int)$moisdu] . ' ';
                        break;
                    default:
                        $res .= $numjourdu . " " . $tabmois[$lang][(int)$moisdu] . ' ';
                        break;
                }
                if ($options['afficher_annee_debut']) {
                    $res .= $andu;
                }
            }
        } else { // Dates différentes
            if (!$options['en_clair']) {
                $strdu = '';
                $strau = '';
                switch ($lang) {
                    case 'en':
                        $strdu .= $moisdu . '/' . $jourdu;
                        $strau .= $moisau . '/' . $jourau;
                        break;
                    default:
                        $strdu .= $jourdu . '/' . $moisdu;
                        $strau .= $jourau . '/' . $moisau;
                        break;
                }
                if ($options['afficher_annee_debut']) {
                    $strdu .= '/' . $andu;
                }
                if ($options['afficher_annee_fin']) {
                    $strau .= '/' . $anau;
                }

                if ($options['afficher_au']) {
                    $res = $txtDu . $strdu . $txtAu . $strau;
                } else {
                    $res = $strdu . '-' . $strau;
                }
            } else {
                // Affichage des dates en texte
                $numjourdu = self::numjour_clair((int)$jourdu, $lang);
                $numjourau = self::numjour_clair((int)$jourau, $lang);
                switch ($lang) {
                    case 'en':
                        $strdu = $tabmois[$lang][(int)$moisdu] . " " . $numjourdu . ' ';
                        $strau = $tabmois[$lang][(int)$moisau] . " " . $numjourau . ' ';
                        break;
                    case 'de':
                        $strdu = $numjourdu . ". " . $tabmois[$lang][(int)$moisdu] . ' ';
                        $strau = $numjourau . ". " . $tabmois[$lang][(int)$moisau] . ' ';
                        break;
                    case 'es':
                    case 'pt':
                        $strdu = $numjourdu . " de " . $tabmois[$lang][(int)$moisdu] . ' ';
                        $strau = $numjourau . " de " . $tabmois[$lang][(int)$moisau] . ' ';
                        break;
                    default:
                        $strdu = $numjourdu . " " . $tabmois[$lang][(int)$moisdu] . ' ';
                        $strau = $numjourau . " " . $tabmois[$lang][(int)$moisau] . ' ';
                        break;
                }

                $res = $txtDu;

                if ($options['afficher_joursem']) {
                    $res .= $tabjour[$lang][date("w", $du)] . ' ';
                }

                $res .= $strdu;
                if ($options['afficher_annee_debut']) {
                    $res .= $andu . ' ';
                }


                if ($options['afficher_au']) {
                    $res .= $txtAu;
                } else {
                    $res .= ' - ';
                }

                if ($options['afficher_joursem']) {
                    $res .= $tabjour[$lang][date("w", $au)] . ' ';
                }

                $res .= $strau;
                if ($options['afficher_annee_fin']) {
                    $res .= $anau;
                }
            }
        }

        return $res;
    }


    /**
     * Fonction de traduction autonome multi plateformes pour les petis textes des dates
     * @param $text : texte en français à traduire
     * @param $lang : langue de traduction
     * @return array|mixed|string|string[] : texte traduit
     */
    public static function translateDateText($text, $lang)
    {
        if ($lang == 'fr')
            return $text;

        $translation = array();
        $words = ['du', 'le', 'de', 'à', 'au', 'et', 'h'];
        $translation['en'] = array('du' => 'from', 'de' => 'from', 'à' => 'to', 'au' => 'to', 'et' => 'and', 'le' => '', 'h' => ':');
        $translation['de'] = array('du' => 'vom', 'de' => 'von', 'à' => 'bis', 'au' => 'bis', 'et' => 'und', 'le' => '', 'h' => ':');
        $translation['it'] = array('du' => 'dal', 'de' => 'dalle', 'à' => 'alle', 'au' => 'al', 'et' => 'e', 'le' => '', 'h' => ':');
        $translation['es'] = array('du' => 'del', 'de' => 'de', 'à' => 'a', 'au' => 'al', 'et' => 'y', 'le' => '', 'h' => ':');
        $translation['nl'] = array('du' => 'van', 'de' => 'van', 'à' => 'tot', 'au' => 'tot', 'et' => 'en', 'le' => '', 'h' => ':');
        $translation['pt'] = array('du' => 'de', 'de' => 'das', 'à' => 'às', 'au' => 'a', 'et' => 'e', 'le' => '', 'h' => ':');

        $result = $text;
        foreach ($words as $word) {
            $translated = $translation[$lang][strtolower($word)];
            $result = str_replace($word, $translated, $result);
        }

        return $result;
    }


    /**
     * getFicheHoursAsArrayForDisplayAsArray
     * Retourne un tableau contenant toutes les périodes et pour chacune tous les jours de la semaine avec les horaires de la fiche
     * @param $fiche : objet product tel que retourné par l'API Bridge
     * @param $lang : langue d'affichage
     * @param $options : options d'affichage
     */
    public static function getFicheHoursAsArrayForDisplayAsArray($fiche, $lang, $options=array()) {
        if(!isset($fiche->hours) || !is_array($fiche->hours) || empty($fiche->hours))
            return array();

        $joursem = array('LUNDI', 'MARDI', 'MERCREDI', 'JEUDI', 'VENDREDI', 'SAMEDI', 'DIMANCHE');

        $optionsInt = array(
            'dateFrom' => '',
            'dateTo' => '',
            'one_strict_period' => false,
            'afficher_de_a' => true,
            'afficher_h_heure' => true,
        );
        $options = array_merge($optionsInt, $options);

        $rawData = self::prepareHoursForDisplay($fiche, $lang, $options);

        $tabHours = array();
        foreach($rawData as $key => $period) {
            $translatedJours = array();
            if(is_array($period['jours']) && count($period['jours']) > 0 ) {
                foreach($joursem as $idx => $key ) {
                    if(isset($period['jours'][$key]))
                        $translatedJours[self::$nomJourLundi[$lang][$idx]] = $period['jours'][$key];
                    else
                        $translatedJours[self::$nomJourLundi[$lang][$idx]] = null;
                }
            }

            $tabHours[] = array(
                "key" => $key,
                "du" => $period['du'],
                "au" => $period['au'],
                "datesText" => self::affiche_intervalle_dates($period['du'], $period['au'], $lang, $options),
                "jours" => $translatedJours,
                "comm" => $period['comm'], // TODO : Pas encore géré dans Bridge
                "fin" => '', // TODO : Pas encore géré dans Bridge
            );
        }
        return $tabHours;

    }

    /**
     * getFicheHoursAsArrayForDisplayAsCalendar
     * Retourne un tableau contenant toutes les dates au jour le jour avec un statut et les infos d'horaires correspondant - utilisé pour l'affichage en calendrier
     * @param $fiche : objet product tel que retourné par l'API Bridge
     * @param $lang : langue d'affichage
     * @param $options : options d'affichage
     */
    public static function getFicheHoursAsArrayForDisplayAsCalendar($fiche, $lang, $options=array()) {

        $tabJoursPHP = array(1 =>'LUNDI', 2 =>'MARDI', 3 => 'MERCREDI', 4 =>'JEUDI', 5=> 'VENDREDI', 6=>'SAMEDI', 7 =>'DIMANCHE');

        $startDate = new \DateTime('last monday');
        $startDate->setTime(0,0,0,0);
        $endDate = new \DateTime();
        $endDate->setTime(23,59,0,0);
        $endDate->modify('+1  year');

        // On initialise $tabDates => tout est fermé par défaut
        $currentDate = clone($startDate);
        $tabDates = array();
        while($currentDate < $endDate) {
            $tabDates[$currentDate->format('Y-m-d')] = array(
                "ladate" => $currentDate->format('m-d-Y'),
                "statut" => 0,
                "horaires" => array(),
                "comm" => ''
            );
            $currentDate->modify('+1 day');
        }


        if(isset($fiche->hours) && is_array($fiche->hours) && !empty($fiche->hours)) {
            $joursem = array('LUNDI', 'MARDI', 'MERCREDI', 'JEUDI', 'VENDREDI', 'SAMEDI', 'DIMANCHE');

            $optionsInt = array(
                'dateFrom' => '',
                'dateTo' => '',
                'one_strict_period' => false,
                'afficher_de_a' => true,
                'afficher_h_heure' => true,
            );
            $options = array_merge($optionsInt, $options);

            // On récupère les données d'horaires déjà moulinés et recollés
            $rawData = self::prepareHoursForDisplay($fiche, $lang, $options);
            foreach($rawData as $key => $period) {
                $translatedJours = array();
                // On retrouve les dates où il se passe qulquechose et on met à jour $tabDates
                $currentDate = clone($startDate);
                $dateFrom = date_create_from_format('Y-m-d', $period['duYmd']);
                if($dateFrom > $currentDate)
                    $currentDate = clone($dateFrom);

                $dateTo = date_create_from_format('Y-m-d', $period['auYmd']);

                while($currentDate < $endDate && $currentDate < $dateTo) {
                    $dayOfWeek = (int) $currentDate->format( "N");
                    $jour = $tabJoursPHP[$dayOfWeek];
                    if(isset($period['jours'][$jour]) && !empty($period['jours'][$jour])) {
                        $tabDates[$currentDate->format('Y-m-d')]['statut'] = 1;
                        $tabDates[$currentDate->format('Y-m-d')]['horaires'] = array_merge($tabDates[$currentDate->format('Y-m-d')]['horaires'], $period['jours'][$jour]);
                    } elseif(isset($period['jours'][$jour]) && $period['jours'][$jour] === '') {
                        // Toute la journée
                        $tabDates[$currentDate->format('Y-m-d')]['statut'] = 1;
                    }
                    $currentDate->modify('+1 day');
                }
            }
        }
        return $tabDates;
    }
}
