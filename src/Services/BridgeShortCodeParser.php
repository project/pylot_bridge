<?php

namespace Drupal\pylot_bridge\Services;

class BridgeShortcodeParser {

    // Regex101 reference: https://regex101.com/r/pJ7lO1
    const SHORTOCODE_REGEXP = "/(?P<shortcode>(?:(?:\\s?\\[))(?P<name>[\\w\\-]{3,})(?:\\s(?P<attrs>[\\w\\d,\\s=\\\"\\'\\-\\+\\#\\%\\!\\~\\`\\&\\.\\s\\:\\/\\?\\|]+))?(?:\\])(?:(?P<content>[\\w\\d\\,\\!\\@\\#\\$\\%\\^\\&\\*\\(\\\\)\\s\\=\\\"\\'\\-\\+\\&\\.\\s\\:\\/\\?\\|\\<\\>]+)(?:\\[\\/[\\w\\-\\_]+\\]))?)/u";

    // Regex101 reference: https://regex101.com/r/sZ7wP0
    const ATTRIBUTE_REGEXP = "/(?<name>\\S+)=[\"']?(?P<value>(?:.(?![\"']?\\s+(?:\\S+)=|[>\"']))+.)[\"']?/u";

    public static function parse_shortcodes($text) {
        preg_match_all(self::SHORTOCODE_REGEXP, $text, $matches, PREG_SET_ORDER);
        $shortcodes = array();
        foreach ($matches as $i => $value) {
            $shortcodes[$i]['shortcode'] = $value['shortcode'];
            $shortcodes[$i]['name'] = $value['name'];
            if (isset($value['attrs'])) {
                $attrs = self::parse_attrs($value['attrs']);
                $shortcodes[$i]['attrs'] = $attrs;
            }
            if (isset($value['content'])) {
                $shortcodes[$i]['content'] = $value['content'];
            }
        }

        return $shortcodes;
    }

    private static function parse_attrs($attrs) {
        preg_match_all(self::ATTRIBUTE_REGEXP, $attrs, $matches, PREG_SET_ORDER);
        $attributes = array();
        foreach ($matches as $i => $value) {
            $key = $value['name'];
            $attributes[$i][$key] = $value['value'];
        }
        return $attributes;
    }


    /**
     * BridgeShortCodeParser::process
     *
     * Traite tous les shortcodes Bridge du texte fourni en entrée
     * @param string $text : texte à filtrer contenant potentiellement les shortcodes
     * @param $langcode : code de langue (2 lettres)
     * @return object Objet ayant comme propriétés success (bool), text (string) : texte filtré, build (array of arrays)  : tableau de tableaux de données de rendu Drupal, shortcodeFicheMatched (bool) : true si un short de fiche a été trouvé et remplacé (nécessité de charger les librairies afférentes), , shortcodeListeMatched (bool) : true si un short de liste a été trouvé et remplacé (nécessité de charger les librairies afférentes),  errMsg : message en cas d'erreur
     */
    public static function process($text, $langcode) {
        $res = (object) array(
            'success' => true,
            'text' => $text,
            'build' => array(),
            'shortcodeFicheMatched' => false,
            'shortcodeListeMatched' => false,
            'errMsg' => ''
        );

        // On parse les shortcodes du texte original
        $shortcodes = self::parse_shortcodes($text);
        // On traite les shortcodes de liste
        $dataLists = BridgeShortCodeParser::processWeblists($res->text, $shortcodes, $langcode);
        $res->text = $dataLists->text;
        $res->success = $res->success && $dataLists->success;
        $res->build = array_merge($res->build, $dataLists->build );
        $res->shortcodeListeMatched = $dataLists->shortcodeMatched;
        $res->errMsg .= $dataLists->errMsg;

        // On traite les shortcodes de fiche
        $dataFiches = BridgeShortCodeParser::processWebPages($res->text, $shortcodes, $langcode);
        $res->text = $dataFiches->text;
        $res->success = $res->success && $dataFiches->success;
        $res->build = array_merge($res->build, $dataFiches->build );
        $res->shortcodeFicheMatched = $dataFiches->shortcodeMatched;
        $res->errMsg .= $dataFiches->errMsg;

        return $res ;
    }

    /**
     * Traite le shortcode brliste
     * @param string $text : texte à filtrer contenant potentiellement le shortcode
     * @param $shortcodes : tableau des shortcodes trouvés avec la fonction  BridgeShortcodeParser::parse_shortcodes
     * @param $langcode : code de langue (2 lettres)
     * @return object Objet ayant comme propriétés success (bool), text (string) : texte filtré, data (Object)  : données Bridge, shortcodeMatched (bool) : true si un short a bien été trouvé et remplacé (nécessité de charger les librairies afférentes),  errMsg : message en cas d'erreur
     */
    public static function processWeblists($text, $shortcodes, $langcode) {
        $res = (object) array(
            'success' => true,
            'text' => $text,
            'build' => array(),
            'shortcodeMatched' => false,
            'errMsg' => ''
        );

        $new_text = $text;
        if(is_array($shortcodes) && count($shortcodes) > 0 ) {
            foreach($shortcodes as $shortcode) {
                if($shortcode['name'] !== 'brliste')
                    continue;
                $attributes = array();
                if(empty($shortcode['attrs']))
                    continue;
                foreach($shortcode['attrs'] as $attr) {
                    foreach($attr as $key => $value) {
                        $attributes[$key] = str_replace('"', '', $value);
                    }
                }
                if(!isset($attributes['id']))
                    continue;
                $attributes['lang'] = $langcode;
                $attributes['change'] = '1';
                $attributes['useRequestFilters'] = false;
                $res->shortcodeMatched = true;
                try {
                    // Appel à Bridge, récupération des données
                    $data = BridgeUtils::getListData($attributes);
                    // On prépare l'organisation des données en vue de l'affichage - avec conversion des tableaux associatifs en objets au passage pour simplifier
                    $data = BridgeUtils::prepareListDataForRender($data);

                    // On attache les librairies scripts, css ainsi que le tableau de données à passer aux templates d'affichage
                    $build = BridgeUtils::prepareListViewRenderer($data, 'liste_sit_shortcode');
                    // On retourne les données de rendu Drupal brutes Bridge ainsi que le résultat du parsing HTML
                    $res->build[] = $build;
                    // On fait le rendu HTML
                    $html = \Drupal::service('renderer')->renderPlain($build);
                    $res->text = str_replace($shortcode['shortcode'], $html, $new_text);

                } catch (\Exception $e) {
                    $res->errMsg =  "Erreur 07 shortcode Bridge : " . $e->getMessage() ;
                    $res->text = str_replace($shortcode['shortcode'], $res->errMsg, $new_text);
                    $res->success = false;
                }
            }
        }
        return $res;
    }

    /**
     * Traite le shortcode brfiche
     * @param string $text : texte à filtrer contenant potentiellement le shortcode
     * @param $shortcodes : tableau des shortcodes trouvés avec la fonction  BridgeShortcodeParser::parse_shortcodes
     * @param $langcode : code de langue (2 lettres)
     * @return object Objet ayant comme propriétés success (bool), text (string) : texte filtré, build (array of arrays)  : tableau de tableaux de données de rendu Drupal, shortcodeMatched (bool) : true si un short a bien été trouvé et remplacé (nécessité de charger les librairies afférentes),  errMsg : message en cas d'erreur
     */
    public static function processWebPages($text, $shortcodes, $langcode) {
        $res = (object) array(
            'success' => true,
            'text' => $text,
            'data' => null,
            'build' => array(),
            'shortcodeMatched' => false,
            'errMsg' => ''
        );

        $new_text = $text;
        if(is_array($shortcodes) && count($shortcodes) > 0 ) {
            foreach($shortcodes as $shortcode) {
                if($shortcode['name'] !== 'brfiche') {
                    continue;
                }
                $attributes = array();
                if(empty($shortcode['attrs'])) {
                    continue;
                }

                foreach($shortcode['attrs'] as $attr) {
                    foreach($attr as $key => $value) {
                        $attributes[$key] = str_replace('"', '', $value);
                    }
                }
                if(!isset($attributes['id']) || !isset($attributes['product_code'])) {
                    continue;
                }

                $res->shortcodeMatched = true;

                $attributes['lang'] = $langcode;
                try {
                    // Récupération des données de la fiche SIT
                    $data = BridgeUtils::getDataFiche($attributes['product_code'], $attributes['id']);
                    if($data === false) {
                        $res->errMsg =  "Erreur 08 shortcode Bridge : aucune donnée retournée" ;
                        $res->text = str_replace($shortcode['shortcode'], $res->errMsg, $new_text);
                        $res->success = false;
                    } else {
                        // On prepare la vue
                        $build = BridgeUtils::prepareFicheViewRenderer($data, 'fiche_sit_ajax');
                        // On retourne les données de rendu Drupal brutes Bridge ainsi que le résultat du parsing HTML
                        $res->build[] = $build;
                        // On fait le rendu HTML
                        $html = \Drupal::service('renderer')->renderPlain($build);
                        $res->text = str_replace($shortcode['shortcode'], $html, $new_text);
                    }
                } catch (\Exception $e) {
                    $res->errMsg =  "Erreur 071 shortcode Bridge : " . $e->getMessage();
                    $res->text = str_replace($shortcode['shortcode'], $res->errMsg, $new_text);
                    $res->success = false;
                }
            }
        }
        return $res;
    }
}