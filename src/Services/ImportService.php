<?php

namespace Drupal\pylot_bridge\Services ;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;

class ImportService implements ImportServiceInterface {




    private $errorMsg;
    private $warnings;
    private $log = array("maximum" => array(), "simple" => array());
    private $totalWebLists;
    private $webListsImported;
    private $currentLog;
    private $isCmdLine = false;
    private $tmpDebug = false;
    private $container;

    /**
     * Constructs a new  object.
     */
    public function __construct() {
        $this->errorMsg = '';
        $this->warnings = array();
        $this->log = array("maximum" => array(), "simple" => array());
        $this->totalWebLists = 0;
        $this->webListsImported = 0;
    }
    /*
    ************
    Fonctions permettant l'import en ajax
    ************
    */

    /**
     * Fonction appelée en AJAX au clic sur le bouton Importer
     * Récupère la liste des langues associées
     * Retourne un JSON qui contient bool success et json data - data contient un tableau de langues (si success) ou est null (si !success)
     *
     * @return json
     */
    public function bridge_list_langs_ajax()
    {
        $languages = BridgeUtils::getLanguagesList();

        $tmp = new stdClass();
        $tmp->key = 0;
        $tmp->lang = 'fr';
        $tabLangs = array($tmp);
        foreach ($languages as $key => $lang) {
            if ($lang == 'fr') continue; // Il faut sauter le français car déjà fait
            $tmp = new stdClass();
            $tmp->key = $key;
            $tmp->lang = $lang;
            $tabLangs[] = $tmp;
        }
        header('Content-Type: application/json');

        BridgeUtils::sendJsonSuccess($tabLangs);
    }

    /**
     * Fonction appelée en AJAX au clic sur le bouton Importer
     * Récupère la liste des weblists Bridge associées, filtrées selon le site indiqué dans les paramètres d'import
     * Retourne un JSON qui contient bool success et json data - data contient un tableau de langues (si success) ou un message d'erreur (si !success)
     *
     * @return json
     */
    public function bridge_list_weblists_ajax()
    {
        $urlBridge = BridgeUtils::getUrlBridge();

        if (empty($urlBridge)) {
            $this->errorMsg = 'URL Bridge indéfinie - contrôlez les paramètres';
            BridgeUtils::sendJsonError($this->errorMsg);
        }

        // on envoie vers la bonne route
        // JM 22/03/2022 : on change le nom du paramètre (update -> v2), moins risqué
        $urlImport = $urlBridge . "/weblist/getForImport?v2=1";

        // si un site est renseigné, on ne récupère que les listes de ce site
        $siteId = BridgeUtils::getSiteBridge();

        if (empty($siteId)) {
            $this->errorMsg = 'Site indéfini dans les paramètres - contrôlez les paramètres';
            BridgeUtils::sendJsonError($this->errorMsg);
        }
        $urlImport = $urlImport . '&site=' . $siteId;
        $data = array();
        $tmpData = file_get_contents($urlImport);

        if (!empty($tmpData)) {
            $tmpDataDecoded = json_decode($tmpData);

            if ($tmpDataDecoded->success) {
                $data = $tmpDataDecoded->data;
                BridgeUtils::sendJsonSuccess($data);
            } else {
                $this->errorMsg = $tmpDataDecoded->message;
                BridgeUtils::sendJsonError($this->errorMsg);
            }
        } else {
            $this->errorMsg = "ERREUR : Échec de l'import depuis l'URL " . $urlImport;
            BridgeUtils::sendJsonError($this->errorMsg);
        }
    }

    /**
     * Fonction appelée en AJAX par la fonction traiterImport()
     * Met à jour les catégories, récupère la liste des produits (posts) d'une liste (catégorie) donnée dans une langue donnée
     * Retourne un JSON qui contient bool success et json data - data contient un tableau de codes produits (si success) ou un message d'erreur (si !success)
     *
     * @return json
     */
    public function import_bridge_ajax()
    {
        $lang = $_POST['lang'];
        $webListId = $_POST['weblist'];

        // Mise à jour des catégories et réécritures d'urls
        $temp1 = $this->update_sit_terms_from_bridge($webListId, $lang);

        if (!$temp1) {
            BridgeUtils::sendJsonError($this->errorMsg);
        } elseif ($temp1 === 'skip') {
            // Si cette liste n'existe pas dans cette langue, on renvoie un tableau vide
            BridgeUtils::sendJsonSuccess([]);
        }

        // On lance l'import
        $temp2 = $this->doImportForLang(false, $lang, $webListId);

        if (!$temp2) {
            BridgeUtils::sendJsonError($this->errorMsg);
        }

        $importedProducts = $temp2['products'];
        $term = $temp2['term'];

        $result = [$importedProducts, $term];

        BridgeUtils::sendJsonSuccess($result);
    }

    /**
     * Fonction appelée en AJAX
     * Récupère chaque produit et les données associées
     *
     * @return json
     */
    function bridge_list_products_ajax()
    {
        $language = $_REQUEST['lang'];
        $productCode = $_REQUEST['product'];
        $term = $_REQUEST['term'];
        // TODO : il va falloir ajouter l'argument webList'
        $res = $this->importPostByProductCodeAndLanguage($productCode, $language, $term);

        if ($res) {
            BridgeUtils::sendJsonSuccess();
        } else {
            BridgeUtils::sendJsonError($this->errorMsg);
        }
    }

    /**
     * Appelée en AJAX, met à jour les catégories et les associe aux posts
     */
    public function update_terms_ajax()
    {
        $isCmdLine = false;

        $res = $this->update_sit_terms_from_bridge();

        if (!$res) {
            BridgeUtils::sendJsonError($this->errorMsg);
        } else {
            BridgeUtils::sendJsonSuccess();
        }
    }

    public function update_post_categories_ajax()
    {
        $isCmdLine = false;

        // On commence par affecter les catégories à tous les posts à partir d'un flux Bridge
        $res = $this->updatePostCategories($isCmdLine);
        if ($res) {
            $tabPosts = $res['tabPosts'];
            $productsInBridge = $res['productsInBridge'];

            $result = array('tabPosts' => $tabPosts, 'productsInBridge' => $productsInBridge);
            BridgeUtils::sendJsonSuccess($result);
        } else {
            BridgeUtils::sendJsonError($this->errorMsg);
        }
    }

    public function delete_obsolete_posts_ajax()
    {
        $langs = $_REQUEST['langs'];

        $product_code = $_REQUEST['productCode'];

        $res = $this->deleteObsoletePosts($langs, $product_code);

        if ($res) {
            BridgeUtils::sendJsonSuccess();
        } else {
            BridgeUtils::sendJsonError($this->errorMsg);
        }
    }

    /**
     * Appelée en AJAX, met à jour les règles de réécriture d'URL
     */
    public function update_rewrite_rules_ajax()
    {
        $isCmdLine = false;

        $res = $this->updateRewriteRules($isCmdLine);

        if ($res) {
            $this->setLog('Règles de réécriture des URL mises à jour');
            BridgeUtils::sendJsonSuccess();
        } else {
            $this->errorMsg = 'Erreur lors de la mise à jour des règles de réécriture';
            BridgeUtils::sendJsonError($this->errorMsg);
        }
    }

    /**
     * Appelée en AJAX, met à jour les liens de traduction
     */
    public function update_translation_links_ajax()
    {
        $isCmdLine = false;

        $res = $this->majLiensTrads($isCmdLine);
        if ($res) {
            $this->setLog("Mise à jour des liens de traduction");
            BridgeUtils::sendJsonSuccess();
        } else {
            $this->errorMsg = 'Erreur lors de la mise à jour des liens de traduction';
            BridgeUtils::sendJsonError($this->errorMsg);
        }
    }

    /**
     * Permet la suppression des images liées aux fiches SIT lors de la suppression de celles-ci
     *
     * @return json
     */
    public function delete_all_data_ajax()
    {
        $successMessage = "Toutes les données SIT ont été supprimées";
        $success = $this->DeleteAllData();
        if($success !== true) {
            BridgeUtils::sendJsonError($success);
        } else {
            BridgeUtils::sendJsonSuccess($successMessage);
        }
    }

    public function deleteAllData() {

        $successMessage = '';

        $isEmpty = 0;

        $this->setLog("Début de la suppression");
        $query = \Drupal::entityQuery('node')
            ->condition('type', 'fiche_sit');
        $results = $query->execute();
        $controller = \Drupal::entityTypeManager()->getStorage('node');
        $entities = $controller->loadMultiple($results);
        $entities = $controller->delete($entities);
/*
        if (isset($results) && is_array($results)) {
            if (empty($results)) {
                $successMessage .= "Pas de contenus à supprimer\n";
                $isEmpty += 1;
            }
        } else {
            return 'Erreur lors de la suppression des images';
        }

        // Suppression de tous les posts
        foreach($results as $node) {
            if($node)
                $node->delete($node);
        }
        */
        // Suppression des catégories
        $controller = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
        $entities = BridgeUtils::getAllBridgeTerms();
        $controller->delete($entities);

        $successMessage = "Toutes les données SIT ont été supprimées";
        $this->setLog($successMessage);
        return true;
    }

    /**
     *  Met à jour les terms rubrique_sit_primaire à partir des weblists de Bridge
     *
     * @return bool
     *
     */
    public function update_sit_terms_from_bridge($webListId = '', $lang = '')
    {
        // Options connexion à Bridge et import
        $config = \Drupal::config('pylot_bridge.bridgeconfig');
        $urlBridge = $config->get('url_bridge');
        $siteId = $config->get('bridge_site');
        $languages = BridgeUtils::getLanguagesList();

        // si on n'importe pas une liste en particulier mais toutes
        if (empty($webListId)) {
            // JM 22/03/2022 : on change le nom du paramètre (update -> v2), moins risqué
            $urlImport = $urlBridge . "/weblist/getForImport?v2=1";

            // si un site est enregistré, on n'update que les données associées à ce site
            if (empty($siteId) ) {
                $this->errorMsg = 'Veuillez renseigner le numéro de site dans les paramètres.';
                return false;
            }
            $urlImport = $urlImport . '&site=' . $siteId;

            $data = array();
            $doublonsListSlug = array();

            $tmpData = file_get_contents($urlImport);
            if (!empty($tmpData)) {
                $tmpDataDecoded = json_decode($tmpData);
                if ($tmpDataDecoded->success) {
                    $data = $tmpDataDecoded->data;
                } else {
                    $this->errorMsg = 'Erreur appel Bridge getForImport : ' . $tmpDataDecoded->message;
                    return false;
                }
            } else {
                $this->errorMsg = "ERREUR : Échec de l'import des listes depuis l'URL " . $urlImport;
                return false;
            }

            if (!empty($data) && is_array($data)) {
                $this->totalWebLists = count($data);

                foreach ($languages as $key => $lang) {
                    $langUpper = strtoupper($lang);
                    // On parcourt les WebLists
                    foreach ($data as $webList) {
                        $term = BridgeUtils::createTermFromWebList($webList, $lang);
                    }
                }
            } else {
                $this->errorMsg = 'Aucune liste à importer - Bridge est peut-être inaccessible - Code 1';
                return false;
            }
        } else {
            // JM 22/03/2022 : on change le nom du paramètre (update -> v2), moins risqué
            $urlImport = $urlBridge . "/weblist/getForImport?id=" . $webListId . '&language=' . $lang . '&v2=1';

            // si un site est enregistré, on n'update que les données associées à ce site
            if (!empty($siteId) && $siteId !== -1) {
                $urlImport = $urlImport . '&site=' . $siteId;
            }

            $webLists = array();
            $webList = null;
            $tmpWebList = file_get_contents($urlImport);

            if (!empty($tmpWebList)) {
                $tmpWebListDecoded = json_decode($tmpWebList);
                if ($tmpWebListDecoded->success) {
                    $webLists = $tmpWebListDecoded->data;
                } else {
                    $this->errorMsg = $tmpWebListDecoded->message;
                    return false;
                }
            } else {
                $this->errorMsg = "ERREUR : Échec de l'import de la liste (" . $lang . ") depuis l'URL " . $urlImport;
                return false;
            }

            if (!empty($webLists) && is_array($webLists) && count($webLists) > 0) {
                $webList = $webLists[0];

                $term = BridgeUtils::createTermFromWebList($webList);
            } else {
                $this->errorMsg = 'Aucune liste à importer - Bridge est peut-être inaccessible - Code 2';
                return false;
            }
        }
        return true;
    }

    /**
     * Importe la liste des produits associés à chaque weblist
     *
     * @return bool si $isCmdLine
     * @return json si !$isCmdLine
     */
    public function doImportForLang($isCmdLine = false, $language = 'fr', $webListId = null)
    {

        global $wpdb;
        $doublonsListSlug = array();
        $langUpper = strtoupper($language);

        $this->isCmdLine = $isCmdLine;
        $this->errorMsg = '';
        $this->warnings = array();
        $this->log = array("maximum" => array(), "simple" => array());
        $this->totalWebLists = 0;
        $this->webListsImported = 0;

        $res = array('success' => false, 'products' => array(), 'term' => array());
        $importerImages = false;

        // Options connexion à Bridge
        $urlBridge = BridgeUtils::getUrlBridge();
        $siteId = BridgeUtils::getSiteBridge();
        if (empty($urlBridge)) {
            $this->errorMsg = 'URL Bridge indéfinie - contrôlez les paramètres';
            return false;
        }
        if (empty($siteId)) {
            $this->errorMsg = 'Site Bridge indéfini - contrôlez les paramètres';
            return false;
        }

        if ($webListId === null) {
            $this->setLog("Récupération des listes ($langUpper)");
        }

        // JM 22/03/2022 : on change le nom du paramètre (update -> v2), moins risqué
        $urlImport = $urlBridge . "/weblist/getForImport?v2=1";

        // si on ne veut importer qu'une seule liste
        if (!empty($webListId)) {
            $urlImport .= "&id=" . $webListId;
        } elseif ($siteId !== -1) {
            // si un site est enregistré, on n'importe que les listes qui lui sont associées
            $urlImport = $urlImport . '&site=' . $siteId;
        }

        if ($this->tmpDebug) {
            $this->setLog("URL : " . $urlImport);
        }


        $data = array();
        $tmpData = file_get_contents($urlImport);
        if (!empty($tmpData)) {
            $tmpDataDecoded = json_decode($tmpData);
            if ($tmpDataDecoded->success) {
                $data = $tmpDataDecoded->data;
            } else {
                $this->errorMsg = $tmpDataDecoded->message;
                return false;
            }
        } else {
            $this->errorMsg = "ERREUR : Échec de l'import de listes depuis l'URL " . $urlImport;
            return false;
        }

        if (!empty($data) && is_array($data)) {
            $this->totalWebLists = count($data);

            // Ce tableau servira à la fin pour supprimer les posts SIT qui ne sont plus dans les flux WebList
            $tabPostID = array();
            // Ce tableau servira à affecter tous les terms de chaque post à la fin (nécessité d'avoir tout parcouru avant)
            $tabTerms = array();
            // On parcourt les WebList
            foreach ($data as $webList) {
                $this->setLog("on récupère les $webList->slug");
                $slug = $webList->slug;
                if ($language != 'fr' && empty($webList->slugTranslation->$langUpper)) {
                    $slug = 'weblist-' . $webList->id; // On continue même si le slug en langue est vide car il n'est pas utilisé sur Drupal
                } elseif ($language != 'fr') {
                    $slug = $webList->slugTranslation->$langUpper;
                    $title = $webList->titleTranslations->$langUpper;
                } elseif ($language == 'fr') {
                    $title = $webList->title;
                }
/* Pas besoin de slug sur Drupal
                if (empty($slug) && !empty($title)) {
                    $this->errorMsg = "Erreur 2 : Slug vide dans la liste n°" . $webList->id . " (" . $language . ")";
                    return false;
                }
*/
                if (empty($title) && empty($slug)) {
                    return 'skip';
                }

                $term = null;
                // On cherche si le term (catégorie) existe
                $terms = BridgeUtils::getTermIdByWeblistId($webList->id);
                if(empty($terms)) {
                    $this->errorMsg = "Erreur 5 : pas de term correspondant à  la liste n°" . $webList->id . " (" . $language . ")";
                    return false;
                }
                if(is_numeric($terms)) {
                    $res['term'] = $terms;
                } elseif(is_array($terms)) {
                    foreach($terms as $tid => $term) {
                        $res['term'] = $term;
                    }
                }

                // Maintenant on insère les posts et on les rattache au term ( $term['term_id'] de la taxonomie $term['term_taxonomy_id'])
                // JM 22/03/2022 : on change le nom du paramètre (update -> v2), moins risqué
                $urlImport = $urlBridge . "/weblist/getProductsForImport/" . $webList->id . '?language=' . $language . '&v2=1';

                $webList = new \stdClass();
                $this->setLog($urlImport);
                $tmpWebList = file_get_contents($urlImport);

                if (!empty($tmpWebList)) {
                    $tmpWebListDecoded = json_decode($tmpWebList);
                    if ($tmpWebListDecoded->success) {
                        $webList = $tmpWebListDecoded->data;
                    } else {
                        $this->errorMsg = $tmpWebListDecoded->message;
                        return false;
                    }
                } else {
                    $this->errorMsg = "ERREUR : Échec de l'import des listes depuis l'URL " . $urlImport;
                    return false;
                }

                if ($this->tmpDebug) {
                    $this->setLog("WEBLIST urlImport : $urlImport");
                }

                if (!empty($webList) && is_object($webList)) {
                    if (isset($webList->selection)
                        && isset($webList->selection->results)
                        && isset($webList->selection->results->products)
                        && is_array($webList->selection->results->products)
                        && count($webList->selection->results->products) > 0) {

                        $products = $webList->selection->results->products;
                        foreach ($products as $product) {
                            if ($isCmdLine) {
                                // Quand on fait un import en ligne de commande, c'est OK, on lance direct l'import de la fiche
                                $tempSuccess = $this->importPostByProductCodeAndLanguage($product->productCode, $language, $term, $webList);
                                if($tempSuccess == false) {
                                    $this->errorMsg = 'Erreur à l\'import de la fiche n°' . $product->productCode. ' : ' . $this->errorMsg;
                                    $this->setLog( $this->errorMsg);
                                    return false;
                                }
                                //$this->setLog('On récupère la fiche '. $product->productCode.' de la weblist '.$webList->slug.' en '.$language);
                            } else {
                                // Quand on fait un import via la page Imports du plug-in, on fragmente en une requête AJAX par fiche (pour éviter timeout si une seule grosse requête) donc ici on ne récupère que les numéros des fiches à importer
                                $res['products'][] = (string)$product->productCode;
                            }
                        }
                    } // Fin if resultats dans la WebList
                } else {
                    $this->errorMsg = 'La liste est vide';
                    $this->setLog('La liste est vide');
                    return false;
                } // Fin isset(webList)
                $this->webListsImported++;
            } // Fin Foreach WebList

            // Maintenant, on va affecter les terms aux posts selon le tableau construit auparavant
            // On  en profite pour repérer les fiches qui n'ont pas de catégorie primaiure avec URL canonique

            // NE FAIRE QUE LORS D'UN IMPORT COMPLET DE TOUTES LES WEBLIST SINON ON GENERE DES 404
            // Patch Nicolas 22/10/2021 : la suppression des fiches obsolètes est faite dans la fonction updatePostCategories appelée dans updateRewriteRules
            if ($webListId === null) {
                $res = $this->updateRewriteRules($isCmdLine);
                if (!$res) {
                    // $this->errorMsg = 'Erreur lors de la mise à jour des règles de réécriture';
                    return false;
                }
                $this->setLog("Affectation des rubriques aux fiches ($langUpper) => ✅");
                $this->setLog("Enregistrement des règles de réécriture d'URL ($langUpper) => ✅");
                $this->setLog("Suppression des fiches obsolètes ($langUpper) => ✅");
            }

        } else {
            $this->errorMsg = 'Aucune liste à importer - Bridge est peut-être inaccessible - Code 3';
            return false;
        }

        if ($webListId === null) {
            $this->setLog("Fin de l'import $langUpper");
        }
        if ($isCmdLine) {
            return true;
        } else {
            $res['success'] = true;

            return $res;
        }
    }
    /**
     * Cette fonction met à jour les catégories de tous les posts selon les weblists de Bridge
     * Elle met également à jour les règles de réécriture d'URL
     * Et elle supprime les posts de type fiche-sit qui n'existent plus dans Bridge
     * @return bool
     */
    function updateRewriteRules($isCmdLine = false)
    {

        $this->isCmdLine = $isCmdLine;
        $this->setLog('On commence la réécriture');

        // Si on n'est pas en ligne de commande, on commence par affecter les catégories à tous les posts à partir d'un flux Bridge (sinon on le fait dans une requête séparée)
        if ($isCmdLine) {
            $this->setLog('On commence la maj des catégorie');
            $tmp = $this->updatePostCategories($isCmdLine);
            if (!$tmp) {
                return false;
            }
            $this->setLog('Maj des catégorie finie');
        }
/* sous drupal, la mise en place des alias est faite à l'import des termes
        $this->setLog('Début de la reecriture des liens');
        $rules = array();

        $terms = get_terms(array(
            'hide_empty' => false,
            'taxonomy' => 'rubrique_sit_primaire'
        ));

        foreach ($terms as $term) {
            $reecriture_faite = false;
            // Si un permalien personnalisé est défini, il a priorité sur le reste
            $termLang = BridgeUtils::getTermLanguage($term->term_id);
            //  $termTranslations = pll_get_term_translations($term->term_id);

            $langs = BridgeUtils::getLanguagesList();

            $permalink = get_term_meta($term->term_id, 'permalien', true);
            // Par défaut on considère que le permalien est automatique
            $isAutomaticPermalink = true;
            // Si l'utilisateur a laissé l'option "Automatique activé", le permalien est vide, on le remplit
            if (empty($permalink)) {
                // Sous Drupal, on a mis en place les liens en alias à l'import
                $catRoot = BridgeUtils::getTaxonomyRootUrl();
                $permalink = $catRoot . "/" . $term->slug;
            } else {
                // si qqchose dans permalien, on prend le permalien
                $isAutomaticPermalink = false;
            }

            // Ajout 09/20  on ajoute l'ID de modèle de fiche dans la réécriture de manière à pouvoir avoir plusieurs fiches de détail différentes par fiche et pas juste celle attachée à la rubrique primaire
            $idFicheBridge = get_term_meta($term->term_id, 'weblist_ficheId', true);
            $doNotCreateRewriteRuleForList = get_term_meta($term->term_id, 'weblist_doNotCreateRewriteRuleForList', true);
            if (!empty($permalink)) {
                if(!$isAutomaticPermalink) {
                    // Regex pour la catégorie : uniquement si on a pas coché la case doNotCreateRewriteRuleForList
                    if(empty($doNotCreateRewriteRuleForList) || $doNotCreateRewriteRuleForList === false)
                        $rules[$permalink.'$'] = 'index.php?rubrique_sit_primaire='.$term->slug;

                    // Regex pour les fiches de détails
                    $rules[$permalink . '/(.*)[/]?$'] = 'index.php?post_type=fiche_sit&fiche_sit=$matches[1]&name=$matches[1]&idFicheBridge=' . $idFicheBridge . '&cat=' . $term->term_id;
                } else {
                    // Regex pour les fiches de détails
                    $rules[$permalink . '/(.*)[/]?$'] = 'index.php?post_type=fiche_sit&fiche_sit=$matches[1]&name=$matches[1]&idFicheBridge=' . $idFicheBridge . '&cat=' . $term->term_id;

                    // Patch 29/11/2021 : on ajoute une deuxième règle de réécriture avec un préfixe de langue (suite bug sur certaines instances polylang)
                    $rules['(' . implode('|',$langs) . ')/' . $permalink . '/(.*)[/]?$'] = 'index.php?post_type=fiche_sit&lang=$matches[1]&fiche_sit=$matches[2]&name=$matches[2]&idFicheBridge=' . $idFicheBridge . '&cat=' . $term->term_id;
                }
            }
        }
*/
        /*
        foreach ($hiddenOptions['rewrite_rules'] as $expr => $rewrite) {
            add_rewrite_rule($expr, $rewrite, 'top');
        }

        flush_rewrite_rules();
        $this->setLog('Reecriture terminée');
        */
        return true;
    }

    /**
     * Met à jour toutes les catégories de toutes les fiches sit d'après un gros flux Bridge
     * Et supprime les posts fiche-sit qui n'existent plus dans Bridge
     * @return bool
     */
    public function updatePostCategories($isCmdLine = false)
    {
        // DRUPAL : tout le travail est fait dans l'import des fiches car on ne traite que les catégories principales
        // return true ;

        $res = array(
            'success' => false,
            'tabPosts' => array(),
            'productsInBridge' => array()
        );

        $siteId = BridgeUtils::getSiteBridge();

        // Options connexion à Bridge
        $urlBridge = BridgeUtils::getUrlBridge();

        // si un site est sélectionné, on n'update que les listes associées à ce site
        // JM 22/03/2022 : on change le nom du paramètre (update -> v2), moins risqué
        $urlImport = $urlBridge . "/weblist/getAllProductCategories?v2=1";
        if (!empty($siteId)) {
            $urlImport = $urlImport . '&site=' . $siteId;
        }
        $languages = BridgeUtils::getLanguagesList();

        // Ce tableau servira à savoir quelles fiches doivent être supprimées de WordPress car absentes de Bridge
        // On y stocke des product codes
        $productsInBridge = array();

        // On récupère de Bridge toutes les weblists et leurs fiches associées
        $data = array();
        $tmpData = file_get_contents($urlImport);
        $this->setLog('On attaque la vérification des fiches présentes');

        if (!empty($tmpData)) {
            $tmpDataDecoded = json_decode($tmpData);
            if ($tmpDataDecoded->success) {
                $data = $tmpDataDecoded->data;
                $data = (array)$data;
                $this->setLog($urlImport.' il y a '.count($data).' fiches');
            } else {
                $this->errorMsg = $tmpDataDecoded->message;
                return false;
            }
        } else {
            $this->errorMsg = "Échec de l'import des catégories";
            return false;
        }

        if (!empty($data)) {

            // On construit un tableau d'associations productCode <=> postID à partir des données WordPress
            $this->setLog("On construit un tableau d'associations productCode <=> postID à partir des données CMS");
            $node_manager = \Drupal::entityTypeManager()->getStorage('node');

            $query = \Drupal::entityQuery('node')
                ->condition('type', 'fiche_sit')
                ->condition('langcode', 'fr');
            $products = $query->execute();
            $tabPosts = array();
            if (!empty($products) && is_array($products)) {
                foreach ($products as $nid) {
                    $node = $node_manager->load($nid);

                    $productCode = $node->get('field_code_sit')->value;
                    $postId = $nid;
                    if (!isset($tabPosts[(string)$productCode])) {
                        $tabPosts[(string)$productCode] = array();
                    }
                    // $tabPosts[(string)$productCode][$lang] = $postId;
                    $tabPosts[(string)$productCode] = $postId;
                }

                if (!$isCmdLine) {
                    $res['tabPosts'] = $tabPosts;
                }
            } else {
                $this->errorMsg = "Erreur à la récupération des produits. Vérifiez que la base n'est pas vide.";
                return false;
            }


            // On construit un tableau d'associations weblistId <=> term_id
            $tabTerms = array();
            $terms = BridgeUtils::getAllBridgeTerms();
            // $term_manager = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
            foreach ($terms as $term) {
                // Le numéro de WebList sert d'identifiant entre langues
                $webListId = $term->get('field_sit_list_id')->value;
                // $lang = BridgeUtils::getTermLanguage($term->term_id);
                if (!isset($tabTerms[(string)$webListId]))
                    $tabTerms[(string)$webListId] = array();
                $tabTerms[(string)$webListId] = $term->id();
            }


            // $alias_manager = \Drupal::service('path_alias.manager');
            // $aliasNode = $alias_manager->getAliasByPath("/node/" . $postId, 'fr');

            $alias_manager = \Drupal::entityTypeManager()->getStorage('path_alias');
            $ficheRootUrl = BridgeUtils::getFicheRootUrl();
            $langs = BridgeUtils::getLanguagesList();
            // Maintenant on peut mettre à jour toutes les catégories de tous les posts
            foreach ($data as $product_code => $infos) {
                // On référence le productCode comme étant dans Brigde
                if(!in_array($product_code, $productsInBridge))
                    $productsInBridge[] = $product_code;
                // On retrouve le post pour lui affecter les catégories selon les weblists
                if(!isset($tabPosts[$product_code])) {
                    $this->errorMsg = 'ANOMALIE : la fiche n° ' . $product_code . ' n\'existe pas dans Drupal, veuillez relancer un import complet.';
                    return false;
                }
                $postId = $tabPosts[$product_code];
                $postTerms = array(); // Tous les termes associés au post
                $postTermPrimaryCategory = null; // le terme qui correpondent à une catégorie primaire
                if (!empty($infos->categories)) {
                    foreach ($infos->categories as $webList) {
                        if (isset($tabTerms[( string )$webList->id]) && isset($tabTerms[( string )$webList->id])) {
                            $postTerms[] = $tabTerms[$webList->id];
                            if(isset($webList->permalinkIsCanonical) && $webList->permalinkIsCanonical === true)
                                $postTermPrimaryCategory = $tabTerms[$webList->id];
                            /*  INUTILE SOUS DRUPAL CAR LES ALIAS SON AFFECTES UNIQUEMENT SUR LA RUBRIQUE CANONIQUE
                            // TODO : ici forcer le lien canonique du post si la case rubrique principale est cochée sur la liste
                            $permalink = "";
                            if(!empty($webList->permalinkUrl) && isset($webList->permalinkIsCanonical) && $webList->permalinkIsCanonical === true) {
                                $permalink = $webList->permalinkUrl ;
                                if(mb_substr($permalink, 0, 1) !== '/')
                                    $permalink = '/' . $permalink;
                            } else {
                                // Todo : voir s'il faut gérer l'insertion de l'url de categorie avant
                                $permalink = $ficheRootUrl ;
                            }
                            if(!empty($permalink)) {
                                $permalink .= '/' . $infos->slug;
                            }
                            // On recherche s'il existe un alias pour ce node
                            $aliasNode = $alias_manager->loadByProperties([
                                'path'     => "/node/" . $postId,
                                'langcode' => 'fr'
                            ]);

                            // On force l'alias du node - on le créé s'il n'y en a pas
                            if(empty($aliasNode)) {
                                $alias_manager->create([
                                    'path'     => "/node/" . $postId,
                                    'alias'    => $permalink,
                                    'langcode' => 'fr'
                                ])->save();
                            } else {
                                foreach($aliasNode as $alias_object) {
                                    $alias_object->alias = $permalink;
                                    $alias_object->save();
                                }
                            }


                            // Maintenant on va également mettre à jour les alias des traductions
                            if(count($langs) > 0) {
                                foreach($langs as $lang) {
                                    if($lang == 'fr') continue;
                                    $permalink = "";
                                    $langupper = strtoupper($lang);
                                    if(!empty($webList->permalinkUrlTranslation) && is_object($webList->permalinkUrlTranslation) && !empty($webList->permalinkUrlTranslation->$langupper) && isset($webList->permalinkIsCanonical) && $webList->permalinkIsCanonical === true) {
                                        $permalink = $webList->permalinkUrlTranslation->$langupper;
                                    } else {
                                        // Todo : voir s'il faut gérer l'insertion de l'url de categorie avant
                                        $permalink = $ficheRootUrl ;
                                    }

                                    if(!empty($webList->slugTranslation) && is_object($webList->permalinkUrlTranslation) && !empty($webList->permalinkUrlTranslation->$langupper) && isset($webList->permalinkIsCanonical) && $webList->permalinkIsCanonical === true) {
                                        $permalink = $webList->permalinkUrlTranslation->$langupper;
                                    } else {
                                        // Todo : voir s'il faut gérer l'insertion de l'url de categorie avant
                                        $permalink = $ficheRootUrl ;
                                    }
                                    if(!empty($permalink)) {
                                        $permalink .= '/' . $infos->slug;
                                    }
                                    $aliasNode = $alias_manager->loadByProperties([
                                        'path'     => "/node/" . $node->id(),
                                        'langcode' => $lang
                                    ]);
                                    if(empty($aliasNode)) {
                                        $alias_manager->create([
                                            'path'     => "/node/" . $postId,
                                            'alias'    => $permalink,
                                            'langcode' => $lang
                                        ])->save();
                                    } else {
                                        // On force l'alias du node
                                        foreach ($aliasNode as $alias_object) {
                                            $alias_object->alias = $permalink;
                                            $alias_object->save();
                                        }
                                    }
                                }
                            }
                            */
                        }
                    }
                }
                // Ici on affecte les rubriques additionnelles
                if(!empty($postTerms)) {
                    // Dans ce mode, on affecte une seule taxonomie à la fiche
                    if (count($langs) > 0) {
                        foreach ($langs as $lang) {
                            if ($lang == 'fr') {
                                $node = $node_manager->load($postId);
                                $node->set('field_rubriques_sit', $postTerms);
                                $node->save();
                            } else {
                                // Mise à jour des catégories traduites sur les posts traduits
                                $nodeFr = $node_manager->load($postId);
                                $translatedTerms = array();
                                foreach($postTerms as $tidFr) {
                                    $termFr = \Drupal\taxonomy\Entity\Term::load($tidFr);
                                    if(!empty($termFr)) {
                                        if($termFr->hasTranslation(($lang))) {
                                            $term = $termFr->getTranslation($lang);
                                            $translatedTerms[] = $term;
                                        }
                                    }
                                }
                                if(!empty($nodeFr) && $nodeFr->hasTranslation($lang) && !empty($translatedTerms)) {
                                    $node = $nodeFr->getTranslation($lang);
                                    $node->set('field_rubriques_sit', $translatedTerms);
                                    $node->save();
                                }
                            }
                        }
                    }
                }
                //if(!empty($postTerms)) {
                //    // Dans ce mode, on affecte une seule taxonomie à la fiche
                //    $node = $node_manager->load($postId);
                //    $node->set('field_all_rubriques', $postTerms);
                //    $node->save();
                //}
            }

            // si on est en ligne de commande on appelle directement la suite
            if ($isCmdLine) {
                foreach ($tabPosts as $product_code => $nodeId) {
                    if (!in_array((string)$product_code, $productsInBridge)) {
                        $this->setLog($product_code);
                        $this->deleteObsoletePosts($nodeId, $product_code);
                        // $this->setLog("DEBUGGAGE - Suite à la suppression inopinnée de la fiche");
                    }
                }
                $this->setLog('Catégories mises à jour');
                return true;

            } else {
                // si on est en AJAX on retourne les données nécessaires pour relancer ensuite un appel par fiche
                $res['success'] = true;
                $res['productsInBridge'] = $productsInBridge;

                return $res;
            }

        } else {
            if (!$isCmdLine) {
                $res['success'] = true;
                return $res;
            }
        }
    }

    public function deleteObsoletePosts($nodeId, $product_code)
    {
        // Suppression du post
        $node = Node::load($nodeId);
        if ($node) {
            $node->delete();
        }
        $this->setLog("Suppression du post n°$nodeId, fiche Bridge n°$product_code");

        return true;
    }

    public function majLiensTrads($isCmdLine = false)
    {
        // Inutile pour Drupal : Le lien est fait dès l'import
        return true;
    }

    /**
     * Lancée par import en ligne de commande uniquement
     */
    public function doImport($isCmdLine = false)
    {
        // On commence par importer les catégories (= weblists)
        $this->setLog("Mise à jour des catégories\n");

        $tmp = $this->update_sit_terms_from_bridge();

        if (!$tmp) {
            $this->errorMsg = 'Erreur lors de la mise à jour des catégories : ' . $this->errorMsg;
            return false;
        }

        // Puis les autres langues
        $languages = BridgeUtils::getLanguagesList();

        foreach ($languages as $key => $lang) {
            $temp = $this->doImportForLang($isCmdLine, $lang);
            if (!$temp) {
                // $this->errorMsg = 'Erreur lors de l\'import en ' . $lang;
                return false;
            }
        }

        $this->setLog('Toutes les fiches sont importées');

        $res = $this->updateRewriteRules($isCmdLine);
        if (!$res) {
            $this->errorMsg = 'Erreur lors de la mise à jour des règles de réécriture';
            return false;
        }
        $this->setLog("Mise à jour des règles de réécriture d'URL");

        // Après les imports, on met à jour les liens entre catégories et entre posts SIT en langues
        $res = $this->majLiensTrads($isCmdLine);
        if (!$res) {
            $this->errorMsg = 'Erreur lors de la mise à jour des liens de traduction';
            return false;
        }

        $this->setLog("Mise à jour des liens de traduction");

        return true;
    }

    /**
     * Mise à jour des meta tags d'une fiche
     * @param $post_id numéro du post
     * @param $metatitle meta title
     * @param string $metadesc meta description
     * @return bool|void true si la mise à jour a été effectue
     */
    function updateMetasSeo($post_id, $metatitle, $metadesc = '')
    {
        return true;
        $ret = false;
        $node = \Drupal\node\Entity\Node::load($post_id);
        if(empty($node))
            return false;

        $tags = array(
            'title' => $metatitle
        );

        /*$metaTags = $node->get('field_meta_tags')->value;
        if (!empty($metaTags)) {
            $tags = unserialize($metaTags);
        }

        if (!empty($metatitle)) {
            $tags['title'] = $metatitle;
        }
        if (!empty($metadesc)) {
            $tags['description'] = $metadesc;
        }

        //$updated_kw = update_post_meta($post_id, '_yoast_wpseo_metakeywords', $metakeywords);

        if ($metatitle !== '' || $metadesc !== '') {
            $node->set('field_meta_tags', serialize($tags));
            $ret = true;
        }

        return $ret;
        */
    }

    /**
     * Importe chaque produit et données associées, dont la catégorie
     * @return bool
     */
    function importPostByProductCodeAndLanguage($productCode, $language, $term=null, $webList=null)
    {
        // Ce tableau servira à la fin pour supprimer les posts SIT qui ne sont plus dans les flux WebList
        $tabPostID = array();
        // Ce tableau servira à affecter tous les terms de chaque post à la fin (nécessité d'avoir tout parcouru avant)
        $tabTerms = array();

        // Options connexion à Bridge
        $urlBridge = BridgeUtils::getUrlBridge();

        if (empty($urlBridge)) {
            $this->errorMsg = 'URL Bridge indéfinie - contrôlez les paramètres';
            return false;
        }

        $urlImport = $urlBridge . "/product/getOneProductForImport?code=" . $productCode . "&language=" . $language;
        $importerImages = false;
        $data = new \stdClass();

        $nbErrors = 0;

        do {
            $tmpData = new \stdClass();

            try {
                $tmpData = file_get_contents($urlImport);
            }
            catch(Exception $err) {
                sleep(1);
            }
            
            if (!empty($tmpData)) {
                $tmpDataDecoded = json_decode($tmpData);
                if ($tmpDataDecoded->success) {
                    $data = $tmpDataDecoded->data;
                    break;
                } else {
                    $this->errorMsg = "ERREUR : Erreur lors de l'import avec l'url $urlImport : \n" . $tmpDataDecoded->message;
                }
            }
        } while ($nbErrors < 3);

        if($nbErrors === 3) {
            if(empty($this->errorMsg))
                $this->errorMsg = "ERREUR : Échec de l'import du produit " . $productCode . ' en ' . $lang . "depuis l'URL " . $urlImport;
            
            return false;
        }
        

        if (!empty($data) && isset($data->product) && is_object($data->product)) {
            $product = $data->product;

            $importerImagesTemp = $importerImages;
            $lastUpdate = null;
            // On recherche s'il existe un produit dans la langue
            $post = BridgeUtils::findPostByProductCode($product->productCode, $language);

            // S'il n'existe pas on le crée
            if (empty($post)) {
                $metadesc = ((mb_strlen($product->comment) > 127) ? mb_substr(strip_tags($product->comment), 0, 127) . '...' : strip_tags($product->comment));
                if($language === 'fr') {
                    $data = array(
                        'type' => 'fiche_sit',
                        'langcode' => $language,
                        'title' => $product->name,
/*                        'body' => ['value'=>$product->comment, 'summary' => $metadesc, 'format' => 'full_html' ],
                        'field_code_sit' => $product->productCode,
                        'field_ville' => $product->city,
                        'field_meta_tags' => serialize(array('title' => $product->name, 'description' => $metadesc))
*/
                    );
                    $post = \Drupal\node\Entity\Node::create($data);
                    $postId = $post->save();
                } else {
                    $postFr = BridgeUtils::findPostByProductCode($product->productCode, 'fr');
                    if(empty($postFr)) {
                        $this->errorMsg = "ERREUR : le produit  " . $product->productCode . ' existe en ' . $language . "mais pas en français ";
                        return false;
                    }

                    // Patch 18/05/2022 : si pas de titre, on passe
                    if(empty($product->name))
                        return true;

                    $postIdFr = $postFr->id();
                    if(!$postFr->hasTranslation($language)) {
                        $post = $postFr->addTranslation($language);
                        $post->set('title', $product->name);
                        $postId = $post->save();
                    } else {
                        $post = $postFr->getTranslation($language);
                        $post->set('title', $product->name);
                        $postId = $post->save();
                    }
                }
                // Remarque 24/09/2021 : en cas de doublon de slug, wordpress va ajouter -2, -3 au slug demandé
                // Et cela peut générer des redirections non souhaitées vers des posts en autres langues qui ont le slug exact du lien demandé
                // $postId = wp_insert_post($post);
                // BridgeUtils::setPostLanguage($postId, $language);
            } else {

                // Patch 24/09/21 : on force le slug car lors de l'insertion, en cas d'anomalie, il se peut qu'on ait créé un post
                // dont le slug comporte un suffixe -2, -3 non désiré
               /* wp_update_post([
                    "post_name" => $product->slug,
                    "ID" => $postId,
                ]);
                */
            }

            // Ensuite, on met à jour les champs de post
            $content = '';
            if (isset($product->commentHtml) && $product->commentHtml != '') {
                $content = strip_tags($product->commentHtml, '<br>');
            }
            elseif ($product->comment != '') {
                $content = $product->comment;
            }
            elseif ($product->comment1 != '') {
                $content = $product->comment1;
            }
           // dump($postId);
           //  die('postID : ' . $postId);
            $metadesc = ((mb_strlen($product->comment) > 127) ? mb_substr(strip_tags($product->comment), 0, 127) . '...' : strip_tags($product->comment));

            $post->set('title', $product->name);
            $post->set('body', ['value'=>$content, 'summary' => $metadesc, 'format' => 'full_html' ]);
            $post->set('field_code_sit', $product->productCode);
            $post->set('field_ville', $product->city);
            $post->set('field_sit_slug', $product->slug);
            // $post->set('field_meta_tags', serialize(array('title' => $product->name, 'description' => $metadesc)));
            $postId = $post->save();

            $alias_manager = \Drupal::entityTypeManager()->getStorage('path_alias');
            $ficheRootUrl = BridgeUtils::getFicheRootUrl();
            // Ici on va attribuer la catégorie primaire à la fiche et son alias
            if(isset($webList->permalinkIsCanonical) && $webList->permalinkIsCanonical === true) {
                $postTermPrimaryCategoryFr = BridgeUtils::getTermIdByWeblistId($webList->id);
                $permalink = "";
                if ($language == 'fr') {
                    // Ici on va affecter la catégorie principale
                    $post->set('field_rubrique_sit_canonique', $postTermPrimaryCategoryFr);
                    $post->save();

                    if (!empty($webList->permalinkUrl) && isset($webList->permalinkIsCanonical) && $webList->permalinkIsCanonical === true) {
                        $permalink = $webList->permalinkUrl;
                        if (mb_substr($permalink, 0, 1) !== '/')
                            $permalink = '/' . $permalink;
                        // Il faut enlever le code de langue de l'URL car Drupal est mltilingue by design
                        if(substr($permalink, 0, 4) == '/fr/') {
                            $permalink = substr($permalink,3);
                        }
                    } else {
                        // Todo : voir s'il faut gérer l'insertion de l'url de categorie avant
                        $permalink = $ficheRootUrl;
                    }
                    if (!empty($permalink)) {
                        $permalink .= '/' . $product->slug;
                    }
                    // On recherche s'il existe un alias pour ce node
                    $aliasNode = $alias_manager->loadByProperties([
                        'path' => "/node/" . $post->id(),
                        'langcode' => 'fr'
                    ]);

                    // On force l'alias du node - on le créé s'il n'y en a pas
                    if (empty($aliasNode)) {
                        $alias_manager->create([
                            'path' => "/node/" . $post->id(),
                            'alias' => $permalink,
                            'langcode' => 'fr'
                        ])->save();
                    } else {
                        foreach ($aliasNode as $alias_object) {
                            $alias_object->alias = $permalink;
                            $alias_object->save();
                        }
                    }

                } else {
                    // Mise à jour des catégories traduites sur les posts traduits
                    $termFr = \Drupal\taxonomy\Entity\Term::load($postTermPrimaryCategoryFr);
                    if(!empty($termFr) && $termFr->hasTranslation($language)) {
                        $term = $termFr->getTranslation($language);
                        $post->set('field_rubrique_sit_canonique', $term->id());
                        $post->save();
                    } else {

                    }
                    // Maintenant on va également mettre à jour les alias des traductions
                    $permalink = "";
                    $langupper = strtoupper($language);
                    // print_r($webList->permalinkUrlTranslation);
                    if (!empty($webList->permalinkUrlTranslation) && is_object($webList->permalinkUrlTranslation) && !empty($webList->permalinkUrlTranslation->$langupper)) {
                        $permalink = $webList->permalinkUrlTranslation->$langupper;
                    } else {
                        // Todo : voir s'il faut gérer l'insertion de l'url de categorie avant
                        $permalink = $ficheRootUrl;
                    }
                    // Les alias drupal doivent commencer par un /
                    if (mb_substr($permalink, 0, 1) !== '/')
                        $permalink = '/' . $permalink;
                    // Il faut enlever le code de langue de l'URL car Drupal est mltilingue by design
                    // Mais il faut les laisser dans Bridge car c'est une convention
                    if(substr($permalink, 0, 4) == '/' . $language . '/') {
                        $permalink = substr($permalink,3);
                    }
                    // die('permalink : ' . $permalink);

                    if (!empty($permalink)) {
                        $permalink .= '/' . $product->slug;
                    }
                    $aliasNode = $alias_manager->loadByProperties([
                        'path' => "/node/" . $post->id(),
                        'langcode' => $language
                    ]);
                    if (empty($aliasNode)) {
                        $alias_manager->create([
                            'path' => "/node/" . $post->id(),
                            'alias' => $permalink,
                            'langcode' => $language
                        ])->save();
                    } else {
                        // On force l'alias du node
                        foreach ($aliasNode as $alias_object) {
                            $alias_object->alias = $permalink;
                            $alias_object->save();
                        }
                    }
                }
            }
            return true;

        } else {
            if (empty($data)) {
                $this->errorMsg = 'Erreur : aucun produit ' . $productCode . 'à importer';
                return false;
            } else {
                $this->errorMsg = "Erreur à l'importation du produit" . $productCode;
                return false;
            }
        }
    }

    private function setLog($msg, $niveau = "simple")
    {
        $tmp = new \DateTime();
        $this->currentLog = $msg;
        $ligne = $tmp->format('d/m/Y H:i:s') . ' - ' . $msg;
        $this->log[$niveau] = $ligne;
        if ($this->isCmdLine) {
            // $this->container->getIo()->info($ligne);
            echo ("\n" . $ligne);
        } else {
            // echo("\n" . $ligne);
        }
    }

    public function getErrorMsg()
    {
        return $this->errorMsg;
    }

    public function getWarnings()
    {
        return $this->warnings;
    }

    public function getCurrentLog()
    {
        return $this->currentLog;
    }

    function get_http_response_code($url)
    {
        $headers = get_headers($url);
        return substr($headers[0], 9, 3);
    }
}
