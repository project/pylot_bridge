<?php

namespace Drupal\pylot_bridge\Commands;

use Drush\Commands\DrushCommands;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Drupal\Console\Core\Command\ContainerAwareCommand;
use Drupal\pylot_bridge\Services\BridgeUtils;
use Drupal\pylot_bridge\Services\BridgeShortCodeParser;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Drush9BridgeCommands extends DrushCommands {

    /**
     * Dcommande drush qui importe les fiches et taxonomies SIT depuis l'applicaiton Bridge
     *
     * @command pylot_bridge:import
     * @aliases bridge-import
     * @usage pylot_bridge:import
     */
    public function import() {
        $this->output()->writeln('Début de l\'import Bridge complet');

        $credentials = BridgeUtils::getBridgeCredentials();
        $importService = \Drupal::service('pylot_bridge.import');
        $res = $importService->doImport(true);
        if($res === false) {
            $this->output()->writeln("\nImport arrêté en erreur\n");
            $this->output()->writeln($importService->getErrorMsg());
            die('');
        } else {
            $warnings = $importService->getWarnings();
            if(count($warnings) > 0) {
                $this->output()->writeln ("\nImport terminé avec avertissements\n");
                echo implode("\n", $warnings);
                die('');
            } else {
                $this->output()->writeln("\nImport terminé avec succès\n");
            }
        }
        // $data = \Drupal::service('pylot_bridge.front_utils')->hello2();
        // $data = BridgeUtils::foo();

        $this->output()->writeln('Import terminé');

        // $this->output()->writeln($text);
    }
    /**
     * commande Drush qui supprime les nodes et termes de taxonomie SIT issus de Bridge
     *
     * @command pylot_bridge:delete
     * @aliases bridge-delete
     * @usage pylot_bridge:delete
     */
    public function delete() {
        $this->output()->writeln('Début de la suppression');

        $credentials = BridgeUtils::getBridgeCredentials();
        $importService = \Drupal::service('pylot_bridge.import');
        $res = $importService->deleteAllData(true);
        if($res === false) {
            $this->output()->writeln("\nSuppression arrêtée en erreur\n");
            $this->output()->writeln($importService->getErrorMsg());
            die('');
        } else {
            $this->output()->writeln("\nSuppression terminée avec succès\n");
        }
    }
    /**
     * commande Drush qui met uniquement à jour les termes de taxonomie SIT depuis l'application  Bridge
     *
     * @command pylot_bridge:import_taxonomies
     * @aliases bridge-import-taxonomies
     * @usage pylot_bridge:import_taxonomies
     */
    public function import_taxonomies() {
        $this->output()->writeln('Début de l\'import des termes');

        $importService = \Drupal::service('pylot_bridge.import');
        $res = $importService->update_sit_terms_from_bridge();
        if($res === false) {
            $this->output()->writeln("\nImport arrêté en erreur - erreur à la mise à jour initiale des termes\n");
            $this->output()->writeln($importService->getErrorMsg());
            die('');
        } else {
            $this->output()->writeln("\nImport terminé avec succès\n");
        }
    }

    /**
     * commande Drush qui met uniquement à jour les termes de taxonomie SIT depuis l'application  Bridge
     *
     * @command pylot_bridge:test
     * @aliases bridge-test
     * @usage pylot_bridge:test
     */
    public function test_shortcode() {
        // $this->output()->writeln('Début de l\'import des termes');
        print_r(BridgeShortCodeParser::parse_shortcodes('uisqudoisq oisq [include file="header.html"] dsiodisqopd idoq idoq disqo d'));

    }
}