<?php

namespace Drupal\pylot_bridge\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class BridgeConfigForm.
 */
class BridgeConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'pylot_bridge.bridgeconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bridge_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('pylot_bridge.bridgeconfig');
    $form['url_bridge'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL Bridge'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('url_bridge'),
    ];
    $form['login_bridge'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Identifiant Bridge'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('login_bridge'),
    ];
    $form['password_bridge'] = [
      '#type' => 'password',
      '#title' => $this->t('Mot de passe Bridge'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('password_bridge'),
    ];
    $form['custom_css_sit'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL Custom CSS'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('custom_css_sit'),
    ];
    $form['custom_js_sit'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL Script JS'),
      '#maxlength' => 512,
      '#size' => 64,
      '#default_value' => $config->get('custom_js_sit'),
    ];
    $form['bridge_site'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Identifiant du site dans Pylot Bridge'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('bridge_site'),
    ];
      $form['product_media_block'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Identifiant Bridge du bloc photos'),
          '#maxlength' => 64,
          '#size' => 64,
          '#default_value' => $config->get('product_media_block'),
      ];
      $form['maps_center_lat'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Cartes : latitude par défaut du centre'),
          '#maxlength' => 64,
          '#size' => 64,
          '#default_value' => $config->get('maps_center_lat'),
      ];
      $form['maps_center_lon'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Cartes : longitude par défaut du centre'),
          '#maxlength' => 64,
          '#size' => 64,
          '#default_value' => $config->get('maps_center_lon'),
      ];
      $form['marker_map'] = [
      '#type' => 'url',
      '#title' => $this->t('URL du pointeur de carte'),
      '#default_value' => $config->get('marker_map'),
    ];
    $form['marker_map_actif'] = [
      '#type' => 'url',
      '#title' => $this->t('URL du pointeur de carte actif'),
      '#default_value' => $config->get('marker_map_actif'),
    ];
      $form['url_page_cv'] = [
          '#type' => 'url',
          '#title' => $this->t('URL de la page aperçu carnet de voyages'),
          '#default_value' => $config->get('url_page_cv'),
      ];
    $form['taxonomy_root_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Réécritures des URL : racine URL catégories'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('taxonomy_root_url'),
    ];
    $form['fiche_root_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Réécritures des URL : racine URL des fiches'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('fiche_root_url'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('pylot_bridge.bridgeconfig')
      ->set('url_bridge', $form_state->getValue('url_bridge'))
      ->set('login_bridge', $form_state->getValue('login_bridge'))
      ->set('password_bridge', $form_state->getValue('password_bridge'))
      ->set('custom_css_sit', $form_state->getValue('custom_css_sit'))
      ->set('custom_js_sit', $form_state->getValue('custom_js_sit'))
      ->set('bridge_site', $form_state->getValue('bridge_site'))
      ->set('marker_map', $form_state->getValue('marker_map'))
      ->set('marker_map_actif', $form_state->getValue('marker_map_actif'))
      ->set('maps_center_lat', $form_state->getValue('maps_center_lat'))
      ->set('maps_center_lon', $form_state->getValue('maps_center_lon'))
      ->set('product_media_block', $form_state->getValue('product_media_block'))
      ->set('url_page_cv', $form_state->getValue('url_page_cv'))
      ->set('taxonomy_root_url', $form_state->getValue('taxonomy_root_url'))
      ->set('fiche_root_url', $form_state->getValue('fiche_root_url'))
      ->save();
  }

}
