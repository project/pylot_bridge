<?php

namespace Drupal\pylot_bridge\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;
use Drupal\Core\Routing\RoutingEvents;

class TaxonomyListingViewsRoutingSubscriber extends RouteSubscriberBase {

    public function alterRoutes(RouteCollection $collection) {
        if ($route = $collection->get('entity.taxonomy_term.canonical')) {
            $route->setDefault('_controller', '\Drupal\pylot_bridge\Controller\TaxonomyTermViewPageController::handle');
        }
    }

    public static function getSubscribedEvents() {
        $events = parent::getSubscribedEvents();
        $events[RoutingEvents::ALTER] = array('onAlterRoutes', -180);
        return $events;
    }

}