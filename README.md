# Module Drupal Pylot Bridge

## Installation
### Installer le module components 
Le module components est utilisé pour gérer l'override des templates bridge dans un theme ou un module custom via les namespaces twig 
https://www.drupal.org/project/components
- Utiliser le namespace pylot_bridge dans le fichier .info.yml
- Copier les fichiers twigs à overrider dans votre theme/module depuis le dossier templates du module pylot_bridge en respectant la même arborescence
- Chaque fichier twig peut être overridé individuellement, n'overrider que le minimum requis

### Installer le module Drupal console 
Le module Drupal console est utilisé pour lancer des commandes d'import 
https://www.drupal.org/project/components
- Utiliser le namespace pylot_bridge dans le fichier .info.yml
- Copier les fichiers twigs à overrider dans votre theme/module depuis le dossier templates du module pylot_bridge en respectant la même arborescence
- Chaque fichier twig peut être overridé individuellement, n'overrider que le minimum requis

### Configuration des langues
* Aller dans Configuration > Régionalisation et langues > langues
* Ajouter les langues de traduction du site
* Activer la traduction de l'interface pour la langue anglaise, sinon les textes de Pylot Bridge seront en français sur l'interface anglaise
* Bridge supporte nativement : FR, EN , DE, ES, IT - si vous avez besoin d'une autre langue, contactez DN Consultants
    
### Installation du module
* Extraire l'archive dans modules/contrib/pylot_bridge
* Aller dans Extensions, cocher Pylot Bridge et cliquez sur Installer

### Configuration du module
* Aller dans Configuration > Système > Configuration Bridge
* Renseigner à minima les paramètres : URL Bridge, identifiant, Mot de passe Bridge, Id de site Bridge et n° de bloc photos Bridge

### Activation de la traduction du contenu
* Aller dans Configuration > Régionisation et langue > langues du contenu et traductions
* Cocher la case "Traduisible" sur Contenu > Fiche SIt et Terme > Rubrique SIT

### Activation du filtre de texte pour les shortcodes
* Aller dans Configuration > rédaction de contenu > format de texte et éditeur
* Pour chacun des format où vous souhaitez activer le shortcode, cliquer sur configurer et cocher la case "Pylot Bridge : shortcodes"
* Le shortcode de liste doit être formaté ainsi : [brliste id="XX"] où XX est un numéro de liste Bridge (copier-coller disponible depuis l'application Bridge) 


## Commandes Drupal console ou Drush
### Import des données Bridge
`pylot_bridge:import`
* Importe les fiches Bridge en nodes de type fiche_sit et les listes Bridge en termes de la taxonomie rubrique_sit
* Met à jour les champs additionnels des fiches et termes
* Supprime les nodes qui ne sont plus dans les flux Bridge

`pylot_bridge:import_taxonomies`
* Importe uniquement les termes de la taxonomie rubrique_sit

`pylot_bridge:delete`
* Supprime toutes les fiches Bridge les termes de la taxonomie rubrique_sit

## Utilisation
Le module surcharge les vues d'affichage :
* Des nodes de type fiche_sit
* Des termes de taxonomie rubrique_sit

> En complément, les shortcodes permettent d'insérer des contenus dynamiques issus de Bridge dans des pages rédactionnelles. 


#Changelog

## v8.x-1.7
* 16/12/2022
* FEAT : nouveaux templates spécifique : mosl-slider et mosl-slider-item
* FEAT : nouvelle fonction BridgeShortCodeParser::process pour traiter les shortcodes d'un texte (récupération de tableaux de rendu et/ou de l'HTML filtré)

## v8.x-1.6.8
* 14/12/2022
* FEAT : fonction BridgeUtils::getBridgeCategories
* FIX : patchs templates calendar (espace superflu dans un attribut html)
* FEAT : liste application à la volée des traduction sur les balises title et description (intégration modif code Vanksen)

## v8.x-1.6.7
* 30/11/2022
* FEAT : templates MOSL : ajout de balises block twig pour donner une plus grande granularité dans les surcharges
* FEAT : MOSL : liste : ajout de la zone Commentaire dans une balie HTML <div class="card__review"> (affichage des avis TA dans la liste) 
* FIX : correction syntaxe dans composer.json (virgules surnuméraires)
* FIX : MOSL : fiche : le lien vers Tripadvisor pointait tojours vers Tripadvisor en anglais => tient compte de la langue et redirige vers la bonne version de TripAdvisor (FR, DE, EN) 
* FIX : MOSL : fiche : le nombre d'avis affiché était bloqué sur 120 

## v8.x-1.6.6
* 07/11/2022
* FEAT : MOSL liste : ne pas zoomer la carte au survol d'un élément de liste mais simplement centrer

## v8.x-1.6.5
* 28/10/2022
* FEAT : ajout de blocks twig dans les templates de liste
* FEAT : fiche MOSL : ajout de la zone infos supplémentaires 5 en haut
* FEAT : mosl : ajout d'un bouton d'effacement de tous les filtres
* FEAT: mosl : le lien tripadvisor amène désormais a la section commentaires
* FIX : mosl liste : n'afficher que les points de la page courante sur la carte
* FIX : mosl : adaptation du script de déplacement du curseur distance
* FIX : lorsqu'on efface un filtre par gelocalisation, les informations restent en session
* FIX : mosl : intégration des overrides dans les vues spécifiques
* FIX : le lien vers l'itinéraire géolocalise désormais l'utilisateur en utilisant sa position précise
* FIX : fiche MOSL : ne pas faire apparaitre la colonne d'1/4 si pas de photo 2
* FIX : liste : lorsqu'on ne saisit qu'une date DU ou au, la date sélectionnée n'apparait pas dans les tags de recherche
* FIX : mosl liste :au chargement , la carte ne montre pas toujours tous les points
* FIX : ajout attribut uk-tooltip sur les images svg inline

## v8.x-1.6.4
* 21/10/2022`
* FEAT : fiche MOSL utilisation de la zone info supplémentaire 5
* FIX : correction fonction lien itinéraire


## v8.x-1.6.3
* 02/09/2022
* FEAT : fiche mosl : ajout de la zone pictogrammes par-dessus l'image princial dans le modèle de fiche
* FEAT : liste : affichage des seuls points de la page en cours sur la carte
* FEAT : filtre par distance : augmentation du maximum à  100km
* FEAT : fiche mosl : ajout des informations trip advisor 
* FEAT : liste : lors d'une recherche géographique, ajout d'un pointeur localisant le point de recherche
* FEAT : fiche mosl : le lien de réservation apparait même lorsuq'il n'y a pas d'intitulé défini dans Bridge
* FIX : liste/fiche mosl : arrondissement de l'affichage de la distance par raport au point de recherche à 1 décimale
* FIX : le paramètres de cordonnées du centre de la carte interactive n'est pas pris en compte 
* FIX : mosl correction sur le lien du boton itinéraire


## v8.x-1.6.2
* 13/07/2022
* FIX : bug sur le redimensionneur d'images (malformation last-modified lorsqu'on est pas connecté sur Drupal)

## v8.x-1.6.1
* 17/06/2022
* FIX : bug avec les paramètres de pointeur de carte


## v8.x-1.6
* 16/06/2022
* FEAT : nouveaux gabarits MOSL

## v8.x-1.5
* 23/05/2022
* FEAT : commandes Drush en plus des commande console
* FEAT : mise en cache des images redimensionnées
* FEAT : composant calendrier de disponibilités Avizi
* FEAT : passage des images SVG en balises html SVG au lieu des balises IMG
* FEAT : ajout de nouveaux paramètres sur les listes permettant de mieux gérer le rendu : "Afficher dans un conteneur", "Afficher le titre", "Afficher la description" et "Afficher l'image"
* FEAT : fiches : ajout de 2 nouvelles zones infos supplémentaires
* FEAT : fiches : ajout de 2 nouveaux composants liens de téléchargement
* FEAT : fiches : ajout du composant disponibilités LEI/SITLOR
* FEAT : fiches : ajout du composant tarifs en tableau (LEI alsace)
* FEAT : liste : support du mode pagination classique
* FEAT : composant player video 
* FEAT : composants galeries photos
* FEAT : fiches : fonctionnalité formulaire de contact avec antispam recaptcha
* FEAT : fiches : ajout de balises sur les liens de contact pour tracking
* FEAT : nouveau shortcode brfiche

* FIX : mise à jour des fichiers de traductions pour prendre en compte les nouveaux messages ajoutés
* FIX : amélioration du rendu du slider de la fiche 1
* FIX : redimensionnement d'image incorrect sur les fiches
* FIX : améliorations CSS de cohérence de rendu selon les thèmes
* FIX : affichage incorrect des dates uniques sur les composants dates calendrier et dates tableau
* FIX : fiches : correction sur le positionnement des blocs infos supplémentaires
* FIX : fiches : interprétation des composants dans les zones infos supplémentaires
* FIX : fiches : calages responsive sur les fiches
* FIX : fiches : bug sur la zone lien de commercialisation
* FIX : fiches : ajout du sommaire sur modèle fiche 1
* FIX : fiches : prise en compte des paramètres de fiche concernant le sommaire
* FIX : listes : sur les modèles 1 et 2, en mobile lors du passage en carte plein écran, il n'y a plus de points
* FIX : listes : le loader passe sous la carte après un retour de carte plein écran
* FIX : listes : sur les modèles cartes, les pictogrammes n'apparaissent pas
* FIX : listes : limitation du nombre de liens de pagination
* OPT : listes : ne charger les points qu'au passage sur l'onglet carte pour les modèles sans carte directement visible
* OPT : listes : gestion plus fine du zoom sur hover ou click en passant par des classes css dédiées
* FIX : listes : fix d'affichage css
* FIX : listes : corrections de positionnement des zones info3 et info4 masquées sur les modèles de liste cartes
* FIX : listes : séparateurs non visibles dans certains blocs (bug dans RenderBlock)
* FIX : listes : désactivation de la bascule entre liste et carte par "Swipe" tactile
* FIX : listes : patch css taille de caractère des titres de fiches en liste
* FIX : améliorations sur la CSS de base

